#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <limits.h>
#include <math.h>

#define mod(a,b) (b + (a % b)) % b

typedef enum direcao {
        NORTE,
        LESTE,
        SUL, 
        OESTE
} Edirecao;

/* mini-cidades sem ruas */
unsigned int SUBURBIOS[10000][4], INI[3], FIM[3];
int TAM, DIRECAO;
int conta_driveways;


void imprimeSuburbios()
{
   int i;
   for (i=0;i<TAM;i++)
   {
       if (SUBURBIOS[i][0])
          printf("S%d %d %d\n", SUBURBIOS[i][1], SUBURBIOS[i][2], SUBURBIOS[i][3]);
       else
          printf("A%d %d %d\n", SUBURBIOS[i][1], SUBURBIOS[i][2], SUBURBIOS[i][3]);
   }
   printf("##################\n");
}

void moveCarro()
{

}

bool entraSuburbio(unsigned int tipo, unsigned int rua, unsigned int pos)
{
   int i;
   for (i=0;i<TAM;i++)
       if ((SUBURBIOS[i][0] == tipo) && (SUBURBIOS[i][1] == rua))
       {
          if ((pos >= SUBURBIOS[i][1]) && (pos <= SUBURBIOS[i][2]))
             return true;
       }
   return false;
}

int main()
{
    char c;
    unsigned int tipo, rua, ini, fim;
    /* lista de suburbios */
    TAM = 0;
    while ((c = fgetc(stdin)) != '#')
    {
       if (c == '\n')
          continue;
       tipo = (c == 'S');
       fscanf(stdin, "%u %u %u", &rua, &ini, &fim);
       SUBURBIOS[TAM][0] = tipo;
       SUBURBIOS[TAM][1] = rua;
       SUBURBIOS[TAM][2] = ini;
       SUBURBIOS[TAM][3] = fim;
       TAM++;
    }
    imprimeSuburbios();
    char d;
    while ((c = fgetc(stdin)) != '#')
    {
       if (c == '\n')
          continue;
       INI[0] = (c == 'S');
       fscanf(stdin, "%u %u %c%u %u", &INI[1], &INI[2], &d, &FIM[1], &FIM[2]);
       FIM[0] = (d == 'S');
       printf("%c%u %u %c%u %u\n", c, INI[1], INI[2], d, FIM[1], FIM[2]);
       conta_driveways = (((int) (ceil(INI[2] / 100.0) * 100)) - INI[2]) >> 1;
       printf("%d\n", conta_driveways);
       if (INI[2] % 2 == 1)
       {
           if (INI[0])
              DIRECAO = OESTE;
           else
              DIRECAO = SUL;
       } 
       else
       {
           if (INI[0])
              DIRECAO = LESTE;
           else
              DIRECAO = NORTE;
       }
    }
    return 0;
}

