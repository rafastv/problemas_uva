#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

typedef int bool;
#define true 1
#define false 0

typedef struct termo
{
  char sinal;
  int ex;
  int ey;
  int coeficiente;
} Ttermo;

typedef struct equacao
{
   Ttermo parte[160];
   int tamanho;
} Tequacao;

void iniciaTermo(Ttermo *t)
{
    t->sinal = '+';
    t->ex = 0;
    t->ey = 0;
    t->coeficiente = 1;
}

void copiaTermoEquacao(Tequacao *EQ, Ttermo *T)
{
    EQ->parte[EQ->tamanho].sinal = T->sinal;
    EQ->parte[EQ->tamanho].ex = T->ex;
    EQ->parte[EQ->tamanho].ey = T->ey;
    EQ->parte[EQ->tamanho].coeficiente = T->coeficiente;
    EQ->tamanho++;
}

void trocaTermos(Ttermo *A, Ttermo *B)
{
    Ttermo C;
    C.sinal = A->sinal;
    A->sinal = B->sinal;
    B->sinal = C.sinal;
    C.ex  = A->ex;
    A->ex = B->ex;
    B->ex = C.ex;
    C.ey  = A->ey;
    A->ey = B->ey;
    B->ey = C.ey;
    C.coeficiente  = A->coeficiente;
    A->coeficiente = B->coeficiente;
    B->coeficiente = C.coeficiente;
}

Ttermo* somaTermos(Ttermo *A, Ttermo *B)
{
    Ttermo *C = malloc(sizeof(Ttermo));
    iniciaTermo(C);
    int ca = A->coeficiente;
    int cb = B->coeficiente;
    if (A->sinal == '-')
       ca *= -1;
    if (B->sinal == '-')
       cb *= -1;
    int cc = ca+cb;
    if (cc < 0)
    {
       C->sinal = '-';	
       cc *= -1;
    }
    /* expoentes sao conservados */
    C->ex = A->ex;
    C->ey = A->ey;
    C->coeficiente = cc;
    return C;
}

Ttermo* multiplicaTermos(Ttermo *A, Ttermo *B)
{
    Ttermo *C = malloc(sizeof(Ttermo));
    iniciaTermo(C);
    /* so preciso considerar casos em que sinal resulta em negativo */
    if (((A->sinal == '-') && (B-> sinal == '+'))
      ||((A->sinal == '+') && (B-> sinal == '-')))
	C->sinal = '-';
    C->ex = A->ex + B->ex;
    C->ey = A->ey + B->ey;
    C->coeficiente = A->coeficiente * B->coeficiente;
    return C;
}

void imprimeTermo(Ttermo *t, bool espaco, bool linha_de_expoente)
{
    /* printf("%c%dx%dy%d",t->sinal,t->coeficiente,t->ex,t->ey);*/
     int conta;
     char linha[10];
     if (!linha_de_expoente)
     {
         if (espaco) 
             printf(" %c ", t->sinal);
         else
	     if (t->sinal == '-')
                 printf("%c", t->sinal);
         if ((t->coeficiente > 1) || ((t->ex == 0) && (t->ey == 0)))
	 {
	     if (t->coeficiente != 0)
             { 
                 printf("%d", t->coeficiente);
             }
	 }
         if (t->ex > 0)
         {
             if (t->ex == 1)
                 printf("x");
             else
             {
	         sprintf(linha, "%d", (int) t->ex);
	         conta = 0;
	         printf("x");
	         while (conta < strlen(linha))
                 {
                     printf(" ");
		     conta++;
                 }
             }
         }
         if (t->ey > 0) 
         {
             if (t->ey == 1)
                 printf("y");
             else
             {
	         sprintf(linha, "%d", (int) t->ey);
	         conta = 0;
	         printf("y");
	         while (conta < strlen(linha))
                 {
                     printf(" ");
		     conta++;
                 }
             }
         }
     }
     else
     {
         if (espaco) 
             printf("   ", t->sinal);
         else
	     if (t->sinal == '-')
                 printf(" ", t->sinal);
         if ((t->coeficiente > 1) || ((t->ex == 0) && (t->ey == 0)))
	 {
	     if (t->coeficiente != 0)
             { 
	         sprintf(linha, "%d",t->coeficiente);
	         conta = 0;
	         while (conta < strlen(linha))
                 {
                     printf(" ");
	       	     conta++;
                 }
             }
	 }
         if (t->ex > 0)
         {
             if (t->ex == 1)
                 printf(" ");
             else
	         printf(" %d", (int)t->ex);
         }
         if (t->ey > 0) 
         {
             if (t->ey == 1)
                 printf(" ");
             else
	         printf(" %d", (int)t->ey);
         }
     }
}

Tequacao* multiplicaEquacoes(Tequacao *A, Tequacao *B)
{
    int u, v;
    Tequacao *C = malloc(sizeof(Tequacao));
    Ttermo *T;
    C->tamanho = 0;
    for (u=0; u<A->tamanho;u++)
        for (v=0; v<B->tamanho;v++)
	{
           T = multiplicaTermos(&A->parte[u], &B->parte[v]);
           copiaTermoEquacao(C, T);
	   free(T);
	}
    return C;
}

void ordenaTermosEquacao(Tequacao *A)
{
    int u, v;
    Tequacao *C = malloc(sizeof(Tequacao));
    Ttermo *T;
    C->tamanho = 0;
    for (u=A->tamanho-1;u>0;u--)
        for (v=u-1;v>=0;v--)
	{
	   if (A->parte[u].ex > A->parte[v].ex) 
	      trocaTermos(&A->parte[u], &A->parte[v]);
	   else
	   {
	       if ((A->parte[u].ex == A->parte[v].ex) && 
                   (A->parte[u].ey < A->parte[v].ey))
	           trocaTermos(&A->parte[u], &A->parte[v]);
	   }
	}
}

Tequacao* combinaTermosEquacao(Tequacao *A)
{
    int u, v;
    Tequacao *B = malloc(sizeof(Tequacao));
    Ttermo *Ta, *Tb;
    B->tamanho = 0;
    for (u=0; u<A->tamanho;u++)
         if ((A->parte[u].ex == A->parte[u+1].ex) &&
             (A->parte[u].ey == A->parte[u+1].ey))
         {
               Ta = somaTermos(&A->parte[u], &A->parte[u+1]);
	       u++;
	       v = u+1;
               while ((Ta->ex == A->parte[v].ex) &&
                      (Ta->ey == A->parte[v].ey))
	       {
                   Tb = somaTermos(Ta, &A->parte[v]);
	           v++;
	           free(Ta);
		   Ta = Tb;
		   if (v >= A->tamanho)
		       break;
	       }
	       u = v-1; 
	       if (Ta->coeficiente > 0)
                   copiaTermoEquacao(B, Ta);
	       free(Ta);
	 }
         else
	 {
            /* copiar termos de A para B caso nao exista combinacao */
	    copiaTermoEquacao(B, &A->parte[u]);
	 }
    return B;
}

void imprimeEquacao(Tequacao *e)
{
    int i;
    imprimeTermo(&e->parte[0], false, true);
    for (i=1;i<e->tamanho;i++)
        imprimeTermo(&e->parte[i], true, true);
    printf("\n");
    imprimeTermo(&e->parte[0], false, false);
    for (i=1;i<e->tamanho;i++)
        imprimeTermo(&e->parte[i], true, false);
    printf("\n");
}

Tequacao *extraiTermos(char *buffer)
{
    int i, ini=0, len_num=0;
    bool ex=false, ey=false;
    char numero[40] = "";
    Tequacao *eq = malloc(sizeof(Tequacao));
    eq->tamanho=0;
  
    iniciaTermo(&eq->parte[0]);
    if (buffer[0] == '-')
    {
	ini=1; 
	eq->parte[0].sinal = '-';
    }
    for (i=ini;i<80;i++)
    {
         if (buffer[i] == '\0')
	 {
             numero[len_num]='\0'; 
             if (len_num>0) 
             {
	         if (ex)
                     eq->parte[eq->tamanho].ex=atoi(numero); 
                 if (ey)
                     eq->parte[eq->tamanho].ey=atoi(numero); 
	         if ((!ex) && (!ey))
                     eq->parte[eq->tamanho].coeficiente=atoi(numero); 
             }
	     else
             {
	         if (ex)
                     eq->parte[eq->tamanho].ex=1; 
                 if (ey)
                     eq->parte[eq->tamanho].ey=1; 
             }
	     eq->tamanho++;
             break;
	 }
         switch (buffer[i])
         {
           case '+': {  
                         numero[len_num]='\0'; 
                         if (len_num>0) 
                         {
			     if (ex)
                                 eq->parte[eq->tamanho].ex=atoi(numero); 
                             if (ey)
                                 eq->parte[eq->tamanho].ey=atoi(numero); 
			     if ((!ex) && (!ey))
                                 eq->parte[eq->tamanho].coeficiente=atoi(numero); 
                         }
			 else
			 {
			     if (ex)
                                 eq->parte[eq->tamanho].ex=1; 
                             if (ey)
                                 eq->parte[eq->tamanho].ey=1; 
			 }
                         /* inicio do prox termo */
                         iniciaTermo(&eq->parte[++eq->tamanho]);
                         eq->parte[eq->tamanho].sinal='+'; 
                         ex=false;
                         ey=false;
                         len_num=0;
                         numero[len_num]='\0'; 
                         break; 
                     }
           case '-': {  
                         numero[len_num]='\0'; 
                         if (len_num>0)
                         {
                             if (ex)
                                 eq->parte[eq->tamanho].ex=atoi(numero); 
                             if (ey)
                                 eq->parte[eq->tamanho].ey=atoi(numero); 
			     if ((!ex) && (!ey))
                                 eq->parte[eq->tamanho].coeficiente=atoi(numero); 
                         }
			 else
			 {
			     if (ex)
                                 eq->parte[eq->tamanho].ex=1; 
                             if (ey)
                                 eq->parte[eq->tamanho].ey=1; 
			 }
                         /* inicio do prox termo */
                         iniciaTermo(&eq->parte[++eq->tamanho]);
                         eq->parte[eq->tamanho].sinal='-';
                         ex=false;
                         ey=false;
                         len_num=0;
                         numero[len_num]='\0'; 
                         break; 
                     }
           case 'x': {
                         numero[len_num]='\0'; 
                         if (len_num > 0)
                         {
                            if (ey)
                               eq->parte[eq->tamanho].ey=atoi(numero);
                            else
                               eq->parte[eq->tamanho].coeficiente=atoi(numero);
                         }
			 else
			 {
                            if (ey)
                               eq->parte[eq->tamanho].ey=1;
			 }
			 ex=true;
			 ey=false;
			 len_num=0;
                         numero[len_num]='\0'; 
		         break;
                     }
           case 'y': {
                         numero[len_num]='\0'; 
                         if (len_num>0)
                         {
                            if (ex)
                               eq->parte[eq->tamanho].ex=atoi(numero);
                            else
                               eq->parte[eq->tamanho].coeficiente=atoi(numero);
                         }
			 else
			 {
                            if (ex)
                               eq->parte[eq->tamanho].ex=1;
			 }
			 ex=false;
			 ey=true;
			 len_num=0;
                         numero[len_num]='\0'; 
		         break;
                     }
           default: {
                       numero[len_num++]=buffer[i];
		       break;
                    } 
         }
    }
    return eq;
}


int main()
{
    char buffer[80];
    Tequacao *saida, *tmp, *eq1 = NULL, *eq2 = NULL;
    while (fscanf(stdin, "%s", buffer) != EOF)
    {
        if (buffer[0] =='#')
            break;
	if (eq1 == NULL)
            eq1 = extraiTermos(buffer);
	else
	{
            eq2 = extraiTermos(buffer);
            tmp=multiplicaEquacoes(eq1, eq2);
            ordenaTermosEquacao(tmp);
            saida=combinaTermosEquacao(tmp);
            imprimeEquacao(saida);
	    /* limpa buffer */
	    free(tmp);
	    free(saida);
	    free(eq1);
	    free(eq2);
	    eq1 = NULL;
	    eq2 = NULL;
	}
        memset(buffer, 0, 81);
    }
    return 0;
}

