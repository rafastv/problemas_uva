import sys, operator, math
from itertools import islice

VERTICES = []
ARESTAS = []
DISTANCIAS = {}
PREDECESSORES = {}

linhas = sys.stdin.readlines()
N = int(linhas[0])
conta = 0
for linha in linhas[1:]:
   linha = linha.replace("\n","")
   if linha[0] == "#":
      break
   if (conta < N):
      caminho = linha.split(":")
      nome = caminho[0].strip()
      vertices = caminho[1].strip()
      atual = nome + vertices[0]
      VERTICES += [atual]
      tam = iter(range(len(vertices)-1))
      for i in tam:
         if vertices[i+1] != "=":
            proximo = nome + vertices[i+1]
            # evita insercao de dois vertices iguais (loop)
            if proximo not in VERTICES:
               VERTICES += [proximo]
            ARESTAS += [[atual, proximo]]
            atual = proximo
         else:
            conexao = vertices[i+2] + vertices[i+3]
            # evita insercao de arestas duplicadas
            if ([atual, conexao] not in ARESTAS) and\
               ([conexao, atual] not in ARESTAS):
               ARESTAS += [[atual, conexao]]
            next(islice(tam, 2, 2), None)
   else:
      # Bellman-Ford 1958
      origem = linha[:2].strip()
      destino = linha[2:].strip()
      for v in VERTICES:
         DISTANCIAS.update({v:float("inf")})
         PREDECESSORES.update({v:None})
      DISTANCIAS[origem]=0
      for i in range(len(VERTICES)-1):
         for aresta in ARESTAS:
            o = aresta[0]
            d = aresta[1]
            w = 1
            if (aresta[0][0] != aresta[1][0]):
               w = 3
            if (DISTANCIAS[o] + w) < DISTANCIAS[d]:
               DISTANCIAS[d] = DISTANCIAS[o] + w
               PREDECESSORES[d] = o
            elif (DISTANCIAS[d] + w) < DISTANCIAS[o]:
               DISTANCIAS[o] = DISTANCIAS[d] + w
               PREDECESSORES[o] = d
            else:
               pass
      #print(DISTANCIAS, PREDECESSORES)
      # geraCaminho
      fim = destino
      caminho = ""
      passos = 0
      while (fim != origem):
         caminho += fim[1]
         if PREDECESSORES[fim][0] != fim[0]: 
            salva = fim[0]
            ultimo = fim
            while PREDECESSORES[fim][0] != fim[0]: 
               ultimo = fim
               fim = PREDECESSORES[fim]
               if not PREDECESSORES[fim]:
                  break
            fim = ultimo
            passos += 3
            caminho += salva + "=" 
         else:
            passos += 1
         fim = PREDECESSORES[fim]
      caminho += fim[1]
      caminho = fim[0] + caminho[::-1]
      sys.stdout.write("{:>3}".format(str(passos)) + ": " + caminho + "\n")
   conta+=1

#print (VERTICES)
#print (ARESTAS)
exit(0)
