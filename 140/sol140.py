import sys

SMIN = 10

def imprimeSaida(sordem, svalor):
   sys.stdout.write(sordem[0])
   for idx in range(1,len(sordem)):
      sys.stdout.write(" " + sordem[idx])
   sys.stdout.write(" -> " + str(svalor) + "\n")

def calculaDistanciaMaxima(lv, ordem):
   pos = {}
   for idx in range(len(ordem)):
      pos.update({ordem[idx]: idx})
   tamanhos = [-1]*len(ordem)
   global SMIN
   for vertice in list(pos.keys()):
      for vizinho in lv[vertice]:           
         valor = abs(pos[vizinho] - pos[vertice])
         if (valor > SMIN):
            return len(ordem)
         if (valor > tamanhos[pos[vertice]]):
            tamanhos[pos[vertice]] = valor
   return max(tamanhos)

def geraCombinacoes(vertices):
   if len(vertices) == 0:
      return "";
   texto = []
   for pos in range(len(vertices)):
      proximo = vertices[:pos] + vertices[pos+1:]
      if (len(proximo) > 0):
         complementos = geraCombinacoes(proximo)
         for c in complementos:
            texto += [vertices[pos] + c]
      else:
         texto += [vertices[pos]]
   return texto 

def insere(lv, indice, valor):
   if indice in list(lv.keys()):
      lv[indice] += valor
   else:
      lv.update({indice: valor})

linhas = sys.stdin.readlines()
for linha in linhas:
   if linha == "#":
      break
   if ":" not in linha:
      continue

   linha = linha.replace("\n","")
   linha = linha.replace("\t","")
   linha = linha.replace(" ","")
   if linha[-1] == ";":
       linha = linha[:-1]
   linha = linha.split(";")

   vertices = {}
   for texto in linha:
      if not texto:
         continue
      texto = texto.split(":")
      if len(texto) < 2:
          continue
      insere(vertices, texto[0], texto[1])
      for vizinho in texto[1]:
         insere(vertices, vizinho, texto[0])

   if len(vertices) <= 1:
      continue

   for indice in list(vertices.keys()):
      vertices[indice] = list(set(vertices[indice]))

   CS = geraCombinacoes(list(vertices.keys()))
   CS.sort()
   SMIN = len(vertices)-1 
   SORDEM = CS[0]
   for ordem in CS:
      if ordem[-1] in vertices[ordem[0]]:
         continue
      valor = calculaDistanciaMaxima(vertices, ordem)
      if (valor < SMIN) and (valor > 0):
         SMIN = valor
         SORDEM = ordem
   imprimeSaida(SORDEM, SMIN)
exit(0)
