#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <float.h>
#include <limits.h>
#include <math.h>

typedef int bool;
#define false 0
#define true 1
#define mod(a,b) (b + (a % b)) % b
#define PRECISAO 1e-9

int BB[4];
double BBd[4];

void encontraBoundingBox(double triangulo[3][2])
{
    double x_min, y_min, x_max, y_max;
    int i;
    x_min = triangulo[0][0];
    x_max = triangulo[0][0];
    y_min = triangulo[0][1];
    y_max = triangulo[0][1];
    for (i=1;i<3;i++)
    {
        if (x_min > triangulo[i][0])
           x_min = triangulo[i][0];
        if (x_max < triangulo[i][0])
           x_max = triangulo[i][0];
        if (y_min > triangulo[i][1])
           y_min = triangulo[i][1];
        if (y_max < triangulo[i][1])
           y_max = triangulo[i][1];
    }
    BBd[0] = x_min;
    BBd[1] = y_min;
    BBd[2] = x_max;
    BBd[3] = y_max;
    x_min = (x_min >= 1.00)?x_min:1.00;
    x_min = (x_min <= 99.0)?x_min:99.0;
    y_min = (y_min >= 1.00)?y_min:1.00;
    y_min = (y_min <= 99.0)?y_min:99.0;
    x_max = (x_max >= 1.00)?x_max:1.00;
    x_max = (x_max <= 99.0)?x_max:99.0;
    y_max = (y_max >= 1.00)?y_max:1.00;
    y_max = (y_max <= 99.0)?y_max:99.0;
    BB[0] = x_min;
    BB[1] = y_min;
    BB[2] = x_max;
    BB[3] = y_max;
}

bool iguaisD(double A[2], double B[2])
{
     return ((fabs(A[0] - B[0]) < PRECISAO) && 
             (fabs(A[1] - B[1]) < PRECISAO));
}

bool iguais(double pd[2], int pi[2])
{
     return (pi[0] == pd[0]) && (pi[1] == pd[1]);
}

bool pontoDentroTriangulo(double triangulo[3][2], int ponto[2])
{
    /* caso triangulo seja um ponto */
    if ((iguaisD(triangulo[1], triangulo[2])) && 
        (iguaisD(triangulo[0], triangulo[1])))
       return (iguais(triangulo[0], ponto) || iguais(triangulo[1], ponto) ||
               iguais(triangulo[2], ponto));

    /* caso ponto seja igual a um dos vertices do triangulo */
    if (iguais(triangulo[0], ponto) || iguais(triangulo[1], ponto) ||
        iguais(triangulo[2], ponto))
       return ((ponto[0] >= 1) && (ponto[0] <= 99) && 
               (ponto[1] >= 1) && (ponto[1] <= 99));
    double A[2] = {(triangulo[1][0] - triangulo[0][0]), (triangulo[1][1] - triangulo[0][1])};
    double B[2] = {(triangulo[2][0] - triangulo[0][0]), (triangulo[2][1] - triangulo[0][1])};
    double C[2] = {(ponto[0] - triangulo[0][0]), (ponto[1] - triangulo[0][1])};
    double u, v;
    /* C = Au + Bv      */
    /* C = Au + Bv (xA) */
    /* C = Au + Bv (xB) */
    double numerador_v   = (C[0]*A[1] - C[1]*A[0]); 
    double denominador_v = (B[0]*A[1] - B[1]*A[0]); 
    double numerador_u   = (C[0]*B[1] - C[1]*B[0]);
    double denominador_u = (A[0]*B[1] - A[1]*B[0]); 
    /* caso seja uma reta */
    if ((fabs(numerador_v) < PRECISAO) && (fabs(numerador_u) < PRECISAO))
       return ((ponto[0] >= BBd[0]) && (ponto[0] <= BBd[2]) && 
               (ponto[1] >= BBd[1]) && (ponto[1] <= BBd[3]) &&
               (ponto[0] >= 1) && (ponto[0] <= 99) && 
               (ponto[1] >= 1) && (ponto[1] <= 99));
    if (fabs(denominador_u) < PRECISAO)
        return false;
    u = numerador_u / denominador_u;
    v = numerador_v / denominador_v;
    /* printf("U V %.50f %.15f %d\n",u,v, u > -PRECISAO);*/
    return ((u > -PRECISAO) && (u < 1 + PRECISAO) &&
            (v > -PRECISAO) && (v < 1 + PRECISAO) &&
            ((u+v) > -PRECISAO) && ((u+v) < 1 + PRECISAO));
}

/* linha scan */
int algoritmoScanLinha(double triangulo[3][2])
{
    int i, j, conta=0;
    int ponto[2];
    for (i=BB[0]; i<=BB[2]; i++)
        for (j=BB[1]; j<=BB[3]; j++)
        {
            ponto[0] = i;
            ponto[1] = j;
            if (pontoDentroTriangulo(triangulo, ponto))
                conta++;
        }
    return conta;
} 

bool seZero(double triangulo[3][2])
{
    return ((triangulo[0][0] == 0.0) &&
            (triangulo[0][1] == 0.0) &&
            (triangulo[1][0] == 0.0) &&
            (triangulo[1][1] == 0.0) &&
            (triangulo[2][0] == 0.0) &&
            (triangulo[2][1] == 0.0));
}

void imprimeTriangulo(double triangulo[3][2])
{
    printf("(%lf, %lf) ", triangulo[0][0], triangulo[0][1]);
    printf("(%lf, %lf) ", triangulo[1][0], triangulo[1][1]);
    printf("(%lf, %lf)\n", triangulo[2][0], triangulo[2][1]);
}

int main()
{
    double triangulo[3][2];
    while (fscanf(stdin, "%lf %lf %lf %lf %lf %lf",\
            &triangulo[0][0], &triangulo[0][1],\
            &triangulo[1][0], &triangulo[1][1],\
            &triangulo[2][0], &triangulo[2][1]) != EOF)
    {
        if (seZero(triangulo))
            break;   
        encontraBoundingBox(triangulo);
        /*imprimeTriangulo(triangulo);
        printf("BB %d %d %d %d\n", BB[0], BB[1], BB[2], BB[3]);*/
        printf("%4d\n", algoritmoScanLinha(triangulo));
    }
    return 0;
}
