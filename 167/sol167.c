#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

typedef int bool;
#define VERDADEIRO 1
#define FALSO 0

int TABULEIROS[1000][8][8];
int TAM;

/* o problema de 8 x 8 para 8 rainhas tem 92 solucoes segundo wikipedia */
int TABULEIRO_TESTE[8][8];
int RAINHAS[92][8][2];
int TAMR;

void imprimeTabuleiroTeste()
{
   int i;
   for (i=0; i<8; i++)
      printf("%4d %4d %4d %4d %4d %4d %4d %4d\n", TABULEIRO_TESTE[i][0], TABULEIRO_TESTE[i][1],\
        				    	  TABULEIRO_TESTE[i][2], TABULEIRO_TESTE[i][3],\
           				   	  TABULEIRO_TESTE[i][4], TABULEIRO_TESTE[i][5],\
         				    	  TABULEIRO_TESTE[i][6], TABULEIRO_TESTE[i][7]);
}

void removeRainha(int i, int j)
{
   int m, soma = i + j, subtrai = j - i;
   for (m=0;m<8;m++)
   {
       TABULEIRO_TESTE[i][m]--;
       TABULEIRO_TESTE[m][j]--;
       if (((soma-m) < 8) && ((soma-m) >= 0))
          TABULEIRO_TESTE[m][soma-m]--;
       if (((subtrai+m) < 8) && ((subtrai+m) >= 0))
          TABULEIRO_TESTE[m][subtrai+m]--;
   }
}

void ocupaComRainha(int i, int j)
{
   int m, soma = i + j, subtrai = j - i;
   for (m=0;m<8;m++)
   {
       TABULEIRO_TESTE[i][m]++;
       TABULEIRO_TESTE[m][j]++;
       if (((soma-m) < 8) && ((soma-m) >= 0))
          TABULEIRO_TESTE[m][soma-m]++;
       if (((subtrai+m) < 8) && ((subtrai+m) >= 0))
          TABULEIRO_TESTE[m][subtrai+m]++;
   }
}

void buscaConfiguracoesValidas(int pos)
{
   int i;
   if (pos < 8)
   {
      for (i=0; i<8; i++)
         if (TABULEIRO_TESTE[pos][i] == 0)
         {
             RAINHAS[TAMR][pos][0] = pos;
             RAINHAS[TAMR][pos][1] = i;
             ocupaComRainha(pos, i);
             buscaConfiguracoesValidas(pos+1);
             removeRainha(pos, i);
         }
   }
   else
   {
       /* pos == 8, achou conf valida */
       TAMR++;
       if (TAMR < 92)
       { 
          for (i=0; i<8; i++)
          {
             RAINHAS[TAMR][i][0] = RAINHAS[TAMR-1][i][0];
             RAINHAS[TAMR][i][1] = RAINHAS[TAMR-1][i][1];
          } 
       }
   }
}


void imprimeConfiguracoes()
{
   int i,j;
   for (i=0; i<TAMR; i++)
   {
      printf("SOLUCAO %d\n",i+1);
      for (j=0; j<8; j++)
         printf("%d %d\n", RAINHAS[i][j][0], RAINHAS[i][j][1]);
   }
}

void imprimeTabuleiros()
{
   int i,j;
   for (i=0; i<TAM; i++)
   {
      printf("TABULEIRO %d\n",i+1);
      for (j=0; j<8; j++)
         printf("%4d %4d %4d %4d %4d %4d %4d %4d\n", TABULEIROS[i][j][0], TABULEIROS[i][j][1],\
        				    	     TABULEIROS[i][j][2], TABULEIROS[i][j][3],\
           				   	     TABULEIROS[i][j][4], TABULEIROS[i][j][5],\
         				    	     TABULEIROS[i][j][6], TABULEIROS[i][j][7]);
   }
}

int main()
{
    int i, j, k;
    int u, v, soma, maior;
    TAM = 0;
    TAMR = 0;
    for (i=0; i<8; i++)
       memset(TABULEIRO_TESTE[i], 0, sizeof(int)*8);
    buscaConfiguracoesValidas(0);
    /*imprimeConfiguracoes();*/
    while (fscanf(stdin, "%d", &k) != EOF)
    {
        while (TAM < k)
        {
           for (i=0;i<8;i++)
           	fscanf(stdin, "%d %d %d %d %d %d %d %d", &TABULEIROS[TAM][i][0], &TABULEIROS[TAM][i][1],\
           						 &TABULEIROS[TAM][i][2], &TABULEIROS[TAM][i][3],\
           						 &TABULEIROS[TAM][i][4], &TABULEIROS[TAM][i][5],\
           						 &TABULEIROS[TAM][i][6], &TABULEIROS[TAM][i][7]);
           TAM++;
        }
    }
    for (i=0; i<TAM; i++)
    {
       maior = 0;
       for (j=0; j<TAMR; j++)
       {
          soma = 0;
          for (k=0; k<8; k++)
          {
              u = RAINHAS[j][k][0];
              v = RAINHAS[j][k][1];
              soma += TABULEIROS[i][u][v];
          }
          if (soma > maior)
             maior = soma;
       }
       printf("%5d\n", maior);
    }
    /*imprimeTabuleiros();*/
    return 0;
}

