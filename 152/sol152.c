#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <float.h>
#include <limits.h>
#include <math.h>

#define mod(a,b) (b + (a % b)) % b
typedef int bool;
#define FALSE 0
#define TRUE 1
#define PRECISAO 1e-9
#define PI 3.14159265358979323846264338327

int ARVORES[5000][3];
int TAM;

int FRONT[5000];
int FTAM;

/* distancia a fronteira */
double DF[5000];

/* distancia a um site */
double DS[5000];

/* saida do algoritmo */
int SAIDA[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

void removerFront(int k)
{
   int i, pos;
   for (i=0;i<FTAM;i++)
      if (FRONT[i] == k)
         pos = i;
   for (i=pos;i<FTAM-1;i++) 
      FRONT[i]=FRONT[i+1];
   FTAM--;
}

void imprimeSaida()
{
   int i;
   for (i=0;i<10;i++)
      printf("%4d", SAIDA[i]);
   printf("\n");
}

void imprimeArvores()
{
   int i;
   for (i=0;i<TAM;i++)
      printf("%d %d %d\n", ARVORES[i][0], ARVORES[i][1], ARVORES[i][2]);
}

double distancia(int A[3], int B[3])
{
   double x = A[0] - B[0];
   double y = A[1] - B[1];
   double z = A[2] - B[2];
   return sqrt(fabs(x*x + y*y + z*z));
}

void mescla(int ini, int meio, int fim)
{
   int i=ini, j=meio+1, conta=0;
   int tmp[fim-ini+1][3];
   while (i<=meio && j<=fim)
   {
      if (ARVORES[i][0] < ARVORES[j][0])
      {
         tmp[conta][0] = ARVORES[i][0];
         tmp[conta][1] = ARVORES[i][1];
         tmp[conta][2] = ARVORES[i][2];
         conta++;
         i++;
         continue;
      }
      if (ARVORES[i][0] == ARVORES[j][0])
      {
         if (ARVORES[i][1] < ARVORES[j][1])
         {
            tmp[conta][0] = ARVORES[i][0];
            tmp[conta][1] = ARVORES[i][1];
            tmp[conta][2] = ARVORES[i][2];
            conta++;
            i++;
            continue;
         }
         if (ARVORES[i][1] == ARVORES[j][1])
         {
            if (ARVORES[i][2] < ARVORES[j][2])
            {
               tmp[conta][0] = ARVORES[i][0];
               tmp[conta][1] = ARVORES[i][1];
               tmp[conta][2] = ARVORES[i][2];
               conta++;
               i++;
               continue;
            }
         }
      }
      tmp[conta][0] = ARVORES[j][0];
      tmp[conta][1] = ARVORES[j][1];
      tmp[conta][2] = ARVORES[j][2];
      conta++;
      j++;
   }
   while (i<=meio)
   {
      tmp[conta][0] = ARVORES[i][0];
      tmp[conta][1] = ARVORES[i][1];
      tmp[conta][2] = ARVORES[i][2];
      conta++;
      i++;
   }
   while (j<=fim)
   {
      tmp[conta][0] = ARVORES[j][0];
      tmp[conta][1] = ARVORES[j][1];
      tmp[conta][2] = ARVORES[j][2];
      conta++;
      j++;
   }
   for (i=ini;i<=fim;i++)
   {
      ARVORES[i][0]=tmp[i-ini][0];
      ARVORES[i][1]=tmp[i-ini][1];
      ARVORES[i][2]=tmp[i-ini][2];
   }
}

void mesclagem(int ini, int fim)
{
    if (ini < fim)
    {
        int meio = (fim+ini)>>1;
        mesclagem(ini, meio);
        mesclagem(meio+1, fim);
        mescla(ini, meio, fim);
    }
}

int main()
{
    int x, y, z; 
    int i, j, k, fronteira;
    double d;

    TAM = 0;
    FTAM = 0;
    while (fscanf(stdin, "%d %d %d", &x, &y, &z) != EOF)
    {
        /* fim da entrada */
        if ((x == 0) && (y == 0) && (z == 0))
            break;
        ARVORES[TAM][0] = x;
        ARVORES[TAM][1] = y;
        ARVORES[TAM][2] = z;
        TAM++;
    }
    /* algoritmo de Fortune 1987 mod */
    /* nao esta com melhor velocidade, pois exigiria uma arvore binaria balanceada de busca */
    /* contudo ja eh mais rapido que O(n**2) e combinacoes 2 a 2 */
    /* ordena vetor lexicograficamente */
    mesclagem(0, TAM-1);
    int v_max[3] = {255, 255, 255};
    int v_min[3] = {0, 0, 0};
    int remove[1000];
    int trem = 0;

    double dmax = distancia(v_max, v_min);
    for (i=0;i<TAM;i++)
    {
       fronteira = ARVORES[i][0];
       DF[i] = -1;
       DS[i] = dmax;
       for (j=0;j<FTAM;j++)
       {
          k = FRONT[j];
          /* distancia entre o novo site e os sites acima da fronteira */
          d = distancia(ARVORES[k], ARVORES[i]);
          /* distancia entre site e fronteira ao inves de parabola */
          DF[k] = abs(ARVORES[k][0] - fronteira);
          if (d <= DS[i] + PRECISAO)
             DS[i] = d;
          if (d <= DS[k] + PRECISAO)
             DS[k] = d;
          /* como ha um limite de distancia, nao precisamos verificar se esfera esta vazia */
          if (DF[k] >= 10 + PRECISAO)
             remove[trem++] = k;
       }
       while (trem > 0)
       {
           removerFront(remove[trem-1]);
           trem--;
       }
       FRONT[FTAM] = i;
       FTAM++;
    }
    for (i=0; i<TAM; i++)
    {
        if (DS[i] <= (10 - PRECISAO))
           SAIDA[(int) DS[i]]++;
    }
    imprimeSaida();
    /*imprimeArvores();*/
    return 0;
}
