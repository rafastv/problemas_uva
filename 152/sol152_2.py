import sys, math

ARVORES = []

def imprimeHistograma(valores):
   for i in range(10):
      sys.stdout.write("{:>4}".format(str(valores[i])))
   sys.stdout.write("\n")

def distancia(A, B):
   x = A[0]-B[0]
   y = A[1]-B[1]
   z = A[2]-B[2]
   return math.sqrt(x*x + y*y + z*z)

linhas = sys.stdin.readlines()
for linha in linhas:
   linha = linha.replace("\n","")
   arvore = [int(i) for i in linha.split()]
   if not arvore[0] and not arvore[1] and not arvore[2]:
       break
   ARVORES += [arvore]


# O(n**2/2)
ds = [distancia([255, 255, 255], [0, 0, 0])]*len(ARVORES)
for i in range(len(ARVORES)-1):
   for j in range(i+1,len(ARVORES)):
      d = distancia(ARVORES[i], ARVORES[j])
      if d <= 9.99999999999999:
         if d < ds[i]:
            ds[i] = d
         if d < ds[j]:
            ds[j] = d

saida = [0]*10
for i in ds:
   if i <= 9.99999999999999:
      saida[int(i)]+=1

imprimeHistograma(saida)

exit(0)
