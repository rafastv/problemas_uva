import sys, math

ARVORES = []
PRECISAO = 1e-9

def imprimeHistograma(valores):
   for i in range(10):
      sys.stdout.write("{:>4}".format(str(valores[i])))
   sys.stdout.write("\n")

def distancia(A, B):
   x = A[0]-B[0]
   y = A[1]-B[1]
   z = A[2]-B[2]
   return math.sqrt(x*x + y*y + z*z)

linhas = sys.stdin.readlines()
for linha in linhas:
   linha = linha.replace("\n","")
   arvore = [int(i) for i in linha.split()]
   if not arvore[0] and not arvore[1] and not arvore[2]:
       break
   ARVORES += [arvore]

# Implicit Mod Fortune Algorithm 1987
ARVORES.sort()
ds = [distancia([255, 255, 255], [0, 0, 0])]*len(ARVORES)
df = [-1]*len(ARVORES)
floresta = []
for i in range(len(ARVORES)):
   fronteira = ARVORES[i][0]
   df[i] = abs(ARVORES[i][0] - fronteira)*0.5
   removeF = []
   for j in floresta:
      d = distancia(ARVORES[j], ARVORES[i])
      df[j] = abs(ARVORES[j][0] - fronteira)*0.5 
      if d <= ds[i]:
         ds[i] = d
      if d <= ds[j]:
         ds[j] = d
      # nao eh preciso verificar se esfera vazia devido limite de 10
      if df[j] >= (10 + PRECISAO):
         removeF += [j]
   # remove sites muito distantes da fronteira
   for k in removeF:
      floresta.remove(k)
   floresta += [i]

saida = [0]*10
for i in ds:
   if i <= (10 - PRECISAO):
      saida[int(i)]+=1

imprimeHistograma(saida)

exit(0)
