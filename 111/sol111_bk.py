import sys

lista = []
for linha in sys.stdin:
    vars = linha.split()
    lista += [[int(i) for i in vars]]

while lista:
    elemento = lista.pop(0)
    if len(elemento) == 1:
        tamanho = elemento[0]
        c_ordem = {}
        evento = 1
        TMP = [0]*tamanho
        for rank in lista.pop(0):
            c_ordem.update({evento:rank})
            TMP[rank-1]=evento
            evento+=1
    else:
        conta_max = {}
        for i in range(tamanho):
            conta_max.update({i+1:0}) 
        r_evento = {}
        evento = 1
        for rank in elemento:
            r_evento.update({rank:evento})
            evento+=1
        # tamanho da maior sequencia
        for t in range(tamanho,1,-1):
            # pontos de inicio da sequencia (pi)
            for i in range(tamanho-t+1):
                k = i+1
                # numero de repeticoes de acordo com pi
                while (tamanho-k+1) >= t:
                    conta = 0
                    l = i
                    # varre o vetor pela sequencia
                    for j in range(k,k+t-1):
                        if (c_ordem[r_evento[l+1]]<c_ordem[r_evento[j+1]]):
                            #print ("conta",conta, "tamanho", t, "par:", l,j)
                            conta+=1
                            l = j
                    if conta >= conta_max[i+1]:
                        conta_max[i+1]=(conta+1)
                    k += 1
            # se ja encontrou o tamanho maior que o proximo, para
            if max(conta_max.values())>(t-1):
                break
        print TMP
        print max(conta_max.values())
        sys.stdout.write(str(c_ordem[r_evento[1]]))
        for i in range(2,tamanho+1):
            sys.stdout.write(",")
            sys.stdout.write(str(c_ordem[r_evento[i]]))
        sys.stdout.write("\n")
        sys.stdout.write(str(r_evento[1]))
        for i in range(2,tamanho+1):
            sys.stdout.write(",")
            sys.stdout.write(str(r_evento[i]))
        sys.stdout.write("\n")
sys.stdout.write("\n")
exit(0)
