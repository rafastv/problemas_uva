import sys

def procuraSubLista(i, k, t, tamanho, c_ordem, r_evento):
    if ((k+t-1) > tamanho) or (t<=1):
        return 0
    c1 = 0
    # varre o vetor pela sequencia
    #print (t,":",i,k)
    if (c_ordem[r_evento[i+1]] < c_ordem[r_evento[k+1]]):
        #print "V"
        c1 = 1 + procuraSubLista(k, k+1, t-1, tamanho, c_ordem, r_evento)
    c2 = 0 + procuraSubLista(i, k+1, t, tamanho, c_ordem, r_evento)
    return max(c1,c2)

lista = []
for linha in sys.stdin:
    vars = linha.split()
    lista += [[int(i) for i in vars]]

while lista:
    elemento = lista.pop(0)
    if len(elemento) == 1:
        tamanho = elemento[0]
        c_ordem = {}
        evento = 1
        for rank in lista.pop(0):
            c_ordem.update({evento:rank})
            evento+=1
    else:
        if not elemento:
            continue
        conta_max = {}
        for i in range(tamanho):
            conta_max.update({i+1:0}) 
        r_evento = {}
        evento = 1
        for rank in elemento:
            r_evento.update({rank:evento})
            evento+=1
        # tamanho da maior sequencia
        for t in range(tamanho,1,-1):
            # pontos de inicio da sequencia (pi)
            for i in range(tamanho-t+1):
                valor = procuraSubLista(i, i+1, t, tamanho, c_ordem, r_evento)
                if valor >= conta_max[i+1]:
                    conta_max[i+1]=(valor+1)
            # se ja encontrou o tamanho maior que o proximo, para
            #print "TAMANHO"
            if max(conta_max.values())>(t-1):
                break
        vmax = max(conta_max.values())
        sys.stdout.write(str(vmax)+"\n")
        #print "FIM"
exit(0)
