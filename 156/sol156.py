import sys, operator, math

def extraiConfiguracao(palavra):
   configuracao = 0
   palavraMin = palavra.lower()
   Letras = [i for i in palavraMin]
   Letras.sort()
   conta = 0
   for letra in Letras:
      algarismo = ord(letra) - 97
      configuracao += algarismo*(31**conta)
      conta += 1
   return configuracao

linhas = sys.stdin.readlines()
dicionario = []
mapa_configuracoes = {}
for linha in linhas:
   linha = linha.replace("\n","")
   if linha[0] == "#":
      break
   dicionario += [i for i in linha.split()]

#dicionario = sorted(dicionario, key=lambda palavra: palavra.lower())
dicionario.sort()
for palavra in dicionario:
   idx = extraiConfiguracao(palavra)
   if idx in mapa_configuracoes.keys():
      mapa_configuracoes[idx] += [palavra]
   else:
      mapa_configuracoes.update({idx:[palavra]})

#print(dicionario)
#print(mapa_configuracoes)
for palavra in dicionario:
   idx = extraiConfiguracao(palavra)
   if len(mapa_configuracoes[idx]) == 1:
      sys.stdout.write(palavra+"\n")
exit(0)
