import sys, operator, math
from itertools import islice
   
PRIMOS = []

def ehPrimo(n):
   if not PRIMOS:
      return True
   for i in range(len(PRIMOS)):
      if n % PRIMOS[i] == 0:
         return False
   return True
   
for i in range(2,101):
   if ehPrimo(i):
      PRIMOS += [i]

CONFIGURACAO = {}

def fat(n):
   if n == 1:
      return 1
   if n in PRIMOS:
      CONFIGURACAO[n]+=1
   else:
      m = n
      while (m > 1):
         for i in range(len(PRIMOS)):
            if m < PRIMOS[i]:
               break
            if m % PRIMOS[i] == 0:
               CONFIGURACAO[PRIMOS[i]]+=1
               m = m // PRIMOS[i]
   return fat(n-1)

#print(PRIMOS)
linhas = sys.stdin.readlines()
for linha in linhas:
   linha = linha.replace("\n","")
   if linha[0] == "0":
      break
   for i in PRIMOS:
      CONFIGURACAO.update({i:0})
   N = int(linha)
   #print("N",N)
   fat(N)
   inicio = "{:>3}".format(str(N)) + "! =";
   parte = ""
   for i in PRIMOS:
      if CONFIGURACAO[i] == 0:
         break
      parte += "{:>3}".format(str(CONFIGURACAO[i]))
    
   # 15 numeros * espacamento de tamanho 3      
   n = (len(parte) // 45) + 1
   for i in range(n):
      saida = parte[i*45:(i+1)*45]
      if not saida:
         break
      if i == 0:
         sys.stdout.write(inicio) 
      else:
         sys.stdout.write(" "*len(inicio)) 
      sys.stdout.write(saida + "\n")

exit(0)
