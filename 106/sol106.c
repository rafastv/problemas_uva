#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main()
{
    int N;
    int i, j, k, cn, ct, tmp;
    char *vc;
    long int a, b, c, d, e, f, u, v, w;
    vc = (char*) malloc(1000000 * sizeof(char));
    while (scanf("%d\n", &N) == 1)
    {
	/*fator = ceil(N / (sizeof(int) * 8.0f));
	v = (int*) calloc(fator, sizeof(int));*/
    	for (i=0; i< N; i++)
		vc[i] = '0';
    	ct = 0;
	k = 1;
    	for (i=1; i< N; i++)
    		for (j=i+1; j< N; j++)
		{
			d = (long int) i;
			e = (long int) j;
			a = (e*e - d*d);
			b = (2*d*e);
			c = (e*e + d*d);
			if ((a > N) || (b > N) || (c > N))
			    break;
			vc[a-1] = '1';
			vc[b-1] = '1';
			vc[c-1] = '1';
			/* mmc */
			e = a;
			f = b;
			if (a > b)
			{
			    f = a;
			    e = b;
			}
			while (f != 0)
			{
			    tmp = f;
		 	    f = e % f;
			    e = tmp;
		    	}
		    	if (e == 1)
			{
			    ct++;
			    /*printf("t %ld %ld %ld\n", a, b, c);*/
			    for (k=2; k< N; k++)
		            {	
				f = (long int) k;
				u = a*f;
				v = b*f;
				w = c*f;
				if ((u > N) || (v > N) || (w > N))
			    		break;
				/*printf("[%ld] %ld %ld %ld\n", f, u, v, w);*/
				vc[u-1] = '1';
				vc[v-1] = '1';
				vc[w-1] = '1';
			    }
			}
		}
	cn = 0;
	for (i=0; i<N; i++)
	    if (vc[i] == '1')
		cn++;
	printf("%d %d\n", ct, N-cn);
    }
    free(vc);
    return 0;
}

