#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <float.h>
#include <limits.h>
#include <math.h>

#define mod(a,b) (b + (a % b)) % b
typedef int bool;
#define FALSE 0
#define TRUE 1
#define PRECISAO 1e-9

double INTERVALOS[10000][2][2];
int QT_INTERVALOS;

double CENTRO[2];
double RAIO;
double MIN_COS;

void imprimeIntervalos()
{
   int i;
   printf("========== INTERVALOS ==========\n");
   for (i=0;i<QT_INTERVALOS;i++)
      printf("ini:(%lf %lf) fim:(%lf %lf)\n", INTERVALOS[i][0][0], INTERVALOS[i][0][1], INTERVALOS[i][1][0], INTERVALOS[i][1][1]);
}

double produtoVetorial(double A[2], double B[2])
{
   return (A[0]*B[1]) - (A[1]*B[0]);
}

double produtoEscalar(double A[2], double B[2])
{
   return (A[0]*B[0]) + (A[1]*B[1]);
}

double distancia(double A[2], double B[2])
{
   double x = A[0] - B[0];
   double y = A[1] - B[1];
   return sqrt(fabs(x*x + y*y));
}

double modulo(double V[2])
{
   double zero[2] = {0, 0};
   return distancia(V, zero);
}

void normaliza(double V[2])
{
   double m = modulo(V);
   V[0] /= m;
   V[1] /= m;
}

double calculaCosseno(double A[2], double B[2])
{
   return produtoEscalar(A,B) * (1.0/(modulo(A)*modulo(B)));
}

/* segmentos tangentes ao circulo a partir de um ponto   */
/* sabemos que tais segmentos formam 90 graus com o raio */
void criaIntervalo(double arvore[2], double intervalo[2][2])
{
   double vetor[2];
   vetor[0] = arvore[0] - CENTRO[0];
   vetor[1] = arvore[1] - CENTRO[1];
   double sen_angulo = RAIO * (1.0/distancia(CENTRO, arvore));
   double cos_a = sqrt(1.0 - sen_angulo*sen_angulo);
   double tmp = distancia(CENTRO,arvore);
   double lado = sqrt(tmp*tmp - RAIO*RAIO); 
   double cos_angulo = lado/tmp;
   //printf("%.20lf == %.20lf?\n",cos_a, cos_angulo);
   intervalo[0][0] = cos_angulo*vetor[0] - sen_angulo*vetor[1];
   intervalo[0][1] = cos_angulo*vetor[1] + sen_angulo*vetor[0];
   intervalo[1][0] = cos_angulo*vetor[0] + sen_angulo*vetor[1];
   intervalo[1][1] = cos_angulo*vetor[1] - sen_angulo*vetor[0];
   //normaliza(intervalo[0]);
   //normaliza(intervalo[1]);
   int i,j;
   for (i=0; i<2; i++)
      for (j=0; j<2; j++)
         if (fabs(intervalo[i][j]) <= PRECISAO)
            intervalo[i][j] = 0.0;
}

bool atendeCriterio(double intervalo[2][2])
{
   return TRUE;
   int j;
   double s1, s2, s3;
   for (j=0; j<QT_INTERVALOS;j++)
   {
      s1 = produtoEscalar(intervalo[0], INTERVALOS[j][1]);
      s1 /= (modulo(intervalo[0]) * modulo(INTERVALOS[j][1]));
      s2 = produtoEscalar(intervalo[1], INTERVALOS[j][0]);
      s2 /= (modulo(intervalo[1]) * modulo(INTERVALOS[j][0]));
      s3 = produtoEscalar(intervalo[1], intervalo[0]);
      s3 /= (modulo(intervalo[1]) * modulo(intervalo[0]));
      //printf("CRITERIO %lf %lf\n", s1, s2);
      if ((fabs(s1) > MIN_COS) ||
          (fabs(s2) > MIN_COS) ||
          (fabs(s3) > MIN_COS))
         return FALSE;
   }
   return TRUE;
}

/* verifica se um intervalo esta dentro de outro ja existente */
bool dentroIntervalos(double intervalo[2][2])
{
   double s1_ini = 0, s1_fim = 0, s2_ini = 0, s2_fim = 0;
   double r1_ini = 0, r1_fim = 0, r2_ini = 0, r2_fim = 0;
   double ref_1, ref_2;
   bool intercepta = FALSE;
   int j,k;
   for (j=0; j<QT_INTERVALOS;j++)
   {
      s1_ini = produtoVetorial(intervalo[0], INTERVALOS[j][0]);
      s1_fim = produtoVetorial(intervalo[0], INTERVALOS[j][1]);
      s2_ini = produtoVetorial(intervalo[1], INTERVALOS[j][0]);
      s2_fim = produtoVetorial(intervalo[1], INTERVALOS[j][1]);
      if (((s1_ini > PRECISAO) && (s1_fim <= PRECISAO)) && (s2_fim > PRECISAO))
      {
         intercepta = TRUE;
         INTERVALOS[j][1][0] = intervalo[1][0];
         INTERVALOS[j][1][1] = intervalo[1][1];
         //printf("ATUALIZEI INTERVALO %lf %lf %lf %lf\n", intervalo[0][0], intervalo[0][1], intervalo[1][0], intervalo[1][1]);
         //printf("SEGMENTO CONTIDO NO INICIO foram em S2 (%d)\n",j);
         break;
      }
      else if (((s2_ini >= -PRECISAO) && (s2_fim < -PRECISAO)) && (s1_ini < -PRECISAO))
      {
         intercepta = TRUE;
         INTERVALOS[j][0][0] = intervalo[0][0];
         INTERVALOS[j][0][1] = intervalo[0][1];
         //printf("ATUALIZEI INTERVALO %lf %lf %lf %lf\n", intervalo[0][0], intervalo[0][1], intervalo[1][0], intervalo[1][1]);
         //printf("SEGMENTO CONTIDO NO FIM foram em S1 (%d)\n",j);
         break;
      }
      else if (((s1_ini >= -PRECISAO) && (s1_fim <  PRECISAO)) && 
               ((s2_ini >  -PRECISAO) && (s2_fim <= PRECISAO)))
      {
         intercepta = TRUE;
         //printf("INTERVALO IGUAL TAL QUAL %lf %lf %lf %lf\n", intervalo[0][0], intervalo[0][1], intervalo[1][0], intervalo[1][1]);
         //printf("SEGMENTO CONTIDO em %d\n",j);
         break;
      }
      else
      {
         continue;
      }
   }
   return intercepta;
}

/* conta arvores nas 4 direcoes */
void contaArvoresNaDirecaoAB(int x_ini, int y_ini, int x_fim, int y_fim)
{
    //printf("NOVO PASSO\n");
    double intervalo[2][2], arvore[2];
    bool intercepta, encontrou = FALSE;
    int i;
    for (i=x_ini;i<=x_fim;i++)
    {
       arvore[0] = i;
       arvore[1] = y_ini;
       //printf("%lf %lf\n", arvore[0],arvore[1]);
       criaIntervalo(arvore, intervalo);
       intercepta = dentroIntervalos(intervalo);
       if (!intercepta)
       {
          encontrou = TRUE;
          INTERVALOS[QT_INTERVALOS][0][0] = intervalo[0][0];
          INTERVALOS[QT_INTERVALOS][0][1] = intervalo[0][1];
          INTERVALOS[QT_INTERVALOS][1][0] = intervalo[1][0];
          INTERVALOS[QT_INTERVALOS][1][1] = intervalo[1][1];
          QT_INTERVALOS++;
          //printf("INSERI INTERVALO %lf %lf %lf %lf\n", intervalo[0][0], intervalo[0][1], intervalo[1][0], intervalo[1][1]);
          //printf("QT_INTERVALOS %d\n", QT_INTERVALOS);
       }
    }
    /* suficiente que se tenha criado um novo intervalo para testar por mais */
    if (encontrou)
       contaArvoresNaDirecaoAB(x_ini-1, y_ini+1, x_fim+1, y_fim+1);
}

void contaArvoresNaDirecaoBC(int x_ini, int y_ini, int x_fim, int y_fim)
{
    //printf("NOVO PASSO\n");
    double intervalo[2][2], arvore[2];
    bool intercepta, encontrou = FALSE;
    int i;
    for (i=y_ini;i>=y_fim;i--)
    {
       arvore[0] = x_ini;
       arvore[1] = i;
       //printf("%lf %lf\n", arvore[0],arvore[1]);
       criaIntervalo(arvore, intervalo);
       intercepta = dentroIntervalos(intervalo);
       if (!intercepta)
       {
          encontrou = TRUE;
          INTERVALOS[QT_INTERVALOS][0][0] = intervalo[0][0];
          INTERVALOS[QT_INTERVALOS][0][1] = intervalo[0][1];
          INTERVALOS[QT_INTERVALOS][1][0] = intervalo[1][0];
          INTERVALOS[QT_INTERVALOS][1][1] = intervalo[1][1];
          QT_INTERVALOS++;
          //printf("INSERI INTERVALO %lf %lf %lf %lf\n", intervalo[0][0], intervalo[0][1], intervalo[1][0], intervalo[1][1]);
          //printf("QT_INTERVALOS %d\n", QT_INTERVALOS);
       }
    }
    /* suficiente que se tenha criado um novo intervalo para testar por mais */
    if (encontrou) 
       contaArvoresNaDirecaoBC(x_ini+1, y_ini+1, x_fim+1, y_fim-1);
}

void contaArvoresNaDirecaoCD(int x_ini, int y_ini, int x_fim, int y_fim)
{
    //printf("NOVO PASSO\n");
    double intervalo[2][2], arvore[2];
    bool intercepta, encontrou = FALSE;
    int i;
    for (i=x_ini;i>=x_fim;i--)
    {
       arvore[0] = i;
       arvore[1] = y_ini;
       //printf("%lf %lf\n", arvore[0],arvore[1]);
       criaIntervalo(arvore, intervalo);
       intercepta = dentroIntervalos(intervalo);
       if (!intercepta)
       {
          encontrou = TRUE;
          INTERVALOS[QT_INTERVALOS][0][0] = intervalo[0][0];
          INTERVALOS[QT_INTERVALOS][0][1] = intervalo[0][1];
          INTERVALOS[QT_INTERVALOS][1][0] = intervalo[1][0];
          INTERVALOS[QT_INTERVALOS][1][1] = intervalo[1][1];
          QT_INTERVALOS++;
          //printf("INSERI INTERVALO %lf %lf %lf %lf\n", intervalo[0][0], intervalo[0][1], intervalo[1][0], intervalo[1][1]);
          //printf("QT_INTERVALOS %d\n", QT_INTERVALOS);
       }
    }
    /* suficiente que se tenha criado um novo intervalo para testar por mais */
    if (encontrou) 
       contaArvoresNaDirecaoCD(x_ini+1, y_ini-1, x_fim-1, y_fim-1);
}

void contaArvoresNaDirecaoDA(int x_ini, int y_ini, int x_fim, int y_fim)
{
    //printf("NOVO PASSO\n");
    double intervalo[2][2], arvore[2];
    bool intercepta, encontrou = FALSE;
    int i;
    for (i=y_ini;i<=y_fim;i++)
    {
       arvore[0] = x_ini;
       arvore[1] = i;
       //printf("%lf %lf\n", arvore[0],arvore[1]);
       criaIntervalo(arvore, intervalo);
       intercepta = dentroIntervalos(intervalo);
       if (!intercepta)
       {
          encontrou = TRUE;
          INTERVALOS[QT_INTERVALOS][0][0] = intervalo[0][0];
          INTERVALOS[QT_INTERVALOS][0][1] = intervalo[0][1];
          INTERVALOS[QT_INTERVALOS][1][0] = intervalo[1][0];
          INTERVALOS[QT_INTERVALOS][1][1] = intervalo[1][1];
          QT_INTERVALOS++;
          //printf("INSERI INTERVALO %lf %lf %lf %lf\n", intervalo[0][0], intervalo[0][1], intervalo[1][0], intervalo[1][1]);
          //printf("QT_INTERVALOS %d\n", QT_INTERVALOS);
       }
    }
    /* suficiente que se tenha criado um novo intervalo para testar por mais */
    if (encontrou)
       contaArvoresNaDirecaoDA(x_ini-1, y_ini-1, x_fim-1, y_fim+1);
}

void scanQuadrados(int a, int b, int passo)
{
   int x = a, y = b;
   int conta = 1, ciclo = 1;
   double intervalo[2][2], arvore[2];
   bool intercepta, encontrou = FALSE;
   int i;
   /* assume passo > 1 */
   do 
   {
       //printf("%d %d\n",x,y);
       arvore[0] = x;
       arvore[1] = y;
       criaIntervalo(arvore, intervalo);
       intercepta = dentroIntervalos(intervalo);
       if (!intercepta)
       {
          encontrou = TRUE;
          INTERVALOS[QT_INTERVALOS][0][0] = intervalo[0][0];
          INTERVALOS[QT_INTERVALOS][0][1] = intervalo[0][1];
          INTERVALOS[QT_INTERVALOS][1][0] = intervalo[1][0];
          INTERVALOS[QT_INTERVALOS][1][1] = intervalo[1][1];
          QT_INTERVALOS++;
       }
       switch (ciclo)
       {
          case 1: { y++; break; }
          case 2: { x++; break; }
          case 3: { y--; break; }
          case 4: { x--; break; }
       }
       conta++;
       ciclo=(conta<passo)?ciclo:ciclo+1;
       conta=(conta<passo)?conta:(conta%passo)+1;
   } while ((x != y) || (x != a));
   if (encontrou)
      scanQuadrados(a-1, b-1, passo+1);
}

void contaArvores()
{
    /**********/
    /*  A  B  */
    /*  D  C  */
    /**********/
    contaArvoresNaDirecaoAB(-1, 2, 2, 2);
    contaArvoresNaDirecaoBC( 2, 2, 2,-1);
    contaArvoresNaDirecaoCD( 2,-1,-1,-1);
    contaArvoresNaDirecaoDA(-1,-1,-1, 2);
    /* depois simplificar (aglomerar) */
}

int main()
{
    double arvore[4][2] = {{0,1}, {1,1}, {1,0}, {0,0}};
    double diametro;
    QT_INTERVALOS = 0;
    /* intervalo minimo de 0.01 graus */
    MIN_COS = cos(M_PI*(1.0/18000.0));
    while (fscanf(stdin, "%lf %lf %lf", &diametro, &CENTRO[0], &CENTRO[1]) != EOF)
    {
        /* fim da entrada */
        if (diametro == 0)
            break;
        RAIO = diametro * 0.5;
        //criaIntervalo(arvore[0], INTERVALOS[QT_INTERVALOS++]);
        //criaIntervalo(arvore[1], INTERVALOS[QT_INTERVALOS++]);
        //criaIntervalo(arvore[2], INTERVALOS[QT_INTERVALOS++]);
        //criaIntervalo(arvore[3], INTERVALOS[QT_INTERVALOS++]);
        //printf("CENTRO %lf %lf RAIO %lf\n", CENTRO[0],CENTRO[1],RAIO);
        //imprimeIntervalos();
        scanQuadrados(-1,-1,2);
        //contaArvores();
        printf("%d\n", QT_INTERVALOS);
        QT_INTERVALOS = 0;
    }
    /*imprimeIntervalos();*/
    return 0;
}
