#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <float.h>
#include <limits.h>
#include <math.h>

#define mod(a,b) (b + (a % b)) % b
typedef int bool;
#define FALSE 0
#define TRUE 1
#define PRECISAO 1e-9
#define PI 3.14159265358979323846264338327

double INTERVALOS[10000][2][2];
int QT_INTERVALOS;

int REGIOES_ANGULOS[4];
double MIN_COS;

double CENTRO[2];
double RAIO;

double produtoVetorial(double A[2], double B[2])
{
   return (A[0]*B[1]) - (A[1]*B[0]);
}

double produtoEscalar(double A[2], double B[2])
{
   return (A[0]*B[0]) + (A[1]*B[1]);
}

double distancia(double A[2], double B[2])
{
   double x = A[0] - B[0];
   double y = A[1] - B[1];
   return sqrt(fabs(x*x + y*y));
}

double modulo(double V[2])
{
   double zero[2] = {0, 0};
   return distancia(V, zero);
}

double calculaCosseno(double A[2], double B[2])
{
   return produtoEscalar(A,B) * (1.0/(modulo(A)*modulo(B)));
}

void imprimeIntervalos()
{
   int i;
   printf("========== INTERVALOS ==========\n");
   for (i=0;i<QT_INTERVALOS;i++)
   {
      printf("ini:(%lf %lf) fim:(%lf %lf)\n", INTERVALOS[i][0][0], INTERVALOS[i][0][1], INTERVALOS[i][1][0], INTERVALOS[i][1][1]);
      printf("ANGULO ENTRE INTERVALOS %lf\n", acos(calculaCosseno(INTERVALOS[i][1],INTERVALOS[(i+1)%QT_INTERVALOS][0]))*180/PI);
   }
}

/* segmentos tangentes ao circulo a partir de um ponto   */
/* sabemos que tais segmentos formam 90 graus com o raio */
void criaIntervalo(double arvore[2], double intervalo[2][2])
{
   double vetor[2];
   vetor[0] = arvore[0] - CENTRO[0];
   vetor[1] = arvore[1] - CENTRO[1];
   double d = distancia(CENTRO,arvore);
   double sen_angulo = RAIO * (1.0/d);
   double lado = sqrt(d*d - RAIO*RAIO); 
   double cos_angulo = lado/d;
   /* sentido anti-horario (assume-se angulo positivo) */
   intervalo[0][0] = cos_angulo*vetor[0] - sen_angulo*vetor[1];
   intervalo[0][1] = cos_angulo*vetor[1] + sen_angulo*vetor[0];
   /* sentido horario      (assume-se angulo positivo) */
   intervalo[1][0] = cos_angulo*vetor[0] + sen_angulo*vetor[1];
   intervalo[1][1] = cos_angulo*vetor[1] - sen_angulo*vetor[0];
   int i,j;
   for (i=0; i<2; i++)
      for (j=0; j<2; j++)
         if (fabs(intervalo[i][j]) <= PRECISAO)
            intervalo[i][j] = 0.0;
}

bool intervaloAtendeCriterio(double intervalo[2][2])
{
   double cos_atual = calculaCosseno(intervalo[0], intervalo[1]);
   if (cos_atual >= MIN_COS)
      return FALSE;
   return TRUE;
}

bool scanAtendeCriterio()
{
   double cos_atual;
   int j;
   for (j=0;j<QT_INTERVALOS;j++)
   {
      cos_atual = calculaCosseno(INTERVALOS[j][1], INTERVALOS[(j+1)%QT_INTERVALOS][0]);
      if (cos_atual < MIN_COS)
         return TRUE;
   }
   return FALSE;
}

void removeArvore(int pos)
{
    int i;
    /* shift de todos os intervalos posteriores */
    QT_INTERVALOS--;
    for (i=pos;i<QT_INTERVALOS;i++)
    {
       INTERVALOS[i][0][0] = INTERVALOS[i+1][0][0];
       INTERVALOS[i][0][1] = INTERVALOS[i+1][0][1];
       INTERVALOS[i][1][0] = INTERVALOS[i+1][1][0];
       INTERVALOS[i][1][1] = INTERVALOS[i+1][1][1];
    }
}

void salvaIntervaloPos(double intervalo[2][2], int pos)
{
    int i;
    double intervalo_novo[2][2], intervalo_velho[2][2];
    /* shift de todos os intervalos posteriores menos o primeiro */
    QT_INTERVALOS++;
    intervalo_novo[0][0] = intervalo[0][0];
    intervalo_novo[0][1] = intervalo[0][1];
    intervalo_novo[1][0] = intervalo[1][0];
    intervalo_novo[1][1] = intervalo[1][1];
    for (i=pos;i<QT_INTERVALOS;i++)
    {
       intervalo_velho[0][0] = INTERVALOS[i][0][0];
       intervalo_velho[0][1] = INTERVALOS[i][0][1];
       intervalo_velho[1][0] = INTERVALOS[i][1][0];
       intervalo_velho[1][1] = INTERVALOS[i][1][1];
       INTERVALOS[i][0][0] = intervalo_novo[0][0];
       INTERVALOS[i][0][1] = intervalo_novo[0][1];
       INTERVALOS[i][1][0] = intervalo_novo[1][0];
       INTERVALOS[i][1][1] = intervalo_novo[1][1];
       intervalo_novo[0][0] = intervalo_velho[0][0];
       intervalo_novo[0][1] = intervalo_velho[0][1];
       intervalo_novo[1][0] = intervalo_velho[1][0];
       intervalo_novo[1][1] = intervalo_velho[1][1];
    }
}

/* verifica qual posicao intervalo deve ser inserido */
int posIntervalos(double intervalo[2][2])
{
   double sp_ini, sp_fim, s1_ini, s1_fim, s2_ini, s2_fim;
   double cos_1, cos_2;
   int j;
   for (j=0;j<QT_INTERVALOS;j++)
   {
      s1_ini = produtoVetorial(intervalo[0], INTERVALOS[j][0]);
      s1_fim = produtoVetorial(intervalo[0], INTERVALOS[j][1]);
      s2_ini = produtoVetorial(intervalo[1], INTERVALOS[j][0]);
      s2_fim = produtoVetorial(intervalo[1], INTERVALOS[j][1]);
      sp_ini = produtoVetorial(intervalo[1], INTERVALOS[(j+1)%QT_INTERVALOS][0]);
      sp_fim = produtoVetorial(intervalo[1], INTERVALOS[(j+1)%QT_INTERVALOS][1]);
      cos_1 = calculaCosseno(INTERVALOS[j][1], intervalo[0]);
      cos_2 = calculaCosseno(INTERVALOS[(j+1)%QT_INTERVALOS][0], intervalo[1]);
      if (((s1_ini >= -PRECISAO) && (s1_fim <= PRECISAO)) && 
          ((s2_fim >   PRECISAO) && (s2_ini >  PRECISAO)))
      {
         INTERVALOS[j][1][0] = intervalo[1][0];
         INTERVALOS[j][1][1] = intervalo[1][1];
         /*printf("(%d) ATUALIZEI INTERVALO %lf %lf %lf %lf\n", j,intervalo[0][0], intervalo[0][1], intervalo[1][0], intervalo[1][1]);
         printf("SEGMENTO CONTIDO NO INICIO fora em S2 (%d)\n",j);*/
         return -2;
      }
      else if (((s2_ini >= -PRECISAO) && (s2_fim <= PRECISAO)) && 
               ((s1_ini <  -PRECISAO) && (s1_fim < -PRECISAO)))
      {
         INTERVALOS[j][0][0] = intervalo[0][0];
         INTERVALOS[j][0][1] = intervalo[0][1];
         /*printf("(%d) ATUALIZEI INTERVALO %lf %lf %lf %lf\n", j,intervalo[0][0], intervalo[0][1], intervalo[1][0], intervalo[1][1]);
         printf("SEGMENTO CONTIDO NO FIM fora em S1 (%d)\n",j);*/
         return -2;
      }
      else if (((s1_ini >= -PRECISAO) && (s1_fim <= PRECISAO)) && 
               ((s2_ini >= -PRECISAO) && (s2_fim <= PRECISAO)))
      {
         /*printf("(%d) INTERVALO IGUAL TAL QUAL %lf %lf %lf %lf\n", j,intervalo[0][0], intervalo[0][1], intervalo[1][0], intervalo[1][1]);
         printf("SEGMENTO CONTIDO em %d\n",j);*/
         return -2;
      }
      else if (((s1_fim > PRECISAO) && (sp_ini < -PRECISAO)) &&
               ((s1_ini > PRECISAO) && (sp_fim < -PRECISAO)))
      {
         /* gap too small after insertion */
         if ((cos_1 >= MIN_COS) && (cos_2 >= MIN_COS))
         {
             /* join_trees */
             INTERVALOS[j][1][0] = INTERVALOS[(j+1)%QT_INTERVALOS][1][0];
             INTERVALOS[j][1][1] = INTERVALOS[(j+1)%QT_INTERVALOS][1][1];
             removeArvore((j+1)%QT_INTERVALOS);
             return -2;
         }
         if (cos_1 >= MIN_COS) 
         {
            INTERVALOS[j][1][0] = intervalo[1][0];
            INTERVALOS[j][1][1] = intervalo[1][1];
            /*printf("(%d) ATUALIZEI INTERVALO %lf %lf %lf %lf\n", j,intervalo[0][0], intervalo[0][1], intervalo[1][0], intervalo[1][1]);
            printf("SEGMENTO CONTIDO NO INICIO fora em S2 (%d)\n",j);*/
            return -2;
         }
         if (cos_2 >= MIN_COS) 
         {
            INTERVALOS[(j+1)%QT_INTERVALOS][0][0] = intervalo[0][0];
            INTERVALOS[(j+1)%QT_INTERVALOS][0][1] = intervalo[0][1];
            /*printf("(%d) ATUALIZEI INTERVALO %lf %lf %lf %lf\n", j,intervalo[0][0], intervalo[0][1], intervalo[1][0], intervalo[1][1]);
            printf("SEGMENTO CONTIDO NO FIM fora em S1 (%d)\n",j);*/
            return -2;
         }
         /*printf("cos1 %d cos2 %d VALIDO\n",cos_1 >= MIN_COS, cos_2>=MIN_COS);
         printf("INSERIR INTERVALO (%d) %lf %lf %lf %lf\n", j,intervalo[0][0], intervalo[0][1], intervalo[1][0], intervalo[1][1]);
         printf("INTERVALO %lf %lf %lf %lf\n", INTERVALOS[j][0][0], INTERVALOS[j][0][1], INTERVALOS[j][1][0], INTERVALOS[j][1][1]);
         printf("INTERVALO S1FIM %lf SPINI %lf\n",s1_fim,sp_ini);*/
         return j+1;
      }
      else
      {
         continue;
      }
   }
   if (QT_INTERVALOS < 4)
   {
      /*printf("INSERIR INTERVALO (FIM) %lf %lf %lf %lf\n", intervalo[0][0], intervalo[0][1], intervalo[1][0], intervalo[1][1]);*/
      return QT_INTERVALOS;
   }
   return -2;
}

void scanQuadrados(int a, int b, int passo, int n)
{
   double intervalo[2][2], arvore[2];
   int x = a, y = b, k;
   int conta = 1, ciclo = 1, intervalos_pequenos = 0;
   /* assume passo > 1 */
   do 
   {
       arvore[0] = x;
       arvore[1] = y;
       criaIntervalo(arvore, intervalo);
       if (intervaloAtendeCriterio(intervalo))
       {
          k = posIntervalos(intervalo);
          if (k >= -1)
             salvaIntervaloPos(intervalo, k);
       }
       else
          intervalos_pequenos++;
       /* efetua scan circular em volta do quadrado */
       switch (ciclo)
       {
          case 1: { y++; break; }
          case 2: { x++; break; }
          case 3: { y--; break; }
          case 4: { x--; break; }
       }
       conta++;
       ciclo=(conta<passo)?ciclo:ciclo+1;
       conta=(conta<passo)?conta:(conta%passo)+1;
   } while ((x != y) || (x != a));
   /* existe algum espaco entre intervalos de angulo > 0.01 */
   if (scanAtendeCriterio() && (intervalos_pequenos < QT_INTERVALOS) && (n > 0))
      scanQuadrados(a-1, b-1, passo+2, n-1);
}

int main()
{
    double diametro;
    int i;
    /* intervalo minimo de 0.01 graus */
    double angulo = 0.001 * (PI/18.0);
    MIN_COS = sin(angulo)*(1./tan(angulo));
    
    while (fscanf(stdin, "%lf %lf %lf", &diametro, &CENTRO[0], &CENTRO[1]) != EOF)
    {
        /* fim da entrada */
        if (diametro == 0)
            break;
        RAIO = diametro * 0.5;
        QT_INTERVALOS = 0;
        /*printf("CENTRO %lf %lf RAIO %lf\n", CENTRO[0],CENTRO[1],RAIO);*/
        scanQuadrados(0,0, 2, 20);
        /*imprimeIntervalos();*/
        printf("%d\n", QT_INTERVALOS);
    }
    return 0;
}
