import sys, operator, math
from itertools import islice

#####################################################################
# Operadores devem ser avaliados da direita para a esquerda,        #
# contudo parenteses devem ser avaliados da esquerda para a direita #
#####################################################################

VARIAVEIS = {}

def sinal(numero):
   return (numero >= 0)

def encontraFimParenteses(texto, pos):
   conta = 1
   for i in range(pos+1, len(texto)):
      letra = texto[i]
      if letra == "(":
         conta+=1
         continue
      if letra == ")":
         conta-=1
      if conta == 0:
         return i
   return -1

def NaN(texto):
   for letra in texto:
      if ord(letra) < ord("0") or ord(letra) > ord("9"):
         return True
   return False

def imprimeVariaveis(original):
   if len(VARIAVEIS.keys()) == 0:
      sys.stdout.write("No Change\n")
   else:
      alfabeto = list(VARIAVEIS.keys())
      alfabeto.sort()
      saida = ""
      for letra in alfabeto:
         if (letra not in original.keys() and VARIAVEIS[letra] != 0) or (letra in original.keys() and original[letra]!=VARIAVEIS[letra]):
            saida += letra + " = " + str(VARIAVEIS[letra]) + ", "
      if saida != "":     
         sys.stdout.write(saida[:-2] + "\n")
      else:
         sys.stdout.write("No Change\n")

def encontraOperador(texto):
   minimo = len(texto)
   for i in range(len(texto)):
      if texto[i] == "=" and i < minimo:
         minimo = i 
         break
      if texto[i] == "+" and i < minimo:
         minimo = i 
         break
      if texto[i] == "-" and i < minimo:
         minimo = i 
         break
      if texto[i] == "/" and i < minimo:
         minimo = i 
         break
      if texto[i] == "*" and i < minimo:
         minimo = i 
         break
      if texto[i] == "(" and i < minimo:
         minimo = i 
         break
   return minimo

def avalia(texto):
   if len(texto) == 0:
      return 0
   pos1 = encontraOperador(texto)
   if pos1 < len(texto) and texto[pos1] == "(":
      pos2 = encontraFimParenteses(texto, pos1)
      parte1 = texto[pos1+1:pos2]
      parte2 = texto[pos2+2:]
      operador = ""
      if (pos2+1) < len(texto):
         operador = texto[pos2+1]
      if operador == "=":
         return avalia(parte1)
      elif operador == "+":
         return avalia(parte1) + avalia(parte2)
      elif operador == "-":
         return avalia(parte1) - avalia(parte2)
      elif operador == "/":
         numerador = avalia(parte1)
         denominador = avalia(parte2)
         if sinal(numerador) != sinal(denominador):
            return -(abs(numerador)//abs(denominador))
         else:
            return abs(numerador)//abs(denominador)
      elif operador == "*":
         return avalia(parte1) * avalia(parte2)
      else:
         return avalia(parte1)
   else:
      global VARIAVEIS
      parte1 = texto[:pos1]
      parte2 = texto[pos1+1:]
      operador = ""
      numero1 = 0
      numero2 = 0
      if pos1 < len(texto):
         operador = texto[pos1]
         numero2 = avalia(parte2)
      if len(parte1) == 1 and ord(parte1) >= ord("A") and ord(parte1) <= ord("Z"):
         if parte1 not in VARIAVEIS.keys():
            VARIAVEIS.update({parte1: 0})
         numero1 = VARIAVEIS[parte1]
      else:
         if parte1[0] == "_":
            if not NaN(parte1[1:]):
               numero1 = - int(parte1[1:])
         else:
            if not NaN(parte1):
               numero1 = int(parte1)
      if operador == "=":
         if parte1 in VARIAVEIS.keys():
            VARIAVEIS[parte1] = numero2
            return VARIAVEIS[parte1]
         else: 
            return 0
      elif operador == "+":
         return numero1 + numero2
      elif operador == "-":
         return numero1 - numero2
      elif operador == "/":
         numerador = numero1
         denominador = numero2
         if sinal(numerador) != sinal(denominador):
            return -(abs(numerador)//abs(denominador))
         else:
            return abs(numerador)//abs(denominador)
      elif operador == "*":
         return numero1 * numero2
      else:
         return numero1

linhas = sys.stdin.readlines()
VARIAVEIS = {}
for linha in linhas:
   linha = linha.replace("\n","")
   if linha == "#":
      break
   texto = linha.replace(" ", "")
   original = VARIAVEIS.copy()
   avalia(texto)
   imprimeVariaveis(original)
exit(0)
