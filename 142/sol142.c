#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <float.h>
#include <limits.h>
#include <math.h>

typedef int bool;
#define false 0
#define true 1
#define mod(a,b) (b + (a % b)) % b

const char ALFABETO[26] = {'A','B','C','D','E','F',
                           'G','H','I','J','K','L',
                           'M','N','O','P','Q','R',
                           'S','T','U','V','W','X',
                           'Y','Z'};
int ICONES[50][3];
int TAM_I;

int REGIOES[25][4];
int TAM_R;

void imprimeIcones()
{
    int i;
    for (i=0;i<TAM_I;i++)
        printf("%d (%d %d) v: %d\n", i+1, ICONES[i][0], ICONES[i][1], ICONES[i][2]);
}

int distancia(int ponto[2], int x, int y)
{
   int dx = (ponto[0] - x);
   int dy = (ponto[1] - y);
   return dx*dx + dy*dy;
}

int menorDistanciaIcones(int x, int y)
{
    int i, tmp, k = -1, d_min = INT_MAX;
    for (i=0;i<TAM_I;i++)
    {
        if (!ICONES[i][2])
            continue;
        tmp = distancia(ICONES[i], x, y);
        /*printf("%d: %d\n", i+1, tmp);*/
        if (tmp < d_min)
        {
            k = i;
            d_min = tmp;
        }
    }
    return k;
}

int dentroRegioes(int x, int y)
{
    int i;
    /* ordem por Z-buffer */
    for (i=(TAM_R-1);i>=0;i--)
        if ((x >= REGIOES[i][0]) && (x <= REGIOES[i][2]) &&
            (y >= REGIOES[i][1]) && (y <= REGIOES[i][3]))
                return i;
    return -1;
}
            
void escondeIcones()
{
    int i, k;
    for (i=0;i<TAM_I;i++)
    {
        if (!ICONES[i][2])
            continue;
        k = dentroRegioes(ICONES[i][0], ICONES[i][1]);
        if (k >= 0)
            ICONES[i][2] = 0;
    }
}

int main()
{
    int x_1,y_1,x_2,y_2;
    int k, menor, idx, i, tmp;
    char tipo;
    bool primeiro = false;

    TAM_I = 0;
    TAM_R = 0;
    while (fscanf(stdin, "%c", &tipo) != EOF)
    {
        /* fim da entrada */
        if (tipo == '#')
            break;
        else if (tipo == 'I')
        {
            fscanf(stdin, "%d %d", &x_1, &y_1);
            ICONES[TAM_I][0] = x_1;
            ICONES[TAM_I][1] = y_1;
            /* terceira coordenada diz se o icone eh visivel */
            ICONES[TAM_I][2] = 1;
            TAM_I++;
        } 
        else if (tipo == 'R')
        {
            fscanf(stdin, "%d %d %d %d", &x_1, &y_1, &x_2, &y_2);
            REGIOES[TAM_R][0] = x_1;
            REGIOES[TAM_R][1] = y_1;
            REGIOES[TAM_R][2] = x_2;
            REGIOES[TAM_R][3] = y_2;
            TAM_R++;
            /* icones que janelas sobrepoem */
            escondeIcones();
        } 
        else if (tipo == 'M')
        {
            /* icones que janelas sobrepoem */
            if (!primeiro)
                escondeIcones();
            primeiro = true;
            fscanf(stdin, "%d %d", &x_1, &y_1);
            k = dentroRegioes(x_1, y_1);
            if (k >= 0)
                printf("%c\n", ALFABETO[k]);
            else
            {
                idx = menorDistanciaIcones(x_1, y_1);
                menor = distancia(ICONES[idx], x_1, y_1);
                for (i=idx;i<TAM_I;i++)
                {
                    if (!ICONES[i][2])
                        continue;
                    tmp = distancia(ICONES[i], x_1, y_1);
                    if (tmp == menor)
                        printf("%3d", i+1);
                }
                printf("\n");
                /*imprimeIcones();*/
            }
        } 
        else
            continue;
    }
    return 0;
}
