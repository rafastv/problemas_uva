#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <float.h>
#include <limits.h>
#include <math.h>

/* matriz 50 x 64 bits; 4 posicoes p/cada matriz */
long int MOVIMENTOS[101][4][50];
int POS = 1;
int N;

#define mod(a,b) (b + (a % b)) % b
typedef int bool;
#define false 0
#define true 1

bool existeMovimento()
{
   int i,j,k;
   int c1, c2, c3, c4;
   for (i=1;i<POS-1;i++)
      for (j=0;j<4;j++)
      {
         c1 = 0;
         c2 = 0;
         c3 = 0;
         c4 = 0;
         for (k=0;k<N;k++)
         {
            if (MOVIMENTOS[POS-1][0][k] == MOVIMENTOS[POS-1-i][j][k])
                c1++;
            if (MOVIMENTOS[POS-1][1][k] == MOVIMENTOS[POS-1-i][j][k])
                c2++;
            if (MOVIMENTOS[POS-1][2][k] == MOVIMENTOS[POS-1-i][j][k])
                c3++;
            if (MOVIMENTOS[POS-1][3][k] == MOVIMENTOS[POS-1-i][j][k])
                c4++;
         }
         if ((c1 == N) || (c2 == N) || (c3 == N) || (c4 == N))
             return true;
      }
   return false;
}

void salvaMovimento(long int x, long int y, char movimento)
{
   int i;
   long int um = 1;
   for (i=0;i<N;i++)
   {
       MOVIMENTOS[POS][0][i] = MOVIMENTOS[POS-1][0][i];
       MOVIMENTOS[POS][1][i] = MOVIMENTOS[POS-1][1][i];
       MOVIMENTOS[POS][2][i] = MOVIMENTOS[POS-1][2][i];
       MOVIMENTOS[POS][3][i] = MOVIMENTOS[POS-1][3][i];
   }
   if (movimento == '+')
   {
       MOVIMENTOS[POS][0][x-1] |= (um << (N-y));
       MOVIMENTOS[POS][1][N-y] |= (um << (N-x));
       MOVIMENTOS[POS][2][y-1] |= (um << (x-1));
       MOVIMENTOS[POS][3][N-x] |= (um << (y-1));
   }
   else
   {
       MOVIMENTOS[POS][0][x-1] &= ~(um << (N-y));
       MOVIMENTOS[POS][1][N-y] &= ~(um << (N-x));
       MOVIMENTOS[POS][2][y-1] &= ~(um << (x-1));
       MOVIMENTOS[POS][3][N-x] &= ~(um << (y-1));
   }
   POS++;
}

void imprimeMovimentos()
{
    int i, j, k, l;
    long int um;
    for (i=1;i<POS;i++)
       for (j=0;j<4;j++)
       {
          printf("MATRIZ %d %d\n", i, j+1);
          for (k=0;k<N;k++)
          {
             um = 1;
             um <<= (N-1);
             for (l=0;l<N;l++)
             {
                 if (MOVIMENTOS[i][j][k] & um)
                     printf("1");
                 else
                     printf("0");
                 um >>= 1;
             } 
             printf("\n");
          }
       }
}


int main()
{
    int i, k, x, y, passos, conta;
    bool saida;
    char movimento;
    /* inicia movimentos */
    for (i=0;i<50;i++)
    {
        MOVIMENTOS[0][0][i] = 0;
        MOVIMENTOS[0][1][i] = 0;
        MOVIMENTOS[0][2][i] = 0;
        MOVIMENTOS[0][3][i] = 0;
    }
    while (fscanf(stdin, "%d", &N) != EOF)
    {
        /* fim da entrada */
        if (N == 0)
            break;
        passos = N << 1;
        conta = 0;
        saida = false;
        while (conta < passos)
        {
            fscanf(stdin, "%d %d %c", &x, &y, &movimento);
            conta++;
            if (!saida)
            {
               salvaMovimento(x, y, movimento);
               saida = existeMovimento();
               k = conta;
            }
        }
        if (saida)
           printf("Player %d wins on move %d\n",(k%2)+1,k);
        else
           printf("Draw\n");
         
        /*imprimeMovimentos();*/
        POS = 1;
    }
    return 0;
}
