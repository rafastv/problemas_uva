#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

typedef int bool;
#define true 1
#define false 0

typedef struct par
{
    char primeiro;
    char segundo;
} Tpar;

void mescla(char variaveis[], int ini, int meio, int fim)
{
   int i=ini, j=meio+1, conta=0;
   char tmp[fim-ini+1];
   while (i<=meio && j<=fim)
   {
      if (variaveis[i] < variaveis[j])
         tmp[conta++] = variaveis[i++];
      else
         tmp[conta++] = variaveis[j++];
   }
   while (i<=meio)
      tmp[conta++] = variaveis[i++];
   while (j<=fim)
      tmp[conta++] = variaveis[j++];
   tmp[conta] = '\0';
   for (i=ini;i<=fim;i++)
      variaveis[i]=tmp[i-ini];
}

void mesclagem(char variaveis[], int ini, int fim)
{
    if (ini < fim)
    {
        int meio = (fim+ini)>>1;
        mesclagem(variaveis, ini, meio);
        mesclagem(variaveis, meio+1, fim);
        mescla(variaveis, ini, meio, fim);
    }
}

bool violaPares(char *sequencia, char b, int tam, Tpar *pares, int c2)
{
    int i, j, conta;
    char a;
    for (j=0;j<c2;j++)
    {
	conta = 0;
        for (i=0;i<tam;i++)
        {
            a = sequencia[i];
	    if ((b == pares[j].primeiro) && (a == pares[j].segundo))
                return true;
            else if (b == a)
                return true;
	    else if ((a != pares[j].primeiro) && (b == pares[j].segundo))
		conta++;
            else
		continue;
        }
	if (conta == tam)
	    return true;
    }
    return false;
}

void imprimeSequencias(char *sequencia, char *variaveis, int pos, int c1, Tpar *pares, int c2)
{
   if (pos == c1)
   {
       sequencia[pos] = '\0'; 
       printf("%s\n", sequencia);
   }
   else
   {
       int i;
       for (i=0;i<c1;i++)
       {
           sequencia[pos+1]='\0';
           if (!violaPares(sequencia, variaveis[i], pos+1, pares, c2))
           {
               sequencia[pos] = variaveis[i];
               imprimeSequencias(sequencia, variaveis, pos+1, c1, pares, c2);
           }
       } 
   }
}

int main()
{
    char variaveis[20], sequencia[21]; 
    Tpar pares[50];
    int conta = -1, c1 = 0, c2 = 0;
    char c,d;
    sequencia[0] = 'z'+1;
    while (fscanf(stdin, "%c", &c) != EOF)
    {
	if ((c == ' ') || (c == '\t'))
	    continue;
        if (c != '\n')
	{
	    variaveis[c1++] = c;
	}
	else
        {
	    if (c1 > 0)
            {
		if (conta >= 0)
		    printf("\n");
                variaveis[c1] = '\0';
                mesclagem(variaveis, 0, c1-1);
	        conta = 0;
                while (fscanf(stdin, "%c", &d) != EOF)
                {
	             if ((d == ' ') || (d == '\t'))
	                 continue;
		     if (d == '\n')
		         break;
		     if ((conta % 2) == 0) 
                         pares[c2].primeiro = d;
	             else
                         pares[c2++].segundo = d;
		     conta++;
                }
                imprimeSequencias(sequencia, variaveis, 0, c1, pares, c2);
                /* reseta cache para nova iteracao */
                sequencia[0] = 'z' + 1;
                c1 = 0;
                c2 = 0;
	     }
	     else
             {
		break;
	     }
        }
    }
    return 0;
}
