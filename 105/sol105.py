import sys

dici = {}
for linha in sys.stdin:
	vars = linha[0:-1].split()
	if (int(vars[0]) in dici.keys()):
		dici[int(vars[0])].append(int(vars[1]))	
	else:
		dici.update({int(vars[0]) : [int(vars[1])]})	
	if (int(vars[2]) in dici.keys()):
		dici[int(vars[2])].append(int(vars[1]))	
	else:
		dici.update({int(vars[2]) : [int(vars[1])]})	

lista = []
for i,j in dici.items():
	lista.append((i,j))
lista.sort()

vmax = 0
lmin = [0]


saida = ""
for chave, lvalor in lista:	
	for valor in lvalor:
		if valor in lmin:
			lmin.remove(valor)
		else:
			lmin.append(valor)
	lmin.sort()
	valor = max(lvalor)
	if valor >  vmax:
		saida += str(chave) + " " + str(valor) + " "
		vmax =  valor
	elif valor == vmax:
		saida += str(chave) + " " + str(lmin[-1]) + " "
		vmax = lmin[-1]

sys.stdout.write(saida[0:-1] + "\n")
exit(0)
