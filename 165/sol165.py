import sys, operator, math
from itertools import islice

MAXIMO = 0
CONJUNTO = []

def imprime():
   for i in range(len(CONJUNTO)-1,-1,-1):
      sys.stdout.write("{:>3}".format(str(CONJUNTO[i])))
   sys.stdout.write(" ->")
   sys.stdout.write("{:>3}".format(str(MAXIMO)))
   sys.stdout.write("\n")

def soma(numeros, n, h):
   if n == 0:
      return True
   # se n > 0 e h == 0
   if h == 0:
      return False
   # se n > 0 e h > 0
   for i in numeros:
      if i<=n and i > 0 and h > 0:
         if soma(numeros, n-i, h-1):
            return True
   return False

# busca em largura
def geraDominio(h, k, elementos):
   global MAXIMO, CONJUNTO
   if k > 0:
      #print ("NIVEL",k, elementos)
      lista = []
      for e in elementos:
         #print ("NO",e)
         numeros = e[0]
         inij = e[1]
         inii = max(numeros)+1 if numeros else 1 
         maxi = 0
         conj = []
         # o intervalo tem como minimo o maior numero no dominio
         # e como maximo a maxima soma continua com repeticao dos numeros
         for i in range(inii,inij+1):
            # ordenar pelo maior numero primeiro
            novo = [i] + numeros
            # podemos comecar com o maximo valor obtido no nivel anterior 
            for j in range(inij,100):
               if soma(novo, j, h):
                  if j > maxi:
                     maxi = j
                     conj = novo
               else:
                  break
            if j > inij:
               lista += [[novo, j]]
               #print ("INSERIU", novo, j)
            #else:
               #print ("DESCARTOU", novo, j)
         if maxi > MAXIMO:
            MAXIMO = maxi
            CONJUNTO = conj
      geraDominio(h, k-1, lista)

linhas = sys.stdin.readlines()
for linha in linhas:
   linha = linha.replace("\n","")
   if len(linha) <= 1:
      continue
   linha = linha.split()
   if linha[0] == "0" and linha[1] == "0":
      break
   h = int(linha[0])
   k = int(linha[1])
   geraDominio(h, k, [[[], 1]])
   imprime()
   MAXIMO = 0
   CONJUNTO = []

exit(0)
