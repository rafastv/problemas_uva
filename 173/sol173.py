import sys

def imprimeSaida(lisper, paskill, lisp_ultimo, pask_ultimo, lisp_destruido, pask_destruido):
   if paskill == "":
      sys.stdout.write("Paskill trapped in node " + chr(pask_ultimo + ord("A")))
   if lisper == "" and not lisp_destruido:
      if paskill == "":
         sys.stdout.write(" ")
      sys.stdout.write("Lisper trapped in node " + chr(lisp_ultimo + ord("A")))
   if (lisp_destruido and pask_destruido) or (paskill == "" and pask_ultimo == lisp_ultimo):
      if paskill == "":
         sys.stdout.write(" ")
      sys.stdout.write("Both annihilated in node " + chr(pask_ultimo + ord("A")))
   else:
      if lisp_destruido:
         if paskill == "":
            sys.stdout.write(" ")
         sys.stdout.write("Lisper destroyed in node " + chr(lisp_ultimo + ord("A")))
   sys.stdout.write("\n")

def proxLisper(atual, vertices, prisao):
   for i in range(len(vertices)-1,-1,-1):
      if vertices[i] < atual and vertices[i] not in prisao:
         return i   
   for i in range(len(vertices)-1,-1,-1):
      if vertices[i] not in prisao:
         return i   
   return -1  

def proxPaskill(atual, vertices, bombas, prisao):
   for i in range(len(vertices)):
      if (vertices[i] > atual) and (vertices[i] not in bombas) and (vertices[i] not in prisao):
         return i   
   for i in range(len(vertices)):
      if (vertices[i] not in bombas) and (vertices[i] not in prisao):
         return i   
   return -1  

def insere(lv, indice, valor):
   unico = list(set(valor))
   lv[ord(indice)-ord("A")] +=  [ord(i)-ord("A") for i in unico if ord(i)-ord("A") not in lv[ord(indice)-ord("A")] and i != indice]

linhas = sys.stdin.readlines()
for linha in linhas:
   if "#" in linha:
      break
   linha = linha.replace("\n","")
   linha = linha.replace("\t","")
   linha = linha.replace(" ","")
   linha = linha.split(";")

   vertices = [[], [], [], [], [], [], [], [], [], [], [], [], [],\
               [], [], [], [], [], [], [], [], [], [], [], [], []]
   for texto in linha:
      if not texto:
         continue
      if "." in texto:
         parte = texto.split(".")
         texto = parte[0]
         parte = parte[1]
      texto = texto.split(":")
      if len(texto) < 2:
          continue
      insere(vertices, texto[0], texto[1])
      for vizinho in texto[1]:
         insere(vertices, vizinho, texto[0])
   paskill = ord(parte[0]) - ord("A")
   lisper = ord(parte[1]) - ord("A")
   #print("INICIO", paskill, lisper)
   for i in range(26):
      vertices[i].sort()

   bombas_pask = []
   prisao_lisp = []
   lisp_destruido = False
   pask_destruido = False
   lisp_ultimo = ""
   pask_ultimo = ""
   while paskill != "" and lisper != "":
      if paskill == lisper:
         lisp_destruido = True
         pask_destruido = True
         lisp_ultimo = lisper
         pask_ultimo = paskill
         break
      if vertices[lisper]:
         idx = proxLisper(lisper, vertices[lisper], prisao_lisp)
         if idx >= 0: 
            prisao_lisp += [lisper]
            if (vertices[lisper][idx] in bombas_pask) or (vertices[lisper][idx] == paskill):
               lisp_ultimo = vertices[lisper][idx]
               lisper = ""
               lisp_destruido = True
            else:
               lisp_ultimo = lisper 
               lisper = vertices[lisper][idx]
         else:
            lisp_ultimo = lisper
            lisper = "" 
      else:
         lisp_ultimo = lisper
         lisper = "" 
      if vertices[paskill]:
         idx = proxPaskill(paskill, vertices[paskill], bombas_pask, prisao_lisp)
         if idx >= 0: 
            bombas_pask += [paskill]
            paskill = vertices[paskill][idx]
         else:
            pask_ultimo = paskill
            paskill = ""
      else:
         pask_ultimo = paskill
         paskill = ""
   #print("PASKILL", paskill, "ULTIMO", pask_ultimo, "DESTRUIDO",pask_destruido)    
   #print("LISPER", lisper, "ULTIMO", lisp_ultimo, "DESTRUIDO",lisp_destruido)    
   #print(vertices)
   imprimeSaida(lisper, paskill, lisp_ultimo, pask_ultimo, lisp_destruido, pask_destruido)
         
exit(0)
