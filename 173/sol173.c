#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>

typedef int bool;
#define VERDADEIRO 1
#define FALSO 0
#define mod(a,b) (b + (a % b)) % b


unsigned char BOMBAS[26];
char VERTICES[26][26];

bool lisp_destruido, pask_destruido;
bool lisp_preso, pask_preso;
unsigned char lisp_ultimo, pask_ultimo;

void imprimeFinalGuerra()
{
   if (!pask_destruido)
   {
      if (pask_preso)
         printf("Paskill trapped in node %c", pask_ultimo);
      if (!lisp_destruido)
      {
         if (lisp_preso)
         {
            if (pask_preso)
               printf(" ");
            printf("Lisper trapped in node %c\n", lisp_ultimo);
         } 
         else
            printf("\n");
      } 
      else
      {
         if (pask_preso)
            printf(" ");
         if (lisp_ultimo == pask_ultimo)
            printf("Both annihilated in node %c\n", lisp_ultimo);
         else
            printf("Lisper destroyed in node %c\n", lisp_ultimo);
      }
   }
   else
   {
      printf("Both annihilated in node %c\n", pask_ultimo);
   }
}

void iniciaGuerra(unsigned char paskill, unsigned char lisper)
{
   int i, j;
   bool encontrei;
   lisp_destruido = FALSO;
   pask_destruido = FALSO;
   lisp_preso = FALSO;
   pask_preso = FALSO;
   while (paskill && lisper)
   {
      if (paskill == lisper)
      {
          lisp_destruido = VERDADEIRO;
          pask_destruido = VERDADEIRO;
          lisp_ultimo = lisper;
          pask_ultimo = lisper;
          paskill = 0;
          lisper = 0;
          break;
      }
      encontrei = FALSO;
      /* proximo passo de lisper */
      lisp_ultimo = lisper;
      for (i=lisper-1;i>=lisper-25;i--) 
      {
         /* se existe um caminho */
         if (VERTICES[lisper-'A'][mod(i-'A',26)])
         {
            /* ninguem mais pode acessar aquele vertice */
            for (j=0;j<26;j++)
            {
               VERTICES[lisper-'A'][j] = 0;
               VERTICES[j][lisper-'A'] = 0;
            }
            lisper = mod(i-'A',26) + 'A';
            encontrei = VERDADEIRO; 
            break;
         }
      }
      if (!encontrei)
      { 
         /* lisper nao consegue se mover */
         lisp_preso = VERDADEIRO;
         lisper = 0;
      }
      else
      {
         /* 
            se lisper esta se movendo para aonde paskill esta saindo, ou 
            aonde paskill ja esteve 
         */
         if  ((BOMBAS[lisper-'A']) || (lisper == paskill)) 
         {
             lisp_destruido = VERDADEIRO;
             lisp_ultimo = lisper;
             lisper = 0;
         }  
      }
      encontrei = FALSO;
      /* proximo passo de paskill */
      pask_ultimo = paskill;
      for (i=paskill+1;i<paskill+26;i++) 
         /* se existe um caminho para vertice sem bomba */
         if ((VERTICES[paskill-'A'][mod(i-'A',26)]) && (!BOMBAS[mod(i-'A',26)]))
         {
            /* paskill arma uma bomba: the bomb has been planted! */
            BOMBAS[paskill-'A'] = 1;
            paskill = mod(i-'A',26) + 'A';
            encontrei = VERDADEIRO; 
            break;
         }
      if (!encontrei)
      { 
         /* paskill nao consegue se mover */
         pask_preso = VERDADEIRO;
         paskill = 0;
         break;
      }
   }
   if (paskill)
      pask_ultimo = paskill;
   if (lisper)
      lisp_ultimo = lisper;
}
               
void imprimeVertices(unsigned char paskill, unsigned char lisper)
{
   int i,j;
   bool imprime;
   for (i=0;i<26;i++)
   {
      imprime = FALSO;
      for (j=0;j<26;j++)
      {
         if (VERTICES[i][j])
         {
            printf("%c", j+'A');
            imprime = VERDADEIRO;
         } 
      }
      if (imprime)
         printf(":%c;", i+'A');
   } 
   printf(" paskill %c  lisper %c\n", paskill, lisper);
}

int main()
{
    char c, buffer[1000];
    unsigned char idx_v, paskill = 0, lisper = 0;
    int i, idx = 0;
    bool final = FALSO;
    for (i=0; i<26; i++)
       memset(VERTICES[i], 0, 26);
    memset(BOMBAS, 0, 26);
    while ((c  = fgetc(stdin)) != EOF)
    {
	if ((c!=' ') && (c!='\n') && (c!='\t') &&\
	    (c!='#') && (c!=';') && (c!='.') && (c!= ':'))
	{
	    buffer[idx] = c;
	    idx++;
	}
	else
	{
	    if (c == '#')
		break;
	    if (idx == 0)
		continue;
            buffer[idx] = '\0';
            if (c == ':')
            {
               idx_v = buffer[0] - 'A';
	       /** limpa buffer **/
               memset(buffer,0,idx);
	       idx = 0;
            }
            if ((c == ';') || (c == '.'))
            {
               for (i=0;i<idx;i++)
               {
                  if ((buffer[i]-'A') != idx_v)
                  {
                     VERTICES[idx_v][buffer[i]-'A'] = 1; 
                     VERTICES[buffer[i]-'A'][idx_v] = 1; 
                  }
               }
               if (c == '.')
                  final = VERDADEIRO;
	       /** limpa buffer **/
               memset(buffer,0,idx);
	       idx = 0;
            }
            if (final)
            {
               if (paskill == 0)
                  paskill = buffer[0];
               else
                  lisper = buffer[0];
	       /** limpa buffer **/
               memset(buffer,0,idx);
	       idx = 0;
            }
            if (paskill && lisper)
            {
               /** executa algoritmo **/
               /*imprimeVertices(paskill, lisper);*/
               iniciaGuerra(paskill, lisper);
               imprimeFinalGuerra();
	       /** limpa buffer **/
               memset(buffer,0,idx);
	       idx = 0;
               final = FALSO;
               lisper = 0;
               paskill = 0;
               for (i=0; i<26; i++)
                  memset(VERTICES[i], 0, 26);
               memset(BOMBAS, 0, 26);
            }
	}
    }
    return 0;
}

