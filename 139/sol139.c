#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>

typedef int bool;
#define true 1
#define false 0

typedef struct telefone
{
    char ddd[7];
    char local[26];
    float valor;
} Ttarifa;

Ttarifa TARIFAS[10000];
int TAM;

void imprimeTabelaTarifas()
{
    int i;
    printf("TABELA DE TARIFAS\n");
    for (i=0;i<TAM;i++)
        printf("%s %s %.2f\n", TARIFAS[i].ddd, TARIFAS[i].local, TARIFAS[i].valor);
}

void imprimeFone(int id_tarifa, char numero[16], float valor)
{
    if (id_tarifa < 0)
    {
        printf("%s %s          %5.0f       %6.2f\n", numero, TARIFAS[0].local, valor, TARIFAS[0].valor);
    }
    else
    {  
        char tmp[16], fone_pessoal[16]="", vazios[16]="               ";
        int i, k, tam = strlen(TARIFAS[id_tarifa].ddd);
        if (id_tarifa <= 1)
            tam--;
        k = 0;
        for (i=tam;i<strlen(numero);i++)
        {
            if ((numero[i] == ' ') || (k >= 15))
                break;
            tmp[i-tam] = numero[i];
            k++;
        }
        tmp[k] = '\0';
        if (k>10) 
            strncpy(fone_pessoal, vazios, k-strlen(tmp));
        else
            strncpy(fone_pessoal, vazios, 10-strlen(tmp));
        strcat(fone_pessoal, tmp);
        printf("%s %s%s%5.0f %5.2f %6.2f\n", numero, TARIFAS[id_tarifa].local, fone_pessoal, valor, TARIFAS[id_tarifa].valor, TARIFAS[id_tarifa].valor*valor);
    }
}

int encontraTabelaTarifas(char numero[16])
{
    /* tarifa local */
    if (numero[0] != '0')
        return 1;
    int i, mt = 0, idx = -1; 
    int mm, mm_t, qt_max, qt_zeros;
    mm = strlen(numero);
    for (i=0;i<strlen(numero);i++)
    {
        if (numero[i] == ' ')
        {
            mm = i;
            break;
        }
    }
    if ((numero[0] == '0') && (numero[1] == '0'))
        qt_max = 10;
    else if (numero[0] == '0') 
        qt_max = 7;
    else
        qt_max = 15;
    for (i=2;i<TAM;i++)
    { 
        mm_t = mm - strlen(TARIFAS[i].ddd);
        if ((strncmp(numero, TARIFAS[i].ddd, strlen(TARIFAS[i].ddd)) == 0) &&
            (strlen(TARIFAS[i].ddd) > mt) && (mm_t >= 4) && (mm_t <= qt_max)) 
        {
            idx = i; 
            mt = strlen(TARIFAS[i].ddd);
        }
    }   
    return idx;
}

int main()
{
    char c, numero[16]; 
    char buffer[52] = "Unknown                  "; 
    char tlocal[52] = "Local                    ";
    char vazios[52] = "                         ";
    bool primeiroEspaco = false, tabelaExiste = false, telefoneLido = false;
    int pos = 0, idx = 0, id_tarifa;
    float valor;

    strcpy(TARIFAS[0].ddd, "-1");
    strcpy(TARIFAS[0].local, buffer);
    TARIFAS[0].valor = -1.0;
    strcpy(TARIFAS[1].ddd, "0");
    strcpy(TARIFAS[1].local, tlocal);
    TARIFAS[1].valor = 0.0;
    TAM = 2;
    memset(buffer,0,26);
    while ((c  = fgetc(stdin)) != EOF)
    {
	if ((c!=' ') && (c!= '$') && (c!='\n') && (c!='\t') && (c!= '#'))
	{
	    buffer[idx] = c;
	    idx++;
	}
	else
	{
	    if (c == '#')
		break;
	    if (idx == 0)
		continue;
	    buffer[idx] = '\0';
            if (idx > 26)
                return 1;
	    if ((!primeiroEspaco) || (c == '$') || (c == '\n'))
	    {
		/** aplica algoritmo **/
                if (!tabelaExiste)
                {
                    /* se o nome do local estiver vazio */
                    if ((pos == 1) && (c == '\n'))
                    {
                        strncpy(TARIFAS[TAM].local,vazios,25); 
                        pos++; 
                    }
                    switch (pos)
                    {
                        case 0: { strcpy(TARIFAS[TAM].ddd, buffer);                                 pos++; break; }
                        case 1: { strncat(buffer,vazios,25-idx); strcpy(TARIFAS[TAM].local,buffer); pos++; break; }
                        case 2: { TARIFAS[TAM].valor = atof(buffer)/100.0;                   pos=0; TAM++; break; }
                    }
                    if (strcmp(buffer, "000000") == 0)
                    {
                        tabelaExiste = true;
                        /*imprimeTabelaTarifas();*/
                    }
                }
                else
                {
                    if (!telefoneLido)
                    {

                        strncat(buffer, vazios, 15-idx);
                        strcpy(numero, buffer);
                        telefoneLido = true;
                    }
                    else
                    {
                        valor = atof(buffer);
                        telefoneLido = false;
                        id_tarifa = encontraTabelaTarifas(numero);
                        imprimeFone(id_tarifa, numero, valor);
                    }
                }
	        /** limpa buffer **/
	        idx = 0;
                memset(buffer,0,26);
	    }
            if ((primeiroEspaco) && (c == ' '))
            {
	        buffer[idx] = c;
	        idx++;
            }
            if (c == ' ')
                primeiroEspaco = true; 
            if (c == '\n')
                primeiroEspaco = false; 
	}
    }
    return 0;
}

