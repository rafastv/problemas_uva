#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define TAMANHO_VETOR 40
#define BASE_NUMERICA 16
#define MAIOR_NUMERO  4096 /* potencia da base */
int main()
{
	int numeros[TAMANHO_VETOR], balde[BASE_NUMERICA][TAMANHO_VETOR+1], i, j, indice, fator;
	srand(time(0));
	/* preenche vetor com numeros aleatórios */
	for (i=0;i<TAMANHO_VETOR;i++)
	{
		numeros[i] = rand() % MAIOR_NUMERO;
		balde[i%BASE_NUMERICA][0] = 0;
		printf("%i ", numeros[i]);
	}
	printf("\n");
	for (fator=1;fator<MAIOR_NUMERO;fator*=BASE_NUMERICA)
	{
		/* ordena */
		for (i=0;i<TAMANHO_VETOR;i++)
		{
			indice = (numeros[i]/fator)%BASE_NUMERICA;
			balde[indice][++balde[indice][0]] = numeros[i];
		}
		/* copia de volta */
		indice = 0;
		for (i=0;i<BASE_NUMERICA;i++)
		{
			for (j=0;j<balde[i][0];j++)
			{
				numeros[indice++] = balde[i][j+1];
			}
			balde[i][0] = 0;
		}
	}
	for (i=0;i<TAMANHO_VETOR;i++)
	{
		printf("%i ", numeros[i]);
	}
	printf("\n");
	return (0);
}
