#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <netdb.h>
#include <sys/socket.h>
#include <arpa/inet.h>

/* nao inclua http:// ou https:// */
#define PAGINA_WEB "www.rafaelstvieira.com"
#define PORTA 80

struct entrada_in {
	short int familia;
        unsigned short int porta;
        struct in_addr nome;
        unsigned char zeros[8];
};

int main(){
  int entrada, nerro;
  int bytes_enviados, bytes_recebidos, total, limite;
  int ini, fim;
  struct entrada_in endereco_ip;
  struct hostent *endereco;
  /* dados enviados(max): 1KB por vez, pode ser mais */
  /* dados recebidos(max): 1MB por vez, pode ser mais */
  char msg[1024], pagina[1048576], *pos;


  /* resolve o endereco em ip atraves de DNS */
  if ((endereco = gethostbyname(PAGINA_WEB))== NULL)
  {
     herror("Problema no dns");
     exit(1);
  }

  /* cria socket */
  entrada = socket(AF_INET, SOCK_STREAM, 0);
  if (entrada < 0)
  {
     perror("Erro criando socket: ");
     exit(1);
  }

  /* dados da conexao */
  endereco_ip.familia = AF_INET;
  endereco_ip.porta = htons(PORTA);
  endereco_ip.nome = *((struct in_addr*) endereco->h_addr); //inet_addr(IP_DESTINO);
  memset(&(endereco_ip.zeros), '\0', 8);

  /* cria conexao TCP */
  nerro = connect(entrada, (struct sockaddr *)&endereco_ip, sizeof(struct sockaddr)); 
  if (nerro < 0)
  {
     perror("Erro ao conectar: ");
     exit(1);
  }

  /* comandos do protocolo HTTP */
  strcpy(msg, "GET / HTTP/1.1\n");
  strcat(msg, "Host: ");
  strcat(msg, PAGINA_WEB);
  strcat(msg, "\n\n");
  
  /* envia dados via conexao */
  bytes_enviados = send(entrada, msg, strlen(msg), 0);
  /* limpa msg para ser usada novamente */
  memset(msg, '\0', strlen(msg));

  /* recebe dados via conexao */  
  while ((bytes_recebidos = recv(entrada, pagina, 1048576, 0))>0)
  {
          total += bytes_recebidos;
          /* parametro do protocolo HTTP que diz tamanho da pagina WEB */
          if ((pos = strstr(pagina, "Content-Length:")) > 0)
          {
            /* inicio do campo Content-Length */
            ini = (int)pos - (int)pagina + strlen("Content-Length:");  
            pos = strstr(pos, "\n");
            /* final do campo Content-Length */
            fim = (int)pos - (int)pagina - 1;
            strncpy(msg, pagina+ini, fim-ini);
            pos = strstr(pos, "<");
            /* inicio da pagina */
            total -= ((int)pos-(int)pagina);
            limite = atoi(msg);
          } 
          /* imprime dados recebidos */
	  printf("%s\n", pagina);

          /* limpa pagina para ser usada novamente */
          memset(pagina, '\0', bytes_recebidos);

          /* se leu todos os dados termina */
          if (total == limite) 
             break;
  }

  /* fecha conexao */
  close(entrada);
  
  return 0;
}
