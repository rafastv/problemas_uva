import pygame
import sys

# Bresenham para Circulos correto
# nao eh bom comecar de (r,0) pois o alg. volta para (r-1,y), gerando ruido
def desenhaCirculo2(superficie, x0, y0, raio):
    x = raio-1 # raio
    y = 0
    dy = 1
    dx = 1
    erro = dx-(raio<<1) # 0
    while (x>=y):
        superficie.set_at((x0+x,y0+y), (255,0,0))
        superficie.set_at((x0-x,y0+y), (255,0,0))
        superficie.set_at((x0+x,y0-y), (255,0,0))
        superficie.set_at((x0-x,y0-y), (255,0,0))
        superficie.set_at((x0+y,y0+x), (255,0,0))
        superficie.set_at((x0-y,y0+x), (255,0,0))
        superficie.set_at((x0+y,y0-x), (255,0,0))
        superficie.set_at((x0-y,y0-x), (255,0,0))
        erro_REAL = (x**2+y**2)-(raio**2)
        erro_ACUMULADO = erro
        print(erro_REAL,erro_ACUMULADO)
        if (erro <= 0):
            y += 1
            erro += dy
            dy += 2
        if (erro > 0):
            x  -= 1
            dx += 2
            erro += (-raio << 1) + dx 
            #dx += 2 (caso 0)

# Bresenham para Circulos segundo WIKIPEDIA em 4 de julho de 2017 e outros
def desenhaCirculo(superficie, x0, y0, raio):
    x = raio;
    y = 0;
    erro = 0;

    while (x >= y):
        superficie.set_at((x0+x,y0+y), (0,0,0))
        superficie.set_at((x0-x,y0+y), (0,0,0))
        superficie.set_at((x0+x,y0-y), (0,0,0))
        superficie.set_at((x0-x,y0-y), (0,0,0))
        superficie.set_at((x0+y,y0+x), (0,0,0))
        superficie.set_at((x0-y,y0+x), (0,0,0))
        superficie.set_at((x0+y,y0-x), (0,0,0))
        superficie.set_at((x0-y,y0-x), (0,0,0))
        #erro_REAL = (x**2+y**2)-(raio**2)
        #erro_ACUMULADO = erro
        #print("WIKIPEDIA",erro_REAL,erro_ACUMULADO)
        y += 1
        if (erro <= 0):
            erro += 2*y + 1;
        else:
            x -= 1;
            erro += 2 * (y - x) + 1;

def captura_eventos(m):
    for evento in pygame.event.get():
        if evento.type == pygame.QUIT:
            pygame.quit()
            sys.exit()

pygame.init()
superficie = pygame.display.set_mode((800, 600))
pygame.display.set_caption("Desenha")
superficie.fill((255,255,255))

while(True):
    captura_eventos(superficie)
    desenhaCirculo(superficie, 200, 300, 175)
    desenhaCirculo2(superficie, 600, 300, 175)
    desenhaCirculo(superficie, 200, 300, 150)
    desenhaCirculo2(superficie, 600, 300, 150)
    desenhaCirculo(superficie, 200, 300, 125)
    desenhaCirculo2(superficie, 600, 300, 125)
    pygame.display.flip()
