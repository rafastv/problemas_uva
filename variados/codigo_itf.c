/*
 *	Copyright (C) 2018 Rafael Siqueira Telles Vieira
 *
 *	This program is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful, 
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 *	See the GNU General Public License for more details.
 *
 *	License: http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
 *	Licença em português: http://rafaelstvieira.com/licencas/licenca_GPL_br.txt
 *
 *	You should have received a copy of the GNU General Public License 
 *	along with this program; if not, write to the Free Software Foundation, 
 *	Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA 
 *
 */
#include <stdio.h>
#include <stdlib.h>
/* 1 = largo(wide) e 0 = curto(narrow) */
int codigos[10][5] = {{0, 0, 1, 1, 0}, 
                      {1, 0, 0, 0, 1}, 
                      {0, 1, 0, 0, 1}, 
                      {1, 1, 0, 0, 0}, 
                      {0, 0, 1, 0, 1}, 
                      {1, 0, 1, 0, 0}, 
                      {0, 1, 1, 0, 0}, 
                      {0, 0, 0, 1, 1}, 
                      {1, 0, 0, 1, 0}, 
                      {0, 1, 0, 1, 0}};
void imprimeLargo(FILE *arquivo, int cor)
{
	if (cor)
		fprintf(arquivo, "255 255 255 ");
	else
		fprintf(arquivo, "0 0 0 ");
}
void imprimeCurto(FILE *arquivo, int cor)
{
	if (cor)
		fprintf(arquivo, "255 ");
	else
		fprintf(arquivo, "0 ");
}
void imprimeCodigo(FILE *arquivo, int p, int q)
{
	for (int i=0; i<5; i++)
	{
		if (codigos[p][i])
			imprimeLargo(arquivo, 0);
		else
			imprimeCurto(arquivo, 0);
		if (codigos[q][i])
			imprimeLargo(arquivo, 1);
		else
			imprimeCurto(arquivo, 1);
	}
}
int main()
{
	int i, j, largura=450, altura=50;
	/* numeros impresso boleto banco (44 numeros): 1-4; 33-47 (15 ultimos numeros); 5-9; 11-20; 22-31; 
	   (10, 21, e 32 são DVs que não devem ser inclusos no i25) 
	   numeros impresso boletos como luz (44 numeros): 1-11; 13-23; 25-35; 37-47; 
	   (12, 24, 36 e 48 são DVs que não devem ser inclusos no i25)  */
        /*exemplo 1*/
        /*int numero[44] = {0,3,3,9,
                            7,7,5,5,1,0,0,0,0,0,9,1,1,3,6,
                            9,8,7,1,2,
                            3,9,5,0,0,0,0,0,1,1,
                            4,2,0,1,1,1,0,1,0,1}; */
	/*exemplo 2*/
	int numero[44] = {8,3,8,8,0,0,0,0,0,0,1,
	                  3,2,8,2,0,0,3,1,0,0,0,
	                  0,0,0,7,3,9,5,9,2,7,0,
	                  0,0,2,6,3,9,2,6,2,9,7};
	FILE *arquivo = fopen("codigo.pgm", "wb"); 
	fprintf(arquivo, "P2\n%d %d\n255\n", largura, altura);
	for (j = 0; j < altura; ++j)
	{
		/* 4 barras de inicio */
		imprimeCurto(arquivo, 0);
		imprimeCurto(arquivo, 1);
		imprimeCurto(arquivo, 0);
		imprimeCurto(arquivo, 1);
		/* conteudo */
  		for (i = 0; i < 44; i+=2)
			imprimeCodigo(arquivo, numero[i], numero[i+1]);
		/* 3 barras de fim */
		imprimeLargo(arquivo, 0);
		imprimeCurto(arquivo, 1);
		imprimeCurto(arquivo, 0);
		for (i = 0; i < largura-(45*9); ++i)
			imprimeCurto(arquivo, 1);
	}
	fclose(arquivo);
	return (0);
}
