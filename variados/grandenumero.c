#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <math.h>

/* N_DIGITOS deve ser multiplo de 2 para evitar padding com zeros no struct */
#define N_DIGITOS 256000
#define POSITIVO 1
#define NEGATIVO -1 

typedef int bool;
#define VERDADEIRO 1
#define FALSO 0

typedef struct grandeNumero {
   char digitos[N_DIGITOS];
   int sinal;
   int tam;
} TgrandeNumero;

void divideGrandeNumero(TgrandeNumero *a, TgrandeNumero *b, TgrandeNumero *c);
void multiplicaGrandeNumero(TgrandeNumero *a, TgrandeNumero *b, TgrandeNumero *c);
void subtraiGrandeNumero(TgrandeNumero *a, TgrandeNumero *b, TgrandeNumero *c);
void somaGrandeNumero(TgrandeNumero *a, TgrandeNumero *b, TgrandeNumero *c);
bool maiorGrandeNumero(TgrandeNumero *a, TgrandeNumero *b);
bool igualGrandeNumero(TgrandeNumero *a, TgrandeNumero *b);
void imprimeGrandeNumero(TgrandeNumero *m);
void intParaGrandeNumero(TgrandeNumero *m, int n);
long long grandeNumeroParaLongLong(TgrandeNumero *m);
void removerZeros(TgrandeNumero *m);
void shiftGrandeNumero(TgrandeNumero *m, int n);
void copiaGrandeNumero(TgrandeNumero *a, TgrandeNumero *b);
void inverteGrandeNumero(TgrandeNumero *m);
void iniciaGrandeNumero(TgrandeNumero *m);

void iniciaGrandeNumero(TgrandeNumero *m)
{
   memset(m->digitos, 0, N_DIGITOS);
   m->sinal = POSITIVO;
   m->tam = N_DIGITOS-1;
}

void imprimeGrandeNumero(TgrandeNumero *m)
{
   int i;
   if (m->sinal < 0)
      printf("-");
   for (i=m->tam; i<N_DIGITOS; i++)
      printf("%u", (unsigned int) m->digitos[i]);
   printf("\n");
}

void copiaGrandeNumero(TgrandeNumero *a, TgrandeNumero *b)
{
   int i;
   b->sinal = a->sinal;
   b->tam = a->tam;
   for (i=a->tam; i<N_DIGITOS; i++)
      b->digitos[i] = a->digitos[i];
}

void inverteGrandeNumero(TgrandeNumero *m)
{
   int tmp, ini, fim;
   ini = m->tam;
   fim = N_DIGITOS-1;
   while (ini < fim)
   {
      tmp = m->digitos[ini];
      m->digitos[ini] = m->digitos[fim];
      m->digitos[fim] = tmp;
      ini++;
      fim--;
   }
}

void removerZeros(TgrandeNumero *m)
{
   if (m->tam < N_DIGITOS-1)
   {
      while (m->digitos[m->tam] == 0)
      {
         m->tam++;
         if (m->tam == N_DIGITOS-1)
            break;
      }
   }
   /* remove -0 */
   if (m->digitos[m->tam] == 0)
       m->sinal = POSITIVO;
}

void shiftGrandeNumero(TgrandeNumero *m, int n)
{
   int i;
   if ((n > 0) && (m->tam >= n))
   {
      for (i=(m->tam-n);i<N_DIGITOS-n;i++)
         m->digitos[i] = m->digitos[i+n];
      for (i=N_DIGITOS-n;i<N_DIGITOS;i++)
         m->digitos[i] = 0;
      m->tam-=n;
   }
   if (m->tam < n)
      printf("(erro de overflow) numero muito grande!\n");
}

void intParaGrandeNumero(TgrandeNumero *m, int n)
{
   int digito;
   m->tam = N_DIGITOS;
   m->sinal = POSITIVO;
   memset(m->digitos, 0, N_DIGITOS);
   if (n < 0)
   {
      m->sinal = NEGATIVO;
      n *= -1;
   }
   if (n == 0)
      m->tam--;
   while (n > 0)
   {
      digito = n % 10;
      m->digitos[m->tam-1] = digito;
      n /= 10;
      m->tam--;
   }
}

long long grandeNumeroParaLongLong(TgrandeNumero *m)
{
   long long saida=0;
   int i;
   if (N_DIGITOS-m->tam > 18)
      printf("Numero muito grande, nao pode converter!\n");
   else
      for (i=m->tam;i<N_DIGITOS;i++)
         saida += m->digitos[i]*pow(10, N_DIGITOS-i);
   return saida;
}

/* em valor absoluto */
bool maiorGrandeNumero(TgrandeNumero *a, TgrandeNumero *b)
{
   int i;
   if (a->tam == b->tam)
   {
      for (i=a->tam;i<N_DIGITOS;i++)
         if (a->digitos[i] != b->digitos[i])
            return (a->digitos[i] > b->digitos[i]);
   }
   return (a->tam < b->tam);
}

/* em valor absoluto */
bool igualGrandeNumero(TgrandeNumero *a, TgrandeNumero *b)
{
   int i;
   if (a->tam == b->tam)
      for (i=a->tam;i<N_DIGITOS;i++)
         if (a->digitos[i] != b->digitos[i])
            return FALSO;
   return (a->tam == b->tam);
}

void subtraiGrandeNumero(TgrandeNumero *a, TgrandeNumero *b, TgrandeNumero *c)
{
   b->sinal *= -1;
   /* sinais diferentes = subtracao */
   /* sinais iguais = soma */
   if (a->sinal != b->sinal)
   {
      TgrandeNumero *u, *v;
      u = b;
      v = a;
      if (maiorGrandeNumero(a, b))
      {
         u = a;
         v = b;
      }
      int i, res, empresta = 0;
      c->sinal = u->sinal;
      int tam=(a->tam<b->tam)?a->tam:b->tam;
      c->tam = tam;
      for (i=N_DIGITOS-1;i>=tam;i--)
      {
          res = (u->digitos[i] - (v->digitos[i] + empresta));     
          empresta = 0;
          if (res < 0)
          {
             res += 10;
             empresta = 1;
          }
          c->digitos[i] = res;
      }
      removerZeros(c);
   }
   else
   {
      somaGrandeNumero(a, b, c);
   }
}

void somaGrandeNumero(TgrandeNumero *a, TgrandeNumero *b, TgrandeNumero *c)
{
   int tam, i, extra = 0;
   /* sinais iguais = soma */
   /* sinais diferentes = subtracao */
   if (a->sinal == b->sinal)
   {
      c->sinal = a->sinal;
      tam=(a->tam<b->tam)?a->tam:b->tam;
      c->tam = tam;
      int x, y;
      for (i=N_DIGITOS-1;i>=tam;i--)
      {
          /* para poder fazer ops do tipo a+=b */
          x = a->digitos[i];
          y = b->digitos[i]; 
          c->digitos[i] = (extra + x + y) % 10;     
          extra = (extra + x + y) / 10;     
      }
      if (extra > 0)
      {
         if (tam > 0)
         {
            c->digitos[tam-1] = extra;
            c->tam--;
         }
         else
            printf("(erro de overflow) numero muito grande!\n");
      }
   }
   else
   {
       if (b->sinal == NEGATIVO)
       {
          b->sinal *= -1;
          subtraiGrandeNumero(a, b, c);
       }
       else
       {
          a->sinal *= -1;
          subtraiGrandeNumero(b, a, c);
       }
   }
}

void multiplicaGrandeNumero(TgrandeNumero *a, TgrandeNumero *b, TgrandeNumero *c)
{
   int i, j, extra = 0;
   TgrandeNumero tmp;
   c->sinal = a->sinal * b->sinal;
   iniciaGrandeNumero(&tmp);
   /* nao importa quem eh o maior dos grandes numeros (ops iguais)  */
   int x, y;
   for (j=N_DIGITOS-1;j>=b->tam;j--)
   {
       tmp.tam = a->tam;
       extra = 0;
       for (i=N_DIGITOS-1;i>=a->tam;i--)
       {
          /* permite ops do tipo a*=b */
          x = a->digitos[i];
          y = b->digitos[j];
          tmp.digitos[i] = (extra + (x * y)) % 10;     
          extra = (extra + (x * y)) / 10;     
       }
       if (extra > 0)
       {
          if (tmp.tam > 0)
          {
             tmp.digitos[tmp.tam-1] = extra;
             tmp.tam--;
          }
          else
             printf("(erro de overflow) numero muito grande!\n");
       }
       shiftGrandeNumero(&tmp, N_DIGITOS-1-j);
       somaGrandeNumero(c, &tmp, c);
   }
   /* caso multi seja por zero */
   removerZeros(c);
}

/* divisao inteira no momento*/
void divideGrandeNumero(TgrandeNumero *a, TgrandeNumero *b, TgrandeNumero *c)
{
   int i, j, extra = 0;
   c->sinal = a->sinal * b->sinal;
   if ((b->digitos[N_DIGITOS-1] == 0) && (b->tam == N_DIGITOS-1))
   {
      printf("(erro) divisao por zero detectada!\n");
      c->digitos[N_DIGITOS-1] = 0;
      c->sinal = POSITIVO;
      c->tam = N_DIGITOS-1;
   }
   else
   {
      /* soh pode dividir se a > b  */
      if (maiorGrandeNumero(a, b))
      {
         TgrandeNumero tmp, aux, zero;
         int casas_shift = (N_DIGITOS - a->tam) - (N_DIGITOS - b->tam)+1; 
         int quociente;
         c->sinal = a->sinal * b->sinal;
         iniciaGrandeNumero(&zero);
         for (j=a->tam;j<=b->tam;j++)
         {
             iniciaGrandeNumero(&tmp);
             iniciaGrandeNumero(&aux);
             quociente = 1;
             tmp.digitos[N_DIGITOS-casas_shift] = quociente;
             tmp.tam = N_DIGITOS-casas_shift;
             multiplicaGrandeNumero(&tmp, b, &aux);
             /* pula casa e adicona zero ao quociente */
             if (maiorGrandeNumero(&aux, a))
             {
                if (igualGrandeNumero(&zero, a))
                {
                   /* copiar trailing zeros */
                   c->tam-=(casas_shift-1);
                   break;
                }
                c->digitos[c->tam] = 0;
                c->tam--;
                casas_shift--;
                continue;
             }
             while (maiorGrandeNumero(a, &aux) && (quociente < 10))
             {
                quociente++;
                iniciaGrandeNumero(&aux);
                tmp.digitos[N_DIGITOS-casas_shift] = quociente;
                tmp.tam = N_DIGITOS-casas_shift;
                multiplicaGrandeNumero(&tmp, b, &aux);
             }
             if (!(igualGrandeNumero(a, &aux) || maiorGrandeNumero(a, &aux)))
             {
                quociente--;
                iniciaGrandeNumero(&aux);
                tmp.digitos[N_DIGITOS-casas_shift] = quociente;
                multiplicaGrandeNumero(&tmp, b, &aux);
             }
             c->digitos[c->tam] = quociente;
             c->tam--;
             subtraiGrandeNumero(a, &aux, a);
             casas_shift--;
             if (igualGrandeNumero(&zero, a))
             {
                /* copiar trailing zeros */
                c->tam-=casas_shift;
                break;
             }
         }
         /* ajusta o quociente para human-readable */
         c->tam++;
         inverteGrandeNumero(c);
         removerZeros(c);
      }
      else
      {
         bool iguais = VERDADEIRO;
         if (a->tam == b->tam)
         {    
            for (i=a->tam;i<N_DIGITOS;i++)
               if (a->digitos[i] != b->digitos[i])
               {
                  iguais = FALSO;
                  break;
               }
         }   
         else
         {
              iguais = FALSO;
         }
         /* se a == b retorna 1 */
         if (iguais)
         { 
            c->digitos[N_DIGITOS-1] = 1;
            c->sinal = a->sinal * b->sinal;
            c->tam = N_DIGITOS-1;
         } 
         /* se a < b quociente 0, se sinais iguais */
         /* se a < b quociente -1, se sinais diferentes */
         else
         {
            c->digitos[N_DIGITOS-1] = (a->sinal != b->sinal);
            c->sinal = a->sinal * b->sinal;
            c->tam = N_DIGITOS-1;
         }
      }
   }
}

TgrandeNumero* fatorial(int n)
{
   TgrandeNumero *tmp, *e, *s;
   tmp = malloc(sizeof(TgrandeNumero));
   iniciaGrandeNumero(tmp);
   intParaGrandeNumero(tmp, n);
   if (n<=1)
      return tmp;
   e = fatorial(n-1);
   s = malloc(sizeof(TgrandeNumero));
   iniciaGrandeNumero(s);
   multiplicaGrandeNumero(tmp, e, s);
   free(tmp);
   free(e);
   return s;
}

int main()
{
   TgrandeNumero x,y,z, *s, *r, *t;

   printf("INICIALIZA\n");
   intParaGrandeNumero(&x, 0);
   imprimeGrandeNumero(&x);
   intParaGrandeNumero(&x, -1);
   imprimeGrandeNumero(&x);
   intParaGrandeNumero(&x, -24986129);
   imprimeGrandeNumero(&x);
   intParaGrandeNumero(&x, 297024129);
   imprimeGrandeNumero(&x);
   printf("SOMA & SUBTRAI\n");
   intParaGrandeNumero(&x, -1);
   intParaGrandeNumero(&y, 1);
   somaGrandeNumero(&x, &y, &z);
   imprimeGrandeNumero(&z);
   intParaGrandeNumero(&x, 12424);
   intParaGrandeNumero(&y, -29872);
   somaGrandeNumero(&x, &y, &z);
   imprimeGrandeNumero(&z);
   intParaGrandeNumero(&y, 12424);
   intParaGrandeNumero(&x, -29872);
   somaGrandeNumero(&x, &y, &z);
   imprimeGrandeNumero(&z);
   intParaGrandeNumero(&x, -12424);
   intParaGrandeNumero(&y, -29872);
   subtraiGrandeNumero(&x, &y, &z);
   imprimeGrandeNumero(&z);
   intParaGrandeNumero(&y, -12424);
   intParaGrandeNumero(&x, -29872);
   subtraiGrandeNumero(&x, &y, &z);
   imprimeGrandeNumero(&z);
   printf("SHIFT\n");
   imprimeGrandeNumero(&x);
   shiftGrandeNumero(&x, 3);
   imprimeGrandeNumero(&x);
   imprimeGrandeNumero(&y);
   shiftGrandeNumero(&y, 6);
   imprimeGrandeNumero(&y);
   printf("MULTIPLICA\n");
   iniciaGrandeNumero(&z);
   intParaGrandeNumero(&x, 12121473);
   intParaGrandeNumero(&y, 1241427);
   multiplicaGrandeNumero(&x, &y, &z);
   imprimeGrandeNumero(&z);
   int i;
   /*for (i=0;i<2000;i++)
   {
      iniciaGrandeNumero(&z);
      intParaGrandeNumero(&x, i*100);
      intParaGrandeNumero(&y, i*100);
      multiplicaGrandeNumero(&x, &y, &z);
      imprimeGrandeNumero(&z);
   }*/
   iniciaGrandeNumero(&z);
   intParaGrandeNumero(&x, 12121473);
   intParaGrandeNumero(&y, 0);
   multiplicaGrandeNumero(&x, &y, &z);
   imprimeGrandeNumero(&z);
   printf("DIVIDE\n");
   iniciaGrandeNumero(&z);
   printf("100 / 100\n");
   intParaGrandeNumero(&x, 100);
   intParaGrandeNumero(&y, 100);
   divideGrandeNumero(&x, &y, &z);
   imprimeGrandeNumero(&z);
   iniciaGrandeNumero(&z);
   printf("100 / 0\n");
   intParaGrandeNumero(&x, 100);
   intParaGrandeNumero(&y, 0);
   divideGrandeNumero(&x, &y, &z);
   imprimeGrandeNumero(&z);
   iniciaGrandeNumero(&z);
   printf("100 / 1\n");
   intParaGrandeNumero(&x, 100);
   intParaGrandeNumero(&y, 1);
   divideGrandeNumero(&x, &y, &z);
   imprimeGrandeNumero(&z);
   iniciaGrandeNumero(&z);
   printf("100 / 25\n");
   intParaGrandeNumero(&x, 100);
   intParaGrandeNumero(&y, 25);
   divideGrandeNumero(&x, &y, &z);
   imprimeGrandeNumero(&z);
   iniciaGrandeNumero(&z);
   printf("2199 / 30\n");
   intParaGrandeNumero(&x, 2199);
   intParaGrandeNumero(&y, 30);
   divideGrandeNumero(&x, &y, &z);
   imprimeGrandeNumero(&z);
   iniciaGrandeNumero(&z);
   printf("2200 / 50\n");
   intParaGrandeNumero(&x, 2200);
   intParaGrandeNumero(&y, 50);
   divideGrandeNumero(&x, &y, &z);
   imprimeGrandeNumero(&z);
   iniciaGrandeNumero(&z);
   printf("10094 /98\n");
   intParaGrandeNumero(&x, 10094);
   intParaGrandeNumero(&y, 98);
   divideGrandeNumero(&x, &y, &z);
   imprimeGrandeNumero(&z);
   iniciaGrandeNumero(&z);
   printf("78000390 /78\n");
   intParaGrandeNumero(&x, 78000390);
   intParaGrandeNumero(&y, 78);
   divideGrandeNumero(&x, &y, &z);
   imprimeGrandeNumero(&z);
   iniciaGrandeNumero(&z);
   printf("1008724556/21374\n");
   intParaGrandeNumero(&x, 1008724556);
   intParaGrandeNumero(&y, 21374);
   divideGrandeNumero(&x, &y, &z);
   imprimeGrandeNumero(&z);
   iniciaGrandeNumero(&z);
   printf("2000801208/4\n");
   intParaGrandeNumero(&x, 2000801208);
   intParaGrandeNumero(&y, 4);
   divideGrandeNumero(&x, &y, &z);
   imprimeGrandeNumero(&z);
   iniciaGrandeNumero(&z);
   printf("50000000 /5\n");
   intParaGrandeNumero(&x, 50000000);
   intParaGrandeNumero(&y, 5);
   divideGrandeNumero(&x, &y, &z);
   imprimeGrandeNumero(&z);
   s = fatorial(10);
   imprimeGrandeNumero(s);
   iniciaGrandeNumero(&z);
   intParaGrandeNumero(&x, 720);
   intParaGrandeNumero(&y, 1);
   divideGrandeNumero(&x, &y, &z);
   imprimeGrandeNumero(&z);
   iniciaGrandeNumero(&z);
   intParaGrandeNumero(&x, 7);
   intParaGrandeNumero(&y, 1);
   divideGrandeNumero(&x, &y, &z);
   imprimeGrandeNumero(&z);
   printf("CONVERSAO\n");
   long long k = grandeNumeroParaLongLong(s);
   printf("%lld (%lu)\n",k,sizeof(long long));
   free(s);
   s = fatorial(60);
   imprimeGrandeNumero(s);
   free(s);
   s = fatorial(80);
   imprimeGrandeNumero(s);
   free(s);
   printf("3!\n");
   r = fatorial(3);
   imprimeGrandeNumero(r);
   free(r);
   s = fatorial(580);
   imprimeGrandeNumero(s);
   r = fatorial(530);
   imprimeGrandeNumero(r);
   t = malloc(sizeof(TgrandeNumero));
   iniciaGrandeNumero(t);
   divideGrandeNumero(s,r,t);
   imprimeGrandeNumero(t);
   free(s);
   free(r);
   free(t);
   k = grandeNumeroParaLongLong(t);
   printf("%lld\n",k);
   return 0;
}
