#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <netdb.h>
#include <sys/socket.h>
#include <arpa/inet.h>

/* cabecalho DNS: 12 bytes */
#define TDNS 12
/* id */
#define ID 0x539
/* pesquisa: 0, estado: 2  */
#define OPCODE 0x0
/* use recursao */
#define RD 1

/* NS recursivo */
#define IP_NAMESERVER "8.8.8.8"
/* nao inclua http:// ou https:// */
#define PAGINA_WEB "www.rafaelstvieira.com"
#define PORTA 53

struct entrada_in 
{
    short int familia;
    unsigned short int porta;
    struct in_addr nome;
    unsigned char zeros[8];
};

int main(){
  int entrada, nerro, conta, respostas, i, j;
  int bytes_enviados, bytes_recebidos, total;
  struct entrada_in endereco_ip;
  /* dados enviados & recebidos(max): 512 por vez, truncado caso maior */
  /* endereco web deve ter no maximo 256 bytes */
  char *pos, web[256] = PAGINA_WEB;
  unsigned char msg[1024];

  /* inicializa msg */
  memset(msg, '\0', 1024);

  /* cria socket */
  entrada = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
  if (entrada < 0)
  {
     perror("Erro criando socket: ");
     exit(1);
  }

  /* dados da conexao */
  endereco_ip.familia = AF_INET;
  endereco_ip.porta = htons(PORTA);
  endereco_ip.nome.s_addr = inet_addr(IP_NAMESERVER);
  memset(&(endereco_ip.zeros), '\0', 8);

  /* cabecalho do protocolo DNS: RFC 1035 */
  msg[0] = (ID >> 8) & 0xff;
  msg[1] = ID & 0xff;
  msg[2] = (OPCODE << 3) + RD; 
  msg[5] = 1; /* quantidade de buscas */
  /* encontra nos do espaco do dominio */
  total = strlen((char*)web); /* antes de destrinchar o texto */
  pos = strtok((char*)web, ".");
  conta = TDNS; 
  while (pos != NULL) 
  {
     msg[conta] = strlen(pos); 
     for (i=1; i<=msg[conta]; i++)
        msg[conta+i] = pos[i-1];
     pos = strtok(NULL, ".");
     conta += msg[conta] + 1;
  }
  /* tamanho da zona root */
  msg[conta] = 0;
  msg[conta+2] = 1;
  msg[conta+4] = 1;
  total += TDNS + 6; /* ultimos 6 bytes */

  /* envia dados via conexao */
  bytes_enviados = sendto(entrada, msg, total, 0, (struct sockaddr*)&endereco_ip, sizeof(endereco_ip));
  /* limpa msg para ser usada novamente */
  memset(msg, '\0', total);

  /* conta recebe o tamanho do campo */
  i = total;
  if ((bytes_recebidos = recvfrom(entrada, msg, 1024, 0, (struct sockaddr*)&endereco_ip, (socklen_t*)&conta))>0)
  {
        /* quantas respostas foram dadas */
        respostas = ((msg[6]<<8) + msg[7]); 
        j = 0;
        while (j < respostas)
        {
           /* imprime dados recebidos; 0xc0 indica que foi usado compressao */
           if ((msg[i] & 0xc0) == 0xc0)
           {
              /* vamos pular campos, queremos so o ip, caso exista */
              i+=3;
              if (msg[i] == 1)
              {
                 i+=9;
                 /* assumimos IPv4 */
                 printf("WWW: %s\n IP: ", PAGINA_WEB);
                 printf("%d.", msg[i++]);
                 printf("%d.", msg[i++]);
                 printf("%d.", msg[i++]);
                 printf("%d\n", msg[i++]);
              } 
              else
              {
                 i+=7;
                 i+= ((msg[i]<<8) + msg[i+1] + 2);
              }
           }
           else
           {
              /* repetiu ou usou nome de dominio */
              while (msg[i] > 0)
              {
                 /* poderia ler o nome de dominio, assumimos repeticao */
                 i += msg[i]+1;
              }
              i+=8;
              i+= ((msg[i]<<8) + msg[i+1] + 2);
           }
           j++;
        }
        /*for (i=total;i<1024;i++)
              printf("%02x ", msg[i]); */
  }
  return 0;
}
