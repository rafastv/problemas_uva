#include <stdio.h>
#define mod(a,b) (b + (a % b)) % b
int main()
{
    printf("REMAINDER\n");
    printf("5 % 5 = %d\n",(5%5));
    printf("4 % 5 = %d\n",(4%5));
    printf("3 % 5 = %d\n",(3%5));
    printf("2 % 5 = %d\n",(2%5));
    printf("1 % 5 = %d\n",(1%5));
    printf("0 % 5 = %d\n",(0%5));
    printf("-1 % 5 = %d\n",(-1%5));
    printf("-2 % 5 = %d\n",(-2%5));
    printf("-3 % 5 = %d\n",(-3%5));
    printf("-4 % 5 = %d\n",(-4%5));
    printf("-5 % 5 = %d\n",(-5%5));
    printf("MODULUS\n");
    printf("5 mod 5 = %d\n", mod(5,5));
    printf("4 mod 5 = %d\n", mod(4,5));
    printf("3 mod 5 = %d\n", mod(3,5));
    printf("2 mod 5 = %d\n", mod(2,5));
    printf("1 mod 5 = %d\n", mod(1,5));
    printf("0 mod 5 = %d\n", mod(0,5));
    printf("-1 mod 5 = %d\n", mod(-1,5));
    printf("-2 mod 5 = %d\n", mod(-2,5));
    printf("-3 mod 5 = %d\n", mod(-3,5));
    printf("-4 mod 5 = %d\n", mod(-4,5));
    printf("-5 mod 5 = %d\n", mod(-5,5));
    return 0;
}
