#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>

typedef int bool;
#define true 1
#define false 0

#define mod(a,b) (b + (a % b)) % b


/* maior matriz necessaria para plano projetivo s/ pontos de escape */
int MATRIZ[31][31];
/* maior matriz necessaria para plano projetivo c/ pontos de escape */
int PLANO[993][32];

void imprimePlano(int n, int k)
{
    int u, v;
    /* primeira linha     */
    /* k pontos de escape */
    printf("pontos de escape\n");
    printf("%d", PLANO[0][0]);
    for (u=1;u<k;u++)
       printf(" %d", PLANO[0][u]);
    printf("\n");
    printf("pontos no espaco projetivo\n");
    /* matriz (k-1) */
    for (u=0;u<k-1;u++)
    {
        printf("%d", MATRIZ[u][0]);
        for (v=1;v<k-1;v++)
            printf(" %d", MATRIZ[u][v]);
        printf("\n");
    } 
    if (k == 1)
       printf("vazio\n"); 
}

void imprimeMatriz(int n, int k)
{
    int u, v;
    for (u=0;u<n;u++)
    {
        printf("%d", PLANO[u][0]);
        for (v=1;v<k;v++)
            printf(" %d", PLANO[u][v]);
        printf("\n");
    } 
}

/* n linhas, n pontos, k pontos por linha */
void constroiPlanoProjetivo(int n, int k)
{
    int u,v;
    /* insere k pontos de escape no espaco (primeira linha) */
    for (u=0;u<k;u++)
        PLANO[0][u] = u+1;
        
    /* conta matriz de pontos no espaco projetivo (k-1) x (k-1)  */
    for (u=0;u<k-1;u++)
        for (v=0;v<k-1;v++)
            MATRIZ[u][v] = u*(k-1)+v+1+k;
}

void preencheMatrizAdj(int n, int k)
{
    int u,v,conta=0,pos=1, escape, incremento;
    /* preenche k-1 linhas na horizontal (adiciona primeiro ponto de escape) */
    for (u=pos;u<k;u++)
    {
        PLANO[u][0] = PLANO[0][0];
        for (v=1;v<k;v++)
            PLANO[u][v] = MATRIZ[conta][v-1]; 
        conta++;
    }
    /* preenche linhas na vertical */
    conta=0;
    pos = k;
    for (u=pos;u<(pos+k-1);u++)
    {
        PLANO[u][0] = PLANO[0][1]; 
        for (v=1;v<k;v++)
            PLANO[u][v] = MATRIZ[v-1][conta]; 
        conta++;
    }
    /* preenche linhas nas diagonais */
    conta=0;
    pos += (k-1);
    for (u=pos;u<(pos+k-1) && (u < n);u++)
    {
        PLANO[u][0] = PLANO[0][2]; 
        for (v=1;v<k;v++)
            PLANO[u][v] = MATRIZ[(v-1)%(k-1)][(conta+(v-1))%(k-1)];
        conta++;
    }
    /* preenche rotacao da linha diagonal */
    conta=0;
    pos += (k-1);
    escape = 3;
    incremento = 2;
    for (u=pos;u< n;u++)
    {
        PLANO[u][0] = PLANO[0][escape]; 
        for (v=1;v<k;v++)
            PLANO[u][v] = MATRIZ[(v-1)%(k-1)][mod((conta+(v-1)*incremento),(k-1))];
        conta++;
        if (conta >= (k-1))
        {
            conta = 0;
            incremento++;
            escape++;
        }
    }
    /* preenche linhas nas diagonais invertidas (nao eh preciso com rotacao) */
    /*conta=0;
    pos += (k-1);
    for (u=pos;u<(pos+k-1) && (u < n);u++)
    {
        PLANO[u][0] = PLANO[0][3]; 
        for (v=1;v<k;v++)
            PLANO[u][v] = MATRIZ[(v-1)%(k-1)][mod((conta-(v-1)),(k-1))];
        conta++;
    }*/

}

void zeraMatrizPlano(int n)
{
    int u;
    for (u=0;u<n;u++)
        memset(&PLANO[u], 0, sizeof(int)*32);
}

int main()
{
    int k, n = 0;
    

    while ((fscanf(stdin, "%d", &k)) != EOF)
    {
        if (n > 0)
            printf("\n");

        /************************* ESPACO PROJETIVO ******************************/
        /* calcula n = (k-1)^2 + k                                               */
        /* matriz (k-1)^2 de pontos no espaco projetivo                          */
        /* k pontos de escape (ou fuga)                                          */
        /* n linhas e n pontos                                                   */
        /*************************** PROPRIEDADES ********************************/
        /* nenhuma linha eh paralela no espaco projetivo, logo nao ha retangulos */
        /* todo par de linhas encontra-se em um unico ponto                      */
        /*************************************************************************/
          
        n = k*k-k+1;
        
        zeraMatrizPlano(n);
        constroiPlanoProjetivo(n,k);
        /*imprimePlano(n,k);*/
        preencheMatrizAdj(n, k);
        imprimeMatriz(n, k);
    }
    return 0;
}

