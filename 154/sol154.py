import sys, operator, math

MATERIAIS = {"A":0, "G":1, "N":2, "P":3, "S":4}
LIXOS = {"b":0, "g":1, "o":2, "r":3, "y":4}

cidades = []
#print (MATERIAIS,LIXOS)
ordem_cidades = [[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0]]
linhas = sys.stdin.readlines()
for linha in linhas:
   linha = linha.replace("\n","")
   if linha[0] == "#":
      break
   if linha[0] == "e":
      votos = len(cidades)*[0]
      for i in range(len(cidades)):
         recicla = cidades[i]
         for e in recicla:
            l,m = e.split("/")
            votos[i] += ordem_cidades[LIXOS[l]][MATERIAIS[m]]
      idx, valor = max(enumerate(votos), key=operator.itemgetter(1))
      sys.stdout.write(str(idx+1)+"\n")
      #print(votos)
      #print(ordem_cidades)
      #print(cidades)
      ordem_cidades = [[0,0,0,0,0],[0,0,0,0,0],\
                       [0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0]]
      cidades = []
      continue
   recicla = [i for i in linha.split(",")]
   for e in recicla:
      l,m = e.split("/")
      ordem_cidades[LIXOS[l]][MATERIAIS[m]]+=1
   cidades+=[recicla]

exit(0)
