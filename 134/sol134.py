import sys

############################################
#                                    names #              
# 0 - NAM                                  #
#                             little words #
# 1 - A                                    #
# 2 - MOD                                  #
# 3 - BA                                   #
# 4 - DA                                   #
# 5 - LA                                   # 
#                               predicates #
# 6 - PREDA                                #
############################################

def verifica(vogal, i):
   if vogal[i] == "a" or vogal[i] == "e" or vogal[i] == "i" or \
      vogal[i] == "o" or vogal[i] == "u":
       return True 
   return False

def classifica(palavra):
    if len(palavra) == 2:
       if verifica(palavra,1):
           if palavra[0] == "g":
               return 2
           if palavra[0] == "b":
               return 3
           if palavra[0] == "d":
               return 4
           if palavra[0] == "l":
               return 5
           else:
               return -1
       else:
           return -1
    elif len(palavra) == 1:
       if verifica(palavra,0):
           return 1
       else:
           return -1
    else:
       if verifica(palavra,-1) and not verifica(palavra,-2) and not verifica(palavra,0):
           if (not verifica(palavra,1) and     verifica(palavra,2)) or\
              (    verifica(palavra,1) and not verifica(palavra,2)):
              return 6
           else:
              return -1
       else:
           return 0

def verifyPredstring(conjunto):
    s1 = (len(conjunto) == 1) and (conjunto[0] == 6)
    s2 = False    
    idx = 0
    if len(conjunto) > 0:
        while (conjunto[idx]==6):
            idx+=1
            if idx >= len(conjunto):
               break
        s2 = (idx == len(conjunto))
    # <predstring> -> PREDA | <predstring> PREDA
    #print("PREDSTRING", s1, s2)
    return s1 or s2

def verifyPreds(conjunto):
    s1 = verifyPredstring(conjunto)
    s2 = False
    if 1 in conjunto:
        idx = conjunto.index(1)
        while (1 in conjunto[idx+1:]):
            idx = conjunto[idx+1:].index(1) + len(conjunto[:idx+1])
        #print (conjunto, conjunto[:idx], conjunto[idx+1:])
        s2 = verifyPreds(conjunto[:idx]) and verifyPredstring(conjunto[idx+1:])
    # <preds> ->  <predstring> | <preds> A <predstring>
    #print("PREDS", s1, s2)
    return s1 or s2 

def verifyPredname(conjunto):
    s1 = False
    s2 = False
    # LA <predstring>
    if len(conjunto) > 1:
        s1 = (conjunto[0] == 5) and verifyPredstring(conjunto[1:])
    #print("LA",s1)
    # NAM
    s2 = (len(conjunto) == 1) and (conjunto[0] == 0)
    #print("NAM",s2)
    # <predname> -> LA <predstring> | NAM
    #print("PREDNAME", s1, s2, conjunto)
    return s1 or s2

def verifyPredclaim(conjunto):
    s1 = False
    s2 = False
    # verifica se DA
    if (len(conjunto) > 1) and (conjunto[0] == 4):
        s1 = verifyPreds(conjunto[1:])
    # verifica se BA
    if (len(conjunto) > 2) and (3 in conjunto):
        idx = conjunto.index(3) 
        #print(idx, conjunto[:idx], conjunto[idx+1:])
        s2 = verifyPredname(conjunto[:idx]) and verifyPreds(conjunto[idx+1:])
    # <predclaim> -> <predname>BA<preds>|DA<preds>
    #print("PREDCLAIM", s1, s2)
    return s1 or s2

def verifyStatement(conjunto):
    # verifica verbpred
    # <verbpred> -> MOD <predstring>
    s1 = False
    s2 = False
    # verifica se MOD em conjunto
    if 2 in conjunto:
        idx = conjunto.index(2)
        idx_predstring = idx+1
        # verifica se predstring existe
        # <predstring> -> PREDA | <predstring> PREDA
        if idx_predstring < len(conjunto):
            while (conjunto[idx_predstring]==6):
                idx_predstring+=1
                if idx_predstring >= len(conjunto):
                    break
            idx_predstring-=1
            if (idx_predstring > idx):
                s1 = verifyPredname(conjunto[:idx])
                # s2 pode ser vazio, verdade por vacuidade
                s2 = True
                if conjunto[idx_predstring+1:]:
                    s2 = verifyPredname(conjunto[idx_predstring+1:])
    # <statement> -> <predname> <verbpred> <predname> | <predname> <verbpred>
    # print("STATEMENT", (s1 and s2))
    return s1 and s2

def verificaGramatica(frase):
    # remove ponto
    frase = frase[:frase.index(".")]
    # classifica palavras
    #print (frase)
    conjunto = [classifica(palavra) for palavra in frase.split()]
    # palavras mal formadas
    if -1 in conjunto:
        return False
    # <sentence> -> <statement> | <predclaim>
    return verifyStatement(conjunto) or verifyPredclaim(conjunto)

texto = "" 
for linha in sys.stdin:

   linha = linha.replace("\n"," ")
   texto += linha
   
   if linha[0] == "#":
      break
   
   if "." in linha:
      # executa algoritmo
      if verificaGramatica(texto):
          sys.stdout.write("Good\n") 
      else:
          sys.stdout.write("Bad!\n") 
      texto = ""

exit(0)

