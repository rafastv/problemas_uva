#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

char CONFIGURACAO[26];

void mescla(char variaveis[], int ini, int meio, int fim)
{
   int i=ini, j=meio+1, conta=0;
   char tmp[fim-ini+1];
   while (i<=meio && j<=fim)
   {
      if (variaveis[i] < variaveis[j])
         tmp[conta++] = variaveis[i++];
      else
         tmp[conta++] = variaveis[j++];
   }
   while (i<=meio)
      tmp[conta++] = variaveis[i++];
   while (j<=fim)
      tmp[conta++] = variaveis[j++];
   tmp[conta] = '\0';
   for (i=ini;i<=fim;i++)
      variaveis[i]=tmp[i-ini];
}


void mesclagem(char variaveis[], int ini, int fim)
{
    if (ini < fim)
    {
        int meio = (fim+ini)>>1;
        mesclagem(variaveis, ini, meio);
        mesclagem(variaveis, meio+1, fim);
        mescla(variaveis, ini, meio, fim);
    }
}

long long fatorial (int n)
{
   if (n <= 1)
      return 1;
   return n*fatorial(n-1);
}

void contaRepeticoes(char variaveis[])
{
   int i;
   for (i=0;i<strlen(variaveis);i++)
      CONFIGURACAO[variaveis[i]-97]++;
}

long long contaPosicao(char variaveis[], char ordenado[])
{
   int i,j,k;
   int tam_p = strlen(variaveis);
   int tam_q = strlen(ordenado);
   long long tmp,soma = 0;
   for (i=0;i<tam_p;i++)
      for (j=0;j<tam_q;j++)
      {
         if (variaveis[i] == ordenado[j])
         {
            CONFIGURACAO[variaveis[i]-97]--;
            for (k=j;k<tam_q-1;k++)
               ordenado[k] = ordenado[k+1];
            tam_q--;
            if (i == tam_p-1)
               soma++;
            break;
         }
         else
         {
            CONFIGURACAO[ordenado[j]-97]--;
            tmp = fatorial(tam_p-i-1); 
            for (k=0;k<26;k++)
              tmp /= fatorial((int) CONFIGURACAO[k]);
            soma += tmp;
            CONFIGURACAO[ordenado[j]-97]++;
            j += CONFIGURACAO[ordenado[j]-97]-1;
            
         }
      }
   return soma;
}

int main()
{
    char variaveis[31], ordenado[31]; 
    while (fscanf(stdin, "%s", variaveis) != EOF)
    {
       if (variaveis[0] == '#')
          break;
       strcpy(ordenado,variaveis);
       mesclagem(ordenado, 0, strlen(ordenado)-1);
       memset(CONFIGURACAO, 0, 26);
       contaRepeticoes(variaveis);
       printf("%10lld\n", contaPosicao(variaveis, ordenado));
    }
    return 0;
}
