import sys, operator, math
from itertools import islice
   
CENARIO = []
TEMPO_IT = 0

def imprimeRelogio(tempo):
   horas    = str(tempo // 3600)
   minutos  = str((tempo % 3600)// 60)
   segundos = str((tempo % 3600) % 60)
   
   horas    = "0"*(2-len(horas))    + horas
   minutos  = "0"*(2-len(minutos))  + minutos
   segundos = "0"*(2-len(segundos)) + segundos
   
   return horas + ":" + minutos + ":" + segundos

def intersecaoIntervalos(iA, iB):
   iC = [iB[0], iB[1]]
   if (iA[0] >= iB[1]) or (iA[1] <= iB[0]):
      return []
   elif (iA[0] >= iB[0]) and (iA[1] <= iB[1]):
      iC[0] = iA[0]
      iC[1] = iA[1]
   elif (iA[0] < iB[0]) and (iA[1] >= iB[0]) and (iA[1] <= iB[1]):
      iC[1] = iA[1]
   elif (iA[1] > iB[1]) and (iA[0] >= iB[0]) and (iA[0] <= iB[1]):
      iC[0] = iA[0]
   else:
      pass
   return iC

def estaoSincronizados(tempo, passo):
   # maior intervalo possivel
   min_s2 = tempo 
   # se eh o primeiro passo
   if (tempo % passo != 0):
      max_s2 = ((passo >> 1) - 5) 
   else:
      max_s2 = min_s2 + ((passo >> 1) - 5) 
   intervalos = [[min_s2, max_s2]]
   # percorre os intervalos do maior para o menor
   # com passo maior pode haver mais de uma intersecao entre intervalos
   for i in range(1,len(CENARIO)):
      lista = []
      for j in range(len(intervalos)-1,-1,-1):
         intervalo = intervalos[j]
         repeticao = min_s2 // (CENARIO[i] << 1)
         min_s1 = repeticao * (CENARIO[i] << 1) 
         max_s1 = min_s1 + (CENARIO[i] - 5) 
         #print(min_s1, max_s1, intervalo[0], intervalo[1])
         segundos_finais = min_s2 % (CENARIO[i] << 1) + ((passo>>1)-5)
         while (segundos_finais >= 0):
            novo = intersecaoIntervalos([min_s1, max_s1], intervalo)
            if novo:
               if novo not in lista:
                  lista += [novo]
            # nao ha mais tempo para sinal laranja e vermelho
            if segundos_finais < (CENARIO[i]<<1):
               break
            segundos_finais -= (CENARIO[i]<<1)
            # mais tempo do sinal laranja e vermelho
            min_s1 = max_s1 + (CENARIO[i] + 5)
            finais = segundos_finais
            if segundos_finais > (CENARIO[i] - 5):
               finais = CENARIO[i] - 5
            max_s1 = min_s1 + finais
            #print("com resto", min_s1, max_s1, intervalo[0], intervalo[1], segundos_finais)
      if not lista:
         return False 
      intervalos = lista
                
   global TEMPO_IT
   TEMPO_IT = min(min(intervalos))
   return True

def sincronizaSinais():
   global TEMPO_IT
   passo = CENARIO[0] << 1
   # o maximo do menor eh o minimo do maior para o primeiro passo
   tempo = CENARIO[-1]
   primeiro = False
   while not estaoSincronizados(tempo, passo):
      if not primeiro:
         primeiro = True
         tempo = passo
      else:
         tempo += passo
      # 5 horas
      if tempo > 18000:
         return -1
   return TEMPO_IT

linhas = sys.stdin.readlines()
for linha in linhas:
   linha = linha.replace("\n","")
   linha = linha.split()
   conta = 0
   for valor in linha:
      n = int(valor)
      if n == 0:
         conta+=1
         # computa cenario
         if CENARIO:
            CENARIO.sort(reverse=True)
            tempo = sincronizaSinais()
            if tempo < 0:
               sys.stdout.write("Signals fail to synchronise in 5 hours\n")
            else:
               saida = imprimeRelogio(tempo)
               sys.stdout.write(saida + "\n")
         CENARIO = []
      else:
         CENARIO += [n]
   if conta == 3:
      break
 

exit(0)
