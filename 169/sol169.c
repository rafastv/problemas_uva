#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>

typedef int bool;
#define VERDADEIRO 1
#define FALSO 0

#define MAXIMO 10000

char PALAVRAS[MAXIMO][251];
int CONFIGURACOES[MAXIMO][26];
int REPETICOES[MAXIMO];
int INTERVALOS[MAXIMO][2];
int TAM;

int CONF_FRASE[26];
int INTR_FRASE[26];
int PRIM_FRASE[26];

bool comparaConfiguracoes(int A[26], int B[26])
{
   int i;
   for (i=0;i<26;i++)
      if (A[i] != B[i])
         return FALSO;
   return VERDADEIRO;
}

void copiaConfiguracoes(int A[26], int B[26])
{
   int i;
   for (i=0;i<26;i++)
      A[i]=B[i];
}

void imprimePalavras()
{
   int i;
   for (i=0;i<TAM;i++)
      printf("%s\n",PALAVRAS[i]);
   printf("*\n");
}

void imprimeDadosPalavras()
{
   int i,j;
   printf("PALAVRAS: %d\n",TAM);
   for (i=0;i<TAM;i++)
   {
      printf("%s\n",PALAVRAS[i]);
      printf("%2d",CONFIGURACOES[i][0]);
      for (j=0;j<26;j++)
          printf(":%2d",CONFIGURACOES[i][j]);
      printf(" [%2d %2d] REPETE: %d\n",INTERVALOS[i][0], INTERVALOS[i][1], REPETICOES[i]);
   } 
}

void salvaPalavra(char buffer[1001],int j, int i)
{
    /* insertion sort */
    int  k, chave=0;
    bool achei = FALSO;
    for (k=TAM-1;k>=0;k--)
       if ((i-1) >= INTERVALOS[k][1])
       {
          chave = k+1;
          break;
       } 
    for (k=TAM;k>chave;k--)
    {
       strcpy(PALAVRAS[k], PALAVRAS[k-1]);
       copiaConfiguracoes(CONFIGURACOES[k], CONFIGURACOES[k-1]); 
       INTERVALOS[k][0] = INTERVALOS[k-1][0];
       INTERVALOS[k][1] = INTERVALOS[k-1][1];
    }
    strncpy(PALAVRAS[chave], &buffer[j+1], i-j-1); 
    PALAVRAS[chave][i-j-1]='\0';
    INTERVALOS[chave][0] = j+1;
    INTERVALOS[chave][1] = i-1;
    memset(CONFIGURACOES[chave], 0, 26*sizeof(int));
    for (k=0;k<strlen(PALAVRAS[chave]);k++)
        CONFIGURACOES[chave][PALAVRAS[chave][k]-'a']++;
    TAM++;
}

void removePalavra(int k)
{
   int i;
   for (i=k;i<TAM-1;i++)
   {
      strcpy(PALAVRAS[i], PALAVRAS[i+1]);
      copiaConfiguracoes(CONFIGURACOES[i], CONFIGURACOES[i+1]); 
      INTERVALOS[i][0] = INTERVALOS[i+1][0];
      INTERVALOS[i][1] = INTERVALOS[i+1][1];
      REPETICOES[i] = (REPETICOES[i+1] > (k+1))?REPETICOES[i+1]-1:REPETICOES[i+1];
   }
   TAM--;
}

void removePalavrasUnicas()
{
   int i,j;
   memset(REPETICOES, 0, TAM*sizeof(int));
   for (i=0;i<TAM;i++)
      for (j=i+1;j<TAM;j++)
         if (comparaConfiguracoes(CONFIGURACOES[i], CONFIGURACOES[j])) 
         {
             REPETICOES[i]=(REPETICOES[i]==0)?i+1:(REPETICOES[i]<(i+1))?REPETICOES[i]:i+1;
             REPETICOES[j]=(REPETICOES[j]==0)?i+1:(REPETICOES[j]<(i+1))?REPETICOES[j]:i+1;
         }
   int tam = TAM-1;
   for (i=tam;i>=0;i--)
      if (REPETICOES[i] == 0)
         removePalavra(i);
}

void removePalavrasSemInterseccao()
{
   int i,j,ini_i, fim_i, ini_j, fim_j;
   int tam = TAM-1;
   bool naorem[TAM];
   memset(naorem, 0, sizeof(bool)*TAM);
   for (i=tam;i>=0;i--)
   {
      ini_i = INTERVALOS[i][0];
      fim_i = INTERVALOS[i][1];
      for (j=i-1;j>=0;j--)
      {
         ini_j = INTERVALOS[j][0];
         fim_j = INTERVALOS[j][1];
         if (!((ini_i > fim_j) || (fim_i < ini_j)))
         {
            naorem[REPETICOES[i]-1]=VERDADEIRO;
            naorem[REPETICOES[j]-1]=VERDADEIRO;
         }
      }
   } 
   for (i=tam;i>=0;i--)
      if (!naorem[i])
         removePalavra(i);
}

void encontraPalavras(char buffer[1001])
{
   int i, j;
   memset(INTR_FRASE, 0, 26*sizeof(int));
   memset(CONF_FRASE, 0, 26*sizeof(int));
   memset(PRIM_FRASE, 0, 26*sizeof(int));
   for (i=0;i<strlen(buffer);i++)
   {
      /* conta quantos caracteres de um tipo ocorreram */
      CONF_FRASE[buffer[i]-'a']++; 
      /* captura primeira ocorrencia de um caracter */
      if (PRIM_FRASE[buffer[i]-'a'] == 0)
          PRIM_FRASE[buffer[i]-'a'] = i+1;
      /* captura ultima ocorrencia de um caracter */
      if (INTR_FRASE[buffer[i]-'a'] == 0)
      {
          INTR_FRASE[buffer[i]-'a'] = i+1;
      }
      else
      {
          j = INTR_FRASE[buffer[i]-'a']-1;
          if (((i-j) > 2) && ((i-j) < 252))
             salvaPalavra(buffer, j, i);
          INTR_FRASE[buffer[i]-'a'] = i+1;
      } 
   }
   /* com os 3 vetores de FRASE preenchidos podemos saber quem sao as palavras que usam coringas */
   int ini, fim, tam = strlen(buffer);
   for (i=0;i<26;i++)
   {
       if (CONF_FRASE[i] == 1)
       {
          /* era ini -1 e fim + 1, mas como somou +1 a todos os valores -2 + 0 */
          ini = PRIM_FRASE[i]-2;
          fim = PRIM_FRASE[i];
          if ((ini >= 1) && (ini < 250))
             salvaPalavra(buffer, -1, ini+1);
          if ((fim <= (tam-2)) && (fim >= (tam-250)))
             salvaPalavra(buffer, fim-1, strlen(buffer));
          continue;
       }
       if (CONF_FRASE[i] > 1)
       {
          /* era ini -1 e fim + 1, mas como somou +1 a todos os valores -2 + 0 */
          ini = PRIM_FRASE[i]-2;
          fim = INTR_FRASE[i];
          if ((ini >= 1) && (ini < 250))
             salvaPalavra(buffer, -1, ini+1);
          if ((fim <= (tam-2)) && (fim >= (tam-250)))
             salvaPalavra(buffer, fim-1, strlen(buffer));
       }
   }

}

int main()
{
    char penultimo, c, buffer[1001];
    int conta, idx = 0;
    memset(buffer,0,1001);
    TAM = 0;
    while (fscanf(stdin, "%c", &c)>EOF)
    {
        conta++;
        if (((c-'a') >= 0) && ((c-'a') <= 25))
	{
	    buffer[idx] = c;
            penultimo = c;
	    idx++;
	}
	else
	{
	    buffer[idx] = '\0';
            if ((idx == 0) && (c!= '#') && (penultimo != '#') && (penultimo != '-'))
            {
               if (c == '\n')
                  printf("*\n");
               penultimo = c;
               conta = 0;
               continue;
            } 
	    if (c == '\n')
	    {
               if (penultimo == '-')
               {
                  penultimo = '\n';
                  conta = 0;
		  continue;
               }
               if ((penultimo == '#') && (conta == 2))
                  break;
               /** aplica algoritmo **/
               /*printf("%s(%lu)\n",buffer,strlen(buffer));*/
               encontraPalavras(buffer);
               removePalavrasUnicas();
               removePalavrasSemInterseccao();
               imprimePalavras();
               /*imprimeDadosPalavras();*/
	       /** limpa buffer **/
	       idx = 0;
               memset(buffer,0,1001);
	       buffer[0] = '\0';
               TAM = 0;
               conta = 0;
	    }
            penultimo = c;
	}
    }
    return 0;
}

