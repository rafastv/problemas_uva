import sys

TABELA =  {"A": [0.10, 0.06, 0.02],
           "B": [0.25, 0.15, 0.05],
           "C": [0.53, 0.33, 0.13],
           "D": [0.87, 0.47, 0.17],
           "E": [1.44, 0.80, 0.30]}

INTERVALOS = [[480, 1080], [1080, 1320], [1320, 1920]]

def imprimeSaida(idx, fone, hora_ini, hora_fim, dH):
   s1 = len(fone)
   s2 = len(str(dH[0]))
   s3 = len(str(dH[1]))
   s4 = len(str(dH[2]))
   s5 = 1
   sys.stdout.write(((10-s1)*" "+fone)+((6-s2)*" "+str(dH[0])))
   sys.stdout.write(((6-s3)*" "+str(dH[1]))+((6-s4)*" "+str(dH[2])))
   sys.stdout.write(((3-s5)*" ")+idx+"{0:8.2f}".format(dH[3])+"\n")

def contaMinutos(hora):
   minutos = hora[0] * 60 + hora[1]
   if minutos <= 480:
       minutos += 1440
   return minutos

def classificaHora(hora):
   minutos = contaMinutos(hora)
   if (minutos > 480) and (minutos <= 1080):
       return 0
   elif (minutos > 1080) and (minutos <= 1320):
       return 1
   else:
       return 2

def diferencaHora(idx, horaA, cA, horaB, cB):
   saida = [0, 0, 0, 0]
   minutosA = contaMinutos(horaA)
   minutosB = contaMinutos(horaB)
#   if ((minutosB - minutosA) < 1) and (minutosB >= minutosA):
#       saida[cA] = (minutosB-minutosA)
#       return saida
   tarifaA = TABELA[idx][cA]
   tarifaB = TABELA[idx][cB]
   global INTERVALOS
   if (cA != cB):
       dA = (INTERVALOS[cA][1]-minutosA)
       dB = (minutosB-INTERVALOS[cB][0])
       valor  = dA * tarifaA
       valor += dB * tarifaB
       saida[cA] = dA
       saida[cB] = dB
       cI = (cA + 1) % 3
       if (cI != cB):
           tarifaI = TABELA[idx][cI]
           dI = (INTERVALOS[cI][1] - INTERVALOS[cI][0])
           valor +=  dI * tarifaI
           saida[cI] = dI
   else:
       if (minutosB > minutosA):
           dA = (minutosB-minutosA)
           valor = dA * tarifaA
           saida[cA] = dA
       else:
           dA = (INTERVALOS[cA][1]-minutosA)
           dB = (minutosB-INTERVALOS[cB][0])
           valor  = dA * tarifaA
           valor += dB * tarifaB
           saida[cA] = dA
           saida[cB] += dB
           cI = (cA + 1) % 3
           while (cI != cB):
               tarifaI = TABELA[idx][cI]
               dI = (INTERVALOS[cI][1] - INTERVALOS[cI][0])
               valor +=  dI * tarifaI
               saida[cI] = dI
               cI = (cI + 1) % 3
   saida[3] = valor
   return saida
          
linhas = sys.stdin.readlines()

for linha in linhas:
   linha = linha.replace("\n","")
   vars = linha.split()
   if vars[0] == "#":
       break
   idx = vars[0]
   fone = vars[1]
   hora_ini = [int(vars[2]), int(vars[3])]
   hora_fim = [int(vars[4]), int(vars[5])]
   c_ini = classificaHora(hora_ini)
   c_fim = classificaHora(hora_fim)
   #print(c_ini, c_fim, contaMinutos(hora_ini), contaMinutos(hora_fim))
   dH = diferencaHora(idx, hora_ini, c_ini, hora_fim, c_fim)
   imprimeSaida(idx, fone, hora_ini, hora_fim, dH)

exit(0)
