#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

typedef int bool;
#define true 1
#define false 0

int contaCanosSkew(double *area)
{
    double h = sqrt(3.0)/2.0;
    double altura = area[0];
    double largura = area[1];
    if ((area[0] < 1.0) || (area[1] < 1.0))
        return 0;
    int coeficiente = (int) ((altura-1.0)/h);
    coeficiente++;
    int q1_bolas=(int)largura,q2_bolas=((int)largura)-1;
    int a = coeficiente >> 1;
    if ((largura - ((double)((int)largura))) >= 0.5)
        return a*q1_bolas + (coeficiente-a)*q1_bolas;
    return a*q2_bolas + (coeficiente-a)*q1_bolas;
}

int main()
{
    double area[2], tmp; 
    int canos_grid, canos_skew;
    bool grid = false;

    while ((fscanf(stdin, "%lf %lf", &area[0], &area[1])) == 2)
    {
        canos_grid = floor(area[0]) * floor(area[1]);
	canos_skew = contaCanosSkew(area);
        tmp = area[0];
        area[0] = area[1];
        area[1] = tmp;
	tmp = contaCanosSkew(area);
        if (tmp > canos_skew)
           canos_skew = tmp;
        if (canos_grid >= canos_skew)
            printf("%d grid\n", canos_grid);
        else
            printf("%d skew\n", canos_skew);
    }
    return 0;
}
