#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>

typedef int bool;
#define true 1
#define false 0

#define mod(a,b) (b + (a % b)) % b

unsigned int NUMERO[256];
unsigned int NDIG;

unsigned int calculaParteResto(int qt_casas, unsigned int dividendo, unsigned int divisor)
{
    int casas = 0;
    while (casas <= (qt_casas))
    {
       if (divisor < dividendo)
           dividendo -= divisor;
       divisor >>= 1;
       casas++;
    } 
    return dividendo;
}

unsigned int calculaResto(int tam)
{
    if (tam == 0)
	return 0;
    /* tamanho do tipo que armazena resto deve ser 2x maior a CRC */
    int i, k=-1;
    unsigned int numero, resto = 0;
    /* 2 bytes de CRC */
    unsigned int qt_casas = (sizeof(unsigned int)-2) << 3;
    unsigned int bytes_palavra_incompleta = tam % sizeof(unsigned int); 
    /* aplica divisao binaria */
    for (i=0; i<NDIG; i++)
    {
	if (((i+1)*sizeof(unsigned int)) > tam)
	{
	    k = i;
	    break;
	}
	/* parte corrente do numero */
        resto = calculaParteResto(qt_casas, NUMERO[i], 34943 << qt_casas);
	/* transicao */
	if ((i+1) < NDIG)
	{
	    numero = 0;
	    numero |= (resto << qt_casas);
	    numero |= (NUMERO[i+1] >> qt_casas);
            resto = calculaParteResto(qt_casas, numero, 34943 << qt_casas);
	    NUMERO[i+1] = (resto << qt_casas) + (NUMERO[i+1] & (((unsigned int)-1) >> qt_casas));
	}
    }
    if (k >= 0)
    {
        int desloca = sizeof(unsigned int) + tam  - (k+1)*sizeof(unsigned int);
	bool maisum = false;
	/* tamanho do CRC */
	if (desloca > 2)
	    maisum = true;
	if (desloca >= 2)
	    desloca = 0;
	NUMERO[k] >>= (desloca << 3);
        resto = calculaParteResto(qt_casas, NUMERO[k], 34943 << qt_casas);
	if (maisum)
	{
	    resto <<= (1 << 3);
            resto = calculaParteResto(qt_casas, resto, 34943 << qt_casas);
	}
    }
    else
    {
	/* tamanho do CRC */
	resto <<= (2 << 3);
        resto = calculaParteResto(qt_casas, resto, 34943 << qt_casas);
    }
    return 34943-resto;
}

void salvaNumero(char *buffer, int tam)
{
    unsigned int i, j, palavra;
    unsigned int tam_palavra = (sizeof(int)) << 3;
    unsigned int numero_completo;

    NDIG = 0;
    for (i=0;i<tam;i+=sizeof(int))
    {
        numero_completo = 0;
        for (j=0; j<sizeof(int); j++)
        {
            tam_palavra -= 8;
            if ((i+j) < tam)
            {
                palavra = (((unsigned int) buffer[i+j]) << tam_palavra);
                numero_completo |= palavra;
            }
        }
        NUMERO[NDIG++] = numero_completo;
    }
}

void imprimeHexadecimal(unsigned int resto)
{
   char tmp[5],saida[5];
   int i;
   sprintf(tmp, "%x", resto);
   /* converte para maiusculas */
   for (i=0;i<strlen(tmp);i++)
       if (tmp[i] >= 97 && tmp[i] <= 122)
           tmp[i]-= 32;
   /* corrige tamanho */
   memset(saida,'0',4);
   if (strlen(tmp) < 4)
   {
       for (i=0;i<strlen(tmp);i++)
           saida[i+(4-strlen(tmp))] = tmp[i];
       saida[4]='\0';
   }
   else
       strcpy(saida, tmp);
   /* imprime separado */
   printf("%c%c %c%c\n",saida[0],saida[1],saida[2],saida[3]);
}

int main()
{
    char c, buffer[1024];
    unsigned int resto, idx = 0;
    while ((c  = fgetc(stdin)) != EOF)
    {
	if (c!='\n')
	{
	    buffer[idx] = c;
	    idx++;
	}
	else
	{
	    if (buffer[0] == '#')
		break;
	    buffer[idx] = '\0';
            salvaNumero(buffer, idx);
            resto = calculaResto(idx);
	    imprimeHexadecimal(resto);
	    /** limpa buffer **/
	    idx = 0;
            memset(buffer,0,3);
	}
    }
    return 0;
}

