import sys

for linha in sys.stdin:

   if (linha[0]=='#'):
      break

   soma = 0
   for i in range(len(linha[:-1])):
       caractere = linha[i]
       soma+= (ord(caractere) << ((len(linha[:-1])-(i+1))*8))

   # 2bytes de CRC
   soma <<= 16 

   divisor = soma // 34943
   resto = ((divisor*34943) - soma)
   if resto < 0:
       resto = (((divisor+1)*34943) - soma)
   
   saida = hex(resto)[2:].upper()
   complemento = 4 - len(saida)
   saida = '0'*complemento + saida
   sys.stdout.write(saida[:2] + ' ' + saida[2:] + '\n')

exit(0)
