import sys

def converteTexto(elemento):
    if len(elemento) == 2:
        a = len(str(elemento[0]))
        b = len(str(elemento[1]))
        return (3-a)*' ' + str(elemento[0]) + (3-b)*' ' + str(elemento[1])
    else:
        a = len(str(elemento[0]))
        return (3-a)*' ' + str(elemento[0])

for linha in sys.stdin:

   partes = linha.split()

   if partes[0] == partes[1] == partes[2] and partes[0] == '0':
       break

   n_pessoas = int(partes[0])
   k = int(partes[1])
   m = int(partes[2])

   caminho = []
   ciclo = [i for i in range(n_pessoas)]
   pos_saiK = n_pessoas-1
   pos_saiM = 0
   while n_pessoas > 0:
       pos_saiK = (pos_saiK + k) % n_pessoas
       pos_saiM = (pos_saiM - m) % n_pessoas
       if pos_saiK == pos_saiM:
           caminho += [[ciclo[pos_saiK]+1]]
           salva0 = ciclo[(pos_saiK-1) % n_pessoas]
           salva1 = ciclo[(pos_saiM+1) % n_pessoas]
           ciclo = ciclo[:pos_saiK] + ciclo[pos_saiK+1:]
           n_pessoas -= 1
           if n_pessoas == 0:
              break
           pos_saiK = ciclo.index(salva0)
           pos_saiM = ciclo.index(salva1)
       else:
           caminho += [[ciclo[pos_saiK]+1, ciclo[pos_saiM]+1]]
           salva0 = ciclo[(pos_saiK-1) % n_pessoas]
           salva1 = ciclo[(pos_saiM+1) % n_pessoas]
           if salva0 == ciclo[pos_saiM]:
               salva0 = ciclo[(pos_saiK-2) % n_pessoas]
           if salva1 == ciclo[pos_saiK]:
               salva1 = ciclo[(pos_saiM+2) % n_pessoas]
           selecionado = ciclo[pos_saiM]
           ciclo = ciclo[:pos_saiK] + ciclo[pos_saiK+1:]
           ciclo.remove(selecionado)
           n_pessoas -= 2
           if n_pessoas == 0:
               break
           pos_saiK = ciclo.index(salva0)
           pos_saiM = ciclo.index(salva1)

   sys.stdout.write(converteTexto(caminho[0]))
   for i in caminho[1:]:
       sys.stdout.write(',' + converteTexto(i))
   sys.stdout.write('\n')

exit(0)

