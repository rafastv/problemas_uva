import sys

def imprimirFrases(frases, KWIC, dicionario_palavras):
    for palavra in KWIC:
        idx_antigo = dicionario_palavras[palavra][0]
        acrescimo = 0
        for i in range(len(dicionario_palavras[palavra])):
            idx = dicionario_palavras[palavra][i]
            if idx_antigo != idx:
                acrescimo = 0
            pos = frases[idx][acrescimo:].index(palavra) + acrescimo
            ini = pos
            if (pos+len(palavra)) < len(frases[idx]):
                ini = frases[idx][acrescimo:].index(palavra + ' ') + acrescimo 
            fim = ini + len(palavra)
            saida_frase = frases[idx][:ini] + palavra.upper() + frases[idx][fim:]
            sys.stdout.write(saida_frase + '\n')
            idx_antigo = idx
            acrescimo = fim

fim = False
KWIC = []
frases = []
palavras_ignoradas = []
dicionario_palavras = {}
for linha in sys.stdin:
    vars = linha.split()

    if len(vars) == 0:
        continue

    if not fim and vars[0] != "::":
        palavras_ignoradas += vars

    if (vars[0]=="::"):
        fim = True
        continue

    if fim:
        frases += [linha[:-1].lower()]
        idx = len(frases)-1
        for palavra in vars:
            palavra = palavra.lower()
            if palavra not in palavras_ignoradas:
                if palavra not in dicionario_palavras.keys():
                    dicionario_palavras.update({palavra:[idx]}) 
                    KWIC += [palavra]
                else:
                    dicionario_palavras[palavra]+=[idx]

KWIC.sort()
for i in KWIC:
    dicionario_palavras[i].sort()
imprimirFrases(frases, KWIC, dicionario_palavras)
exit(0)
