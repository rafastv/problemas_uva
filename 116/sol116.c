#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

typedef int bool;
#define true 1
#define false 0

typedef struct sequencia
{
    int soma;
    char* caminho;
} Tsequencia;

Tsequencia* segueCaminho(Tsequencia ***buffer, int **M, int i, int j, int m, int n)
{
     Tsequencia *s0, *s1, *s2, *saida;
     int ordem,u,v;

     saida = malloc(sizeof(Tsequencia));
     saida->caminho = malloc((3*n)+1);
     /* 48 tabela ASCII para numero 0 */
     if (n == 1)
         sprintf(saida->caminho, "%d", (i+1));
     else
         sprintf(saida->caminho, "%d ",(i+1));
     if (n == 1)
     {
        saida->soma = M[i][j];
        return saida;
     }
     if (buffer[i][j] != NULL)
     {
         saida->soma = buffer[i][j]->soma;
         strcpy(saida->caminho, buffer[i][j]->caminho);
         return saida;
     }
     ordem = 0;
     u = i-1;
     v = i+1;
     if (i == 0)
     {
        u = m-1;
        ordem = 1;
     }
     if (i == (m-1))
     {
        v = 0;
        ordem = 2;
     }
     switch (ordem)
     {
         case 0:{
                    s0 = segueCaminho(buffer, M, u, j+1, m, n-1);   
                    s1 = segueCaminho(buffer, M, i, j+1, m, n-1);   
                    s2 = segueCaminho(buffer, M, v, j+1, m, n-1); 
                    break;
 	        }
         case 1:{
                    s0 = segueCaminho(buffer, M, i, j+1, m, n-1);   
                    s1 = segueCaminho(buffer, M, v, j+1, m, n-1); 
                    s2 = segueCaminho(buffer, M, u, j+1, m, n-1);   
                    break;
	        }
         case 2:{
                    s0 = segueCaminho(buffer, M, v, j+1, m, n-1); 
                    s1 = segueCaminho(buffer, M, u, j+1, m, n-1);   
                    s2 = segueCaminho(buffer, M, i, j+1, m, n-1);   
                    break;
	        }
     }
     s0->soma += M[i][j];
     s1->soma += M[i][j];
     s2->soma += M[i][j];
     if (s0->soma <= s1->soma)
     {
       if (s0->soma <= s2->soma)
       {
            saida->soma = s0->soma;
            strcat(saida->caminho, s0->caminho);
       }
       else
       {
            saida->soma = s2->soma;
            strcat(saida->caminho, s2->caminho);
       }
     }
     else
     {
       if (s1->soma <= s2->soma)
       {
          saida->soma = s1->soma;
          strcat(saida->caminho, s1->caminho);
       }
       else
       {
          saida->soma = s2->soma;
          strcat(saida->caminho, s2->caminho);
       }
     }
     free(s0->caminho);
     free(s1->caminho);
     free(s2->caminho);
     free(s0);
     free(s1);
     free(s2);
     if (buffer[i][j] == NULL)
     {
         buffer[i][j] = malloc(sizeof(Tsequencia));
         buffer[i][j]->soma = saida->soma;
         buffer[i][j]->caminho = malloc((3*n)+1);
         strcpy(buffer[i][j]->caminho, saida->caminho);
     }
     return saida;
}

Tsequencia* menorCaminho(int **M, int m, int n)
{
    int i,j,ordem,u,v;
    Tsequencia ***buffer;
    Tsequencia *s0, *s1, *s2, *st, *saida;

    saida = malloc(sizeof(Tsequencia));
    saida->soma = INT_MAX;
    saida->caminho = malloc((3*n)+1);

    /* cria buffer */
    buffer = malloc(sizeof(Tsequencia**)*m); 
    for (i=0; i<m;i++)
    {
        if (n > 1)
        { 
            buffer[i] = malloc(sizeof(Tsequencia*)*(n-1));  
            for (j=0; j<(n-1);j++)
                buffer[i][j] = NULL;
        } 
    }
    /* executa algoritmo */
    for (i=0; i<m;i++)
    {
        ordem = 0;
        u = i-1;
        v = i+1;
        if (i == 0)
        {
             u = m-1;
             ordem = 1;
        }
        if (i == (m-1))
        {
             v = 0;
             ordem = 2;
        }
        if (n > 1)
        {
            switch (ordem)
            {
                case 0:{
                           s0 = segueCaminho(buffer, M, u, 1, m, n-1);   
                           s1 = segueCaminho(buffer, M, i, 1, m, n-1);   
                           s2 = segueCaminho(buffer, M, v, 1, m, n-1); 
                           break;
 	               }
                case 1:{
                           s0 = segueCaminho(buffer, M, i, 1, m, n-1);   
                           s1 = segueCaminho(buffer, M, v, 1, m, n-1); 
                           s2 = segueCaminho(buffer, M, u, 1, m, n-1);   
                           break;
	               }
                case 2:{
                           s0 = segueCaminho(buffer, M, v, 1, m, n-1); 
                           s1 = segueCaminho(buffer, M, u, 1, m, n-1);   
                           s2 = segueCaminho(buffer, M, i, 1, m, n-1);   
                           break;
	               }
            }
            s0->soma += M[i][0];
            s1->soma += M[i][0];
            s2->soma += M[i][0];
            if (s0->soma <= s1->soma)
            {
               if (s0->soma <= s2->soma)
               {
                   st = s0;
               }
               else
               {
                   st = s2;
	       }
            }
            else
            {
               if (s1->soma <= s2->soma)
               {
                   st = s1;
               }
               else
               {
                   st = s2;
	       }
            }
            if (st->soma < saida->soma)
            {
                sprintf(saida->caminho, "%d ", (i+1));
                saida->soma = st->soma;
                strcat(saida->caminho, st->caminho);
            }
            /* limpa vetores */
            free(s0->caminho);
            free(s1->caminho);
            free(s2->caminho);
            free(s0);
            free(s1);
            free(s2);
        }
        else
        {
            if (M[i][0] < saida->soma)
            {
                sprintf(saida->caminho, "%d", (i+1));
                saida->soma = M[i][0];
            }
        }
    }
    /* limpa buffer */
    for (i=0; i<m;i++)
    {
        if (n > 1)
        { 
            for (j=0; j<(n-1);j++)
            {
                if (buffer[i][j] != NULL)
                {
                    free(buffer[i][j]->caminho);
                    free(buffer[i][j]);
                }
            }
            free(buffer[i]);
        } 
    }
    free(buffer);
    /* limpa buffer */
    return saida;
}

void imprimeSequencia(Tsequencia *s)
{
    printf("%s\n", s->caminho);
    printf("%d\n", s->soma);
}

void imprimeMatriz(int **M, int m, int n)
{
    int u,v;
    for (u=0;u<m;u++) 
    {
    	for (v=0;v<n;v++)
	    printf("%d ", M[u][v]);
        printf("\n");
    }
}

void limpaMatriz(int **M, int m)
{
    int u;
    for (u=0;u<m;u++) 
	free(M[u]);
    free(M);
}


int main()
{
    Tsequencia *saida;
    char c;
    char buffer[10];
    int u,v,m=0,n=0,idx=0;
    int **matriz = NULL;
    bool fim = false;
    /* caso linha comece com vazio */
    buffer[0] = 0;
    while ((c  = getc(stdin)) != EOF)
    {
	if ((c!=' ') && (c!='\n') && (c!='\t'))
	{
	    buffer[idx] = c;
	    idx++;
	}
	else
	{
	    if ((int) buffer[0]==0)
		continue;
	    idx++;
	    buffer[idx]='\0';
	    if (m<=0)
	    {
	        /* linhas */
	        m = atoi(buffer);
	    	/* limpa buffer */
	    	memset(buffer,0,10);
	    	idx=0;
		continue;
	    }
	    if ((m>0) && (n<=0))
	    {
	        /* colunas */
	        n = atoi(buffer);
	    	/* limpa buffer */
	    	memset(buffer,0,10);
	    	idx=0;
		continue;
	    }
	    if ((matriz == NULL) && (n>0) && (m>0)) 
	    {
	        /* captura matriz */	     
		matriz = (int**) malloc(sizeof(int*)*m);
		for (u=0;u<m;u++)
		{
		     matriz[u] = (int*) malloc(sizeof(int)*n);
	             memset(matriz[u],0,sizeof(int)*n);
		}
		u = 0;
		v = 0;
		matriz[u][v] = atoi(buffer);
		/* matriz de tamanho 1x1 */
	        if ((u == (m-1)) && (v == (n-1)))
	        {
		    /*imprimeMatriz(matriz,m,n);*/
                    fim = true;
                    saida = menorCaminho(matriz,m,n);
                    imprimeSequencia(saida);
                    free(saida->caminho);
                    free(saida);
		    /* fim do processamento da matriz */
		    limpaMatriz(matriz,m);
		    matriz = NULL;
		    m = 0;
		    n = 0;
		}	    
	    	/* limpa buffer */
	    	memset(buffer,0,10);
	    	idx=0;
		continue;
	    }
	    if (matriz != NULL)
	    {
		v++;
		if (v >= n)
		{
		    u++;
		    v = 0;
		}
		matriz[u][v] = atoi(buffer);
	        if ((u == (m-1)) && (v == (n-1)))
	        {
		    /*imprimeMatriz(matriz,m,n);*/
                    fim = true;
                    saida = menorCaminho(matriz,m,n);
                    imprimeSequencia(saida);
                    free(saida->caminho);
                    free(saida);
		    /* fim do processamento da matriz */
		    limpaMatriz(matriz,m);
		    matriz = NULL;
		    m = 0;
		    n = 0;
		}	    
	    	/* limpa buffer */
	    	memset(buffer,0,10);
	    	idx=0;
	    }
	}
    }
    if (fim == false)
        printf("\n");
    return 0;
}

