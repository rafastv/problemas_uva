#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>

typedef int bool;
#define true 1
#define false 0

typedef enum{
    NORTE,
    LESTE,
    SUL,
    OESTE
} Torientacao;

typedef struct robo
{
  Torientacao dir;
  int pos[2];
} Trobo;

#define mod(a,b) (b + (a % b)) % b

int POSMEM = 0;
int MEMORIA[200];

void viraDireita(Trobo *robo)
{
    robo->dir = (robo->dir + 1) % 4;
}

void viraEsquerda(Trobo *robo)
{
    robo->dir = mod(robo->dir - 1, 4);
}

void andaFrente(Trobo *robo)
{
    switch (robo->dir)
    {
	case NORTE: { robo->pos[1]++; break; }
	case LESTE: { robo->pos[0]++; break; }
        case SUL:   { robo->pos[1]--; break; }
	case OESTE: { robo->pos[0]--; break; }
    }
}

void andaTras(Trobo *robo)
{
    switch (robo->dir)
    {
	case NORTE: { robo->pos[1]--; break; }
	case LESTE: { robo->pos[0]--; break; }
        case SUL:   { robo->pos[1]++; break; }
	case OESTE: { robo->pos[0]++; break; }
    }
}

void salvaMemoria(Trobo *robo)
{
    MEMORIA[POSMEM] = 51*robo->pos[1] + robo->pos[0];
    POSMEM += 1;
}

bool naMemoria(Trobo *robo)
{
    int i, valor = 51*robo->pos[1] + robo->pos[0];
    for (i=0; i<POSMEM; i++)
	if (MEMORIA[i] == valor)
	    return true;
    return false;
}

bool roboArea(Trobo *robo, int *area)
{
    if ((robo->pos[0] > area[0]) || (robo->pos[0] < 0))
	return false;
    if ((robo->pos[1] > area[1]) || (robo->pos[1] < 0))
	return false;
    return true;
}

void imprimeRobo(Trobo *robo, bool dentroArea)
{
    int x, y;
    char dir;
    switch (robo->dir)
    {
	case NORTE: { dir = 'N'; break; }
	case LESTE: { dir = 'E'; break; }
        case SUL:   { dir = 'S'; break; }
	case OESTE: { dir = 'W'; break; }
    }
    printf("%d %d %c", robo->pos[0], robo->pos[1], dir);
    if (!dentroArea)
	printf(" LOST");
    printf("\n");
}

int main()
{
    int area[2] = {-1,-1}; 
    Trobo robo;
    char dir;
    char c;
    char comandos[100];
    int pos,tam,i;
    bool dentroArea;
    /* captura zona de movimento */
    fscanf(stdin, "%d %d", &area[0], &area[1]);
    while ((fscanf(stdin, "%d %d %1c", &robo.pos[0], &robo.pos[1], &dir)) == 3)
    {
	switch (dir)
	{
            case 'N': { robo.dir=NORTE; break; }
            case 'E': { robo.dir=LESTE; break; }
            case 'S': { robo.dir=SUL;   break; }
            case 'W': { robo.dir=OESTE; break; }
	}
	fscanf(stdin, "%s", comandos);
	tam = strlen(comandos);
	for (i=0;i<tam;i++)
	{
	    switch (comandos[i])
 	    {
		case 'R': { viraDireita(&robo);  break; } 
		case 'L': { viraEsquerda(&robo); break; } 
		case 'F': { andaFrente(&robo);   break; } 
	    }
            dentroArea = roboArea(&robo, area);
	    if (!dentroArea)
	    {
		andaTras(&robo);
	        if (!naMemoria(&robo))
		{
		    salvaMemoria(&robo);
		    break;
		}
		/* salvo por sacrificio de amigo */
		dentroArea = true;
	    }
	}
	imprimeRobo(&robo, dentroArea);
	memset(comandos,0,100);
    }
    return 0;
}
