import sys, operator, math
from itertools import islice
   
def imprimeCruz(idx, par):
   # print (idx,par)
   # nivel medio do mar
   NMM = max(len(par[0][1]) - idx[0][1] - 1, len(par[1][1]) - idx[1][1] - 1)
   largura = len(par[0][0]) + 3 + len(par[1][0])
   altura = max(idx[0][1], idx[1][1]) + 1 + NMM

   # linha horizontal
   h1 = idx[0][0]
   h2 = idx[1][0] + 3 + len(par[0][0])
   horizontal = par[0][0] + " "*3 + par[1][0]
   # posicao da linha horizontal na vertical
   pos_v = max(idx[1][1], idx[0][1])

   # linhas verticais
   v1 = idx[0][1]
   v2 = idx[1][1]
   vertical_1 = par[0][1]
   vertical_2 = par[1][1]
   if v1 < v2:
      vertical_1 = (idx[1][1] - idx[0][1]) * " " + vertical_1
   else:
      vertical_2 = (idx[0][1] - idx[1][1]) * " " + vertical_2
   v1 = len(par[0][1]) - idx[0][1]
   v2 = len(par[1][1]) - idx[1][1]
   #print(v1,v2)
   if v1 < v2:
      vertical_1 += (v2 - v1) * " "
   else:
      vertical_2 += (v1 - v2) * " "

   #print (horizontal)
   #print (vertical_1, vertical_2)
   #print(altura, largura)

   for i in range(altura):
      if i == pos_v:
         sys.stdout.write(horizontal + "\n")
      else:
         linha = [" "] * largura
         linha[h1] = vertical_1[i]
         linha[h2] = vertical_2[i]
         if linha[h2] == " ":
            linha = linha[:h1+1]
         else:
            linha = linha[:h2+1]
         linha = "".join(linha)
         sys.stdout.write(linha + "\n")

linhas = sys.stdin.readlines()
entrada = False
for linha in linhas:
   linha = linha.replace("\n","")
   if linha[0] == "#":
      break
   if entrada:
      sys.stdout.write("\n")
   entrada = True
   palavras = linha.split()
   par = [[], []]
   par[0] = [palavras[0], palavras[1]]
   par[1] = [palavras[2], palavras[3]]
   # descobre pontos de contato nos 2 pares de palavras
   idx = [[-1, -1], [-1, -1]] 
   for i in range(2):
       for j in range(len(par[i][0])):
          c = par[i][0][j]
          if c in par[i][1]:
             idx[i] = [j, par[i][1].index(c)]
             break
   # se nao existir, proximos pares
   if min(min(idx)) < 0:
      sys.stdout.write("Unable to make two crosses\n")
      continue
   imprimeCruz(idx, par)

exit(0)
