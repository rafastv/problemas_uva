import sys,math

for linha in sys.stdin:
        vars = linha[0:-1].split()
        h = int(vars[0])
        f = int(vars[1])
        if h == 0:
            break
        if (f > 1):
            cf = math.log(h) / math.log(f)
            for i in range(h):
                if ((i+1) - (i**cf) < 0.001):
                    N = i
                    break
            cn = 0
            trabalhadores_livres = 0
            while ((N**cn) < f):
                trabalhadores_livres += N**cn
                cn += 1
            t = cn
            altura_total = 0
            while (t >= 0):
                altura_total += (N+1)**t * N**(cn-t)
                t-=1
        else:
            N = 1
            cn = 0
            altura_total = 0
            while (((N+1)**cn) < h):
                altura_total += (N+1)**cn
                cn += 1
            altura_total += (N+1)**cn
            trabalhadores_livres = cn

        sys.stdout.write(str(trabalhadores_livres) + " " + str(altura_total) + "\n")
exit(0)
