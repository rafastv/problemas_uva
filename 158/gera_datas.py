import sys, random

DIAS_G = 0
DIAS_J = 0

DIAS_MESES = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
IDX_MESES = {"January": 0, "February": 1,    "March":  2,    "April":  3,
                 "May": 4,     "June": 5,     "July":  6,   "August":  7, 
           "September": 8,  "October": 9, "November": 10, "December": 11}

NOME_MESES = {0:  "January", 1: "February",  2:    "March",  3:    "April",
              4:      "May", 5:     "June",  6:     "July",  7:   "August", 
              8:"September", 9:  "October", 10: "November", 11: "December"}

IDX_SEMANA = {"Sunday": 0, "Monday": 1,  "Tuesday": 2, "Wednesday": 3,
            "Thursday": 4, "Friday": 5, "Saturday": 6}

NOME_SEMANA = {0: "Sunday",   1: "Monday",  2: "Tuesday", 3: "Wednesday",
               4: "Thursday", 5: "Friday",  6: "Saturday"}

# data de inicio calendarios
DATA_GREGORIANO = (1582, 9, 15, 5)

def imprimeData(arquivo, nova_data):
   for i in range(7):
      arquivo.write('A ') 
      arquivo.write(str(nova_data[2])+' ') 
      arquivo.write(str(nova_data[1]+1)+' ')
      arquivo.write(str(i+1)+' ')
      palavra = [chr(int(random.random()*26+65)) for i in range(10)]
      palavra = "".join(palavra)
      arquivo.write(palavra + '\n') 
 
def anoComDiaExtraJuliano(ano):
   return ((ano % 4) == 0)

def anoComDiaExtraGregoriano(ano):
   return (((ano % 4) == 0) and ((ano % 100) > 0)) or ((ano % 400) == 0)

data_ini = list(DATA_GREGORIANO)

primeiro = False
while (data_ini[0] < 2000):
    if (data_ini[0] >= 1901):
       if not primeiro:
          arquivo = open(str(data_ini[0]) + ".txt", "w") 
          arquivo.write(str(data_ini[0]) + "\n")
          primeiro = True
       imprimeData(arquivo, data_ini)
    data_ini[2] += 1
    if anoComDiaExtraGregoriano(data_ini[0]) and (data_ini[1] == 1) and (data_ini[2] == 29):
       if (data_ini[0] >= 1901):
          imprimeData(arquivo, data_ini)
       data_ini[2] += 1
    if data_ini[2] > DIAS_MESES[data_ini[1]]:
       data_ini[2] = 1
       data_ini[1] += 1
       if data_ini[1] > 11:
          data_ini[0] += 1
          data_ini[1] = 0
          primeiro = False

exit(0)
