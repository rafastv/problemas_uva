import sys, operator, math
from itertools import islice

DATAS_IMPORTANTES = []
DIAS_MESES = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

def contaDias(hoje, data, salto):
   # conta dias entre anos inclusive extremos
   dias = 0
   if data[1] == hoje[1]:
      dias += data[0] - hoje[0] 
   elif (data[1] > hoje[1]) or (data[1] == 0 and hoje[1] == 11):
      dias += data[0] 
      dias += DIAS_MESES[hoje[1]] - hoje[0]
   else:
      return 31
   # se estamos em ano com extra, ele nao ocorreu, ou eh o dia extra
   if salto:
      if ((hoje[1] == 1) and (hoje[0] <= 29) and (data[1] == 2)):
         dias += 1
   return dias

linhas = sys.stdin.readlines()
N = int(linhas[0])
primeiroD = False
for linha in linhas[1:]:
   linha = linha.replace("\n","")
   if linha[0] == "#":
      break
   e = linha.split()
   if linha[0] == "A":
      cT = 0
      cP = 0
      palavra = False
      for i in linha:
         if i != " ":
            if not palavra:
               palavra = True 
               cP += 1
         else:
            palavra = False
         if cP >= 5: 
            break
         cT += 1
      descricao = linha[cT:]
      DATAS_IMPORTANTES += [[int(e[3]), [int(e[1]), int(e[2])], [descricao]]]
   elif linha[0] == "D":
      if (primeiroD):
         sys.stdout.write("\n")
      primeiroD = True
      anoComSalto = False
      if ((N % 4 == 0) and (N % 100 != 0)) or (N % 400 == 0):
         anoComSalto = True
      sys.stdout.write("Today is:" + "{:>3}".format(e[1]) + "{:>3}".format(e[2]) + "\n")
      D = int(e[1])
      M = int(e[2])-1
      datas_ordem = []
      conta = 5000
      for data in DATAS_IMPORTANTES:
         d = int(data[1][0])
         m = int(data[1][1])-1
         # se esta um mes antes da data de hoje ou eh o mes, conta dias
         if (M == m) or (((M + 1) % 12) == m):
            dias = contaDias([D, M], [d, m], anoComSalto)
            if dias >= 0 and dias <= data[0]:
               P = data[0] - dias
               texto = "{:>3}".format(str(d)) + "{:>3}".format(str(m+1))
               if dias == 0:
                  texto += " *TODAY* " + data[2][0]
                  datas_ordem += [[(31, 0, conta),texto]]
               else:
                  texto += " " + (P+1)*"*" + (7-P)*" " + data[2][0]
                  datas_ordem += [[(30-dias, P+1, conta),texto]]
               conta -= 1
  
      datas_ordem= sorted(datas_ordem, key=lambda x: x[0], reverse=True)
      for data in datas_ordem:
         sys.stdout.write(data[1]+"\n")
   else:
      pass

#print (DATAS_IMPORTANTES)
exit(0)
