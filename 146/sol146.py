import sys
          
# veja arquivo leiame

linhas = sys.stdin.readlines()

for linha in linhas:
   linha = linha.replace("\n","")
   if linha[0] == "#":
       break
   u = -1
   for idx in range(len(linha)-1):
      if ord(linha[idx]) < ord(linha[idx+1]):
         if idx >= u:
            u = idx
   if u < 0:
       sys.stdout.write("No Successor\n")
       continue
   v = -1
   for idx in range(u+1,len(linha)):
      if ord(linha[idx]) > ord(linha[u]):
         if idx >= v:
            v = idx
   if v < 0:
       sys.stdout.write("No Successor\n")
       continue
   texto = [c for c in linha]
   tmp = texto[v]
   texto[v] = texto[u]
   texto[u] = tmp
   v = len(texto)-1
   u = u+1
   while (u < v):
      tmp = texto[v]
      texto[v] = texto[u]
      texto[u] = tmp
      u += 1
      v -= 1
   saida = ""
   for c in texto:
       saida += c
   sys.stdout.write(saida+"\n")

exit(0)
