import sys
          
linhas = sys.stdin.readlines()

def criaSistemaNumerico(linha):
   chaves = [c for c in linha]
   chaves.sort()
   dic = {}
   conta = 1
   for c in chaves:
      if c not in list(dic.keys()):
         dic.update({c:conta})
         conta+=1
   return dic

def contaCaracteres(linha):
   dic = {}
   for c in linha:
      if c not in list(dic.keys()):
         dic.update({c:1})
      else:
         dic[c]+=1; 
   return dic
   
def converteNumero(dic, linha):
   texto = ""
   for c in linha:
      texto += str(dic[c])
   return texto

def converteLetra(dic, linha):
   ref = {}
   for c in list(dic.keys()):
      ref.update({dic[c]:c})
   texto = ""
   for c in linha:
      texto += ref[int(c)]
   return texto
   
def contagensIguais(qtref, qtc):
   for chave in list(qtref.keys()):
      if chave not in list(qtc.keys()):
         return False
      if qtref[chave] != qtc[chave]:
         return False
   return True

for linha in linhas:
   linha = linha.replace("\n","")
   if linha[0] == "#":
       break
   sig = criaSistemaNumerico(linha)
   numero_txt = converteNumero(sig, linha)
   tamanho = len(numero_txt)
   qtref = contaCaracteres(numero_txt)
   qtc = {}
   estourou = False
   while (not contagensIguais(qtref, qtc)):
      numero = int(numero_txt) + 1
      numero_txt = str(numero)
      qtc = contaCaracteres(numero_txt)
      if len(numero_txt) > tamanho:
         estourou = True
         break
   if not estourou:
       sys.stdout.write(converteLetra(sig, numero_txt) + "\n")
   else:
       sys.stdout.write("No Successor\n")
exit(0)
