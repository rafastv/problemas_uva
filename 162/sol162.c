#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>

typedef int bool;
#define VERDADEIRO 1
#define FALSO 0

typedef struct pilha
{
    unsigned char numero; /*face_value*/
    unsigned char naipe; /* suits */
    struct pilha *anterior;
} Tpilha;

Tpilha MONTES[2]; 
int GANHADOR;

void iniciaPilha(Tpilha *topo)
{
    /* guarda informacoes da pilha */
    topo->numero = (unsigned char) 0;
    topo->naipe = 'R'; /** RAIZ OU ROOT **/
    topo->anterior = NULL;
}

void push(Tpilha *topo, unsigned char naipe, unsigned char numero)
{
    Tpilha *p = malloc(sizeof(Tpilha));
    p->numero = numero;
    p->naipe = naipe;
    p->anterior = topo->anterior;
    topo->anterior = p;
    topo->numero++;
}

Tpilha* pop(Tpilha *topo)
{
    Tpilha *ultimo_elemento = topo->anterior;
    if (ultimo_elemento != NULL)
    {
        topo->anterior = ultimo_elemento->anterior;
	ultimo_elemento->anterior = NULL;
	topo->numero--;
    }
    return ultimo_elemento;
}

void limpaPilha(Tpilha *topo)
{
    while (topo->anterior != NULL)
	free(pop(topo));
}

void imprimePilha(Tpilha *topo)
{
    Tpilha *tmp = topo->anterior;
    printf("TOPO ");
    while (tmp != NULL)
    {
	printf("%c%c ", tmp->naipe, tmp->numero);
	tmp = tmp->anterior;
    }
    printf(" total: %u\n", (unsigned int) topo->numero);
}

void imprimeMontes()
{
    int i;
    for (i=0;i<2;i++)
	imprimePilha(&MONTES[i]);
}

void salvaCarta(char buffer[3], int pos)
{
    push(&MONTES[pos], buffer[0], buffer[1]);
}

void moveMesaParaJogador(Tpilha *mesa, int turno)
{
   Tpilha *carta, tmp;
   iniciaPilha(&tmp);
   while (MONTES[turno].numero > 0)
   {
      carta = pop(&MONTES[turno]);
      push(&tmp, carta->naipe, carta->numero);
      free(carta);
   } 
   while (mesa->numero > 0)
   {
      carta = pop(mesa);
      push(&MONTES[turno], carta->naipe, carta->numero);
      free(carta);
   } 
   while (tmp.numero > 0)
   {
      carta = pop(&tmp);
      push(&MONTES[turno], carta->naipe, carta->numero);
      free(carta);
   } 
}

void jogaBeggarMyNeighbor()
{
   Tpilha *carta, mesa; 
   bool fimJogo = FALSO;
   int descarta, turno = 0;
   char simbolo, custo[43] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                              0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
                              0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
                              0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
                              0, 0, 0};
   custo['J' - 48] = 1;
   custo['Q' - 48] = 2;
   custo['K' - 48] = 3;
   custo['A' - 48] = 4;
   iniciaPilha(&mesa);
   carta = pop(&MONTES[turno]);
   while (!fimJogo)
   {
      simbolo = carta->numero;
      descarta = custo[simbolo - 48];
      push(&mesa, carta->naipe, carta->numero);
      free(carta);

      turno = (turno + 1) % 2;
      /* paga preco das cartas */
      while (descarta > 0)
      {
         descarta--;
         carta = pop(&MONTES[turno]);
         if (carta == NULL)
         {
            fimJogo = VERDADEIRO;
            break;
         }
         simbolo = carta->numero;
         push(&mesa, carta->naipe, carta->numero);
         free(carta);
         /* inverte o pagamento */
         if (custo[simbolo - 48] > 0)
         {
            descarta = custo[simbolo - 48];
            turno = (turno + 1) % 2;
         }
         /* move cartas da mesa para ganhador da pilha */
         if (descarta == 0)
         {
            turno = (turno + 1) % 2;
            moveMesaParaJogador(&mesa, turno);
         }
      } 
      carta = pop(&MONTES[turno]);
      if (carta == NULL)
         fimJogo = VERDADEIRO;
   }
   GANHADOR = turno + 1;
}

void imprimeSaidaJogo()
{
    printf("%d%3d\n", GANHADOR, MONTES[GANHADOR%2].numero);
}

int main()
{
    char c, buffer[3];
    int i, idx = 0, conta = 0;
    iniciaPilha(&MONTES[0]);
    iniciaPilha(&MONTES[1]);
    while ((c  = fgetc(stdin)) != EOF)
    {
	if ((c!=' ') && (c!='\n') && (c!='\t') && (c!= '#'))
	{
	    buffer[idx] = c;
	    idx++;
	}
	else
	{
	    if (buffer[0] == '#')
		break;
	    if (idx == 0)
		continue;
	    buffer[idx] = '\0';
	    salvaCarta(buffer, conta % 2);
            conta++;
	    if (conta == 52)
	    {
		/** aplica algoritmo **/
		/*imprimeMontes();*/
		jogaBeggarMyNeighbor();
                imprimeSaidaJogo();
	        conta = 0;
                limpaPilha(&MONTES[0]);
                limpaPilha(&MONTES[1]);
                iniciaPilha(&MONTES[0]);
                iniciaPilha(&MONTES[1]);
	    }
	    /** limpa buffer **/
	    idx = 0;
            memset(buffer,0,3);
	}
    }
    return 0;
}

