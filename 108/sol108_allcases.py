import sys

primeiro = True

def subtrai_mat(A, B):
    matriz = []
    for i in range(len(A)):
        linha = []
        for j in range(len(A[0])):
            linha += [A[i][j] - B[i][j]]
        matriz += [linha]
    return matriz

def extrai_max(matriz):
    mm = max(matriz[0]);
    for i in matriz:
        tmp = max(i)
        if tmp > mm:
            mm = tmp
    return mm

def matriz_restoV(original, reduzida):
    nlins = len(original) - len(reduzida)
    saida = []
    if (nlins >= 2):
        for i in range(1, len(original)-(nlins-1)):
            linha = []
            for j in range(len(original[0])):
                elemento = original[i][j]
                for k in range(1,nlins-1):
                    elemento += original[i+k][j]
                linha += [elemento]
            saida += [linha] 
    return saida

def matriz_restoH(original, reduzida):
    ncols = len(original[0]) - len(reduzida[0])
    saida = []
    if (ncols >= 2):
        for i in range(len(original)):
            coluna = []
            for j in range(1, len(original[0])-(ncols-1)):
                elemento = original[i][j]
                for k in range(1,ncols-1):
                    elemento += original[i][j+k]
                coluna += [elemento]
            saida += [coluna]
    return saida

# soma elementos na horizontal
def somaH(matriz):
	saida = []
	for i in range(len(matriz)):
		linha = []
		for j in range(len(matriz[0])-1):
			linha += [matriz[i][j] + matriz[i][j+1]]
		saida += [linha]
	return saida

# soma elementos na vertical
def somaV(matriz):
	saida = []
	for i in range(len(matriz)-1):
		coluna = []
		for j in range(len(matriz[0])):
			coluna += [matriz[i][j] + matriz[i+1][j]]
		saida += [coluna]
	return saida

lista = []
for linha in sys.stdin:
    vars = linha[0:-1].split()
    if primeiro:
        N = int(vars[0])
        primeiro = False
    else:
        lista += vars
        if (len(lista) == (N*N)):
            primeiro = True
            # converte lista p/ matriz
            matriz = []
            while (lista != []):
                matriz += [[int(i) for i in lista[:N]]]
                lista = lista[N:]
            maximos = {}
            s1 = list(matriz)
            for i in range(N):
                if i >= 1:
                    s1 = somaV(s1)
                if i >= 2:
                    r1 = matriz_restoV(matriz, s1)
                    s1 = subtrai_mat(s1, r1)
                s2 = list(s1)
                for j in range(N):
                    #print("s1:",(1+i,1+j),s1)
                    if j >= 1:
                        s2 = somaH(s2)
                        #if j == 1:
                        #    print("s2:",(1+i,1+j),s2)
                    if j >= 2:
                        r2 = matriz_restoH(s1, s2)
                        s2 = subtrai_mat(s2, r2)
                        #print("r2:",(1+i,1+j),r2)
                        #print("s2:",(1+i,1+j),s2)
                    maximos.update({(1+i,1+j): extrai_max(s2)})
                    #print (maximos)
            sys.stdout.write(str(max(maximos.values())) + "\n")
exit(0)
