import sys

def soma_linhas(a,b):
    total = []
    for i in range(len(a)):
        total += [a[i] + b[i]]
    return total

def kadane_algoritmo(A):
    max_atual = max_total = A[0]
    m_ini = m_fim = ini = fim = 0 
    for i in range(1, len(A)):
        tmp = max_atual + A[i]
        if (A[i] > tmp):
            ini = i
            fim = i 
            max_atual = A[i]
        else:
            fim = i
            max_atual = tmp
        if (max_atual > max_total):
            m_ini = ini
            m_fim = fim
            max_total = max_atual
    return [max_total, (m_ini, m_fim)]

primeiro = True
lista = []
for linha in sys.stdin:
    vars = linha[0:-1].split()
    if primeiro:
        N = int(vars[0])
        primeiro = False
    else:
        lista += vars
        if (len(lista) == (N*N)):
            primeiro = True
            # converte lista p/ matriz
            matriz = []
            while (lista != []):
                matriz += [[int(i) for i in lista[:N]]]
                lista = lista[N:]
            sk = [-128, (-1,-1), (-1,-1)]
            for i in range(len(matriz)):
                s1 = kadane_algoritmo(matriz[i])
                tmp = matriz[i]
                m_ini = m_fim = i
                for j in range(i+1, len(matriz)):
                    tmp = soma_linhas(tmp, matriz[j])
                    s2 = kadane_algoritmo(tmp)
                    if (s2[0] > s1[0]):
                        s1 = s2
                        m_fim = j
                if (s1[0] > sk[0]):
                   sk = s1 + [(i,j)]
            sys.stdout.write(str(sk[0]) + "\n")
exit(0)
