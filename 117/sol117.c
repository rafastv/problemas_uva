#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>

typedef int bool;
#define true 1
#define false 0

int main()
{
    char c;
    int u,v,w,idx,ini,fim, SOMA;
    int **matriz;
    int *conta, IMPAR_1, IMPAR_2;
    char buffer[100];
    /* inicia uma matriz base para todos os grafos possiveis */
    /* (int) 'a' - (int) 'z' = 26 letras no alfabeto */
    matriz = (int**) malloc(sizeof(int*)*26);
    conta = malloc(sizeof(int)*26);
    for (u=0;u<26;u++)
    {
        matriz[u] = (int*) malloc(sizeof(int)*26);
        conta[u] = 0;
        for (v=0;v<26;v++)
	    matriz[u][v]=INT_MAX>>2;
	matriz[u][u]=0;
    }
    /* caso linha comece com vazio */
    idx = 0;
    buffer[0] = 0;
    SOMA = 0;
    while ((c  = fgetc(stdin)) != EOF)
    {
	if ((c!=' ') && (c!='\n') && (c!='\t'))
	{
	    buffer[idx] = c;
	    idx++;
	}
	else
	{
	    if ((int) buffer[0]==0)
		continue;
            if (strcmp(buffer, "deadend")==0)
            {
                /* descobre impares */
		IMPAR_1 = -1;
		IMPAR_2 = -1;
                for (u=0;u<26;u++)
                {
                    if ((conta[u] % 2 == 1) && (IMPAR_1 < 0))
                    {
                        IMPAR_1 = u;
                        continue;
                    }
                    if ((conta[u] % 2 == 1) && (IMPAR_2 < 0))
                        IMPAR_2 = u;
                    if ((IMPAR_1 >= 0) && (IMPAR_2 >= 0))
                        break;
                }
                /* inicia algoritmo */
                if ((IMPAR_1 < 0) || (IMPAR_2 < 0))
                    printf("%d\n", SOMA);
                else
                {
		    /* Algoritmo de Floyd-Warshall 1962 */
                    for (u=0;u<26;u++)
                        for (v=0;v<26;v++)
                            for (w=0;w<26;w++)
	                         if (matriz[v][w] > (matriz[v][u] + matriz[u][w]))
			             matriz[v][w] = (matriz[v][u] + matriz[u][w]);
                    printf("%d\n", SOMA + matriz[IMPAR_1][IMPAR_2]);
                 }
                /* limpa pilha e matrizes parcialmente */
                for (u=0;u<26;u++)
                {
                    for (v=0;v<26;v++)
	                 matriz[u][v]=INT_MAX >> 2;
		    matriz[u][u]=0;
                    conta[u] = 0;
                }
                /* limpa buffer */
                memset(buffer,0,100);
                idx=0;
                SOMA=0;
                continue;
	    }
	    /* armazena aresta no grafo */
	    /* 'a' em ASCII = 97 */
	    buffer[idx]='\0';
            ini = ((int) buffer[0]) - 97;
            fim = ((int) buffer[idx-1]) - 97;
            SOMA += idx;
            conta[ini]++;
            conta[fim]++;
            if ((matriz[ini][fim] == 0) || (idx < matriz[ini][fim]))
            {
	        matriz[ini][fim]=idx;
	        matriz[fim][ini]=idx;
            }
            /* limpa buffer */
            memset(buffer,0,100);
            idx=0;
	}
    }
    /* limpa pilha e matrizes */
    for (u=0;u<26;u++)
        free(matriz[u]);	
    free(matriz);
    free(conta);
    return 0;
}

