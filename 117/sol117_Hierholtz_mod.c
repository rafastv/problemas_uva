#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>

typedef int bool;
#define true 1
#define false 0

/* TODO */
/* implementar funcao para unir Caminhos no grafo */

void* PILHA_TAMANHO = NULL;

typedef struct pilha
{
    int numero;
    struct pilha *anterior;
} Tpilha;

typedef struct caminho
{
    Tpilha arestas;
    int valor;
} Tcaminho;

void iniciaPilha(Tpilha *topo)
{
    /* guarda informacoes da pilha */
    topo->numero = 0;
    topo->anterior = NULL;
}

void iniciaCaminho(Tcaminho *c)
{
    iniciaPilha(&c->arestas);
    c->valor = 0;
}

void push(Tpilha *topo, int numero)
{
    Tpilha *p = malloc(sizeof(Tpilha));
    p->numero = numero;
    p->anterior = topo->anterior;
    topo->anterior = p;
    topo->numero++;
}

int pop(Tpilha *topo)
{
    int ultimo_elemento = -1;
    Tpilha *velho_topo = topo->anterior;
    if (velho_topo != NULL)
    {
        ultimo_elemento = velho_topo->numero;
        topo->anterior = velho_topo->anterior;
	topo->numero--;
        free(velho_topo);
    }
    return ultimo_elemento;
}

void limpaPilha(Tpilha *topo)
{
    while (topo->anterior != NULL)
	pop(topo);
}

void imprimePilha(Tpilha *topo)
{
    Tpilha *tmp = topo->anterior;
    while (tmp != NULL)
    {
	printf("%d ", tmp->numero);
	tmp = tmp->anterior;
    }
    printf(" total: %d\n", topo->numero);
}

bool naPilha(Tpilha *topo, int numero)
{
    Tpilha *tmp = topo->anterior;
    bool saida = false;
    while ((tmp != NULL) && (saida == false))
    {
	saida = (numero == tmp->numero);
	tmp = tmp->anterior;
    }
    return saida;
}

int maiorGrau(Tpilha *topo, Tpilha *mt_adj)
 {
    Tpilha *tmp = topo->anterior;
    int saida = -1, max = 0;
    while (tmp != NULL)
    {
	if (mt_adj[tmp->numero].numero > max)
	{
	    saida = tmp->numero;
	    max = mt_adj[tmp->numero].numero;
	}
	tmp = tmp->anterior;
    }
    return saida;
}

bool arestaNaPilha(Tpilha *topo, int a, int b)
{
    Tpilha *aux1 = topo->anterior;
    if (aux1 == NULL)
	return false;
    Tpilha *aux2 = topo->anterior->anterior;
    if (aux2 == NULL)
	return false;
    bool saida = false;
    while ((aux1 != NULL) && (aux2!= NULL) && (saida == false))
    {
        saida = ((a == aux1->numero) && (b == aux2->numero)) ||
	        ((b == aux1->numero) && (a == aux2->numero));
	aux1 = aux1->anterior;
	aux2 = aux2->anterior;
    }
    return saida;
}

bool todasArestas(Tcaminho *c, Tpilha *arestas)
{
    Tpilha *aux1 = arestas->anterior;
    Tpilha *aux2 = arestas->anterior->anterior;
    int a, b;
    bool saida = true;
    while ((aux1 != NULL) && (aux2!= NULL) && saida)
    {
	a = aux1->numero;
	b = aux2->numero;
	saida = arestaNaPilha(&c->arestas,a,b);
        aux1 = aux2->anterior;
	if (aux1 == NULL)
	    break;
        aux2 = aux2->anterior->anterior;
	if (!saida)
	printf("NAO ENCONTREI %c %c\n", (char)(a+97),(char)(b+97));
    }
    return saida;
}

bool ehCiclo(Tpilha *c)
{
    c = c->anterior;
    int primeiro = c->numero;
    while (c->anterior != NULL)
        c = c->anterior;
    return (c->numero == primeiro);
}

void imprimeCaminho(Tpilha *c)
{
    c = c->anterior;
    while (c->anterior != NULL)
    {
        printf("%c ",(char)(c->numero+97));
        c = c->anterior;
    }
    if (c != NULL)
        printf("%c\n",(char)(c->numero+97));
}

void duplicaCaminho(Tcaminho *c, Tpilha *mt_adj)
{
    Tpilha tmp, aux, *d;
    int v;
    iniciaPilha(&tmp);
    iniciaPilha(&aux);
    d = &c->arestas;
    while (d->numero > 0)
    {
        v = pop(d);
	push(&tmp, v);
	push(&aux, v);
    }
    /* ignora primeiro elemento pilha */
    v = pop(&tmp);
    c->arestas.anterior = aux.anterior;
    c->arestas.numero = aux.numero;
    /* corrige vizinhos */
    d = c->arestas.anterior;
    while (d->anterior != NULL)
    {
        pop(&mt_adj[d->numero]);
	d = d->anterior;
    }
    aux.anterior = NULL;
    aux.numero = 0;
    while (tmp.anterior != NULL)
    {
         v = pop(&tmp);
	 push(&c->arestas, v);
    }
    imprimeCaminho(&c->arestas);
}

void calculaValorCaminho(Tcaminho *c, int **matriz)
{
    Tpilha *d;
    int i, j;
    d = c->arestas.anterior;
    i = d->numero;
    c->valor=0;
    while (d->anterior!=NULL)
    {
	d = d->anterior;
	j = d->numero;
	printf("ARESTA %c %c\n",(char)(i+97),(char)(j+97));
	c->valor += matriz[i][j];
	i = j;
    }
}


Tcaminho* criaMenorCaminho(int origem_aresta, int vertice_atual, int fim_ciclo, int **matriz, Tpilha *mt_adj)
{
   Tcaminho *caminho = malloc(sizeof(Tcaminho));
   int quantidade_vizinhos = mt_adj[vertice_atual].numero;
   iniciaCaminho(caminho);
   if ((vertice_atual == fim_ciclo) && (origem_aresta > -1))
   {
       printf("CHEGUEI AO FIM DE UM CAMINHO %c\n", (char)(fim_ciclo+97));
       push(&caminho->arestas, vertice_atual); 
       return caminho;
   }
   if (quantidade_vizinhos == 0)
   {
       printf("CHEGUEI AO FIM DE UM CAMINHO SEM VIZINHOS %c\n", (char)(fim_ciclo+97));
       caminho->valor = INT_MAX >> 1;
       push(&caminho->arestas, vertice_atual); 
       return caminho;
   }
   Tcaminho *aux, tmp[quantidade_vizinhos];
   Tpilha visitados, salva;
   int vizinho, novo_vizinho, i, pos, soma, conta = 0;
   iniciaPilha(&visitados);
   iniciaPilha(&salva);
   printf("===== INICIO LOOP DE QUEM EH O MENOR? %c =====\n", (char)(vertice_atual+97));

   printf("QUANTIDADE VIZINHOS %d\n", quantidade_vizinhos);
   while (conta < quantidade_vizinhos)
   {
       vizinho = pop(&mt_adj[vertice_atual]);
       /* nao encontrei vizinho nao visitado */
       if (vizinho < 0)
	   break;
       /* encontra vizinho nao visitado */
       while (naPilha(&visitados, vizinho))
       {
          novo_vizinho = pop(&mt_adj[vertice_atual]);
          push(&salva, vizinho);
	  if (novo_vizinho > -1)
	      vizinho = novo_vizinho;
       }
       /* restaura lista */
       while (salva.numero > 0)
       {
          novo_vizinho = pop(&salva);
          push(&mt_adj[vertice_atual], novo_vizinho);
       }
       push(&visitados, vizinho);
       iniciaCaminho(&tmp[conta]);
       if (vizinho != origem_aresta) 
       {
           /* cria caminho a partir de vizinho diferente da origem */
           aux = criaMenorCaminho(vertice_atual, vizinho, fim_ciclo, matriz, mt_adj);
	   /* copia de aux para tmp */
	   tmp[conta].valor = matriz[vertice_atual][vizinho] + aux->valor;
           tmp[conta].arestas.anterior = aux->arestas.anterior;
           tmp[conta].arestas.numero = aux->arestas.numero;
	   if (tmp[conta].arestas.numero == 0)
	   {
	       printf("1 INSERI %c (%d)\n", (char)(vizinho+97),conta);
               push(&tmp[conta].arestas, vizinho);
	   } 
	   printf("1 CAMINHO ENCONTRADO A PARTIR DE %c (%d)\n", (char)(fim_ciclo+97),conta);
	   imprimeCaminho(&tmp[conta].arestas);
	   if (origem_aresta > -1)
	   printf("1 ORIGEM_ARESTA %c\n", (char)(origem_aresta+97));
	   printf("1 VERTICE_ATUAL %c\n", (char)(vertice_atual+97));
	   printf("1 VIZINHO %c\n", (char)(vizinho+97));
	   printf("1 VALOR %d\n", tmp[conta].valor);
	   printf("1 TAMANHO %d\n", tmp[conta].arestas.numero);
           push(&tmp[conta].arestas, vertice_atual);
	   printf("1 INSERI %c (%d)\n", (char)(vertice_atual+97),conta);
	   free(aux);
       }
       else
       {
	    printf("2 FALSO CAMINHO ENCONTRADO A PARTIR DE %c (%d)\n", (char)(fim_ciclo+97),conta);
	   printf("2 ORIGEM_ARESTA %c\n", (char)(origem_aresta+97));
	   printf("2 VERTICE_ATUAL %c\n", (char)(vertice_atual+97));
	   printf("2 VIZINHO %c\n", (char)(vizinho+97));
	   printf("2 VALOR %d\n", tmp[conta].valor);
	   printf("2 TAMANHO %d\n", tmp[conta].arestas.numero);
	   printf("2 CAMINHO ENCONTRADO A PARTIR DE %c (%d)\n", (char)(fim_ciclo+97),conta);
	   tmp[conta].valor = INT_MAX >> 1;
	   /* pega o proximo vertice adjacente */
           vizinho = pop(&mt_adj[vertice_atual]);
	   /* coloca vertice de volta atras do prox vertice */
	   printf("2 INSERI %c (%d)\n", (char)(origem_aresta+97),conta);
           push(&mt_adj[vertice_atual], origem_aresta);
	   if (vizinho > -1){
	      
	       printf("2 INSERI %c (%d)\n", (char)(vizinho+97),conta);
               push(&mt_adj[vertice_atual], vizinho);}
       }
       conta++;
   }
   quantidade_vizinhos = conta;
   limpaPilha(&visitados);
   /* descobre quem eh o menor */
   pos = -1;
   soma = INT_MAX;
   for (i=0;i<quantidade_vizinhos;i++)
   {
       printf("VALOR EM %d : %d\n", i, tmp[i].valor);
       printf("ARESTAS EM TMP %d: %d\n", i, tmp[i].arestas.numero);
       if (tmp[i].arestas.numero > 0)
           imprimeCaminho(&tmp[i].arestas);
       if ((tmp[i].valor < soma) && 
	  ((tmp[i].arestas.numero>0) || (quantidade_vizinhos==1)))
       {
	   pos = i;
	   soma = tmp[i].valor;
       }
   }
   printf("O MENOR EH %d : %d\n", pos, tmp[pos].valor);
   if (tmp[pos].arestas.numero > 0)
         imprimeCaminho(&tmp[pos].arestas);
   /* restaura caminhos */
   int origem;
   for (i=0;i<quantidade_vizinhos;i++)
   {
       if (i == pos)
	   continue;
       origem = -1;
       while (tmp[i].arestas.numero > 0)
       {
	   vizinho = pop(&tmp[i].arestas);
	   if (origem >= 0){
	       push(&mt_adj[origem], vizinho);
	       printf("ADICIONEI %c em %c\n", (char)(vizinho+97), (char)(origem+97));
	   } origem = vizinho;
       }
       printf("LIMPEI %d %d\n", i, tmp[i].arestas.numero); 
   }
   /* retorna caminho menor */
   caminho->valor = tmp[pos].valor;
   caminho->arestas.anterior = tmp[pos].arestas.anterior;
   caminho->arestas.numero = tmp[pos].arestas.numero;
   tmp[pos].arestas.anterior = NULL;
   tmp[pos].arestas.numero = 0;
   if (caminho->arestas.anterior != NULL)
       imprimeCaminho(&caminho->arestas);
   printf("===== FIM DO LOOP DE QUEM EH O MENOR? %c =====\n", (char)(vertice_atual+97));
   return caminho;
}

Tpilha* criaCaminho(int vertice, Tpilha *mt_adj)
{
   Tpilha *caminho = malloc(sizeof(Tpilha));
   int v_atual = vertice, vizinho = -1;
   iniciaPilha(caminho);
   push(caminho, vertice); 
   while (vizinho != vertice)
   {
       vizinho = pop(&mt_adj[v_atual]);
       v_atual = vizinho; 
       push(caminho, v_atual); 
   }
   return caminho;
}

/* Algoritmo de Hierholzer 1873 */
Tcaminho* algoritmoDeHierholzer(Tpilha *arestas, Tpilha *mt_adj, int **matriz, int qt_arestas)
{
    int u,v;
    v = arestas->anterior->numero;
    v = maiorGrau(arestas,mt_adj);
    /* algoritmo original, caminho aleatorio */
    Tcaminho *h = NULL, *c; /* = criaCaminho(v,mt_adj); */
    Tpilha tmp;
    c = criaMenorCaminho(-1, v, v, matriz, mt_adj);
    if (!ehCiclo(&c->arestas))
    {
	printf("INICIO DUPLICA\n");
	duplicaCaminho(c, mt_adj);
	calculaValorCaminho(c, matriz);
	printf("FIM DUPLICA\n");
    }

    qt_arestas -= (c->arestas.numero-1);
    printf("FIM PRIMEIRO CAMINHO a partir de %c\n", (char)(v+97));
    printf ("QUANTIDADE ARESTAS %d\n", qt_arestas);
    imprimeCaminho(&c->arestas);
    /* saida nao inclusa no algoritmo original */
    printf("PILHA ARESTAS\n");
    imprimePilha(arestas);
    if (todasArestas(c, arestas))
    {
        printf("VALOR TOTAL %d\n", c->valor);
        imprimeCaminho(&c->arestas);
        printf("TODOS OS CAMINHOS FORAM ENCONTRADOS\n");
	return c;
    }
    iniciaPilha(&tmp);
    while (qt_arestas > 0)
    {
        v = pop(&c->arestas);
        u = maiorGrau(&c->arestas,mt_adj);
        while (v!=u)
        {
	 // printf("LOOP INFINITO");
            push(&tmp, v);
            v = pop(&c->arestas);
        }
	if (mt_adj[u].numero == 0){
        /* une c com tmp */
        while (tmp.numero > 0)
        {
            u = pop(&tmp);
            push(&c->arestas, u);
        }
	    break;}
        /* algoritmo original, caminho aleatorio */
        h = criaMenorCaminho(-1, v, v, matriz, mt_adj);
    if (!ehCiclo(&h->arestas))
    {
	printf("INICIO DUPLICA\n");
	duplicaCaminho(h, mt_adj);
	calculaValorCaminho(h, matriz);
	printf("FIM DUPLICA\n");
    }
	printf("CAMINHO ACHADO\n");
        imprimeCaminho(&h->arestas);
        qt_arestas -= (h->arestas.numero-1);
        /* une tmp com h */
        while (h->arestas.numero > 0)
        {
            u = pop(&h->arestas);
            push(&tmp, u);
        }
        /* une c com tmp */
        while (tmp.numero > 0)
        {
            u = pop(&tmp);
            push(&c->arestas, u);
        }
        c->valor += h->valor;
        printf("FIM CAMINHO a partir de %c\n", (char)(v+97));
        printf ("QUANTIDADE ARESTAS %d\n", qt_arestas);
        free(h);
        imprimeCaminho(&c->arestas);
	/* parada nao inclusa no algoritmo original */
        if (todasArestas(c, arestas))
	    break;
	else
	    printf("NAO TEM TODAS AS ARESTAS?\n");
    }
    imprimeCaminho(&c->arestas);
    if (c->valor > (INT_MAX >> 1))
	c->valor -= (INT_MAX >> 1);
    printf("VALOR TOTAL %d\n", c->valor);
    printf("TODOS OS CAMINHOS FORAM ENCONTRADOS\n");
    return c;
}

void imprimirMatriz(int **M)
{
    int u,v;
    for (u=0;u<26;u++)
    {
        for (v=0;v<26;v++)
            printf("(%d %d): %d ",u,v,M[u][v]);
        printf("\n");
    }
}

int main()
{
    int a = 0;
    PILHA_TAMANHO = (void*)&a;    
    /* para mais de uma aresta entre dois vertices */
    int ZIG_ZAG = 0;
    char c;
    int u,idx,ini,fim,qt_arestas;
    int **matriz;
    char buffer[100];
    Tpilha pilha, *mt_adj = malloc(sizeof(Tpilha)*26);
    Tcaminho *saida;
    /* inicia uma matriz base para todos os grafos possiveis */
    /* (int) 'a' - (int) 'z' = 26 letras no alfabeto */
    matriz = (int**) malloc(sizeof(int*)*26);
    for (u=0;u<26;u++)
    {
        matriz[u] = (int*) malloc(sizeof(int)*26);
        memset(matriz[u],0,sizeof(int)*26);
        iniciaPilha(&mt_adj[u]);
    }
    /* caso linha comece com vazio */
    idx = 0;
    buffer[0] = 0;
    qt_arestas = 0;
    iniciaPilha(&pilha);
    while ((c  = fgetc(stdin)) != EOF)
    {
	if ((c!=' ') && (c!='\n') && (c!='\t'))
	{
	    buffer[idx] = c;
	    idx++;
	}
	else
	{
	    if ((int) buffer[0]==0)
		continue;
            if (strcmp(buffer, "deadend")==0)
            {
                /* inicia algoritmo */
                saida = algoritmoDeHierholzer(&pilha,mt_adj,matriz,qt_arestas);
		imprimeCaminho(&saida->arestas);
                printf("TOTAL %d\n", ZIG_ZAG + saida->valor);
		//imprimePilha(&pilha);
                /* limpa pilha e matrizes parcialmente */
                for (u=0;u<26;u++)
                {
                    memset(matriz[u],0,sizeof(int)*26);
    		    limpaPilha(&mt_adj[u]);
                    iniciaPilha(&mt_adj[u]);
                }
    		limpaPilha(&pilha);
                limpaPilha(&saida->arestas);
                free(saida);
                /* limpa buffer */
                memset(buffer,0,100);
                idx=0;
                qt_arestas=0;
                continue;
	    }
	    /* armazena aresta no grafo */
	    /* 'a' em ASCII = 97 */
	    buffer[idx]='\0';
            ini = ((int) buffer[0]) - 97;
            fim = ((int) buffer[idx-1]) - 97;
            if ((matriz[ini][fim] == 0) || (idx < matriz[ini][fim]))
            {
		ZIG_ZAG += matriz[ini][fim];
	        matriz[ini][fim]=idx;
	        matriz[fim][ini]=idx;
                /* torna o grafo euleriano */
                push(&mt_adj[ini], fim);
                push(&mt_adj[fim], ini);
                qt_arestas+=2;
                /* armazena lista de arestas */
	        push(&pilha, ini);
	        push(&pilha, fim);
            }
            else
            {
		ZIG_ZAG += idx;
            }
            /* limpa buffer */
            memset(buffer,0,100);
            idx=0;
	}
    }
    /* limpa pilha e matrizes */
    limpaPilha(&pilha);
    for (u=0;u<26;u++)
    {
        free(matriz[u]);	
        limpaPilha(&mt_adj[u]);
    }
    free(matriz);
    free(mt_adj);
    return 0;
}

