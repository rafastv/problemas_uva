#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef int bool;
#define true 1
#define false 0

/* TODO */
/* implementar funcao para unir Caminhos no grafo */

typedef struct pilha
{
    int numero;
    struct pilha *anterior;
} Tpilha;

typedef struct caminho
{
    Tpilha arestas;
    int valor;
} Tcaminho;

void iniciaPilha(Tpilha *topo)
{
    /* guarda informacoes da pilha */
    topo->numero = 0;
    topo->anterior = NULL;
}

void iniciaCaminho(Tcaminho *c)
{
    iniciaPilha(&c->arestas);
    c->valor = 0;
}

void push(Tpilha *topo, int numero)
{
    Tpilha *p = malloc(sizeof(Tpilha));
    p->numero = numero;
    p->anterior = topo->anterior;
    topo->anterior = p;
    topo->numero++;
}

int pop(Tpilha *topo)
{
    int ultimo_elemento = -1;
    Tpilha *velho_topo = topo->anterior;
    if (velho_topo != NULL)
    {
        ultimo_elemento = velho_topo->numero;
        topo->anterior = velho_topo->anterior;
	topo->numero--;
        free(velho_topo);
    }
    return ultimo_elemento;
}

void limpaPilha(Tpilha *topo)
{
    while (topo->anterior != NULL)
	pop(topo);
}

void imprimePilha(Tpilha *topo)
{
    Tpilha *tmp = topo->anterior;
    while (tmp != NULL)
    {
	printf("%d ", tmp->numero);
	tmp = tmp->anterior;
    }
    printf(" total: %d\n", topo->numero);
}

bool naPilha(Tpilha *topo, int numero)
{
    Tpilha *tmp = topo->anterior;
    bool saida = false;
    while ((tmp != NULL) && (saida == false))
    {
	saida = (numero == tmp->numero);
	tmp = tmp->anterior;
    }
    return saida;
}

void imprimeCaminho(Tpilha *c)
{
    c = c->anterior;
    while (c->anterior != NULL)
    {
        printf("%c ",(char)(c->numero+97));
        c = c->anterior;
    }
    if (c != NULL)
        printf("%c\n",(char)(c->numero+97));
}

Tcaminho* criaMenorCaminho(int origem_aresta, int vertice_atual, int fim_ciclo, Tpilha *mt_adj, int **matriz)
{
   Tcaminho *caminho = malloc(sizeof(Tcaminho));
   iniciaCaminho(caminho);
   push(&caminho->arestas, vertice_atual); 
   if ((vertice_atual == fim_ciclo) && (origem_aresta > -1))
       return caminho;
   Tcaminho *aux, tmp[mt_adj[vertice_atual].numero];
   int vizinho, i, pos, soma, conta = 0, numero = mt_adj[vertice_atual].numero;
   while (mt_adj[vertice_atual].numero != conta)
   {
       vizinho = pop(&mt_adj[vertice_atual]);
       iniciaCaminho(&tmp[mt_adj[vertice_atual].numero]);
       if (vizinho != origem_aresta) 
       {
           /* cria caminho a partir de vizinho diferente da origem */
           aux = criaMenorCaminho(vertice_atual, vizinho, fim_ciclo, mt_adj, matriz);
	   /* copia de aux para tmp */
	   tmp[mt_adj[vertice_atual].numero].valor = matriz[vertice_atual][vizinho] + aux->valor;
           tmp[mt_adj[vertice_atual].numero].arestas.anterior = aux->arestas.anterior;
           tmp[mt_adj[vertice_atual].numero].arestas.numero = aux->arestas.numero;
	   free(aux);
       }
       else
       {
	   /* pega o proximo vertice adjacente */
           vizinho = pop(&mt_adj[vertice_atual]);
	   /* coloca vertice de volta atras do prox vertice */
           push(&mt_adj[vertice_atual], origem_aresta);
	   if (vizinho > -1)
               push(&mt_adj[vertice_atual], vizinho);
	   /* incrementa a contagem da lista que nao zera */
	   conta++;
       }
   }
   /* descobre quem eh o menor */
   pos = 0;
   soma = tmp[pos].valor; 
   for (i=1;i<numero;i++)
       if (tmp[i].valor < soma)
       {
	   pos = i;
	   soma = tmp[i].valor;
       }
   /* restaura caminhos */
   for (i=0;i<numero;i++)
   {
       if (i == pos)
	   continue;
       while (tmp[i].arestas.numero > 0)
       {
	   vizinho = pop(&tmp[i].arestas);
	   if (origem_aresta < 0)
	       push(&mt_adj[vertice_atual], vizinho);
	   else
	       push(&mt_adj[origem_aresta], vizinho);
	   origem_aresta = vizinho;
       }
   }
   /* retorna caminho menor */
   caminho->valor = tmp[pos].valor;
   caminho->arestas.anterior = tmp[pos].arestas.anterior;
   caminho->arestas.numero = tmp[pos].arestas.numero;
   return caminho;
}

Tpilha* criaCaminho(int vertice, Tpilha *mt_adj)
{
   Tpilha *caminho = malloc(sizeof(Tpilha));
   int v_atual = vertice, vizinho = -1;
   iniciaPilha(caminho);
   push(caminho, vertice); 
   while (vizinho != vertice)
   {
       vizinho = pop(&mt_adj[v_atual]);
       v_atual = vizinho; 
       push(caminho, v_atual); 
   }
   return caminho;
}

/* Algoritmo de Hierholzer 1873 */
Tpilha* algoritmoDeHierholzer(Tpilha *vertices, Tpilha *mt_adj, int **matriz, int qt_arestas)
{
    int u,v;
    v = vertices->anterior->numero;
    /* algoritmo original, caminho aleatorio */
    Tpilha tmp, *h = NULL, *c = criaCaminho(v,mt_adj); 
    qt_arestas -= (c->numero-1);
    imprimeCaminho(c);
    iniciaPilha(&tmp);
    while (qt_arestas > 0)
    {
        v = pop(c);
        while (mt_adj[v].numero == 0)
        {
            push(&tmp, v);
            v = pop(c);
        }
        /* algoritmo original, caminho aleatorio */
        h = criaCaminho(v,mt_adj);
        imprimeCaminho(h);
        qt_arestas -= (h->numero-1);
        /* une c com h */
        while (h->numero > 0)
        {
            u = pop(h);
            push(c, u);
        }
        /* une c com tmp */
        while (tmp.numero > 0)
        {
            u = pop(&tmp);
            push(c, u);
        }
        free(h);
    }
    printf("TODOS OS CAMINHOS FORAM ENCONTRADOS\n");
    return c;
}

void imprimirMatriz(int **M)
{
    int u,v;
    for (u=0;u<26;u++)
    {
        for (v=0;v<26;v++)
            printf("(%d %d): %d ",u,v,M[u][v]);
        printf("\n");
    }
}

int main()
{
    char c;
    int u,idx,ini,fim,qt_arestas;
    int **matriz;
    char buffer[100];
    Tpilha mt_adj[26];
    Tpilha pilha, *saida;
    /* inicia uma matriz base para todos os grafos possiveis */
    /* (int) 'a' - (int) 'z' = 26 letras no alfabeto */
    matriz = (int**) malloc(sizeof(int*)*26);
    for (u=0;u<26;u++)
    {
        matriz[u] = (int*) malloc(sizeof(int)*26);
        memset(matriz[u],0,sizeof(int)*26);
        iniciaPilha(&mt_adj[u]);
    }
    /* caso linha comece com vazio */
    idx = 0;
    buffer[0] = 0;
    qt_arestas = 0;
    iniciaPilha(&pilha);
    while ((c  = getc(stdin)) != EOF)
    {
	if ((c!=' ') && (c!='\n') && (c!='\t'))
	{
	    buffer[idx] = c;
	    idx++;
	}
	else
	{
	    if ((int) buffer[0]==0)
		continue;
            if (strcmp(buffer, "deadend")==0)
            {
                /* inicia algoritmo */
                saida = algoritmoDeHierholzer(&pilha,mt_adj,matriz,qt_arestas);
		imprimeCaminho(saida);
		imprimePilha(&pilha);
                /* limpa pilha e matrizes parcialmente */
                for (u=0;u<26;u++)
                {
                    memset(matriz[u],0,sizeof(int)*26);
    		    limpaPilha(&mt_adj[u]);
                }
    		limpaPilha(&pilha);
                limpaPilha(saida);
                free(saida);
                /* limpa buffer */
                memset(buffer,0,100);
                idx=0;
                qt_arestas=0;
                continue;
	    }
	    /* armazena aresta no grafo */
	    /* 'a' em ASCII = 97 */
	    buffer[idx]='\0';
            ini = ((int) buffer[0]) - 97;
            fim = ((int) buffer[idx-1]) - 97;
	    matriz[ini][fim]=idx;
	    matriz[fim][ini]=idx;
            push(&mt_adj[ini], fim);
            push(&mt_adj[fim], ini);
            /* torna o grafo euleriano */
            qt_arestas+=2;
	    if (!naPilha(&pilha, ini))
		push(&pilha, ini);
	    if (!naPilha(&pilha, fim))
		push(&pilha, fim);
            /* limpa buffer */
            memset(buffer,0,100);
            idx=0;
	}
    }
    /* limpa pilha e matrizes */
    limpaPilha(&pilha);
    for (u=0;u<26;u++)
    {
        free(matriz[u]);	
        limpaPilha(&mt_adj[u]);
    }
    free(matriz);
    return 0;
}

