#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>

typedef int bool;
#define true 1
#define false 0

typedef struct pilha
{
    int numero;
    struct pilha *anterior;
} Tpilha;

void iniciaPilha(Tpilha *topo)
{
    /* guarda informacoes da pilha */
    topo->numero = 0;
    topo->anterior = NULL;
}

void push(Tpilha *topo, int numero)
{
    Tpilha *p = malloc(sizeof(Tpilha));
    p->numero = numero;
    p->anterior = topo->anterior;
    topo->anterior = p;
    topo->numero++;
}

int pop(Tpilha *topo)
{
    int ultimo_elemento = -1;
    Tpilha *velho_topo = topo->anterior;
    if (velho_topo != NULL)
    {
        ultimo_elemento = velho_topo->numero;
        topo->anterior = velho_topo->anterior;
	topo->numero--;
        free(velho_topo);
    }
    return ultimo_elemento;
}

void limpaPilha(Tpilha *topo)
{
    while (topo->anterior != NULL)
	pop(topo);
}

void imprimePilha(Tpilha *topo)
{
    Tpilha *tmp = topo->anterior;
    while (tmp != NULL)
    {
	printf("%d ", tmp->numero);
	tmp = tmp->anterior;
    }
    printf(" total: %d\n", topo->numero);
}

bool naPilha(Tpilha *topo, int numero)
{
    Tpilha *tmp = topo->anterior;
    bool saida = false;
    while ((tmp != NULL) && (saida == false))
    {
	saida = (numero == tmp->numero);
	tmp = tmp->anterior;
    }
    return saida;
}

int menorNaPilha(Tpilha *topo, int *distancia)
{
    Tpilha *tmp = topo->anterior;
    int saida = INT_MAX;
    while (tmp != NULL)
    {
        if (distancia[tmp->numero] < saida)
            saida = tmp->numero;
	tmp = tmp->anterior;
    }
    return saida;
}

void removeDaPilha(Tpilha *topo, int v)
{
    int saida = INT_MAX;
    Tpilha tmp;

    iniciaPilha(&tmp); 
    saida = pop(topo);
    while (saida != v)
    {
        push(&tmp, saida);
        saida = pop(topo);
    }
    while (tmp.numero > 0)
    {
        v = pop(&tmp);
        push(topo, v); 
    }
}

/* Algoritmo Dijkstra 1959: nao resolveu (qual o problema?) */
int algoritmoDijkstra(int origem, int destino, Tpilha *Q, Tpilha *mt_adj, int **matriz)
{
    int i, v, u, valor, distancia[26], pai[26]; 
    Tpilha tmp, aux;
    iniciaPilha(&tmp);
    iniciaPilha(&aux);
    for (i=0;i<26;i++)
    {
       distancia[i] = INT_MAX;
       pai[i] = i;
    }
    distancia[origem] = 0;
    pai[origem] = origem;
    while (Q->numero > 0)
    {
        v = menorNaPilha(Q, distancia);
        removeDaPilha(Q, v);
        push(&aux, v);
        if (v == destino)
            break;
        while (mt_adj[v].numero > 0)
        {
            u = pop(&mt_adj[v]);
            if (naPilha(Q, u))
            {
               valor = distancia[v] + matriz[v][u];
               if (valor <= distancia[u])
               {
                   distancia[u]=valor;
                   pai[u]=v;
               }
            }
            push(&tmp, u);
        }
        while (tmp.numero > 0)
        {
            u = pop(&tmp);
            push(&mt_adj[v], u);
        }
    }
    while (aux.numero > 0)
    {
        u = pop(&aux);
        push(Q, u);
    }
    return distancia[destino];
}

int main()
{
    int ZIG_ZAG = 0;
    int IMPAR_1[26], IMPAR_2[26];
    char c;
    int u,v,idx,ini,fim;
    int **matriz;
    int *conta;
    char buffer[100];
    Tpilha pilha, *mt_adj = malloc(sizeof(Tpilha)*26);
    int saida;
    /* inicia uma matriz base para todos os grafos possiveis */
    /* (int) 'a' - (int) 'z' = 26 letras no alfabeto */
    matriz = (int**) malloc(sizeof(int*)*26);
    conta = malloc(sizeof(int)*26);
    for (u=0;u<26;u++)
    {
        matriz[u] = (int*) malloc(sizeof(int*)*26);
        conta[u] = 0;
        memset(matriz[u],0,sizeof(int)*26);
        iniciaPilha(&mt_adj[u]);
        IMPAR_1[u]=-1;
        IMPAR_2[u]=-1;
    }
    /* caso linha comece com vazio */
    idx = 0;
    buffer[0] = 0;
    iniciaPilha(&pilha);
    while ((c  = fgetc(stdin)) != EOF)
    {
	if ((c!=' ') && (c!='\n') && (c!='\t'))
	{
	    buffer[idx] = c;
	    idx++;
	}
	else
	{
	    if ((int) buffer[0]==0)
		continue;
            if (strcmp(buffer, "deadend")==0)
            {
                /* descobre impares */
                v = 0;
                for (u=0;u<26;u++)
                {
                    if ((conta[u] % 2 == 1) && (IMPAR_1[v] < 0))
                    {
                        IMPAR_1[v] = u;
                        continue;
                    }
                    if ((conta[u] % 2 == 1) && (IMPAR_2[v] < 0))
                        IMPAR_2[v] = u;
                    if ((IMPAR_1[v] >= 0) && (IMPAR_2[v] >= 0))
                        v++;
                }
                /* inicia algoritmo */
                if (v == 0)
                    printf("%d\n", ZIG_ZAG);
                else
                {
                    saida = 0;
                    for (u=0;u<v;u++)
                        saida += algoritmoDijkstra(IMPAR_1[u], IMPAR_2[u], &pilha, mt_adj,matriz);
                    printf("%d\n", ZIG_ZAG + saida);
                 }
                /* limpa pilha e matrizes parcialmente */
                for (u=0;u<26;u++)
                {
                    memset(matriz[u],0,sizeof(int*)*26);
                    conta[u] = 0;
    		    limpaPilha(&mt_adj[u]);
                    iniciaPilha(&mt_adj[u]);
                    IMPAR_1[u]=-1;
                    IMPAR_2[u]=-1;
                }
    		limpaPilha(&pilha);
                /* limpa buffer */
                memset(buffer,0,100);
                idx=0;
                ZIG_ZAG = 0;
                continue;
	    }
	    /* armazena aresta no grafo */
	    /* 'a' em ASCII = 97 */
	    buffer[idx]='\0';
            ini = ((int) buffer[0]) - 97;
            fim = ((int) buffer[idx-1]) - 97;
            ZIG_ZAG += idx;
            conta[ini]++;
            conta[fim]++;
            if (matriz[ini][fim] == 0) 
            {
	        matriz[ini][fim]=idx;
	        matriz[fim][ini]=idx;
                /* torna o grafo euleriano */
                push(&mt_adj[ini], fim);
                push(&mt_adj[fim], ini);
                /* armazena lista de vertices */
                if (!naPilha(&pilha, ini))
	            push(&pilha, ini);
                if (!naPilha(&pilha, fim))
	            push(&pilha, fim);
            }
            else
            {
                if (idx < matriz[ini][fim])
                {
	            matriz[ini][fim]=idx;
	            matriz[fim][ini]=idx;
                }
            }
            /* limpa buffer */
            memset(buffer,0,100);
            idx=0;
	}
    }
    /* limpa pilha e matrizes */
    limpaPilha(&pilha);
    for (u=0;u<26;u++)
    {
        free(matriz[u]);	
        limpaPilha(&mt_adj[u]);
    }
    free(matriz);
    free(mt_adj);
    free(conta);
    return 0;
}

