#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>

typedef int bool;
#define true 1
#define false 0

#define mod(a,b) (b + (a % b)) % b

const char ALFABETO[26] = {'A','B','C','D','E','F',
                           'G','H','I','J','K','L',
                           'M','N','O','P','Q','R',
                           'S','T','U','V','W','X',
                           'Y','Z'};
int NLETRAS;

void imprimirSaida(char buffer[80])
{
    int i;
    for (i=0; i<strlen(buffer);i++)
    {
	if (((i%64)== 0) && (i>0))
	    printf("\n");
	else
	   if (((i % 4) == 0) && (i>0))
               printf(" ");
        printf("%c", buffer[i]);
    }
    printf("\n");
    printf("%lu\n", strlen(buffer));
}

bool testaPartesJuntas(char buffer[80], int idx, int meio)
{
   int i;
   if (meio < 1)
       return false;
   if (buffer[idx] == buffer[idx-meio])
   {
      bool diferente = false;
      for (i=1; i<meio;i++)
         if (buffer[idx-i] != buffer[idx-meio-i])
	 {
	     diferente = true;
	     break;
	 }
      if (!diferente)
         return true;
   }
   if (meio == 1)
       return false;
   return testaPartesJuntas(buffer, idx, meio-1);
}

bool testaSequenciaValida(char buffer[80], int idx)
{
    int tam = strlen(buffer);
    return !(testaPartesJuntas(buffer, idx, tam>>1));
}


void construirPalavra(char buffer[80], int idx, int ini[1], int fim)
{
    int i, j;
    for (i=0; i<NLETRAS;i++)
    {
         if (ini[0] >= fim)
             break;
         buffer[idx] = ALFABETO[i];
         buffer[idx+1] = '\0'; 
         if (testaSequenciaValida(buffer, idx))
	 {    
             /*printf("%s\n",buffer);*/
             ini[0]++;
             construirPalavra(buffer, idx+1, ini, fim);
	 }
         if (ini[0] >= fim)
             break;
    }
}


int main()
{
    char buffer[80];
    int pos[1] = {0};
    int n, L;
    while ((fscanf(stdin, "%d %d", &n, &L)) != EOF)
    {
        if ((n == L) && (!n))
            break;
        NLETRAS = L;
        construirPalavra(buffer, 0, pos, n);
        imprimirSaida(buffer);
        /* limpa buffer */
        pos[0] = 0;
        memset(buffer, 0, 80);
    }
    return 0;
}

