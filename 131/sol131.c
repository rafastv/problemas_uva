#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>

typedef int bool;
#define true 1
#define false 0

typedef struct pilha
{
    unsigned char numero; /*face_value*/
    unsigned char naipe; /* suits */
    struct pilha *anterior;
} Tpilha;

Tpilha MAO, MONTE; 

void iniciaPilha(Tpilha *topo)
{
    /* guarda informacoes da pilha */
    topo->numero = (unsigned char) 0;
    topo->naipe = 'R'; /** RAIZ OU ROOT **/
    topo->anterior = NULL;
}

void push(Tpilha *topo, unsigned char numero, unsigned char naipe)
{
    Tpilha *p = malloc(sizeof(Tpilha));
    p->numero = numero;
    p->naipe = naipe;
    p->anterior = topo->anterior;
    topo->anterior = p;
    topo->numero++;
}

Tpilha* pop(Tpilha *topo)
{
    Tpilha *ultimo_elemento = topo->anterior;
    if (ultimo_elemento != NULL)
    {
        topo->anterior = ultimo_elemento->anterior;
	ultimo_elemento->anterior = NULL;
	topo->numero--;
    }
    return ultimo_elemento;
}

Tpilha* popN(Tpilha *topo, int n)
{
    Tpilha *saida = malloc(sizeof(Tpilha));
    Tpilha *tmp;
    int conta = 0;
    iniciaPilha(saida);
    while (conta < n)
    {
        tmp = pop(topo);
        push(saida, tmp->numero, tmp->naipe); 
        conta++;
    }
    return saida;
}

void pushN(Tpilha *p1, Tpilha *p2, int n)
{
    Tpilha *tmp;
    int conta = 0;
    while (conta < n)
    {
        tmp = pop(p2);
        push(p1, tmp->numero, tmp->naipe); 
        conta++;
    }
}

void limpaPilha(Tpilha *topo)
{
    while (topo->anterior != NULL)
	free(pop(topo));
}

void imprimePilha(Tpilha *topo)
{
    Tpilha *tmp = topo->anterior;
    printf("%c%c", tmp->numero, tmp->naipe);
    tmp = tmp->anterior;
    while (tmp != NULL)
    {
	printf(" %c%c", tmp->numero, tmp->naipe);
	tmp = tmp->anterior;
    }
}

void imprimeMaoMonte()
{
    printf("Hand: ");
    imprimePilha(&MAO);
    printf(" Deck: ");
    imprimePilha(&MONTE);
    printf(" ");
}

bool naPilha(Tpilha *topo, unsigned char numero, unsigned char naipe)
{
    Tpilha *tmp = topo->anterior;
    bool saida = false;
    while ((tmp != NULL) && (saida == false))
    {
	saida = (numero == tmp->numero) || (naipe == tmp->naipe);
	tmp = tmp->anterior;
    }
    return saida;
}

void salvaCarta(char buffer[3])
{
    if (MAO.numero < 5)
        push(&MAO,   buffer[0], buffer[1]);
    else
        push(&MONTE, buffer[0], buffer[1]);
}

void puxaCartas(int q)
{
    Tpilha *tmp;
    int conta = 0;
    while (conta < q)
    {
        tmp = pop(&MONTE);
        push(&MAO, tmp->numero, tmp->naipe);
        conta++;
    }
}

void retornaCartas(int q)
{
    Tpilha *tmp;
    int conta = 0;
    while (conta < q)
    {
        tmp = pop(&MAO);
        push(&MONTE, tmp->numero, tmp->naipe);
        conta++;
    }
}

void flip(Tpilha *monte)
{
    Tpilha aux1, aux2, *tmp;
    iniciaPilha(&aux1);
    iniciaPilha(&aux2);
    while (monte->numero > 0)
    {
        tmp = pop(monte);
        push(&aux1, tmp->numero, tmp->naipe);
    }
    while (aux1.numero > 0)
    {
        tmp = pop(&aux1);
        push(&aux2, tmp->numero, tmp->naipe);
    }
    while (aux2.numero > 0)
    {
        tmp = pop(&aux2);
        push(monte, tmp->numero, tmp->naipe);
    }
}

void imprimeResultado(int valor)
{
    char *palavra;
    switch (valor)
    {
       case 0: { palavra = "highest-card";    break; } 
       case 1: { palavra = "one-pair";        break; } 
       case 2: { palavra = "two-pairs";       break; } 
       case 3: { palavra = "three-of-a-kind"; break; }
       case 4: { palavra = "straight";        break; } 
       case 5: { palavra = "flush";           break; } 
       case 6: { palavra = "full-house";      break; } 
       case 7: { palavra = "four-of-a-kind";  break; } 
       case 8: { palavra = "straight-flush";  break; } 
    }
    printf("Best hand: %s\n", palavra);
}

int obtemMelhorMao(bool cartas_mesmo_naipe, int maior_sequencia, int carta_max,
                      bool existe_dupla, bool existe_tripla, bool existe_quadrupla)
{
    if ((maior_sequencia > -1) && (cartas_mesmo_naipe))
        return 8;
    else if (existe_quadrupla)
        return 7;
    else if (existe_tripla && existe_dupla)
        return 6;
    else if (cartas_mesmo_naipe)
        return 5;
    else if (maior_sequencia > -1)
        return 4;
    else if (existe_tripla)
        return 3;
    else if (existe_dupla > 1)
        return 2;
    else if (existe_dupla)
        return 1;
    else
        return 0;
}

int registraPontos(Tpilha *topo)
{
    int cartas[14] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    int naipe[4] = {0, 0, 0, 0};
    int i, idn, idv;
    
    Tpilha *carta = topo->anterior;
    while (carta != NULL)
    {
        switch (carta->naipe)
        {
            case 'C': { idn = 0; break; } 
            case 'D': { idn = 1; break; } 
            case 'H': { idn = 2; break; } 
            case 'S': { idn = 3; break; } 
        }
        switch (carta->numero)
        {
            case 'A': { idv =  0; break; } 
            case '2': { idv =  1; break; } 
            case '3': { idv =  2; break; } 
            case '4': { idv =  3; break; } 
            case '5': { idv =  4; break; } 
            case '6': { idv =  5; break; } 
            case '7': { idv =  6; break; } 
            case '8': { idv =  7; break; } 
            case '9': { idv =  8; break; } 
            case 'T': { idv =  9; break; } 
            case 'J': { idv = 10; break; } 
            case 'Q': { idv = 11; break; } 
            case 'K': { idv = 12; break; } 
        }
        naipe[idn]++;       
        cartas[idv]++;
        carta = carta->anterior;
    }
    bool cartas_mesmo_naipe = ((naipe[0] == 5) || (naipe[1] == 5) ||
                               (naipe[2] == 5) || (naipe[3] == 5));
    int existe_dupla = 0;
    bool existe_tripla = false;
    bool existe_quadrupla = false;
    int carta_max = -1;
    int maior_sequencia = -1;
    for (i=0;i<13;i++)
    {
        /* printf("%d ", cartas[i]); */
        if ((i<10) && cartas[i] && cartas[i+1] && 
             cartas[i+2] && cartas[i+3] && cartas[(i+4)%13])
            maior_sequencia = i;      
        existe_dupla = (cartas[i] == 2) ? existe_dupla + 1: existe_dupla;
        existe_tripla |= (cartas[i] == 3);
        existe_quadrupla |= (cartas[i] == 4);
        carta_max = (carta_max > (cartas[i]>0))? i: carta_max;
    }
    /* printf("\n"); */
    return obtemMelhorMao(cartas_mesmo_naipe, maior_sequencia, carta_max, existe_dupla, existe_tripla, existe_quadrupla);
}

int registraNCartas(int n, int m)
{
    int u, v, pts, max_atual = m;
    Tpilha *aux1, *aux2, *aux3, *aux4; 
    switch (n)
    {
        case 1: { 
                  /* printf("1 CARTA\n"); */
                  u = 0;
                  while (u < 5)
                  {
                      /* retira n-esima carta */
                      aux2=popN(&MAO, u);
                      aux1=pop(&MAO);
                      pushN(&MAO, aux2, u);
                      free(aux2);
                      /* testa mao */
                      puxaCartas(n);
                      pts = registraPontos(&MAO);
                      if (pts > max_atual)
                          max_atual = pts;
                      retornaCartas(n);
                      /* restaura pilhas */
                      aux2=popN(&MAO, u);
                      push(&MAO, aux1->numero, aux1->naipe);
                      pushN(&MAO, aux2, u);
                      free(aux2);
                      u++;
                  }
                  break; 
                }
        case 2: { 
                  /* printf("2 CARTAS\n"); */
                  u = 0;
                  while (u < 4)
                  {
                      /* retira n-esima carta */
                      aux2=popN(&MAO, u);
                      aux1=pop(&MAO);
                      pushN(&MAO, aux2, u);
                      free(aux2);
                      v = 0;
                      while (v < (5-u-1))
                      {
                          /* retira n-esima carta */
                          aux3=popN(&MAO, u+v);
                          aux4=pop(&MAO);
                          pushN(&MAO, aux3, u+v);
                          free(aux3);
                          /* testa mao */
                          puxaCartas(n);
                          pts = registraPontos(&MAO);
                          if (pts > max_atual)
                              max_atual = pts;
                          retornaCartas(n);
                          /* restaura pilhas */
                          aux3=popN(&MAO, u+v);
                          push(&MAO, aux4->numero, aux4->naipe);
                          pushN(&MAO, aux3, u+v);
                          free(aux3);
                          v++;
                      }
                      /* restaura pilhas */
                      aux2=popN(&MAO, u);
                      push(&MAO, aux1->numero, aux1->naipe);
                      pushN(&MAO, aux2, u);
                      free(aux2);
                      u++;
                  }
                  break; 
                }
        case 3: { 
                  /* printf("3 CARTAS\n"); */
                  u = 0;
                  while (u < 4)
                  {
                      /* retira n-esima carta */
                      aux2=popN(&MAO, u);
                      aux1=pop(&MAO);
                      pushN(&MAO, aux2, u);
                      free(aux2);
                      v = 0;
                      while (v < (5-u-1))
                      {
                          /* retira n-esima carta */
                          aux3=popN(&MAO, u+v);
                          aux4=pop(&MAO);
                          pushN(&MAO, aux3, u+v);
                          free(aux3);
                          /* testa mao */
                          aux3 = popN(&MAO, 3);
                          push(&MAO, aux1->numero, aux1->naipe);
                          push(&MAO, aux4->numero, aux4->naipe);
                          puxaCartas(n);
                          pts = registraPontos(&MAO);
                          if (pts > max_atual)
                              max_atual = pts;
                          retornaCartas(n);
                          aux4=pop(&MAO);
                          aux1=pop(&MAO);
                          pushN(&MAO, aux3, 3);
                          free(aux3);
                          /* restaura pilhas */
                          aux3=popN(&MAO, u+v);
                          push(&MAO, aux4->numero, aux4->naipe);
                          pushN(&MAO, aux3, u+v);
                          free(aux3);
                          v++;
                      }
                      /* restaura pilhas */
                      aux2=popN(&MAO, u);
                      push(&MAO, aux1->numero, aux1->naipe);
                      pushN(&MAO, aux2, u);
                      free(aux2);
                      u++;
                  }
                  break; 
                }
        case 4: { 
                  /* printf("4 CARTAS\n"); */
                  u = 0;
                  while (u < 5)
                  {
                      /* pega a n-esima carta */
                      aux2=popN(&MAO, u);
                      aux1=pop(&MAO);
                      pushN(&MAO, aux2, u);
                      free(aux2);
                      /* testa mao */
                      aux2 = popN(&MAO, 4);
                      push(&MAO, aux1->numero, aux1->naipe);
                      puxaCartas(n);
                      pts = registraPontos(&MAO);
                      if (pts > max_atual)
                          max_atual = pts;
                      retornaCartas(n);
                      aux1=pop(&MAO);
                      pushN(&MAO, aux2, 4);
                      free(aux2);
                      /* restaura pilhas */
                      aux2=popN(&MAO, u);
                      push(&MAO, aux1->numero, aux1->naipe);
                      pushN(&MAO, aux2, u);
                      free(aux2);
                      u++;
                  }
                  break; 
                }
    }
    return max_atual;
}

int main()
{
    char c, buffer[3];
    int s1, s2;
    int idx = 0;

    iniciaPilha(&MAO);
    iniciaPilha(&MONTE);
    while ((c  = fgetc(stdin)) != EOF)
    {
	if ((c!=' ') && (c!='\n') && (c!='\t') && (c!= '#'))
	{
	    buffer[idx] = c;
	    idx++;
	}
	else
	{
	    if (buffer[0] == '#')
		break;
	    if (idx == 0)
		continue;
	    buffer[idx] = '\0';
	    salvaCarta(buffer);
	    if (c == '\n')
	    {
		/** aplica algoritmo **/
                flip(&MAO);
                flip(&MONTE);
                s1 = registraPontos(&MAO);
                s2 = registraPontos(&MONTE);
                if (s2 > s1)
                    s1 = s2;
                s1 = registraNCartas(1, s1);
                s1 = registraNCartas(2, s1);
                s1 = registraNCartas(3, s1);
                s1 = registraNCartas(4, s1);
                imprimeMaoMonte();
                imprimeResultado(s1);
	        /** limpa buffer **/
                limpaPilha(&MAO);
                limpaPilha(&MONTE);
                iniciaPilha(&MAO);
                iniciaPilha(&MONTE);
	    }
	    /** limpa buffer **/
	    idx = 0;
            memset(buffer,0,3);
	}
    }
    return 0;
}

