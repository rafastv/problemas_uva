import sys,math
from turtle import *

def tamanho(a):
    return a[0]*a[0] + a[1]*a[1]

def produto_vetorial(v1, v2, v3):
    vtmp1 = [v1[0] - v2[0], v1[1] - v2[1]]
    vtmp2 = [v3[0] - v2[0], v3[1] - v2[1]]
    return (vtmp1[0] * vtmp2[1]) - (vtmp1[1] * vtmp2[0])

# melhor que gift-wrap (Jarvis)
def algoritmo_Graham (lista):
    pts = list(lista)
    primeiro = min(pts)
    pts.remove(primeiro)
    # order y: sen(prod vetorial); order x: cos(prod escalar)
    # vetor (1,0)
    angulos = {}
    for p in pts:
        x = p[0] - primeiro[0]
        y = p[1] - primeiro[1]
        vtmp = (y/math.sqrt(x*x + y*y))
        angulos.update({tuple(p): vtmp})
    angulos = sorted(angulos.items(), key = lambda angulos: [angulos[1], tamanho(angulos[0])], reverse=True)
    fecho = [primeiro, angulos[0][0]]
    for i in range(1, len(angulos)):
        v = angulos[i][0]
        while (produto_vetorial(v, fecho[-1], fecho[-2]) >= 0):
            fecho.pop()
            if (len(fecho) < 2):
                break 
        fecho.append(v)
    while (produto_vetorial(primeiro, fecho[-1], fecho[-2]) >= 0):
        fecho.pop()
        if (len(fecho) < 2):
            break 
    fecho.append(primeiro)
    return fecho

lista = []

for linha in sys.stdin:
    vars = linha.split()
    if (len(vars) == 1):
        # remove ultimo elemento
        lista = lista[:-1]
        # inicia algoritmo
        if lista:
            fecho = algoritmo_Graham(lista[1:])
            print (fecho)
            penup()
            for p in lista:
                p = list(p)
                p[0] -= lista[0][0]
                p[1] -= lista[0][1]
                p[0] <<= 4
                p[1] <<= 4
                goto(p)
                dot(5, "blue")

            color("black", "red")
            pendown()
            begin_fill()
            for p in fecho:
                p = list(p)
                p[0] -= lista[0][0]
                p[1] -= lista[0][1]
                p[0] <<= 4
                p[1] <<= 4
                goto(p)
            p = list(fecho[0])
            p[0] -= lista[0][0]
            p[1] -= lista[0][1]
            p[0] <<= 4
            p[1] <<= 4
            goto(p)
            end_fill()
            clear()

            minimo = len(lista)+1
            for i in range(len(fecho)-1):
                a = lista.index(list(fecho[i]))
                b = lista.index(list(fecho[i+1]))
                #lim_1 = fecho[i][0] if fecho[i][0] < fecho[i+1][0] else fecho[i+1][0]
                #lim_2 = fecho[i][0] if fecho[i][0] >= fecho[i+1][0] else fecho[i+1][0]
                #if (lista[0][0] >= lim_1) and (lista[0][0] <= lim_2):
                a = a if (a > b) else b
                minimo = minimo if (minimo < a) else a
            #    print (lista[0], minimo, fecho[i], fecho[i+1])
            sys.stdout.write(nome + str(minimo) + '\n')
             
        tam = 20 - len(vars[0])
        nome = vars[0] + ' '*tam
        lista = []
        # termina algoritmo
        if (vars[0] == '#'):
           break
        continue

    # captura pontos
    for i in range(0,len(vars),2):
        a = [int(vars[i]), int(vars[i+1])]
        if a not in lista:
            lista += [a]

#sys.stdout.write(str(cidades)+"\n")

exit(0)


