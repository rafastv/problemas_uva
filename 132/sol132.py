import sys,math

INDICE_VERTICES = {}
INDICE_ARESTAS = {}

def normaliza(vetor):
    modulo = math.sqrt(vetor[0]**2 + vetor[1]**2) 
    vetor[0] /= modulo
    vetor[1] /= modulo

def verificaLinhaBase(referencia, aresta):
    vetor = [aresta[1][0] - aresta[0][0], aresta[1][1] - aresta[0][1]]
    normaliza(vetor)
    #################################################################
    # matriz de rotacao 2D gira o vetor (1,0) para o vetor corrente #
    # | cos   sin |*|-1| = |v0|                                      #
    # |-sin   cos | | 0|   |v1|                                      #
    #################################################################
    cos =-vetor[0] 
    sin = vetor[1]
    #################################################################
    # a inversa dessa matriz eh a transposta logo                   #
    # | cos  -sin |                                                 #
    # | sin   cos |                                                 #
    #################################################################
    v1_rot = [0, 0]
    v2_rot = [aresta[1][0] - aresta[0][0], aresta[1][1] - aresta[0][1]]
    rf_rot = [referencia[0] - aresta[0][0], referencia[1] - aresta[0][1]]
    rf_rot = [rf_rot[0]*cos - rf_rot[1]*sin, rf_rot[1]*cos + rf_rot[0]*sin]
    v2_rot = [v2_rot[0]*cos - v2_rot[1]*sin, v2_rot[1]*cos + v2_rot[0]*sin]
    v1_rot = aresta[0]
    v2_rot = [v2_rot[0] + aresta[0][0], v2_rot[1] + aresta[0][1]]
    rf_rot = [rf_rot[0] + aresta[0][0], rf_rot[1] + aresta[0][1]]
    
    lh_1 = v1_rot[0] if v1_rot[0] < v2_rot[0] else v2_rot[0]
    lh_2 = v1_rot[0] if v1_rot[0] > v2_rot[0] else v2_rot[0]
    lv = v1_rot[1]
    return ((rf_rot[0] >= lh_1) and (rf_rot[0] <= lh_2)) #and (rf_rot[1] >= lv))

def VpertenceA(vertice, aresta):
    numerador = vertice[0] - aresta[0][0]
    denominador = aresta[1][0] - aresta[0][0]
    if abs(denominador) >= 0.0001:
        sx = numerador/denominador
        numerador = vertice[1] - aresta[0][1]
        denominador = aresta[1][1] - aresta[0][1]
        if abs(denominador) < 0.0001:
            return (abs(numerador) < 0.0001)
        sy = numerador/denominador
        return (abs(sx - sy) < 0.0001)
    else:
        denominador = aresta[1][1] - aresta[0][1]
        return ((abs(numerador) < 0.0001) and (abs(denominador) >= 0.0001))

def calculaIndice(fecho, lista):
    # indice das arestas individuais
    for i in range(len(fecho)-1):
       a = INDICE_VERTICES[fecho[i]]
       b = INDICE_VERTICES[fecho[i+1]]
       idx = a if a > b else b
       INDICE_ARESTAS.update({(fecho[i], fecho[i+1]):idx})

    # indice dos vertices que pertencem a arestas (nao pode misturar)
    for i in range(len(fecho)-1):
       aresta = [fecho[i], fecho[i+1]]
       idx = INDICE_ARESTAS[tuple(aresta)]
       idx_antigo = idx
       for vertice in lista:
           if vertice in aresta:
               continue
           if (VpertenceA(vertice, aresta)):
               idx = INDICE_VERTICES[tuple(vertice)] if INDICE_VERTICES[tuple(vertice)] > idx else idx 
       INDICE_ARESTAS[tuple(aresta)] = idx

def tamanho(a):
    return a[0]*a[0] + a[1]*a[1]

def produto_vetorial(v1, v2, v3):
    vtmp1 = [v1[0] - v2[0], v1[1] - v2[1]]
    vtmp2 = [v3[0] - v2[0], v3[1] - v2[1]]
    return (vtmp1[0] * vtmp2[1]) - (vtmp1[1] * vtmp2[0])

# melhor que gift-wrap (Jarvis)
def algoritmo_Graham (lista):
    pts = list(lista)
    primeiro = min(pts)
    pts.remove(primeiro)
    # order y: sen(prod vetorial); order x: cos(prod escalar)
    # vetor (1,0)
    angulos = {}
    for p in pts:
        x = p[0] - primeiro[0]
        y = p[1] - primeiro[1]
        vtmp = (y/math.sqrt(x*x + y*y))
        angulos.update({tuple(p): vtmp})
    angulos = sorted(angulos.items(), key = lambda angulos: [angulos[1], tamanho(angulos[0])], reverse=True)
    fecho = [tuple(primeiro), angulos[0][0]]
    for i in range(1, len(angulos)):
        v = angulos[i][0]
        while (produto_vetorial(v, fecho[-1], fecho[-2]) >= 0):
            fecho.pop()
            if (len(fecho) < 2):
                break 
        fecho.append(v)
    if (len(angulos) > 1):
        while (produto_vetorial(primeiro, fecho[-1], fecho[-2]) >= 0):
            fecho.pop()
            if (len(fecho) < 2):
                break 
    fecho.append(tuple(primeiro))
    return fecho

lista = []

conta = 0
for linha in sys.stdin:
    vars = linha.split()
    if (len(vars) == 1):
        # inicia algoritmo
        if lista:
            fecho = algoritmo_Graham(lista[1:])
            calculaIndice(fecho, lista)
            minimo = 1000000
            for aresta in INDICE_ARESTAS.keys():
                if (verificaLinhaBase(lista[0], aresta)):
                    minimo = minimo if (minimo < INDICE_ARESTAS[aresta]) else INDICE_ARESTAS[aresta]
            sys.stdout.write(nome + str(minimo) + '\n')
             
        tam = 20 - len(vars[0])
        nome = vars[0] + ' '*tam
        lista = []
        conta = 0
        INDICE_VERTICES = {}
        INDICE_ARESTAS = {}
        # termina algoritmo
        if (vars[0] == '#'):
           break
        continue

    # captura pontos
    for i in range(0,len(vars),2):
        a = [int(vars[i]), int(vars[i+1])]
        if ((abs(a[0]) + abs(a[1])) == 0):
            continue
        if a not in lista:
            INDICE_VERTICES.update({tuple(a):conta})
            lista += [a]
        else:
            INDICE_VERTICES[tuple(a)] = conta
        conta += 1

exit(0)
