#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef int bool;
#define true 1
#define false 0

typedef struct parenteses 
{
	int numero;
	bool igual;
} Tparenteses;

typedef struct linha
{
	char* parte1;
	char* parte2;
	int cp;
} Tlinha;

char* removeBrancos(const char *s)
{
	int i, conta=0;
	char *semBrancos;
	for (i=0; i< strlen(s);i++)
		if (s[i] != ' ')
			conta++;
	
	semBrancos = malloc(sizeof(char)*(conta+1));
	conta = 0;
	for (i=0; i< strlen(s);i++)
		if ((s[i] != ' ') && (s[i] != '\t'))
		{
			semBrancos[conta] = s[i];
			conta++;
		}
	semBrancos[conta]='\0';
	return semBrancos;
}

Tlinha* divideString(const char *s)
{
	Tlinha *saida = malloc(sizeof(Tlinha));
	int i, quebra, conta=0, limite = 0;
	for (i=0; i< strlen(s);i++)
	{
		if (s[i] == '(')
		{ 
			limite++;
			conta++;
		}
		if (s[i] == ')') 
		{
			limite--;
			conta++;
			if (limite == 0)
			{
				quebra = i;
				break;	
			}
		}
	}
	saida->parte1 = malloc(sizeof(char)*(quebra+2));
	saida->parte2 = malloc(sizeof(char)*((strlen(s)-quebra)+2));
	for (i=0; i< strlen(s);i++)
	{
		if (i <= quebra)
			saida->parte1[i] = s[i];
		else
			saida->parte2[i-(quebra+1)] = s[i];
	}
	saida->parte1[quebra+1]='\0';
	saida->parte2[strlen(s)-(quebra+1)]='\0';
	saida->cp = conta;
	return saida;
}

bool verificaSoma(int N, int nivel, char *arvore)
{
	char nivel_abaixo[10000];
	int valor = 0;
	int n = nivel;
	bool encontrei;
	Tlinha *galhos;

	n >>= 1;

	/*printf("N: %d n: %d\n", N, n);*/
	if (n <= 1) 
		return false;

	sscanf(arvore, "(%d%s", &valor, nivel_abaixo);
	nivel_abaixo[strlen(nivel_abaixo)-1]='\0';

	/*printf("SAIDA %d %d\n", valor, strlen(nivel_abaixo));*/
	if (((N - valor) == 0) && (strlen(nivel_abaixo) == 4))
		return true;

	/*printf("completo: %s\n", arvore);*/
	/*printf("nivel abaixo: %s\n", nivel_abaixo);*/
	/*printf("valor: %d\n", valor);*/

	/* removi dois parenteses da arvore */
	nivel -= 2;
	galhos = divideString(nivel_abaixo);

	/*printf("parte 1: %s\n", galhos->parte1);*/
	/*printf("parte 2: %s\n", galhos->parte2);*/
	/*printf("FIM NIVEL %d\n", nivel);*/

	encontrei = verificaSoma(N-valor, galhos->cp, galhos->parte1); 
	if (encontrei)
	{
		free(galhos);
		return encontrei;
	}
	encontrei = verificaSoma(N-valor, nivel-galhos->cp, galhos->parte2); 
	free(galhos);
	return encontrei;
}

char* concatenar(const char *s1, const char *s2)
{
    char *resultado = malloc(strlen(s1)+strlen(s2)+1);
    strcpy(resultado,s1);
    strcat(resultado,s2);
    return resultado;
}

Tparenteses* conta_parenteses(const char *s)
{
    int i,abre=0,fecha=0;
    Tparenteses *saida = malloc(sizeof(Tparenteses));
    for (i=0;i<strlen(s);i++)
    {
	if (s[i]=='(')
	{
	    abre++;
	}
	else
	{
		if (s[i] == ')')
		    fecha++;
	}
    }
    saida->numero = abre + fecha;
    saida->igual = (abre == fecha);
    return (saida);
}

int main()
{
    char linha[10000],arvore[10000];
    char *arvoreLimpa, *aux, *tmp = NULL;
    Tparenteses *tp;
    int N;
    while (fgets(linha,10000,stdin)!=NULL)
    {
	if ((linha[0] == '\n') && (tmp == NULL))
	    break;
	linha[strcspn(linha, "\n")]='\0';
	if (tmp != NULL)
	{
	    aux = concatenar(tmp,linha);
	    strcpy(linha,aux);
	    free(aux);
	    free(tmp);
	    tmp = NULL;
	}
	tp = conta_parenteses(linha);
	if (tp->igual)
	{
		sscanf(linha, "%d%10000c", &N, arvore);
	        arvoreLimpa = removeBrancos(arvore);
		/*printf("%d %s\n",N,arvoreLimpa);*/
	        /* arvore vazia */
		if ((arvoreLimpa[0] == '(') && (arvoreLimpa[1] == ')'))
			printf("no\n");	
		else
		{
	       		 if (verificaSoma(N, tp->numero, arvoreLimpa))
				printf("yes\n");
			else
				printf("no\n");	
		}
		/* esvazia string */
		free(arvoreLimpa);
		memset(arvore,0,strlen(arvore));
		memset(linha,0,strlen(linha));
	}
	else
	{
	    tmp = malloc(strlen(linha)+1);
	    strcpy(tmp,linha);
	}
	free(tp);
    }
    return 0;
}

