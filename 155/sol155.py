import sys, operator, math

# meio da matriz de pontos
MEIO = [1024, 1024]
 
def contaQuadrados(meio, k, ponto):
   soma = 0
   # bounding box
   lado = tamanhoQuadrado(k)
   lado >>= 1
   if (ponto[0] >= meio[0]-lado) and (ponto[0] <= meio[0]+lado) and\
      (ponto[1] >= meio[1]-lado) and (ponto[1] <= meio[1]+lado):
      if (ponto[0] >= meio[0]-k) and (ponto[0] <= meio[0]+k) and\
         (ponto[1] >= meio[1]-k) and (ponto[1] <= meio[1]+k):
            soma += 1
      if k > 1:
         meioA = [meio[0]-k, meio[1]-k]
         soma += contaQuadrados(meioA, k>>1, ponto)
         meioB = [meio[0]-k, meio[1]+k]
         soma += contaQuadrados(meioB, k>>1, ponto)
         meioC = [meio[0]+k, meio[1]+k]
         soma += contaQuadrados(meioC, k>>1, ponto)
         meioD = [meio[0]+k, meio[1]-k]
         soma += contaQuadrados(meioD, k>>1, ponto)
   return soma

def tamanhoQuadrado(k):
   soma = 2*k + 1
   while k >= 1:
      k >>= 1
      soma += 2*k
   return soma

linhas = sys.stdin.readlines()
for linha in linhas:
   linha = linha.replace("\n","")
   linha = linha.split()
   entrada = [int(i) for i in linha]
   if entrada[0] == 0 and entrada[1] == 0 and entrada[2] == 0:
      break
   ponto = [0,0]
   k, ponto[0], ponto[1] = entrada
   #print(k, ponto)
   sys.stdout.write("{:>3}".format(str(contaQuadrados(MEIO, k, ponto)))+"\n")

exit(0)
