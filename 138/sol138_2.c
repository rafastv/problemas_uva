#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <math.h>

typedef int bool;
#define true 1
#define false 0

void imprimeSaida(long double n, long double y)
{
    printf(" %9.0Lf %9.0Lf\n",n+1,y);
}

int main()
{
    int conta = 0;
    long double ns,ys,y1,y2;
    long double delta, inteira, fracional;

    ns = 5;
    while (conta < 10)
    {
       delta = 1 + 4*(2*ns*ns + 4*ns + 2);
 
       fracional = modfl(sqrt(delta), &inteira);
       if (fracional == 0.0)
       {
           y1 = (-1.0 - sqrt(delta))/2.0;
           y2 = (-1.0 + sqrt(delta))/2.0;

           if ((y1 > y2) && (y1 > 0))
           {
               fracional = modfl(y1, &inteira);
               if (fracional == 0.0)
               {
                   ys = y1;
                   conta++;
                   imprimeSaida(ns, ys);
               }
           }
           if ((y2 > y1) && (y2 > 0))
           {
               fracional = modfl(y2, &inteira);
               if (fracional == 0.0)
               {
                   ys = y2;
                   conta++;
                   imprimeSaida(ns, ys);
               }
           }
       }
       ns++;
       if (conta >= 10)
          break;
    }
    return 0;
}

