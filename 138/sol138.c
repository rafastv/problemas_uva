#include <stdio.h>

typedef int bool;
#define true 1
#define false 0

long int PARES[11][2];
int TAM;
/* veja o arquivo leiame para explicao */
bool existe(long int valor)
{
    int i;
    for (i=0;i<TAM;i++)
        if (PARES[i][0] == valor)
            return true;
    return false;
}

int main()
{
    long int a, b, c, d, s1, s2;
    int i, j, tmp1, tmp2;
    PARES[0][0] = 2;
    PARES[0][1] = 3;
    PARES[1][0] = 12;
    PARES[1][1] = 17;
    TAM = 2;
    tmp2 = 0;
    s1 = PARES[1][0];
    s2 = PARES[1][1];
    printf(" %9ld %9ld\n",s1>>1,s2>>1);
    while (TAM <= 10)
    {
        tmp1 = TAM;
        for (i=0;i<tmp1;i++)
        {
           a = PARES[i][0]; 
           b = PARES[i][1]; 
           for (j=tmp2+1;j<tmp1;j++)
           {
               c = PARES[j][0]; 
               d = PARES[j][1]; 
               s1 = b*c + a*d;
               s2 = b*d + ((a*c)<<1);
               if (!existe(s1))
               {
                   printf(" %9ld %9ld\n",s1>>1,s2>>1);
                   PARES[TAM][0]=s1;
                   PARES[TAM][1]=s2;
                   TAM++;
                   if (TAM > 10)
                       break;
               }
           }
           if (TAM > 10)
               break;
        }
        tmp2=tmp1-1;
    }
}
