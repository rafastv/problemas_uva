#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <float.h>
#include <limits.h>
#include <math.h>

#define mod(a,b) (b + (a % b)) % b
typedef int bool;
#define false 0
#define true 1
#define PRECISAO 1e-9

/* 300 dolares / 0.05 centavos, max seg problema */
long int VALORES[6000][11];

/* multiplica-se todos os valores por 100 */
long int NOTAS[11] = {10000, 5000, 2000, 1000, 500, 200, 100, 50, 20, 10, 5};

long int contaNotas(long int id_valor, int id_max)
{
    if (id_valor == 0)
        return 1;
    int i, idx; 
    long int soma = 0;
    for (i=id_max; i<11;i++)
        if (NOTAS[i] <= id_valor) 
        {
            idx = (id_valor/5) - 1;
            if (VALORES[idx][i] == 0)
                VALORES[idx][i] = contaNotas(id_valor - NOTAS[i], i);
            soma += VALORES[idx][i];
        }
    return soma;
}

int main()
{
    char buffer[10];
    char simbolo[2] = ".";
    char *pos;
    long int valor, saida;
    int i;
    for (i=0;i<6000;i++)
       memset(VALORES[i], 0, 11*(sizeof(long int)));
    while (fscanf(stdin, "%s", buffer) != EOF)
    {
        pos = strtok(buffer, simbolo);
        valor = atoi(pos) * 100;
        pos = strtok(NULL, simbolo);
        valor += atoi(pos);
        /* fim da entrada */
        if (valor == 0)
            break;
        saida = contaNotas(valor,0); 
        printf("%6.2lf%17ld\n",valor/100.0, saida);
    }
    return 0;
}
