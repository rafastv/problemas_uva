#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>

typedef int bool;
#define VERDADEIRO 1
#define FALSO 0
#define mod(a,b) (b + (a % b)) % b

#define IGUAL       1
#define DIFERENTE   2
#define IF          3
#define OR          4
#define MY          5
#define AND         6
#define THEN        7
#define ELSE        8
#define NUL0        9
#define YOUR       10
#define LAST1      11
#define LAST2      12
#define TRADE      13
#define CHEAT      14

char ESTRATEGIAS[11][250];
int TAM;
/* 10 jogos para cada estrategia que nao ela propria = 10 * 9; 10 extras para evitar calculos de indice */
char ESCOLHAS[10][100];
int PONTOS[10];

int avaliaStatement(char estrategia[250], int pos, char meu[2], char seu[2]);

void imprimeEscolha(char e)
{
    if (e == TRADE)
       printf("TRADE");
    else if (e == CHEAT)
       printf("CHEAT");
    else if (e == NUL0)
       printf("NULL");
    else
       printf("ERRO");
}

void imprimePontos()
{
   int i;
   for (i=0;i<TAM;i++)
      printf("%3d\n", PONTOS[i]);
}

void imprimeEstrategias()
{
   int i, j;
   for (i=0;i<TAM;i++)
   {
      printf(":");
      for (j=0;j<strlen(ESTRATEGIAS[i]);j++)
         printf("%d:", ESTRATEGIAS[i][j]);
      printf("\n");
   }
}

/* retorna indice dos vetores meu, seu */
int avaliaMemory(char estrategia[250], int pos)
{
   if ((estrategia[pos] == MY)   && (estrategia[pos+1] == LAST1))
      return 1;
   if ((estrategia[pos] == MY)   && (estrategia[pos+1] == LAST2))
      return 2;
   if ((estrategia[pos] == YOUR) && (estrategia[pos+1] == LAST1))
      return 3;
   /** if ((estrategia[pos] == YOUR) && (estrategia[pos+1] == LAST2)) **/
   return 4;
}

int avaliaCond(char estrategia[250], int pos, char meu[2], char seu[2])
{
   /* id da escolha, consome 2 posicoes */
   int ide = avaliaMemory(estrategia, pos);
   char escolha;
   if (ide <= 2)
      escolha = meu[ide-1];
   else  
      /** if ((ide > 2) && (ide <= 4)) **/
      escolha = seu[ide-3];
   /* operador e escolha, consomem 2 posicoes */
   if (estrategia[pos+2] == IGUAL)
      return escolha == estrategia[pos+3];
   else 
      /** if (estrategia[pos+2] == DIFERENTE) **/
      return escolha != estrategia[pos+3];
}

int avaliaCondicao(char estrategia[250], int ini, int fim, char meu[2], char seu[2])
{
   if ((ini+3) == fim)
      return avaliaCond(estrategia, ini, meu, seu);
   if (estrategia[ini+4] == AND)
      return avaliaCond(estrategia, ini, meu, seu) && avaliaCondicao(estrategia, ini+5, fim, meu, seu);
   else
      /** ini+4 == OR **/
      return avaliaCond(estrategia, ini, meu, seu) || avaliaCondicao(estrategia, ini+5, fim, meu, seu);
}

int avaliaIfstat(char estrategia[250], int pos, char meu[2], char seu[2])
{
   /** (estrategia[pos] == IF) **/
   int i, idif, contaif, idthen = -1, idelse = -1;
   idif = pos;
   pos = idif + 1;
   for (i=pos;i<strlen(estrategia);i++)
      if (estrategia[i] == THEN)
      {
         idthen = i;
         break;
      }
   pos = idthen + 1;
   contaif = 0;
   for (i=pos;i<strlen(estrategia);i++)
   {
      if (estrategia[i] == IF)
      {
         contaif++;
         continue;
      }
      if ((estrategia[i] == ELSE) && (contaif > 0))
      {
         contaif--;
         continue;
      }
      if (estrategia[i] == ELSE)
      {
         idelse = i;
         break;
      }
   }
   if (avaliaCondicao(estrategia, idif+1, idthen-1, meu, seu))
      return avaliaStatement(estrategia, idthen+1, meu, seu);
   else
      return avaliaStatement(estrategia, idelse+1, meu, seu);
}

int verificaCommand(char estrategia[250], int pos)
{
   return (estrategia[pos] == TRADE) || (estrategia[pos] == CHEAT);
}

int avaliaStatement(char estrategia[250], int pos, char meu[2], char seu[2])
{
   if (verificaCommand(estrategia, pos))
      return estrategia[pos];
   return avaliaIfstat(estrategia, pos, meu, seu);
}

void contaPontos(int escolha_1, int i, int escolha_2, int j)
{
   if ((escolha_1 == CHEAT) && (escolha_2 == CHEAT))
   {
      PONTOS[i]--;
      PONTOS[j]--;
   }
   else if ((escolha_1 == CHEAT) && (escolha_2 == TRADE))
   {
      PONTOS[i]+=2;
      PONTOS[j]-=2;
   }
   else if ((escolha_1 == TRADE) && (escolha_2 == CHEAT))
   {
      PONTOS[i]-=2;
      PONTOS[j]+=2;
   }
   else
   {
      /** ((escolha_1 == TRADE) && (escolha_2 == TRADE)) **/
      PONTOS[i]++;
      PONTOS[j]++;
   }
}

int convertePalavra(char buffer[10])
{
   int tamanho = strlen(buffer);
   if (tamanho == 1)
   {
      if (strcmp(buffer, "=") == 0)
         return IGUAL;
      if (strcmp(buffer, "#") == 0)
         return DIFERENTE;
      return -1;
   }
   else if (tamanho == 2)
   {
      if (strcmp(buffer, "IF") == 0)
         return IF;
      if (strcmp(buffer, "OR") == 0)
         return OR;
      if (strcmp(buffer, "MY") == 0)
         return MY;
      return -1;
   }
   else if (tamanho == 3)
   {
      if (strcmp(buffer, "AND") == 0)
         return AND;
      return -1;
   }
   else if (tamanho == 4)
   {
      if (strcmp(buffer, "THEN") == 0)
         return THEN;
      if (strcmp(buffer, "ELSE") == 0)
         return ELSE;
      if (strcmp(buffer, "NULL") == 0)
         return NUL0;
      if (strcmp(buffer, "YOUR") == 0)
         return YOUR;
      return -1;
   }
   else if (tamanho == 5)
   {
      if (strcmp(buffer, "LAST1") == 0)
         return LAST1;
      if (strcmp(buffer, "LAST2") == 0)
         return LAST2;
      if (strcmp(buffer, "TRADE") == 0)
         return TRADE;
      if (strcmp(buffer, "CHEAT") == 0)
         return CHEAT;
      return -1;
   }
   else
   {
      return -1;
   }
}

int main()
{
    char c, buffer[10];
    int idp, pos, i, j, k, idx = 0;

    TAM = 0;
    ESTRATEGIAS[0][0] = '\0';
    memset(PONTOS, 0, sizeof(int)*10);
    for (i=0;i<10;i++)
       memset(ESCOLHAS[i], 0, 100);
    while ((c  = fgetc(stdin)) != EOF)
    {
	if ((c!=' ') && (c!='\n') && (c!='\t') && (c != '.'))
	{
	    buffer[idx] = c;
	    idx++;
            buffer[idx] = '\0';
            idp = convertePalavra(buffer);
            if (idp >= 0)
            {
               /*printf("%s:%d:%ld\n", buffer, idp, strlen(buffer));*/
               pos = strlen(ESTRATEGIAS[TAM]);
               ESTRATEGIAS[TAM][pos] = idp;
               ESTRATEGIAS[TAM][pos+1] = '\0';
	       /** limpa buffer **/
               memset(buffer,0,idx);
	       idx = 0;
            }
	}
	else
	{
	    if ((buffer[0] == '#') && (c == '\n') && (strlen(buffer) == 1))
		break;
            if (c == '.')
            {
               TAM++;
               ESTRATEGIAS[TAM][0] = '\0';
            }
	}
    }
    /* joga 10 vezes cada estrategia contra a outra */
    char meu[2], seu[2]; 
    for (i = 0; i < TAM; i++)
       for (j = i+1; j < TAM; j++)
       {
          meu[0] = NUL0;
          meu[1] = NUL0;
          seu[0] = NUL0;
          seu[1] = NUL0;
          /*printf("compara %d com % d\n", i, j);*/
          for (k = 0; k < 10; k++)
          {
              if (k > 0)
              {
                 meu[0] = ESCOLHAS[i][j*10 + k-1];
                 seu[0] = ESCOLHAS[j][i*10 + k-1];
              }
              if (k > 1)
              {
                 meu[1] = ESCOLHAS[i][j*10 + k-2];
                 seu[1] = ESCOLHAS[j][i*10 + k-2];
              }
              ESCOLHAS[i][j*10 + k] = avaliaStatement(ESTRATEGIAS[i], 0, meu, seu);
              ESCOLHAS[j][i*10 + k] = avaliaStatement(ESTRATEGIAS[j], 0, seu, meu);
              /*imprimeEscolha(ESCOLHAS[i][j*10 + k]);
              printf(" ");
              imprimeEscolha(ESCOLHAS[j][i*10 + k]);
              printf("\n");*/
              contaPontos(ESCOLHAS[i][j*10 + k], i, ESCOLHAS[j][i*10 + k], j);
          }
          /*printf("================\n");*/
       }
    imprimePontos();
    /*printf("Quantidade de Estrategias: %d\n", TAM);
    imprimeEstrategias();*/
    return 0;
}

