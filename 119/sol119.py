import sys

N = -1
for linha in sys.stdin:

    vars = linha.split()
    if len(vars) == 0:
        continue

    if len(vars) == 1 and N <= 0:
        if N == 0:
            sys.stdout.write('\n');
        N = int(vars[0])
        nomes = {}
        continue

    if (N > 0) and (not nomes):
        for i in range(len(vars)):
            nomes.update({vars[i]:i})
            nomes.update({i: vars[i]})
        custos = [0] * N
        conta = 0
        continue

    if (N > 0) and nomes:
       valor = 0
       if int(vars[2]) > 0:
           custos[nomes[vars[0]]] -= int(vars[1])
           custos[nomes[vars[0]]] += int(vars[1]) % int(vars[2])
           valor = int(vars[1])//int(vars[2])

       for i in range(3,len(vars)):
           custos[nomes[vars[i]]] += valor
       conta += 1

    if conta == N:
        for i in range(N):
            sys.stdout.write(nomes[i] + ' ' + str(custos[i]) + '\n');
        N = 0

exit(0)
