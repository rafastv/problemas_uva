import sys

linhas = sys.stdin.readlines()
for linha in linhas:
   linha = linha.replace("\n","")
   if linha[0] == "0":
       break
   N = int(linha)
   regioes = [i for i in range(2,N+1)]
   saida = [1]
   m = 1
   passo = -1
   while (len(regioes)>1):
      passo = (passo + m) % len(regioes)
      vizinho = regioes[(passo-1) % len(regioes)]
      saida += [regioes.pop(passo)]
      passo = regioes.index(vizinho)
      if (len(regioes)==1) and (regioes[0] != 13):
         regioes = [i for i in range(2,N+1)]
         saida = [1]
         m += 1
         passo = -1
   sys.stdout.write(str(m)+'\n')

exit(0)
