import sys

def verificaGalho(galho, folha):
    if folha in galho:
        return True
    for f in galho:
        if f[0] == folha[0]:
            return True
    return False

def verificaArvore(arvore, nivel_arvore):
    if nivel_arvore == 0 and arvore[0] != 0:
        return True
    encontrei = False
    for i in range(nivel_arvore, 0, -1):
        galho_superior = arvore[i]
        for folha in galho_superior:
            busca = folha[0][:-1]
            galho_inferior = arvore[i-1]
            encontrei = False
            if galho_inferior == 0:
                break
            for folha in galho_inferior:
                if (busca == folha[0]):
                    encontrei = True
                    break
            if not encontrei:
                break
        if not encontrei:
            break
    return encontrei

def imprimeArvore(arvore, nivel_arvore):
    for galho in arvore[:nivel_arvore+1]:
        sys.stdout.write(str(galho[0][1]))
        for folha in galho[1:]:
            sys.stdout.write(' ' + str(folha[1]))
        if galho != arvore[nivel_arvore]:
            sys.stdout.write(' ')
    sys.stdout.write('\n')

fim = False
duplicado = False
arvore = [0]*256
nivel_arvore = -1

for linha in sys.stdin:
    vars = linha.split()

    if len(vars) == 0:
        continue

    for valor in vars:
        if valor == "()":
           fim = True
           break 
        folha = valor[1:-1].split(',')
        folha = (folha[1], int(folha[0]))
        if len(folha[0]) > nivel_arvore:
            nivel_arvore = len(folha[0])
        if arvore[len(folha[0])] == 0:
           arvore[len(folha[0])] = [folha]
        else:
           if not verificaGalho(arvore[len(folha[0])], folha):
               arvore[len(folha[0])] += [folha]
           else:
               duplicado = True

    if fim:
        for g in range(nivel_arvore+1):
            if arvore[g] == 0:
                continue
            arvore[g] = sorted(arvore[g], key=lambda v: v[0])
        if verificaArvore(arvore, nivel_arvore) and (not duplicado):
            imprimeArvore(arvore, nivel_arvore)
        else:
            sys.stdout.write('not complete\n')
        fim = False
        duplicado = False
        arvore = [0]*256
        nivel_arvore = -1

exit(0)
