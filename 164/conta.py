import sys, operator, math
from itertools import islice

linhas = sys.stdin.readlines()
for linha in linhas:
   linha = linha.replace("\n","")
   if linha == "END":
      break
   print(len(linha))

exit(0)
