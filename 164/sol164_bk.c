#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <float.h>
#include <limits.h>
#include <math.h>

#define mod(a,b) (b + (a % b)) % b
typedef int bool;
#define FALSE 0
#define TRUE 1
#define PRECISAO 1e-9
#define PI 3.14159265358979323846264338327

int comparaPalavras(char A[21], char B[21])
{
   char confA[26];
   char confB[26];
   int i;
   memset(confA, 0, 26);
   memset(confB, 0, 26);
   for (i=0; i<strlen(A); i++)
      confA[A[i]-97]++;
   for (i=0; i<strlen(B); i++)
      confA[B[i]-97]++;

   int conta = 0;
   for (i=0; i<26;i++)
      if ((confA[i] == confB[i]) && (confA[i] > 0))
         conta++;
   return conta;
}

void mudaLetra(char palavra[21], char c, int n)
{
   palavra[n] = c;
}

void deletaLetra(char palavra[21], int n)
{
   int i, tam = strlen(palavra);
   for (i=n;i<tam;i++)
      palavra[i] = palavra[i+1];
}

void insereLetra(char palavra[21], char c, int n)
{
   int i, tam = strlen(palavra);
   for (i=tam+1;i>n;i--)
      palavra[i] = palavra[i-1];
   palavra[n] = c;
}

void transformaPalavra(char origem[21], char destino[21], char comandos[82], int n)
{
   if (strcmp(origem, destino) == 0)
   {
      strcat(comandos, "E");
   }
   else if (n >= (strlen(destino) + strlen(origem)))
   {
      /* F = Fracassou */
      strcat(comandos, "F");
   }
   else if (n < strlen(origem) && n < strlen(destino) && origem[n] == destino[n])
   {
      transformaPalavra(origem, destino, comandos, n+1);
   }
   else
   {
      char c0[82], c1[82], c2[82];
      char o0[21], o1[21], o2[21];
      int n0 = INT_MAX, n1 = INT_MAX, n2 = INT_MAX;
      /* DELETAR */
      if (n < strlen(origem))
      {
         strcpy(o0, origem);
         deletaLetra(o0, strlen(origem)-n-1);
         if (n < 9)
            sprintf(c0, "D%c0%d", origem[n], n+1);
         else
            sprintf(c0, "D%c%d" , origem[n], n+1);
         printf("deleta %d: %s\n",n,o0);
         transformaPalavra(o0, destino, c0, n);
         n0 = comparaPalavras(o0, destino);
      }
      /* INSERIR */
      if (n < strlen(destino))
      {
         strcpy(o1, origem);
         insereLetra(o1, destino[n], n);
         if (n < 9)
            sprintf(c1, "I%c0%d", destino[n], n+1);
         else
            sprintf(c1, "I%c%d" , destino[n], n+1);
         printf("insere %d: %s\n",n,o1);
         transformaPalavra(o1, destino, c1, n+1);
         n1 = comparaPalavras(o1, destino);
      }
      /* MUDAR */
      if (n < strlen(origem) && n < strlen(destino))
      {
         strcpy(o2, origem);
         mudaLetra(o2, destino[n], n);
         if (n < 9)
            sprintf(c2, "C%c0%d", destino[n], n+1);
         else
            sprintf(c2, "C%c%d" , destino[n], n+1);
         printf("muda %d: %s\n",n,o2);
         transformaPalavra(o2, destino, c2, n+1);
         n2 = comparaPalavras(o2, destino);
      }
      /* Quem eh a menor sequencia */
      if (n0 < n1)
      {
         if (n0 < n2)
            strcat(comandos, c0); 
         else
            strcat(comandos, c2); 
      } 
      else
      {
         if (n1 < n2)
            strcat(comandos, c1); 
         else
            strcat(comandos, c2); 
      }
   }
}

int main()
{
    char origem[21], destino[21];
    char linha[42], comandos[82]="";
    origem[0] = '\0';
    destino[0] = '\0';
    while (fscanf(stdin, "%s", linha) != EOF)
    {
        /* fim da entrada */
        if (linha[0] == '#')
            break;
        if (origem[0] == '\0')
            strcpy(origem, linha);
        else
            strcpy(destino, linha);
        if ((origem[0] != '\0') && (destino[0] != '\0'))
        {
           printf("%s %s\n", origem, destino);
           transformaPalavra(origem, destino, comandos, 0);
           printf("%s\n", comandos);
           origem[0] = '\0';
           destino[0] = '\0';
        }
    }
    return 0;
}
