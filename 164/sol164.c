#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <float.h>
#include <limits.h>
#include <math.h>

#define mod(a,b) (b + (a % b)) % b
typedef int bool;
#define FALSE 0
#define TRUE 1
#define PRECISAO 1e-9
#define PI 3.14159265358979323846264338327

/* Matriz de Distancia Minima */
char** MDM; 

void inverteString(char palavra[82])
{
   int n = strlen(palavra)/4;
   char tmp[5];
   int i, m = n >> 1;
   for (i = 0; i< m; i++)
   {
      strncpy(tmp, &palavra[(n-i-1)*4], 4);
      strncpy(tmp, &palavra[i*4], 4);
      strncpy(&palavra[i*4], &palavra[(n-i-1)*4], 4);
      strncpy(&palavra[(n-i-1)*4], tmp, 4);
   }
}

void imprimeMatriz(char origem[21], char destino[21])
{
   int i, j;
   printf("  #%s\n", destino);
   for (i=0; i<strlen(origem)+1; i++)
   {
      if (i == 0)
         printf("# ");
      else
         printf("%c ", origem[i-1]);
      for (j=0; j<strlen(destino)+1; j++)
         printf("%d", MDM[i][j]);
      printf("\n");
   } 
}

void preencheMatriz(char origem[21], char destino[21])
{
   int minimo, j, i, o = strlen(origem)+1, d=strlen(destino)+1;
   MDM = malloc(o * sizeof(char*));
   for (i=0; i<o; i++)
      MDM[i] = malloc(d);
   
   /* Wagner–Fischer algorithm 1974 */
   for (i=0; i<o; i++)
      MDM[i][0] = i;
   for (i=0; i<d; i++)
      MDM[0][i] = i;

   for (i=1; i<o; i++)
      for (j=1; j<d; j++)
      {
           if (origem[i-1] == destino[j-1])
           {
              MDM[i][j] = MDM[i-1][j-1];
           }           
           else
           {
             minimo = (MDM[i-1][j-1]) < (MDM[i][j-1])?MDM[i-1][j-1]:MDM[i][j-1];
             minimo = minimo < (MDM[i-1][j])?minimo:MDM[i-1][j];
             MDM[i][j] = ++minimo;
           }
      } 
}

void preencheComandos(char comandos[82], char origem[21], char destino[21])
{
    int i = strlen(origem), j = strlen(destino);
    int deletou = 0, inseriu=0, minimo;
    char cmd[5];
    while ((i > 0) || (j > 0))
    {
       if ((i>0) && (j>0) && (origem[i-1] == destino[j-1]))
       {
          i--;
          j--;
       }
       else
       {
          if ((i > 0) && (j > 0))
          {
             minimo = MDM[i-1][j-1] < MDM[i][j-1]?MDM[i-1][j-1]:MDM[i][j-1];
             minimo = minimo < MDM[i-1][j]?minimo:MDM[i-1][j];
          }
          else if ((j == 0) && (i != 0))
          {
             minimo = MDM[i-1][j];
          }
          else if ((j != 0) && (i == 0))
          {
             minimo = MDM[i][j-1];
          }
          else
          {
            minimo = INT_MAX;
          }
          if ((i>0) && (minimo == MDM[i-1][j]))
          {
             deletou++;
             i--;
          }
          else if ((j>0) && (minimo == MDM[i][j-1]))
          {
             inseriu++;
             j--;
          }
          else if ((i>0) && (j>0) && (minimo == MDM[i-1][j-1]))
          {
             i--;
             j--;
          }
          else
             break;
       }
    }
    i = strlen(origem);
    j = strlen(destino);
    while ((i > 0) || (j > 0))
    {
       if ((i>0) && (j>0) && (origem[i-1] == destino[j-1]))
       {
          i--;
          j--;
       }
       else
       {
          if ((i > 0) && (j > 0))
          {
             minimo = MDM[i-1][j-1] < MDM[i][j-1]?MDM[i-1][j-1]:MDM[i][j-1];
             minimo = minimo < MDM[i-1][j]?minimo:MDM[i-1][j];
          }
          else if ((j == 0) && (i != 0))
          {
             minimo = MDM[i-1][j];
          }
          else if ((j != 0) && (i == 0))
          {
             minimo = MDM[i][j-1];
          }
          else
          {
            minimo = INT_MAX;
          }
          if ((i>0) && (minimo == MDM[i-1][j]))
          {
             deletou--;
             if (i-deletou+inseriu  < 10)
               sprintf(cmd, "D%c0%d", origem[i-1], i-deletou+inseriu );
             else
               sprintf(cmd, "D%c%d", origem[i-1], i-deletou+inseriu );
             strcat(comandos, cmd);
             i--;
          }
          else if ((j>0) && (minimo == MDM[i][j-1]))
          {
             if (i-deletou+inseriu < 10)
               sprintf(cmd, "I%c0%d", destino[j-1], i-deletou+inseriu);
             else
               sprintf(cmd, "I%c%d", destino[j-1], i-deletou+inseriu);
             strcat(comandos, cmd);
             j--;
             inseriu--;
          }
          else if ((i>0) && (j>0) && (minimo == MDM[i-1][j-1]))
          {
             if (i-deletou+inseriu  < 10)
               sprintf(cmd, "C%c0%d", destino[j-1], i-deletou+inseriu);
             else
               sprintf(cmd, "C%c%d", destino[j-1], i-deletou+inseriu);
             strcat(comandos, cmd);
             i--;
             j--;
          }
          else
             break;
       }
    }
    /*printf("%sE\n", comandos); */
    inverteString(comandos);
    strcat(comandos, "E");
}

int main()
{
    char origem[21]="", destino[21]="";
    char linha[42], comandos[82]="";
    while (fscanf(stdin, "%s", linha) != EOF)
    {
        /* fim da entrada */
        if (linha[0] == '#')
            break;
        if (origem[0] == '\0')
            strcpy(origem, linha);
        else
            strcpy(destino, linha);
        if ((origem[0] != '\0') && (destino[0] != '\0'))
        {
           preencheMatriz(origem, destino); 
           /*imprimeMatriz(origem, destino);*/
           preencheComandos(comandos, origem, destino);
           printf("%s\n", comandos);
           origem[0] = '\0';
           destino[0] = '\0';
           comandos[0] = '\0';
        }
    }
    return 0;
}
