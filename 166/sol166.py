import sys, operator, math
from itertools import islice

VALORES = [200, 100, 50, 20, 10, 5]

# vetores para memoizacao
QT_MOEDAS={}
M_MOEDAS={}

def totalDinheiro(moedas):
   soma = 0
   for i in range(6):
      soma += moedas[i] * VALORES[i] 
   return soma

# como todas as moedas sao multiplas de 5 a estrategia gulosa funciona
def contaTroco(n):
   if n in QT_MOEDAS.keys():
      return QT_MOEDAS[n]
   if n == 0:
      return 0
   valor = - 1
   for i in range(6):
      if VALORES[i] <= n:
         qt_moedas = n//VALORES[i]
         valor = qt_moedas + contaTroco(n-(VALORES[i]*qt_moedas))
         QT_MOEDAS.update({n: valor})
         break
   return valor

def conta(moedas, k, n):
   if n in M_MOEDAS.keys():
      return M_MOEDAS[n]
   if n == 0:
      return 0
   valor = [1000]*6
   for i in range(k,6):
      if moedas[i] > 0 and VALORES[i] <= n:
         # subtrai sempre o maximo de moedas possiveis
         atual = min(n//VALORES[i], moedas[i])
         moedas[i] -= atual;
         valor[i] = atual + conta(moedas, i, n-(VALORES[i]*atual))
         moedas[i] += atual;
   if min(valor) > 1000: 
      M_MOEDAS.update({n: min(valor)})
   return min(valor)

def encontraValores(moedas, ini, qt, n, valor):
   if (qt == 0):
      if valor > n:
         return [valor]
      return []
   valores = []
   for i in range(ini,6):
      if moedas[i] > 0:
         moedas[i] -= 1
         valores += encontraValores(moedas, i, qt-1, n, valor+VALORES[i])
         moedas[i] += 1
   return valores

def procuraMenor(moedas, qt, n):
   limite = min(qt,sum(moedas))
   minimo = limite
   # a quantidade de moedas pode ser exata para fazer um valor maior que o custo
   # logo essa quantidade com o troco seria maior que o minimo
   if sum(moedas) == limite:
      minimo = 1000
   for i in range(limite):
      # forma combinacoes de i+1 moedas entre as n moedas disponiveis
      valores = encontraValores(moedas, 0, i+1, n, 0)
      valores = list(set(valores))
      # busca valores do menor para o maior
      # ja que o menor valor ja gera troco
      valores.sort()
      #print("POSSIVEIS",valores)
      fim = False
      for j in valores:
         # eh preciso sempre calcular a quantidade de moedas pagas
         # mesmo sabendo que o valor foi formado por i+1 moedas
         atual = conta(moedas, 0, j) + contaTroco(j-n)
         if atual < minimo:
            minimo = atual
         # para evitar testes desnecessarios
         # este corte pode ser melhorado
         if atual > (minimo<<1):
            fim = True
            break
      if fim:
         break
   return minimo

# transforma flutuante em inteiro
def transforma(i):
   flutuante = i.split(".")
   return int(flutuante[0])*100 + int(flutuante[1])

linhas = sys.stdin.readlines()
for linha in linhas:
   linha = linha.replace("\n","")
   if len(linha) <= 1:
      continue
   texto = linha.split()
   moedas = [int(texto[i]) if i < 6 else transforma(texto[i]) for i in range(len(texto))]
   if moedas[0] == 0 and moedas[1] == 0 and\
      moedas[2] == 0 and moedas[3] == 0 and\
      moedas[4] == 0 and moedas[5] == 0:
      break
   valor = moedas[-1]
   moedas = moedas[:-1]
   moedas.reverse()
   M_MOEDAS={}
   minimo = conta(moedas, 0, valor)
   total = totalDinheiro(moedas)
   # se eh menor ou igual ao valor ja temos o minimo ou ele nao existe
   if (total > valor) and (minimo > 1):
      minimo = procuraMenor(moedas, minimo, valor)
   #print ("TOTAL", total)
   #print ("VALOR", valor, VALORES)
   #print ("MOEDAS", minimo, moedas)
   sys.stdout.write("{:>3}".format(str(minimo)) + "\n")

exit(0)
