import sys, operator, math
from itertools import islice

# instruction = navigational | time - keeping | navigational AND time - keeping 
def instruction(frase):
   # encontra AND que nao eh seguido de THEN
   idx = -1
   for i in range(len(frase)-2,-1,-1):
      if (frase[i] == "AND") and (frase[i+1] != "THEN"):
         idx = i
         break
   if (idx < 0):
      return navigational(frase) or time_keeping(frase)
   else:
      return (navigational(frase[:idx]) and time_keeping(frase[idx+1:]))

# navigational = directional | navigational AND THEN directional
def navigational(frase):
   # encontra AND
   idx = -1
   for i in range(len(frase)-1,-1,-1):
      if (frase[i] == "AND"):
         idx = i
         break
   # encontrou AND e proxima palavra nao eh THEN
   if (idx >= 0) and ((idx+1 == len(frase)) or (frase[idx+1] != "THEN")):
      return False
   if (idx < 0):
      return directional(frase) 
   else:
      return (navigational(frase[:idx]) and directional(frase[idx+2:]))

# directional = how direction | how direction where
def directional(frase):
   if len(frase) > 5 or len(frase) == 1:
      return False
   if len(frase) == 2:
      return how([frase[0]]) and direction(frase[1])
   elif len(frase) == 3:
      return how(frase[:2]) and direction(frase[-1])
   elif len(frase) == 4:
      return how([frase[0]]) and direction(frase[1]) and where(frase[2:])
   else:
      return how(frase[:2]) and direction(frase[2]) and where(frase[3:])

# where = AT sign
def where(frase):
   return (frase[0] == "AT") and (sign(frase[1:]))

# sign = "signwords"
def sign(frase):
   if len(frase) == 1 and frase[0][0] == "\"" and frase[0][-1] == "\"":
      return signwords(frase[0][1:-1].split())
   return False

# signwords = s - word | signwords s - word 
def signwords(frase):
   if not frase:
      return False
   return (s_word(frase)) or (s_word([frase[-1]]) and signwords(frase[:-1]))

# s - word = letter | s - word letter
def s_word(palavra):
   if len(palavra) > 1 or not palavra:
      return False
   return (letter(palavra[0])) or (letter(palavra[0][-1]) and s_word([palavra[0][:-1]]))

# letter = A..Z | .
def letter(letra):
   if len(letra) > 1 or not letra:
      return False
   return (ord(letra) >= ord("A") and ord(letra) <= ord("Z")) or (letra == ".")

# time - keeping = record | change
def time_keeping(frase):
   if (len(frase) == 6) or (len(frase) == 4): 
      return change(frase)
   elif (len(frase) == 2):
      return record(frase)
   else:
      return False

# change = cas TO nnn KMH
def change(frase):
   return (cas(frase[:-3])) and (frase[-3] == "TO") and (nnn(frase[-2])) and (frase[-1] == "KMH")

# nnn = digit | nnn digit
def nnn(palavra):
   return (digit(palavra)) or (digit(palavra[-1]) and nnn(palavra[:-1]))

# digit = 0..9
def digit(letra):
   if len(letra) > 1:
      return False
   return ord(letra) >= ord("0") and ord(letra) <= ord("9")

# record = RECORD TIME
def record(frase):
   return (frase[0] == "RECORD") and (frase[1] == "TIME")

# cas = CHANGE AVERAGE SPEED | CAS 
def cas(frase):
   if len(frase) == 3:
      return (frase[0] == "CHANGE") and (frase[1] == "AVERAGE") and (frase[2] == "SPEED")
   elif len(frase) == 1:
      return (frase[0] == "CAS")
   else:
      return False

# direction = RIGHT | LEFT
def direction(palavra):
   return (palavra == "RIGHT") or (palavra == "LEFT")

# when = FIRST | SECOND | THIRD
def when(palavra):
   return (palavra == "FIRST") or (palavra == "SECOND") or (palavra == "THIRD")

# how = GO | GO when | KEEP
def how(frase):
   if len(frase) == 2:
      return (frase[0] == "GO") and when(frase[1])
   elif len(frase) == 1:
      return (frase[0] == "GO") or (frase[0] == "KEEP")
   else:
      return False

linhas = sys.stdin.readlines()
conta = 0
for linha in linhas:
   linha = linha.replace("\n","")
   if linha == "#":
      break
   frase = []
   texto = linha.split("\"")
   valida = False
   if len(texto) % 2 == 1:
      for i in range(len(texto)):
         if i % 2 == 0:
            frase += texto[i].split()  
         else:
            frase += ["\"" + " ".join(texto[i].split()) + "\""]
      valida = instruction(frase)

   conta += 1
   if valida:
      sys.stdout.write("{:>3}".format(str(conta)) + ". " + " ".join(frase) + "\n")
   else: 
      sys.stdout.write("{:>3}".format(str(conta)) + ". " + "Trap!\n")
exit(0)
