import sys,math
from turtle import *

def acertou_tiro(fecho, tiro):
    for j in tiro:
        acertou = True
        for i in range(len(fecho)-1):
            if (produto_vetorial(fecho[i+1], fecho[i], j) > 0.001):
                acertou = False
                break
        if (acertou):
            break
    return acertou


def calcula_area(fecho):
    x = sum([i[0] for i in fecho])/len(fecho)
    y = sum([i[1] for i in fecho])/len(fecho)
    cm = [x, y]
    soma = 0
    for i in range(len(fecho)-1):
        soma += produto_vetorial_sm(fecho[i+1], cm, fecho[i])
    soma += produto_vetorial_sm(fecho[0], cm, fecho[-1])
    soma *= 0.5
    return soma

def tamanho(a):
    return a[0]*a[0] + a[1]*a[1]

def produto_vetorial_sm(v1, v2, v3):
    vtmp1 = [v1[0] - v2[0], v1[1] - v2[1]]
    vtmp2 = [v3[0] - v2[0], v3[1] - v2[1]]
    return (vtmp1[0] * vtmp2[1]) - (vtmp1[1] * vtmp2[0])

def produto_vetorial(v1, v2, v3):
    vtmp1 = [v1[0] - v2[0], v1[1] - v2[1]]
    #vtmp1[0] /= math.sqrt(vtmp1[0]*vtmp1[0] + vtmp1[1]*vtmp1[1])
    #vtmp1[1] /= math.sqrt(vtmp1[0]*vtmp1[0] + vtmp1[1]*vtmp1[1])
    vtmp2 = [v3[0] - v2[0], v3[1] - v2[1]]
    #vtmp2[0] /= math.sqrt(vtmp2[0]*vtmp2[0] + vtmp2[1]*vtmp2[1])
    #vtmp2[1] /= math.sqrt(vtmp2[0]*vtmp2[0] + vtmp2[1]*vtmp2[1])
    return (vtmp1[0] * vtmp2[1]) - (vtmp1[1] * vtmp2[0])

# melhor que gift-wrap (Jarvis)
def algoritmo_Graham (lista):
    pts = list(lista)
    primeiro = min(pts)
    pts.remove(primeiro)
    # order y: sen(prod vetorial); order x: cos(prod escalar)
    # vetor (1,0)
    angulos = {}
    for p in pts:
        x = p[0] - primeiro[0]
        y = p[1] - primeiro[1]
        vtmp = (y/math.sqrt(x*x + y*y))
        angulos.update({tuple(p): vtmp})
    angulos = sorted(angulos.items(), key = lambda angulos: [angulos[1], tamanho(angulos[0])], reverse=True)
    fecho = [primeiro, angulos[0][0]]
    for i in range(1, len(angulos)):
        v = angulos[i][0]
        while (produto_vetorial(v, fecho[-1], fecho[-2]) > 0):
            fecho.pop()
            if (len(fecho) < 2):
                break 
        fecho.append(v)
    while (produto_vetorial(primeiro, fecho[-1], fecho[-2]) > 0):
        fecho.pop()
        if (len(fecho) < 2):
            break 
    fecho.append(primeiro)
    return fecho

cidades = []
misseis = []
primeiro = True
lista = []
for linha in sys.stdin:
    vars = linha.split()
    if primeiro:
        N = int(vars[0])
        primeiro = False
    else:
        lista += [[int(vars[0]), int(vars[1])]]
        if (len(lista) == N):
            primeiro = True
            cidades += [lista]
            lista = []
        if (N == -1):
            misseis += lista
            lista = []


fechos = []
area_total = 0
for c in cidades:
    fecho = algoritmo_Graham(c)
    if acertou_tiro(fecho, misseis):
        area_total += calcula_area(fecho)
    fechos += [fecho]


penup()
for c in cidades:
    for p in c:
        p = list(p)
        p[0] -=100 
        p[1] -=300
        goto(p)
        dot(5, "blue")

conta = 0
cor = ["red", "yellow", "green"]
for fecho in fechos:
    color("black", cor[conta%3])
    conta += 1
    fecho = fecho[1:]
    pendown()
    begin_fill()
    for p in fecho:
        p = list(p)
        p[0] -=100
        p[1] -=300
        goto(p)
    p = list(fecho[0])
    p[0]-=100
    p[1]-=300
    goto(p)
    end_fill()

sys.stdout.write(str(area_total)+"\n")
exit(0)
