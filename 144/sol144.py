import sys

linhas = sys.stdin.readlines()
for linha in linhas:
   linha = linha.replace("\n","")
   vars = linha.split()
   if vars[0] == "0" and vars[1] == "0": 
       break
   N = int(vars[0])
   k = int(vars[1])
   passo = 1
   ordem_ini = [i for i in range(1,N+1)]
   fila = [40]* N
   pos = 0
   ordem_fim = []
   resto = False
   dispensa = 0
   while len(fila) > 0:
       #print (fila, passo, pos, dispensa)
       if not resto:
           fila[pos] -= passo
       else:
           fila[pos] -= dispensa
           dispensa = 0
       if (fila[pos] < 0):
           resto = True
           dispensa = abs(fila[pos])
           fila[pos] = 0
       else:
           resto = False
       if (fila[pos] == 0):
           #print("antes de remover",fila)
           fila.pop(pos)
           #print("depois de remover",fila)
           ordem_fim += [ordem_ini[pos]]
           ordem_ini.pop(pos)
           #print("ordem",ordem_ini)
           if not fila:
               break
           pos = pos % len(fila)
           if not resto:
               passo = passo + 1 if (passo + 1) <= k else 1
           continue
       #print("aumentei pos")
       pos = (pos + 1) % len(fila)
       passo = passo + 1 if (passo + 1) <= k else 1
       resto = False
   for i in ordem_fim:
       texto = str(i)
       sys.stdout.write((3-len(texto))*" " + texto);
   sys.stdout.write("\n")
exit(0)
