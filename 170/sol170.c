#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>

typedef int bool;
#define VERDADEIRO 1
#define FALSO 0

typedef struct pilha
{
    unsigned char numero; /*face_value*/
    unsigned char naipe; /* suits */
    bool visivel;
    struct pilha *anterior;
} Tpilha;

Tpilha MONTES[13]; 

int CARTAS_EXPOSTAS;
Tpilha *ULTIMA_CARTA;

void iniciaPilha(Tpilha *topo)
{
    /* guarda informacoes da pilha */
    topo->numero = (unsigned char) 0;
    topo->naipe = 'R'; /** RAIZ OU ROOT **/
    topo->anterior = NULL;
    topo->visivel = FALSO;
}

void push(Tpilha *topo, unsigned char numero, unsigned char naipe, bool visivel)
{
    Tpilha *p = malloc(sizeof(Tpilha));
    p->numero = numero;
    p->naipe = naipe;
    p->visivel = visivel;
    p->anterior = topo->anterior;
    topo->anterior = p;
    topo->numero++;
}

Tpilha* pop(Tpilha *topo)
{
    Tpilha *ultimo_elemento = topo->anterior;
    if (ultimo_elemento != NULL)
    {
        topo->anterior = ultimo_elemento->anterior;
	ultimo_elemento->anterior = NULL;
	topo->numero--;
    }
    return ultimo_elemento;
}

void limpaPilha(Tpilha *topo)
{
    while (topo->anterior != NULL)
	free(pop(topo));
}

void flip(Tpilha *monte)
{
    Tpilha aux1, aux2, *tmp;
    iniciaPilha(&aux1);
    iniciaPilha(&aux2);
    while (monte->numero > 0)
    {
        tmp = pop(monte);
        push(&aux1, tmp->numero, tmp->naipe, tmp->visivel);
    }
    while (aux1.numero > 0)
    {
        tmp = pop(&aux1);
        push(&aux2, tmp->numero, tmp->naipe, tmp->visivel);
    }
    while (aux2.numero > 0)
    {
        tmp = pop(&aux2);
        push(monte, tmp->numero, tmp->naipe, tmp->visivel);
    }
}

void imprimePilha(Tpilha *topo)
{
    Tpilha *tmp = topo->anterior;
    printf("TOPO ");
    while (tmp != NULL)
    {
	printf("%c%c ", tmp->numero, tmp->naipe);
	tmp = tmp->anterior;
    }
    printf(" total: %u", (unsigned int) topo->numero);
    printf(" visivel: %d\n", (unsigned int) topo->anterior->visivel);
}

void imprimeMontes()
{
    int i;
    for (i=0;i<13;i++)
	imprimePilha(&MONTES[i]);
}

void salvaCarta(char buffer[3], int pos)
{
    push(&MONTES[pos], buffer[0], buffer[1], FALSO);
}

void moveCarta(int v, int u)
{
   Tpilha tmp;
   Tpilha *carta, *carta_movida;

   iniciaPilha(&tmp);
   carta_movida = pop(&MONTES[v]);
   while (MONTES[u].numero > 0)
   {
       carta = pop(&MONTES[u]);
       push(&tmp, carta->numero, carta->naipe, carta->visivel);
   }
   push(&MONTES[u], carta_movida->numero, carta_movida->naipe, VERDADEIRO);
   while (tmp.numero > 0)
   {
       carta = pop(&tmp);
       push(&MONTES[u], carta->numero, carta->naipe, carta->visivel);
   }
   ULTIMA_CARTA = carta_movida;
   CARTAS_EXPOSTAS++;
}

int posicaoCarta(char valor)
{
   if (valor == 'A')
      return 0;
   else if (valor == 'T')
      return 9;
   else if (valor == 'J')
      return 10;
   else if (valor == 'Q')
      return 11;
   else if (valor == 'K')
      return 12;
   else
      return valor - '1';
}

void jogaPacienciaRelogio()
{
   /* comeca pela carta no centro */
   int destino, origem = 12;
   while (!MONTES[origem].anterior->visivel)
   {
      destino = posicaoCarta(MONTES[origem].anterior->numero);
      /*printf("PASSO %d: %d %d %c%c\n",CARTAS_EXPOSTAS,origem,destino,MONTES[origem].anterior->numero,MONTES[origem].anterior->naipe);
      imprimeMontes();*/
      moveCarta(origem, destino);
      origem = destino;
   }
}

void imprimeSaidaJogo()
{
   if (CARTAS_EXPOSTAS < 10)
      printf("0%d,", CARTAS_EXPOSTAS);
   else
      printf("%d,", CARTAS_EXPOSTAS);
   printf("%c%c\n", ULTIMA_CARTA->numero, ULTIMA_CARTA->naipe);
}

int main()
{
    char c, buffer[3];
    int i, conta, idx = 0;
    for (i=0;i<13;i++)
       iniciaPilha(&MONTES[i]);
    conta = 0;
    CARTAS_EXPOSTAS = 0;
    while ((c  = fgetc(stdin)) != EOF)
    {
	if ((c!=' ') && (c!='\n') && (c!='\t') && (c!= '#'))
	{
	    buffer[idx] = c;
	    idx++;
	}
	else
	{
	    if (buffer[0] == '#')
		break;
	    if (idx == 0)
		continue;
	    buffer[idx] = '\0';
	    salvaCarta(buffer, 12 - (conta % 13));
	    conta++;
	    if (conta == 52)
	    {
    		for (i=0;i<13;i++)
       		   flip(&MONTES[i]);
		/** aplica algoritmo **/
                jogaPacienciaRelogio();
                imprimeSaidaJogo();
                /*imprimeMontes();*/
    		for (i=0;i<13;i++)
                {
       		   limpaPilha(&MONTES[i]);
       		   iniciaPilha(&MONTES[i]);
                }
                conta=0;
    		CARTAS_EXPOSTAS = 0;
	    }
	    /** limpa buffer **/
	    idx = 0;
            memset(buffer,0,3);
	}
    }
    return 0;
}

