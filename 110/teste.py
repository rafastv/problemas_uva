import sys

def separaTuplas(grupo, a, b):
    grupoA = []
    grupoB = []
    for g in grupo:
        if g.index(a) < g.index(b):
            grupoA += [g]
        else:
            grupoB += [g]
    return [grupoA, grupoB]
        
def geraTuplas(minhasVars,linha='', pos=0):
    if (len(minhasVars)==1):
        return [(linha+minhasVars[-1]).split(",")] 
    lista = []
    for i in range(len(minhasVars)):
        s = list(minhasVars)
        c = s.pop(i)
        lista += geraTuplas(s, linha+c+",", pos+1)
    return lista

def preencheIfs(minhasTuplas, k=0, nivel=0):
    if len(minhasTuplas) == 1:
        saida = (str(tuple(minhasTuplas[-1]))).replace("'", "")
        saida = saida.replace(" ","")
        sys.stdout.write((nivel+1)*'  '+"writeln"+saida+"\n")
        return 0
    lista = minhasTuplas[0]
    if ((k+1) % len(lista)) == 0:
        k = 0
    st = separaTuplas(minhasTuplas,lista[k],lista[k+1])
    while (not st[0] or not st[1]):
        k = 0 if (((k + 2) % len(lista)) == 0) else k + 1
        st = separaTuplas(minhasTuplas,lista[k],lista[k+1])
    #print (st)
    sys.stdout.write((nivel+1)*'  '+'if '+lista[k]+' < '+lista[k+1])
    sys.stdout.write(' then\n')
    sys.stdout.write((nivel+1)*'  '+'begin\n')
    preencheIfs(st[0], k+1, nivel+1)
    sys.stdout.write((nivel+1)*'  '+'end\n')
    sys.stdout.write((nivel+1)*'  '+'else\n')
    sys.stdout.write((nivel+1)*'  '+'begin\n')
    preencheIfs(st[1], k+1, nivel+1)
    sys.stdout.write((nivel+1)*'  '+'end\n')

primeiro = True
lista = []

for linha in sys.stdin:
    vars = linha.split()
    if len(vars) == 0:
        continue
    if primeiro:
        N = int(vars[0])
        primeiro = False
    else:
        lista += [int(vars[0])]

conta = 0
nvar = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']

while conta < N:
    numero_vars = lista[conta] 
    conta+=1
    sys.stdout.write("program sort(input,output);\n")
    sys.stdout.write("var\n")
    lvars = ''
    for i in range(numero_vars-1):
        lvars += nvar[i]+','
    lvars += nvar[numero_vars-1]
    sys.stdout.write(lvars + ' : integer;\n')
    sys.stdout.write('begin\n')
    sys.stdout.write('  readln('+lvars+');\n')
    ntuplas = geraTuplas(nvar[:numero_vars])
    #print(ntuplas)
    #print(separaTuplas(ntuplas, ('a', 'b')))
    preencheIfs(ntuplas)
    sys.stdout.write('end.\n')
    #if conta < N:
    sys.stdout.write('\n')

exit(0)
