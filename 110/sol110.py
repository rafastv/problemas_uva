import sys


def separaTuplas(grupo, testados=[]):
    lista = grupo[0]
    grupoA = []
    grupoB = []
    k = 0
    grupoA = []
    grupoB = []
    a = lista[k]
    b = lista[(k+1)%len(lista)]
    while set([a,b]) in testados:
        k = (k+1) % len(lista)
        a = lista[k]
        b = lista[(k+1)%len(lista)]
    for g in grupo:
        if g.index(a) < g.index(b):
            grupoA += [g]
        else:
            grupoB += [g]
    return [[a,b], grupoA, grupoB]
        
def geraTuplas(minhasVars,linha='', pos=0):
    if (len(minhasVars)==1):
        return [(linha+minhasVars[-1]).split(",")] 
    lista = []
    for i in range(len(minhasVars)):
        s = list(minhasVars)
        c = s.pop(i)
        lista += geraTuplas(s, linha+c+",", pos+1)
    return lista

def preencheIfs(minhasTuplas, testados=[], nivel=0, marca=False, fi=True):
    if len(minhasTuplas) == 1:
        saida = (str(tuple(minhasTuplas[-1]))).replace("'", "")
        saida = saida.replace(" ","")
        nulo = ""
        if len(minhasTuplas[-1]) == 1:
            saida = saida.replace(",","")
            #nulo = ";"
        sys.stdout.write('\n')
        sys.stdout.write((nivel+1)*'  '+"writeln"+saida+nulo+"\n")
        return 0
    lista = minhasTuplas[0]
    st = separaTuplas(minhasTuplas, testados)
    t1 = list(testados + [set(st[0])])
    t2 = list(testados + [set(st[0])])
    if ((not marca) or fi):
        sys.stdout.write('\n')
        sys.stdout.write((nivel+1)*'  ')
    else:
        sys.stdout.write(' ')
    sys.stdout.write('if '+st[0][0]+' < '+st[0][1])
    sys.stdout.write(' then')
    preencheIfs(st[1], t1, nivel+1, marca, True)
    sys.stdout.write((nivel+1)*'  '+'else')
    if not(len(st[1]) == len(st[2])):
        nivel -=1
        marca=True
    else:
        marca=False
    preencheIfs(st[2], t2, nivel+1, marca, False)

primeiro = True
lista = []

for linha in sys.stdin:
    vars = linha.split()
    if len(vars) == 0:
        continue
    if primeiro:
        N = int(vars[0])
        primeiro = False
    else:
        lista += [int(vars[0])]

conta = 0
nvar = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']

while conta < N:
    numero_vars = lista[conta] 
    conta+=1
    sys.stdout.write("program sort(input,output);\n")
    sys.stdout.write("var\n")
    lvars = nvar[0]
    for i in range(1, numero_vars):
        lvars += ',' + nvar[i]
    sys.stdout.write(lvars + ' : integer;\n')
    sys.stdout.write('begin\n')
    sys.stdout.write('  readln('+lvars+');')
    ntuplas = geraTuplas(nvar[:numero_vars])
    preencheIfs(ntuplas)
    sys.stdout.write('end.\n')
    if conta < N:
        sys.stdout.write('\n')

exit(0)
