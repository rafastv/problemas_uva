#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>

typedef int bool;
#define VERDADEIRO 1
#define FALSO 0

bool LUZ[26];
int CORREDOR[26][26];
int TCO[26];

void imprimeCavernas()
{
   int i,j;
   bool primeiro;
   for (i=0;i<26;i++)
   {
      primeiro = VERDADEIRO;
      for (j=0;j<TCO[i];j++)
      {
         if (primeiro)
         {
            printf("Caverna: %c(%d)\n", i+65, LUZ[i]);
            printf("Caminhos: ");
            primeiro = FALSO;
         }
         printf("%c(%d) ", CORREDOR[i][j]+65, LUZ[CORREDOR[i][j]+65]);
      }
      if (!primeiro)
         printf("\n");
   } 
}

int main()
{
    char c, buffer[30];
    int i, caverna, minotauro, theseus, idx = 0;
    bool encurralado;
    long pos, passos;
    memset(buffer,0,30);
    memset(LUZ,0,26*sizeof(int));
    memset(TCO,0,26*sizeof(int));
    for (i=0;i<26;i++)
       memset(CORREDOR[i],0,26*sizeof(int));
    while ((c  = fgetc(stdin)) != EOF)
    {
	if ((c!='.') && (c != ':') && (c != ';') &&\
            (c!=' ') && (c!='\t') && (c!='\n') && (c!= '#'))
	{
	    buffer[idx] = c;
	    idx++;
	}
	else
	{
	    buffer[idx] = '\0';
	    if (c == '#')
		break;
            if ((c == ' ') || (c == '\t'))
		continue;
            if (c == ':')
            {
               caverna = buffer[0] - 'A';
               TCO[caverna] = 0;
               memset(CORREDOR[caverna],0,26*sizeof(int));
            }
            if ((c == ';') || (c == '.'))
            {
               for (i=0;i<strlen(buffer);i++)
                  CORREDOR[caverna][TCO[caverna]++] = buffer[i] - 'A';
            }
	    if (c == '\n')
	    {
                minotauro = buffer[0] - 'A';
                theseus = buffer[1] - 'A';
                passos = 0;
                pos = 0;
                for (i=2;i<strlen(buffer);i++)
                   passos = passos*10 + (buffer[i]-'0'); 
		/** aplica algoritmo (Segue minotauro) **/
		while (VERDADEIRO)
		{
		   pos = (pos+1) % passos;
		   /* Theseus coloca luz aonde minotauro estava */
		   if (pos == 0)
		      LUZ[minotauro] = VERDADEIRO;
		   encurralado = VERDADEIRO;
		   for (i=0;i<TCO[minotauro];i++)
		   {
  		     if ((!LUZ[CORREDOR[minotauro][i]]) && (CORREDOR[minotauro][i] != theseus))
		     {
    		        if (LUZ[minotauro])
    		          printf("%c ", minotauro+65);
		        encurralado = FALSO;
		        theseus = minotauro;
                        minotauro = CORREDOR[minotauro][i];
		        break;
   		     }
		   }
		   if (encurralado)
                   {
		      printf("/%c\n", minotauro+65);
                      break;
                   }
                }
                /*
                printf("%c %c %ld\n",minotauro+65,theseus+65,passos);
                imprimeCavernas();
                */
	        /** limpa buffer **/
                memset(LUZ,0,26*sizeof(int));
                memset(TCO,0,26*sizeof(int));
                for (i=0;i<26;i++)
                   memset(CORREDOR[i],0,26*sizeof(int));
	    }
	    /** limpa buffer **/
	    idx = 0;
            memset(buffer,0,30);
	}
    }
    return 0;
}

