import sys, operator, math
from itertools import islice

linhas = sys.stdin.readlines()
for linha in linhas:
   linha = linha.replace("\n","")
   if "#" in linha:
      break
   if len(linha) <= 1:
      continue
   linha = linha.split(";")
   cavernas = {}
   for parte in linha:
      if "." in parte:
         parte = parte.split(".")
         posicao = parte[1].split()
         parte = parte[0]
      elemento = parte.split(":")
      if len(elemento) == 2:
         lista = []
         elemento[0] = elemento[0].strip()
         for e in elemento[1]:
            if e not in lista and e != elemento[0] and e != " " and e != "\t":
               lista += [e]
         if elemento[0] not in cavernas.keys():
            cavernas.update({elemento[0]:lista})
         else:
            cavernas[elemento[0]] += lista
         for i in cavernas[elemento[0]]:
            if i not in cavernas.keys():
               cavernas.update({i:[]})
      else:
         elemento[0] = elemento[0].strip()
         if elemento[0] not in cavernas.keys():
            cavernas.update({elemento[0]:[]})
   minotauro = posicao[0]
   theseus = posicao[1]
   passo = int(posicao[2])
   if theseus not in cavernas.keys():
      cavernas.update({theseus:[minotauro]})
   else:
      if minotauro not in cavernas[theseus]:
         cavernas[theseus] += [minotauro]
   conta = 0
   #print "CAVERNAS",cavernas
   #print("INICIO",minotauro,theseus)
   #print ("PASSO", passo)
   caminho = []
   ultima_caverna = minotauro
   while (theseus != minotauro):
      ultima_caverna = minotauro
      if len(cavernas[minotauro]) > 0: 
         idx = 0
         if cavernas[minotauro][idx] == theseus:
            idx = (idx + 1) % len(cavernas[minotauro])
         minotauro = cavernas[minotauro][idx]
         if minotauro == theseus:
            break
      theseus = ultima_caverna
      #print(minotauro,theseus)
      if minotauro == theseus:
         break
      conta += 1
      if theseus not in caminho:
         caminho += [theseus]
      else:
         # ciclo detectado
         if (minotauro in caminho and\
             caminho.index(minotauro)-1 == caminho.index(theseus)):
            perdido = len(caminho[:caminho.index(theseus)])
            #print perdido
            caminho = caminho[caminho.index(theseus):]
            #print (theseus,caminho)
            conta = passo 
            minotauro = caminho[(passo-perdido)%len(caminho)]
            theseus = caminho[(((passo-perdido)%len(caminho))-1)%len(caminho)]
         else:
            caminho += [theseus]
      #print(minotauro,theseus)
      if conta % passo == 0:
         caminho = []
         # theseus coloca vela na caverna em que esta
         #print("REMOVER", theseus)
         cavernas.pop(theseus)
         for c in cavernas.keys():
            if theseus in cavernas[c]:
               cavernas[c].remove(theseus)
         sys.stdout.write(str(theseus)+" ")
         conta = 0
         #print(cavernas)
         
   # theseus mata minotauro
   sys.stdout.write("/" + str(ultima_caverna) + "\n")
   cavernas.clear()
   del caminho

exit(0)
