#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef int bool;
#define true 1
#define false 0

typedef enum 
{
  CIMA,
  DIREITA,
  BAIXO,
  ESQUERDA
} Tdirecao;

typedef struct obstaculo
{
  int valor;
  int custo;
  bool solido; 
} Tobstaculo;

typedef struct bola
{
  int x;
  int y;
  Tdirecao dir;
  int ttl; /* time-to-live */
} Tbola;

void imprimeBola(Tbola *b)
{
    switch (b->dir)
    {
	case ESQUERDA: printf("move esquerda! "); break;
	case DIREITA:  printf("move direita! ");  break;
	case CIMA:     printf("move cima! ");     break;
	case BAIXO:    printf("move baixo! ");    break;
    }
    printf("(%d, %d):%d\n", b->y, b->x, b->ttl);
}

void imprimeObs(Tobstaculo ***m, Tbola *b, int i, int j)
{
    switch (b->dir)
    {
	case ESQUERDA: printf("acertei esquerda!"); break;
	case DIREITA:  printf("acertei direita!");  break;
	case CIMA:     printf("acertei cima!");     break;
        case BAIXO:    printf("acertei baixo!");    break;
    }
    printf(" (%d, %d)\n", m[i][j]->valor, m[i][j]->custo);
}

void imprimeMatriz(Tobstaculo ***m, int i, int j)
{
    int a, b;
    for (a=0; a<i; a++)
    {
	for (b=0; b<j; b++)
	    printf("%d:(%d, %d) ", m[a][b]->solido, m[a][b]->valor, m[a][b]->custo);
	printf("\n");
    }
}

int joga(Tobstaculo ***matriz, Tbola bola, int pontos)
{
    /*imprimeBola(&bola);*/
    bola.ttl-=1;
    /* fim da vida da bola */
    if (bola.ttl <= 0)
	return pontos;
    switch (bola.dir)
    {
	case CIMA:
	   	 {
		        if (matriz[bola.y-1][bola.x]->solido)
			{
			    /*imprimeObs(matriz, &bola, bola.y-1, bola.x);*/
			    bola.dir = (bola.dir + 1) % 4;
			    bola.ttl -= (matriz[bola.y-1][bola.x]->custo);
			    pontos += matriz[bola.y-1][bola.x]->valor;
			}
			else
			{
			    bola.y -= 1;
			}
			break;
	   	 }
	case BAIXO:
	   	 {
		        if (matriz[bola.y+1][bola.x]->solido)
			{
			    /*imprimeObs(matriz, &bola, bola.y+1, bola.x);*/
			    bola.dir = (bola.dir + 1) % 4;
			    bola.ttl -= (matriz[bola.y+1][bola.x]->custo);
			    pontos += matriz[bola.y+1][bola.x]->valor;
			}
			else
			{
			    bola.y += 1;
			}
			break;
	   	 }
	case DIREITA:
	   	 {
		        if (matriz[bola.y][bola.x+1]->solido)
			{
			    /*imprimeObs(matriz, &bola, bola.y, bola.x+1);*/
			    bola.dir = (bola.dir + 1) % 4;
			    bola.ttl -= (matriz[bola.y][bola.x+1]->custo);
			    pontos += matriz[bola.y][bola.x+1]->valor;
			}
			else
			{
			    bola.x += 1;
			}
			break;
	   	 }
	case ESQUERDA:
	   	 {
		        if (matriz[bola.y][bola.x-1]->solido)
			{
			    /*imprimeObs(matriz, &bola, bola.y, bola.x-1);*/
			    bola.dir = (bola.dir + 1) % 4;
			    bola.ttl -= (matriz[bola.y][bola.x-1]->custo);
			    pontos += matriz[bola.y][bola.x-1]->valor;
			}
			else
			{
			    bola.x -= 1;
			}
			break;
	   	 }
    }
    return joga(matriz, bola, pontos);
}

int main()
{
    int n, m, a, b, c, d;
    bool estado;
    int saida, tmp, custo_parede, n_obstaculos = 0, conta = 0;
    Tobstaculo ***matriz;
    Tbola bl;

    scanf("%d %d\n", &m, &n);
    /*printf("%d x %d\n", n, m);*/ 
    scanf("%d\n", &custo_parede);
    /*printf("custo parede: %d\n", custo_parede);*/
    scanf("%d\n", &n_obstaculos);
    /*printf("numero obstaculos: %d\n", n_obstaculos);*/
    
    matriz = (Tobstaculo***) malloc(sizeof(Tobstaculo**)*n);
    for (a = 0; a < n; a++)
    {
        matriz[a] = (Tobstaculo**) malloc(sizeof(Tobstaculo*)*m);
        c = 0;
	estado = false;
	if ((a == 0) || (a == (n-1)))
	{
	    c = custo_parede;
	    estado = true;
	}
    	for (b = 0; b < m; b++)
	{
       	    matriz[a][b] = (Tobstaculo*) malloc(sizeof(Tobstaculo));
	    matriz[a][b]->valor = 0;
	    matriz[a][b]->custo = c;
	    matriz[a][b]->solido = estado;
	    if ((b == 0) || (b == (m-1)))
	    {
	        matriz[a][b]->custo = custo_parede;
	        matriz[a][b]->solido = true;
	    }
	}
    }

    saida = 0;
    while (scanf("%d %d %d %d\n", &a, &b, &c, &d) == 4)
    {
	if (conta < n_obstaculos)
	{
	    matriz[n-b][a-1]->valor = c;
	    matriz[n-b][a-1]->custo = d;
	    matriz[n-b][a-1]->solido = true;
	    /*printf("obs: %d %d %d %d\n", a, b, c, d);*/
	    conta++;
	}
	else
	{
	    /* comeca jogo */
	    bl.x = (a-1);
	    bl.y = n-b;
	    switch (c)
	    {
		case 0: bl.dir = DIREITA; break;
		case 1: bl.dir = CIMA; break;
		case 2: bl.dir = ESQUERDA; break;
		case 3: bl.dir = BAIXO; break;
	    }
	    bl.ttl = d;
	    /*printf("bola: %d:%d %d:%d %d %d\n", b,bl.y, a,bl.x, c, d);*/
	    tmp = joga(matriz, bl, 0);
	    printf("%d\n", tmp);
	    saida = saida + tmp;
	}
    }
    printf("%d\n",saida);
    /*imprimeMatriz(matriz, n, m);*/
    /* limpa lista */
    for (a = 0; a < n; a++)
    {
    	for (b = 0; b < m; b++)
       	    free(matriz[a][b]); 
	free(matriz[a]);
    }
    free(matriz);
    return 0;
}

