#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>

typedef int bool;
#define true 1
#define false 0

long unsigned int NUMEROS[1500];
int POS;

void salva(long unsigned int arvore[7][1500], int pos_arvore[7], int k, long unsigned int valor)
{
      bool pode = true;
      int j;
      for (j=0;j<pos_arvore[k];j++)
          if (valor == arvore[k][j])
          {
              pode = false;
              break;
          }
      if (pode)
         arvore[k][pos_arvore[k]++] = valor;
}

void imprimeArvores(long unsigned int arvore[7][1500], int pos_arvore[7])
{
      int i, j;
      for (i=0;i<7;i++)
      {
          printf("ARVORE %d (tam %d): %lu", i+1, pos_arvore[i], arvore[i][0]);
          for (j=1;j<pos_arvore[i];j++)
              printf(" %lu", arvore[i][j]);
          printf("\n");
      }
}

int main()
{
    /* existem 7 casos possiveis iniciais      */
    /* 1) potencia de 2        (arvore 1-aria) */
    /* 2) potencia de 3        (arvore 1-aria) */
    /* 3) potencia de 5        (arvore 1-aria) */
    /* 4) potencia de 2 e 3    (arvore 3-aria) */
    /* 5) potencia de 2 e 5    (arvore 3-aria) */
    /* 6) potencia de 3 e 5    (arvore 3-aria) */
    /* 7) potencia de 2, 3 e 5 (arvore 7-aria) */
    long unsigned int potenc[7] = {2,3,5,6,10,15,30};
    long unsigned int numero[7], arvore[7][1500];
    int pos_arvore[7] = {1,1,1,1,1,1,1};
    NUMEROS[0]=1;
    POS=1;
    int i,j,k1,k2;
    long unsigned int menor; 
    /* limpa arvores */
    for (i=0;i<7;i++)
        memset(arvore[i], 0, sizeof(int)*500);
    /* raizes das arvores */
    arvore[0][0] = 2;
    arvore[1][0] = 3;
    arvore[2][0] = 5;
    arvore[3][0] = 6;
    arvore[4][0] = 10;
    arvore[5][0] = 15;
    arvore[6][0] = 30;
    while (POS < 1500)
    {
          /* escolhe o menor atual */
          menor = arvore[0][0];
          k1 = 0;
          k2 = 0;
          for (i=0;i<7;i++)
          {
              for (j=0;j<pos_arvore[i];j++)
              {
                  if (arvore[i][j] < menor)
                  {
                      menor = arvore[i][j];
                      k1 = i;
                      k2 = j;
                  }
              }
          }
          /* salva e incrementa vetor */ 
          NUMEROS[POS]=menor;
          POS++;
          /*printf("%d(k1 %d k2 %d): %lu\n", POS,k1,k2,menor); */
          /* calcula proximas folhas e armazena em resultados na arvore correspondente */
          switch (k1)
          {
               case 0: { arvore[k1][k2] *= potenc[k1]; break; }
               case 1: { arvore[k1][k2] *= potenc[k1]; break; }
               case 2: { arvore[k1][k2] *= potenc[k1]; break; }
               case 3: { 
                         numero[0] = arvore[k1][k2] * potenc[0]; 
                         numero[1] = arvore[k1][k2] * potenc[1]; 
                         numero[2] = arvore[k1][k2] * potenc[k1]; 
                         /* remove o menor da arvore */
                         arvore[k1][k2] = arvore[k1][pos_arvore[k1]-1];
                         pos_arvore[k1]--;
                         /* salva numeros na arvore */
                         salva(arvore, pos_arvore, k1, numero[0]);
                         salva(arvore, pos_arvore, k1, numero[1]);
                         salva(arvore, pos_arvore, k1, numero[2]);
                         break;
                       }
               case 4: { 
                         numero[0] = arvore[k1][k2] * potenc[0]; 
                         numero[1] = arvore[k1][k2] * potenc[2]; 
                         numero[2] = arvore[k1][k2] * potenc[k1]; 
                         /* remove o menor da arvore */
                         arvore[k1][k2] = arvore[k1][pos_arvore[k1]-1];
                         pos_arvore[k1]--;
                         /* salva numeros na arvore */
                         salva(arvore, pos_arvore, k1, numero[0]);
                         salva(arvore, pos_arvore, k1, numero[1]);
                         salva(arvore, pos_arvore, k1, numero[2]);
                         break;
                       }
               case 5: { 
                         numero[0] = arvore[k1][k2] * potenc[1]; 
                         numero[1] = arvore[k1][k2] * potenc[2]; 
                         numero[2] = arvore[k1][k2] * potenc[k1]; 
                         /* remove o menor da arvore */
                         arvore[k1][k2] = arvore[k1][pos_arvore[k1]-1];
                         pos_arvore[k1]--;
                         /* salva numeros na arvore */
                         salva(arvore, pos_arvore, k1, numero[0]);
                         salva(arvore, pos_arvore, k1, numero[1]);
                         salva(arvore, pos_arvore, k1, numero[2]);
                         break;
                       }
               case 6: { 
                         numero[0] = arvore[k1][k2] * potenc[0]; 
                         numero[1] = arvore[k1][k2] * potenc[1]; 
                         numero[2] = arvore[k1][k2] * potenc[2]; 
                         numero[3] = arvore[k1][k2] * potenc[3]; 
                         numero[4] = arvore[k1][k2] * potenc[4]; 
                         numero[5] = arvore[k1][k2] * potenc[5]; 
                         numero[6] = arvore[k1][k2] * potenc[k1]; 
                         /* remove o menor da arvore */
                         arvore[k1][k2] = arvore[k1][pos_arvore[k1]-1];
                         pos_arvore[k1]--;
                         /* salva numeros na arvore */
                         salva(arvore, pos_arvore, k1, numero[0]);
                         salva(arvore, pos_arvore, k1, numero[1]);
                         salva(arvore, pos_arvore, k1, numero[2]);
                         salva(arvore, pos_arvore, k1, numero[3]);
                         salva(arvore, pos_arvore, k1, numero[4]);
                         salva(arvore, pos_arvore, k1, numero[5]);
                         salva(arvore, pos_arvore, k1, numero[6]);
                         break;
                       }
          }
          /*printf("====PASSO====\n");
          imprimeArvores(arvore, pos_arvore);*/
    }
    printf("The 1500'th ugly number is %lu.\n", NUMEROS[1499]);
    return 0;
}

