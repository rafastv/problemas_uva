#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>

typedef int bool;
#define true 1
#define false 0

#define TAM 20
/* 20ˆ3= 8000 numeros */
/* a ordem dos numeros pode estar em qualquer direcao da matriz   */
/* nao eh possivel determinar em que direcao o prox elemento esta */
long unsigned int NUMEROS[TAM][TAM][TAM];

void limpaMatrizNumeros()
{
    int u,v,w;
    for (u=0;u<TAM;u++)
        for (v=0;v<TAM;v++)
            memset(NUMEROS[u][v], 0, sizeof(long unsigned int)*TAM);
}

void imprimeMatrizNumeros()
{
    int u,v,w;
    for (u=0;u<TAM;u++)
    {
        printf("MATRIZ %d\n", u+1);
        for (v=0;v<TAM;v++)
        {
            printf("%lu", NUMEROS[u][v][0]);
            for (w=1;w<TAM;w++)
                printf(" %lu", NUMEROS[u][v][w]);
            printf("\n");
        }
    }
}

void preencheMatrizNumeros()
{
    int u,v,w, linha=1, coluna=1;
    NUMEROS [0][0][0] = 1;
    for (u=0;u<TAM;u++)
    {
        for (v=0;v<TAM;v++)
        {
            for (w=1;w<TAM;w++)
            {
                NUMEROS[u][v][w] = (NUMEROS[u][v][w-1] << 1);
            }
            NUMEROS[u][v+1][0] = (NUMEROS[u][v][0] * 3);
        }
        NUMEROS[u+1][0][0] = (NUMEROS[u][0][0] * 5);
    }
}

int main()
{
    limpaMatrizNumeros();
    preencheMatrizNumeros();
    imprimeMatrizNumeros();
    printf("The 1500'th ugly number is %lu\n", NUMEROS[TAM-1][TAM-1][TAM-1]);
    return 0;
}

