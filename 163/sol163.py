import sys, operator, math
from itertools import islice

DIRECAO = { "N": ( 0, 1),  "S": ( 0,-1),  "W": (-1, 0),  "E": (1, 0),
           "NW": (-1, 1), "SW": (-1,-1), "NE": ( 1, 1), "SE": (1,-1)}

LETRA_DIRECAO = {( 0, 1):  "N", ( 0,-1):  "S", (-1, 0):  "W", (1, 0):  "E",
                 (-1, 1): "NW", (-1,-1): "SW", ( 1, 1): "NE", (1,-1): "SE"}

CICLO = [( 0, 1), (-1, 1), (-1, 0), (-1,-1),
         ( 0,-1), ( 1,-1), ( 1, 0), ( 1, 1)]

DICIONARIO = ["STOP", 
                 "1", "2", "3", "4", "5", "6", "7", "8", "9","10",
                "11","12","13","14","15","16","17","18","19","20",
                "21","22","23","24","25","26","27","28","29","30",
                "31","32","33","34","35","36","37","38","39","40",
                "41","42","43","44","45","46","47","48","49","50",
                "51","52","53","54","55","56","57","58","59","60",
                "61","62","63","64","65","66","67","68","69","70",
                "71","72","73","74","75","76","77","78","79","80",
                "81","82","83","84","85","86","87","88","89","90",
                "91","92","93","94","95","96","97","98","99",
                "TURN", "HALF", "SHARP", "LEFT", "RIGHT", "GO", "STRAIGHT"]

def turnCommand(conjunto):
   return (conjunto[0] == DICIONARIO.index("TURN")) and\
          (((len(conjunto) == 2) and\
            (conjunto[1] == DICIONARIO.index("LEFT")) or (conjunto[1] == DICIONARIO.index("RIGHT"))) or\
           ((len(conjunto) == 3) and\
           ((conjunto[1] == DICIONARIO.index("HALF")) or (conjunto[1] == DICIONARIO.index("SHARP"))) and\
           ((conjunto[2] == DICIONARIO.index("LEFT")) or (conjunto[2] == DICIONARIO.index("RIGHT")))))

def straightCommand(conjunto):
   return (conjunto[0] == DICIONARIO.index("GO")) and\
         ((len(conjunto) == 2) and (conjunto[1] <= 99) and (conjunto[1] >= 1)) or\
         ((len(conjunto) == 3) and\
          (conjunto[1] == DICIONARIO.index("STRAIGHT")) and ((conjunto[2] <= 99) and (conjunto[2] >= 1))) 

def comandoValido(instrucao):
   conj = [DICIONARIO.index(i) if i in DICIONARIO else -1 for i in instrucao]
   # instrucao contem elementos invalidos
   valido = min(conj)
   if valido < 0:
      return False;
   return turnCommand(conj) or straightCommand(conj)

def aplicaComando(instrucao, pos):
   if instrucao[0] == "TURN":
      direcao = (pos[1][0], pos[1][1])
      p_atual = (pos[0][0], pos[0][1])
      p_proxi = moveCarro(p_atual, direcao)
      idx = 1
      passo = 2
      if (instrucao[1] == "HALF"):
         idx += 1
         passo -= 1
      elif (instrucao[1] == "SHARP"):
         idx += 1
         passo += 1
      else:
         pass
      # virar a esquerda eh positivo (anti-horario)
      if (instrucao[idx] == "RIGHT"):
         passo *= -1
      ############################
      #  Esquerda  45 graus =  1 #
      #  Esquerda  90 graus =  2 #
      #  Esquerda 135 graus =  3 #
      #   Direita  45 graus = -1 #
      #   Direita  90 graus = -2 #
      #   Direita 135 graus = -3 #
      ############################
      nova_direcao = []
      # atual esta na via expressa por vacuidade
      if estaNoCentro(p_proxi):
         if __debug__:
            print ("CENTRO")
         nova_direcao = viraCarro(direcao, passo)
      # atual esta na via expressa por vacuidade
      elif estaEmCorners(p_proxi):
         if __debug__:
            print ("CANTO")
         nova_direcao = viraCarro(direcao, passo)
      elif estaEmMetades(p_proxi):
         if __debug__:
            print ("METADE")
         nova_direcao = viraCarro(direcao, passo)
      elif estaEmCruzamentos(p_proxi):
         if __debug__:
            print ("CRUZAMENTO")
         if estaEmViaExpressa(p_atual):
            if passo == 3:
               nova_direcao = viraCarro(direcao, passo)
         elif estaEmBoulevard(p_atual):
            if abs(passo) == 1 or abs(passo) == 3:
               nova_direcao = viraCarro(direcao, passo)
         else:
            # esta vindo por rua horizontal ou vertical
            # de acordo com o quadrante
            if p_proxi[0]*p_proxi[1] < 0:
               if p_proxi[0] == p_atual[0]:
                  if passo != 1 and passo != -3:
                     nova_direcao = viraCarro(direcao, passo)
               else:
                  if passo != -1:
                     nova_direcao = viraCarro(direcao, passo)
            else:
               if p_proxi[0] == p_atual[0]:
                  if passo != -1:
                     nova_direcao = viraCarro(direcao, passo)
               else:
                  if passo != 1 and passo != -3:
                     nova_direcao = viraCarro(direcao, passo)
      elif estaEmBoulevard(p_proxi): 
         if __debug__:
            print ("BOULEVARD")
         if estaEmViaExpressa(p_atual) and estaEmViaExpressa(p_proxi):
            if passo == 3:
               nova_direcao = viraCarro(direcao, passo)
         elif estaEmBoulevard(p_atual):
            if abs(passo) == 1 or abs(passo) == 3:
               nova_direcao = viraCarro(direcao, passo)
         elif estaEmViaExpressa(p_proxi):
            # esta vindo por rua horizontal ou vertical para via expressa
            # de acordo com o quadrante
            if p_proxi[0]*p_proxi[1] < 0:
               if p_proxi[0] == p_atual[0]:
                  if abs(passo) == 2:
                     nova_direcao = viraCarro(direcao, passo)
               else:
                  if abs(passo) == 2 or passo == 3:
                     nova_direcao = viraCarro(direcao, passo)
            else:
               if p_proxi[0] == p_atual[0]:
                  if abs(passo) == 2 or passo == 3:
                     nova_direcao = viraCarro(direcao, passo)
               else:
                  if abs(passo) == 2:
                     nova_direcao = viraCarro(direcao, passo)
         else:
            # esta vindo por rua horizontal ou vertical
            # de acordo com o quadrante
            if p_proxi[0]*p_proxi[1] < 0:
               if p_proxi[0] == p_atual[0]:
                  if passo != 1 and passo != -3:
                     nova_direcao = viraCarro(direcao, passo)
               else:
                  if passo != -1 and passo != 3:
                     nova_direcao = viraCarro(direcao, passo)
            else:
               if p_proxi[0] == p_atual[0]:
                  if passo != -1 and passo != 3:
                     nova_direcao = viraCarro(direcao, passo)
               else:
                  if passo != 1 and passo != -3:
                     nova_direcao = viraCarro(direcao, passo)
      elif estaEmViaExpressa(p_proxi):
         if __debug__:
            print("VIA EXPRESSA")
         if passo == 2:
            nova_direcao = viraCarro(direcao, passo)
      else:
         if __debug__:
            print("COMUM",p_proxi,p_atual)
         if abs(passo) == 2:
            nova_direcao = viraCarro(direcao, passo)
      if nova_direcao and dirValida(p_proxi, nova_direcao):
         pos[1] = nova_direcao
         pos[0] = p_proxi
   else: 
      # GO
      idx = 1
      if instrucao[1] == "STRAIGHT":
         idx = 2
      passos = int(instrucao[idx])
      conta = 0
      direcao = (pos[1][0], pos[1][1])
      p_atual = (pos[0][0], pos[0][1])
      while (conta < passos):
         while not dirValida(p_atual, direcao):
             direcao = viraCarro(direcao, 1)
         p_atual = moveCarro(p_atual, direcao)
         conta +=1
      pos[0] = p_atual
      
def viraCarro(direcao, passo):         
   pos_ciclo = CICLO.index(direcao)
   pos_ciclo = (pos_ciclo + passo) % 8    
   return CICLO[pos_ciclo]

def moveCarro(posicao, direcao):
   return [posicao[0] + direcao[0], posicao[1] + direcao[1]]

def encontraPos(linha):
   x = int(linha[0][1:-1]) * DIRECAO[linha[0][-1]][0]
   y = int(linha[1][1:-1]) * DIRECAO[linha[1][-1]][1]
   d = DIRECAO[linha[2]]
   return [[x, y], d]

def posValida(pos):
   return abs(pos[0]) <= 50 and abs(pos[1]) <= 50

def dirValida(pos, dir):
   prox = moveCarro(pos, dir)
   return not estaEmViaExpressa(pos) or posValida(prox)

# 1 centro
def estaNoCentro(pos):
   return (pos[0] == 0) and (pos[1] == 0)

# 4 corners
def estaEmCorners(pos):
   return (abs(pos[0]) == 50) and (abs(pos[1]) == 50)

# 4 cruzamentos
def estaEmCruzamentos(pos):
   return (abs(pos[0]) == 25) and (abs(pos[1]) == 25)

# 4 metades
def estaEmMetades(pos):
   return ((abs(pos[0]) ==  0) and (abs(pos[1]) == 50)) or\
          ((abs(pos[0]) == 50) and (abs(pos[1]) ==  0))

def estaEmBoulevard(pos):
   return (abs(pos[0]) == abs(pos[1])) or\
         ((abs(pos[0]) + abs(pos[1])) == 50)

def estaEmViaExpressa(pos):
   return (abs(pos[0]) == abs(pos[1])) or\
        (((abs(pos[0]) == 50) or (pos[0] == 0)) or\
         ((abs(pos[1]) == 50) or (pos[1] == 0)))

def posFinalValida(pos):
   prox = moveCarro(pos[0], pos[1])
   if estaNoCentro(pos[0]) or estaEmCorners(pos[0]) or estaNoCentro(prox) or estaEmCorners(prox):
      return not estaEmViaExpressa(prox)
   elif estaEmMetades(pos[0]):
      return not estaEmViaExpressa(prox)
   elif estaEmBoulevard(pos[0]) and estaEmViaExpressa(pos[0]):
      return not estaEmViaExpressa(prox) or (estaEmViaExpressa(prox) and not estaEmBoulevard(prox))
   elif estaEmViaExpressa(pos[0]):
      return not estaEmViaExpressa(prox) or (estaEmViaExpressa(prox) and estaEmBoulevard(prox) and not estaEmMetades(prox))
   else:
      pass
   return True

def converteTexto(pos):
   if pos[0][0] >= 0:
      texto = "A" + str(abs(pos[0][0])) + "E"
   else:
      texto = "A" + str(abs(pos[0][0])) + "W"
   texto += " "
   if pos[0][1] >= 0:
      texto += "S" + str(abs(pos[0][1])) + "N"
   else:
      texto += "S" + str(abs(pos[0][1])) + "S"
   texto += " " + LETRA_DIRECAO[pos[1]]
   return texto

linhas = sys.stdin.readlines()
inicio = True
pos = []
for linha in linhas:
   linha = linha.replace("\n","")
   if linha == "END":
      break
   if len(linha) <= 1:
      continue
   linha = linha.split()

   if inicio:
      pos = encontraPos(linha)
      inicio = False
      if __debug__:
         print([" ".join(linha)])
      continue

   if comandoValido(linha):
      aplicaComando(linha, pos)

   if __debug__:
      if pos:
         sys.stdout.write("{:>20}".format(" ".join(linha)) + ": " + "{:<10}".format(converteTexto(pos)) + "\n")

   if linha[0] == "STOP":
      # ou nao estamos em via expressa ou acabamos de sair dela
      if pos and posFinalValida(pos):
         sys.stdout.write(converteTexto(pos) + "\n")
      else:
         sys.stdout.write("Illegal stopping place\n")
      inicio = True
      pos = []

exit(0)
