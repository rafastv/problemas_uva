import sys
from random import *

# gera profiles
for i in range(50):
   profile = "P: "
   profile += str(randrange(0,255)) + " "
   for j in range(2+int(random()*18)):
      palavra = ""
      for k in range(1+int(random()*4)):
         palavra += chr(randrange(97,122))
      profile += palavra + " "
   print (profile[:-1])

# gera titulos
for i in range(250):
   titulo = "T: "
   # maximo 255 caracteres
   for j in range(5+int(random()*49)):
      palavra = ""
      for k in range(1+int(random()*4)):
         palavra += chr(randrange(97,122))
      titulo += palavra + " "
   # ultimos 5 caracteres
   for k in range(1+int(random()*4)):
      titulo += chr(randrange(97,122))
   titulo += "|"       
   conta = 0
   while conta < len(titulo):
      if conta > 0:
         sys.stdout.write("   ")
      print (titulo[conta:conta+77].strip())
      conta += 77

print("#")
