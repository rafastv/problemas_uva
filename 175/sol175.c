#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>

typedef int bool;
#define VERDADEIRO 1
#define FALSO 0
#define mod(a,b) (b + (a % b)) % b

#define MAX_TITULOS 250
#define MAX_PROFILES 50
#define MAX_PALAVRAS 255
#define MAX_STRING 512

unsigned char SAIDA[MAX_PROFILES][MAX_TITULOS];
char PROFILES[MAX_PROFILES][MAX_PALAVRAS][MAX_STRING];
char TITULOS[MAX_TITULOS][MAX_STRING];
int DISTANCIAS[MAX_PROFILES];

/* 
TAMP: tamanho da lista de profiles
TAMT: tamanho da lista de titulos
POSP: quantas posicoes ocupadas do vetor de palavras existem para cada profile  
POSS: quantas posicoes ocupadas do vetor de titulos existem para cada saida
DISTANCIAS: distancia maxima entre palavras de um profile
*/  

int TAMP, TAMT, POSP[MAX_PROFILES], POSS[MAX_PROFILES];

void salvaSAIDAordenada(int i, unsigned int l)
{
   int k, insere = -1;
   for (k=0;k<POSS[i];k++)
   {
     if (l < SAIDA[i][k])
     {
        if ((k > 0) && (l > SAIDA[i][k-1]))
        {
           insere = k;
           break;
        }
        if (k == 0)
        {
           insere = k;
           break;
        }
     }
   }
   if (l > SAIDA[i][POSS[i]-1])
      insere = POSS[i];
   if (POSS[i] == 0)
      insere = 0;
   if (insere >= 0)
   {
      POSS[i]++;
      for (k=POSS[i]-1;k>insere;k--)
         SAIDA[i][k] = SAIDA[i][k-1];
      SAIDA[i][insere] = l;
   }
}

bool existeNaFrase(char titulo[MAX_STRING], char palavra1[MAX_STRING], char palavra2[MAX_STRING], int distancia)
{
   const char delimitador[2] = " ";
   char tmp[MAX_STRING];
   char *posicao_string;
   bool primeira_apareceu = FALSO, segunda_apareceu = FALSO;
   int conta = 0;
   strcpy(tmp, titulo);
   /*printf("P1 %s P2 %s TITULO %s\n", palavra1, palavra2, titulo);*/
   posicao_string = strtok(tmp, delimitador);
   while (posicao_string != NULL) 
   {
      /*printf("P1 %s P2 %s ATUAL %s\n", palavra1, palavra2, posicao_string);*/
      if ((!primeira_apareceu) && (strcmp(palavra1, posicao_string) == 0))
      {
         if (segunda_apareceu)
         {
            if (conta <= distancia)
               return VERDADEIRO;
            /* se a distancia atual eh muito grande, conta a partir de agora */
            segunda_apareceu = FALSO;
         }
         /* nao se conta a palavra chave */
         conta = -1;
         primeira_apareceu = VERDADEIRO;
      }
      else if ((!segunda_apareceu) && (strcmp(palavra2, posicao_string) == 0))
      {
         if (primeira_apareceu)
         {
            if (conta <= distancia)
               return VERDADEIRO;
            /* se a distancia atual eh muito grande, conta a partir de agora */
            primeira_apareceu = FALSO;
         }
         /* nao se conta a palavra chave */
         conta = -1;
         segunda_apareceu = VERDADEIRO;
      }
      else if ((primeira_apareceu) && (strcmp(palavra1, posicao_string) == 0) &&\
              (strcmp(palavra1, palavra2) != 0))
      {
         /* soh atualiza palavra chave se palavra1 != palavra2 */
         conta = -1;
      }
      else if ((segunda_apareceu) && (strcmp(palavra2, posicao_string) == 0) &&\
              (strcmp(palavra1, palavra2) != 0))
      {
         /* soh atualiza palavra chave se palavra1 != palavra2 */
         conta = -1;
      }
      else
      {
         ;
      }
      conta++;
      posicao_string = strtok(NULL, delimitador);
   }
   /*printf("NAO ENCONTREI\n");*/
   return FALSO;
}

void imprimeSaida()
{
   int i,j;
   for (i=0;i<TAMP;i++)
   {
      printf("%d: ", i+1);
      if (POSS[i] > 0)
         printf("%u", SAIDA[i][0]+1); 
      for (j=1;j<POSS[i];j++)
         printf(",%u", SAIDA[i][j]+1); 
      printf("\n");
   }
}

void imprimeProfilesTitulos()
{
    int i,j;
    printf("TAMP %d TAMT %d\n", TAMP, TAMT);
    for (i=0;i<TAMP;i++)
    {
        printf("PROFILE %d [%d]: ", i+1, DISTANCIAS[i]);
        for (j=0;j<POSP[i];j++)
            printf("%s ", PROFILES[i][j]);
        printf("\n");
    }
    for (i=0;i<TAMT;i++)
        printf("TITULO %d TAM %ld: %s\n", i+1, strlen(TITULOS[i]), TITULOS[i]);
}

void salvaPalavra(char buffer[MAX_STRING])
{
    PROFILES[TAMP][POSP[TAMP]][0] = '\0';
    strcpy(PROFILES[TAMP][POSP[TAMP]], buffer);
    POSP[TAMP]++;
}

void salvaFrase(char buffer[MAX_STRING])
{
    strcpy(TITULOS[TAMT], buffer);
    TAMT++;
}

int main()
{
    char c, buffer[MAX_STRING] = "";
    int idx, i, j, k, l;
    bool ler_profiles, ler_titulos, numero_lido;

    idx = 0;
    TAMP = 0;
    TAMT = 0;
    POSP[TAMP] = 0;
    for (i=0; i<MAX_PROFILES; i++)
    {
       DISTANCIAS[i] = 0;
       for (j=0;j<MAX_PALAVRAS;j++)
          memset(PROFILES[i][j], 0, MAX_STRING);
    }
    for (i=0; i<MAX_TITULOS; i++)
       memset(TITULOS[i], 0, MAX_STRING);
    ler_profiles = FALSO;
    ler_titulos = FALSO;
    numero_lido = FALSO;
    while (fscanf(stdin, "%c", &c)>EOF)
    {
        if ((c == '#') && (!ler_profiles) && (!ler_titulos))
            break;
        else if ((c == 'P') && (!ler_profiles) && (!ler_titulos))
           ler_profiles = VERDADEIRO;
        else if ((c == 'T') && (!ler_profiles) && (!ler_titulos))
           ler_titulos = VERDADEIRO;
        else
        {
            /* ler numero */
            if ((c >= 48) && (c <= 57) && (ler_profiles) && (!numero_lido))
                DISTANCIAS[TAMP] = 10*(DISTANCIAS[TAMP]) + (c-48);
            /* converte para minuscula */
            if ((c >= 65) && (c <= 90) && (ler_titulos))
                c+= 32;
            /* espaco vazio ou tabulacao */
            if ((c == 32) || (c == 9))
            {
               if ((ler_profiles) && (idx > 0))
               {
                  salvaPalavra(buffer);
                  buffer[0] = '\0';
                  idx = 0;
                  numero_lido = VERDADEIRO;
                  continue;
               }
               if ((ler_titulos) && (idx > 0) && (buffer[idx-1] != 32))
               {
	          buffer[idx] = ' ';
	          idx++;
                  buffer[idx] = '\0';
                  continue;
               }
            } 
            /* nova linha */
            if ((c == 10) && (ler_profiles))
            {
               if (idx > 0)
               {
                  salvaPalavra(buffer);
                  TAMP++;
                  POSP[TAMP] = 0;
                  buffer[0] = '\0';
                  idx = 0;
                  ler_profiles = FALSO;
                  numero_lido = FALSO;
                  continue;
               }
            }
            if ((c == '|') && (ler_titulos))
            {
               if (idx == 0)
               {
                  TITULOS[TAMT][0] = '\0';
                  TAMT++;
               }
               if (idx > 0)
               {
                  salvaFrase(buffer);
	          buffer[0] = '\0';
	          idx = 0;
                  ler_titulos = FALSO;
                  continue;
               }
            }
            if ((c >= 97) && (c <= 122) && ((ler_titulos) || (ler_profiles)))
            {
	        buffer[idx] = c;
	        idx++;
                buffer[idx] = '\0';
            }
	}
    }
    /*imprimeProfilesTitulos();*/
    /* percorre lista de profiles */
    for (i=0;i<TAMP;i++)
    {
       SAIDA[i][0] = 0;
       POSS[i] = 0;
       /* escolhe duas palavras da lista de profiles */
       for (j=0;j<POSP[i];j++)
       {
          for (k=j+1;k<POSP[i];k++)
          {
             /* percorre lista de titulos */
             for (l=0;l<TAMT;l++)
             {
                 /*printf("PROFILE %d TESTE TITULO %d\n", i+1, l+1);*/
                 if (existeNaFrase(TITULOS[l], PROFILES[i][j], PROFILES[i][k], DISTANCIAS[i]))
                    salvaSAIDAordenada(i, l);
             }
          } 
       }
    }
    imprimeSaida();
    return 0;
}

