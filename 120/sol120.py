import sys


def imprime(lista):
    sys.stdout.write(str(lista[0]));
    for i in range(1,len(lista)):
        sys.stdout.write(' ' + str(lista[i]));
    sys.stdout.write('\n');

def maior(pilha, n):
    return  max(range(len(pilha)-n), key=pilha.__getitem__)

def flip(pilha, n):
    p = pilha[:n]
    p.reverse()
    p += pilha[n:]
    return p

for linha in sys.stdin:
    vars = linha.split()
    if len(vars) == 0:
        continue
    pilha = []
    lflips = []
    for v in vars:
        pilha += [int(v)]
    tam = len(pilha)
    imprime(pilha)
    for v in range(tam-1, -1, -1):
        idx = maior(pilha, tam-1-v)
        if (idx == v):
            continue
        if (idx > 0):
            pilha = flip(pilha, idx+1)
            lflips += [tam-idx]
        else:
            pilha = flip(pilha, v+1)
            lflips += [tam-v]
        if idx == 0:
            continue
        pilha = flip(pilha, v+1)
        lflips += [tam-v]
    lflips += [0]
    imprime(lflips)
exit(0)
