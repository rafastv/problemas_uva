import sys

def encontraTroca(ciclo, n_pessoas, desloca, pos_morto):
   conta = 0
   pos_troca = pos_morto
   while (conta < desloca):
       pos_troca = (pos_troca + 1) % n_pessoas
       if pos_troca == pos_morto:
           pos_troca = (pos_troca + 1) % n_pessoas
       conta +=1
   return pos_troca

for linha in sys.stdin:

   partes = linha.split()

   if partes[0] == partes[1] and partes[0] == '0':
       break

   n_pessoas = int(partes[0])
   desloca = int(partes[1])

   caminho = []
   tamanho = n_pessoas
   pos_morto = (n_pessoas-1) 
   ciclo = [i for i in range(n_pessoas)]
   while n_pessoas > 1:
       pos_morto = (pos_morto + desloca) % n_pessoas
       pos_troca = encontraTroca(ciclo, n_pessoas, desloca, pos_morto)
       caminho += [ciclo[pos_morto]]
       valor = ciclo[pos_troca]
       ciclo[pos_morto] = ciclo[pos_troca]
       ciclo = ciclo[:pos_troca] + ciclo[pos_troca+1:]
       n_pessoas -= 1
       pos_morto = ciclo.index(valor)

   if 0 in caminho:
       idx = ciclo[0]
       saida = tamanho-1
   else:
       idx = 0
       saida = 0

   while idx != 0:
       saida = (saida+1) % (tamanho)
       idx = (idx+1) % (tamanho+1)

   print(saida+1)

exit(0)

