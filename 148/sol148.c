#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <float.h>
#include <limits.h>
#include <math.h>

#define mod(a,b) (b + (a % b)) % b
typedef int bool;
#define FALSE 0
#define TRUE 1
#define PRECISAO 1e-9

char DICIONARIO[2000][10000];
int CONFIGURACAO_DICIONARIO[2000][26];
int TAM_DIC;

bool encontraPalavra(char frase[10000], char palavra[10000])
{
   int i, j, conta;
   int tam_frase = strlen(frase), tam_palavra = strlen(palavra);
   bool existe = FALSE;
   for (i=0;i<=tam_frase-tam_palavra;i++)
   {
      conta = 0; 
      for (j=0;j<tam_palavra;j++)
         if (frase[i+j] != palavra[j])
            break;
         else
            conta++;
      /* ============ TESTES ============= */
      /* palavra == frase                  */
      /* palavra == ultima palavra frase   */
      /* palavra == primeira palavra frase */
      /* palavra == palavra meio frase     */
      /* ============ TESTES ============= */
      if ((conta == tam_palavra) && 
         (((i+tam_palavra == tam_frase)  && (i == 0)) || 
          ((i+tam_palavra == tam_frase)  && (frase[i-1] == ' ')) || 
          ((frase[i+tam_palavra] == ' ') && (i == 0)) || 
          ((frase[i+tam_palavra] == ' ') && (frase[i-1] == ' '))))
         return TRUE;
   }
   return FALSE;
}

void imprimeConfiguracao(int configuracao[26])
{
   int i;
   for (i=0;i<26;i++)
      printf("%d", configuracao[i]);
   printf("\n");
}

void imprimeDicionario()
{
   int i;
   for (i=0;i<TAM_DIC;i++)
      printf("(%s)\n", DICIONARIO[i]);
}

void defineConfiguracao(char buffer[10000], int configuracao[26])
{
   int i;
   for (i=0; i<10000; i++)
   {
      if (buffer[i] == '\0')
         break;
      if ((buffer[i] == ' ') || (buffer[i] == '\t'))
         continue;
      configuracao[buffer[i]-'A']++;
   }
}

bool configuracaoEstaNula(int configuracao[26])
{
   int i;
   for (i=0; i<26; i++)
      if (configuracao[i] > 0)
         return FALSE;
   return TRUE;
}

bool podeRemoverPalavra(int confMinuendo[26], int confSubtraendo[26])
{
   int i;
   for (i=0; i<26; i++)
      if (confMinuendo[i] < confSubtraendo[i])
         return FALSE;
   return TRUE;
}

void removePalavra(int confMinuendo[26], int confSubtraendo[26])
{
   int i;
   for (i=0; i<26; i++)
      confMinuendo[i] -= confSubtraendo[i];
}

bool encontraAnagramas(char buffer[10000], int configuracao[26], int pos, char frase[10000])
{
   if (configuracaoEstaNula(configuracao))
   {
       /* imprime frase formada */
       printf("%s = %s\n", buffer, frase);
       return TRUE;
   }
   int i,k;
   char *encontrei;
   int tam_frase, confDiferenca[26];
   bool retorno, saida = FALSE;
   for (i=pos; i<TAM_DIC;i++)
   {
       if (encontraPalavra(buffer, DICIONARIO[i]))
          continue;
       if (podeRemoverPalavra(configuracao, CONFIGURACAO_DICIONARIO[i]))
       {
           tam_frase = strlen(frase);
           strcat(frase, DICIONARIO[i]);
           /*imprimeConfiguracao(configuracao);*/
           memcpy(confDiferenca, configuracao, 26*sizeof(int));
           removePalavra(confDiferenca, CONFIGURACAO_DICIONARIO[i]);
           if (!configuracaoEstaNula(confDiferenca))
              strcat(frase, " ");
           retorno = encontraAnagramas(buffer, confDiferenca, i+1, frase);
           saida = saida || retorno;
           /* remove palavra adicionada da frase atual */
           frase[tam_frase] = '\0';
       }
   }
   return saida;
}

int main()
{
    char buffer[10000], frase_saida[10000];
    int configuracao_frase[26];
    bool fim = FALSE;

    TAM_DIC = 0;
    while (fgets(buffer,10000,stdin) != NULL)
    {
        /* remove caractere de nova linha */
        buffer[strlen(buffer)-1]='\0';
        /* fim da entrada */
        if (buffer[0] == '#')
        {
            if (fim)
                break;
            else
                fim = TRUE;
            continue;
        }
        if (!fim)
        {
           /* copia palavras para dicionario */
           memset(CONFIGURACAO_DICIONARIO[TAM_DIC], 0, sizeof(int)*26);
           defineConfiguracao(buffer, CONFIGURACAO_DICIONARIO[TAM_DIC]);
           strcpy(DICIONARIO[TAM_DIC++], buffer);
           /*imprimeConfiguracao(CONFIGURACAO_DICIONARIO[TAM_DIC-1]);*/
        }
        else
        {
           memset(configuracao_frase, 0, sizeof(int)*26);
           frase_saida[0] = '\0';
           defineConfiguracao(buffer, configuracao_frase);
           if (!encontraAnagramas(buffer, configuracao_frase, 0, frase_saida))
              continue;
        }

    }
    /*imprimeDicionario();*/
    return 0;
}
