#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>

typedef int bool;
#define true 1
#define false 0

typedef struct pilha
{
    unsigned char numero; /*face_value*/
    unsigned char naipe; /* suits */
    struct pilha *anterior;
} Tpilha;

Tpilha MONTES[52]; 
int PILHAS_CARTAS = 0;

void iniciaPilha(Tpilha *topo)
{
    /* guarda informacoes da pilha */
    topo->numero = (unsigned char) 0;
    topo->naipe = 'R'; /** RAIZ OU ROOT **/
    topo->anterior = NULL;
}

void push(Tpilha *topo, unsigned char numero, unsigned char naipe)
{
    Tpilha *p = malloc(sizeof(Tpilha));
    p->numero = numero;
    p->naipe = naipe;
    p->anterior = topo->anterior;
    topo->anterior = p;
    topo->numero++;
}

Tpilha* pop(Tpilha *topo)
{
    Tpilha *ultimo_elemento = topo->anterior;
    if (ultimo_elemento != NULL)
    {
        topo->anterior = ultimo_elemento->anterior;
	ultimo_elemento->anterior = NULL;
	topo->numero--;
    }
    return ultimo_elemento;
}

void limpaPilha(Tpilha *topo)
{
    while (topo->anterior != NULL)
	free(pop(topo));
}

void imprimePilha(Tpilha *topo)
{
    Tpilha *tmp = topo->anterior;
    printf("TOPO ");
    while (tmp != NULL)
    {
	printf("%c%c ", tmp->numero, tmp->naipe);
	tmp = tmp->anterior;
    }
    printf(" total: %u\n", (unsigned int) topo->numero);
}

void imprimeMontes()
{
    int i;
    for (i=0;i<PILHAS_CARTAS;i++)
	imprimePilha(&MONTES[i]);
}

bool naPilha(Tpilha *topo, unsigned char numero, unsigned char naipe)
{
    Tpilha *tmp = topo->anterior;
    bool saida = false;
    while ((tmp != NULL) && (saida == false))
    {
	saida = (numero == tmp->numero) || (naipe == tmp->naipe);
	tmp = tmp->anterior;
    }
    return saida;
}

void salvaCarta(char buffer[3])
{
    iniciaPilha(&MONTES[PILHAS_CARTAS]);
    push(&MONTES[PILHAS_CARTAS], buffer[0], buffer[1]);
}

bool podeTrocar(int v, int distancia)
{
    int u = v-distancia;
    if (u >= 0)
    {
        Tpilha *cartaAtual = MONTES[v].anterior;
        Tpilha *cartaLonge = MONTES[u].anterior;
        if ((cartaAtual->naipe == cartaLonge->naipe) || 
            (cartaAtual->numero == cartaLonge->numero)) 
	    return true;
    }
    return false;
}

void trocaCarta(int v, int u)
{
    Tpilha *tmp;
    tmp = pop(&MONTES[v]);
    push(&MONTES[u], tmp->numero, tmp->naipe);
}

void shiftMontes(int pos)
{
    int i;
    if (MONTES[pos].numero == 0)
    {
        for (i=pos;i<PILHAS_CARTAS-1;i++)
        {
            MONTES[i].naipe = MONTES[i+1].naipe; 
            MONTES[i].numero = MONTES[i+1].numero;
            MONTES[i].anterior = MONTES[i+1].anterior;
        }
        PILHAS_CARTAS--;
    }
}

void jogaPacienciaAccordion()
{
    int i, du, dist = 0, v = 0, u = 0;
    int pos = PILHAS_CARTAS+1;
    int movimentos[52]={0,0,0,0,0,0,0,0,0,0,0,0,0,
                        0,0,0,0,0,0,0,0,0,0,0,0,0,
                        0,0,0,0,0,0,0,0,0,0,0,0,0,
                        0,0,0,0,0,0,0,0,0,0,0,0,0};
    while (u < PILHAS_CARTAS)
    {
       	    /** prefere trocar terceira carta **/
            if (podeTrocar(u, 3))
            {
	        trocaCarta(u, u-3);
	        shiftMontes(u);
		u=u-3;
		continue;
            }
            /** tenta trocar primeira carta **/
            if (podeTrocar(u, 1))
            {
	        trocaCarta(u, u-1);
	        shiftMontes(u);
		u=u-1;
		continue;
            }
            u++;
    }
}

void imprimeSaidaJogo()
{
    int i;
    if (PILHAS_CARTAS == 1)
       printf("1 pile");
    else 
       printf("%d piles", PILHAS_CARTAS);
    printf(" remaining:");
    for (i=0;i<PILHAS_CARTAS;i++)
	printf(" %u", MONTES[i].numero);
    printf("\n");
}

int main()
{
    char c, buffer[3];
    int idx = 0;
    while ((c  = fgetc(stdin)) != EOF)
    {
	if ((c!=' ') && (c!='\n') && (c!='\t') && (c!= '#'))
	{
	    buffer[idx] = c;
	    idx++;
	}
	else
	{
	    if (buffer[0] == '#')
		break;
	    if (idx == 0)
		continue;
	    buffer[idx] = '\0';
	    salvaCarta(buffer);
	    PILHAS_CARTAS++;
	    if (PILHAS_CARTAS == 52)
	    {
		/** aplica algoritmo **/
		jogaPacienciaAccordion();
                imprimeSaidaJogo();
		/*imprimeMontes();*/
	        PILHAS_CARTAS = 0;
	    }
	    /** limpa buffer **/
	    idx = 0;
            memset(buffer,0,3);
	}
    }
    return 0;
}

