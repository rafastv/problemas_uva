import sys

DIAS_G = 0
DIAS_J = 0

DIAS_MESES = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
IDX_MESES = {"January": 0, "February": 1,    "March":  2,    "April":  3,
                 "May": 4,     "June": 5,     "July":  6,   "August":  7, 
           "September": 8,  "October": 9, "November": 10, "December": 11}

NOME_MESES = {0:  "January", 1: "February",  2:    "March",  3:    "April",
              4:      "May", 5:     "June",  6:     "July",  7:   "August", 
              8:"September", 9:  "October", 10: "November", 11: "December"}

IDX_SEMANA = {"Sunday": 0, "Monday": 1,  "Tuesday": 2, "Wednesday": 3,
            "Thursday": 4, "Friday": 5, "Saturday": 6}

NOME_SEMANA = {0: "Sunday",   1: "Monday",  2: "Tuesday", 3: "Wednesday",
               4: "Thursday", 5: "Friday",  6: "Saturday"}

# data de inicio calendarios
DATA_GREGORIANO = (1582, 9, 15, 5)
DATA_JULIANO    = (1582, 9,  5, 5)

def imprimeData(nova_data):
   sys.stdout.write(NOME_SEMANA[nova_data[3]]+' ') 
   sys.stdout.write(str(nova_data[2])+' ')
   sys.stdout.write(NOME_MESES[nova_data[1]]+' ') 
   sys.stdout.write(str(nova_data[0])) 
   sys.stdout.write('\n') 
 
def anoComDiaExtraJuliano(ano):
   return ((ano % 4) == 0)

def anoComDiaExtraGregoriano(ano):
   return (((ano % 4) == 0) and ((ano % 100) > 0)) or ((ano % 400) == 0)

data_ini = list(DATA_GREGORIANO)
#data_ini = list(DATA_JULIANO)

conta = data_ini[3]
while (data_ini[0] < 2100):
    if (data_ini[0] >= 1600):
       imprimeData(data_ini)
    data_ini[2] += 1
    conta+=1
    if anoComDiaExtraGregoriano(data_ini[0]) and (data_ini[1] == 1) and (data_ini[2] == 29):
    #if anoComDiaExtraJuliano(data_ini[0]) and (data_ini[1] == 1) and (data_ini[2] == 29):
       data_ini[3] = conta % 7
       if (data_ini[0] >= 1600):
          imprimeData(data_ini)
       data_ini[2] += 1
       conta += 1
    if data_ini[2] > DIAS_MESES[data_ini[1]]:
       data_ini[2] = 1
       data_ini[1] += 1
       if data_ini[1] > 11:
          data_ini[0] += 1
          data_ini[1] = 0
    data_ini[3] = conta % 7

exit(0)
