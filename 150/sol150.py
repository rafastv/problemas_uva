import sys

DIAS_G = 0
DIAS_J = 0

DIAS_MESES = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
IDX_MESES = {"January": 0, "February": 1,    "March":  2,    "April":  3,
                 "May": 4,     "June": 5,     "July":  6,   "August":  7, 
           "September": 8,  "October": 9, "November": 10, "December": 11}

NOME_MESES = {0:  "January", 1: "February",  2:    "March",  3:    "April",
              4:      "May", 5:     "June",  6:     "July",  7:   "August", 
              8:"September", 9:  "October", 10: "November", 11: "December"}

IDX_SEMANA = {"Sunday": 0, "Monday": 1,  "Tuesday": 2, "Wednesday": 3,
            "Thursday": 4, "Friday": 5, "Saturday": 6}

NOME_SEMANA = {0: "Sunday",   1: "Monday",  2: "Tuesday", 3: "Wednesday",
               4: "Thursday", 5: "Friday",  6: "Saturday"}

# data de inicio calendarios
DATA_GREGORIANO = (1582, 9, 15, 5)
DATA_JULIANO    = (1582, 9,  5, 5)

def extraiData(data_txt):
   ano = int(data_txt[3])
   mes = IDX_MESES[data_txt[2]] 
   dia = int(data_txt[1])
   semana = IDX_SEMANA[data_txt[0]]
   return (ano, mes, dia, semana)

def imprimeData(nova_data, tipo):
   if (tipo):
      sys.stdout.write(NOME_SEMANA[nova_data[3]]+' ') 
      sys.stdout.write(str(nova_data[2])+' ')
      sys.stdout.write(NOME_MESES[nova_data[1]]+' ') 
      sys.stdout.write(str(nova_data[0])) 
   else:
      sys.stdout.write(NOME_SEMANA[nova_data[3]]+' ') 
      sys.stdout.write(str(nova_data[2])+'* ') 
      sys.stdout.write(NOME_MESES[nova_data[1]]+' ') 
      sys.stdout.write(str(nova_data[0])) 
   sys.stdout.write('\n') 
 
def anoComDiaExtraJuliano(ano):
   return ((ano % 4) == 0)

def anoComDiaExtraGregoriano(ano):
   return (((ano % 4) == 0) and ((ano % 100) > 0)) or ((ano % 400) == 0)

# segundo problema datas estao entre 1 Janeiro de 1600 a 31 de Dezembro de 2099
def contaDiasGregoriano(data):
   # conta dias entre anos inclusive extremos
   dias  = (data[0]-DATA_GREGORIANO[0]-1)*365
   dias += sum(DIAS_MESES[DATA_GREGORIANO[1]+1:])
   dias += sum(DIAS_MESES[:data[1]])
   dias += (DIAS_MESES[DATA_GREGORIANO[1]]-DATA_GREGORIANO[2])
   dias += data[2]
   # adiciona dias extras
   ajuste0 = DATA_GREGORIANO[0] - (DATA_GREGORIANO[0]%4)
   ajuste1 = DATA_GREGORIANO[0] - (DATA_GREGORIANO[0]%100)
   ajuste2 = DATA_GREGORIANO[0] - (DATA_GREGORIANO[0]%400)
   dias += (data[0]-ajuste0)//4 
   dias -= (data[0]-ajuste1)//100
   dias += (data[0]-ajuste2)//400
   # se estamos em ano com extra, ele nao ocorreu, ou eh o dia extra
   if anoComDiaExtraGregoriano(data[0]):
      if (data[1] == 0) or ((data[1] == 1) and (data[2] <= 29)):
         dias -= 1
   return dias

def contaDiasJuliano(data):
   # conta dias entre anos inclusive extremos
   dias  = (data[0]-DATA_JULIANO[0]-1)*365
   dias += sum(DIAS_MESES[DATA_JULIANO[1]+1:])
   dias += sum(DIAS_MESES[:data[1]])
   dias += (DIAS_MESES[DATA_JULIANO[1]]-DATA_JULIANO[2])
   dias += data[2]
   # adiciona dias extras
   ajuste = DATA_JULIANO[0] - (DATA_JULIANO[0]%4)
   dias += (data[0]-ajuste)//4 
   # se estamos em ano com extra, ele nao ocorreu, ou eh o dia extra
   if anoComDiaExtraJuliano(data[0]):
      if (data[1] == 0) or ((data[1] == 1) and (data[2] <= 29)):
         dias -= 1
   return dias

def avancaDiasGregoriano(dias):
   data = [0, 0, 0, 0]
   # nova semana
   data[3] = (DATA_GREGORIANO[3] + dias)%7
   # terminar o ano de inicio
   dias -= sum(DIAS_MESES[DATA_GREGORIANO[1]+1:])
   dias -= (DIAS_MESES[DATA_GREGORIANO[1]]-DATA_GREGORIANO[2])
   # calcula novo ano
   data[0] = DATA_GREGORIANO[0]
   while (dias >= 365):
       if ((dias - 365) == 0 or 
          ((dias - 366) == 0 and anoComDiaExtraGregoriano(data[0]+1))):
          break
       dias -= 365
       data[0] += 1
       if anoComDiaExtraGregoriano(data[0]):
          dias -= 1
   data[0] += 1
   # consome meses
   k = 0
   for i in  range(12):
      if anoComDiaExtraGregoriano(data[0]) and (i == 1):
         if dias <= 29:
            k = i
            break 
         else:
            dias -= (DIAS_MESES[i]+1)
      elif (dias-DIAS_MESES[i] > 0):
         dias -= DIAS_MESES[i]
      else:
         k = i
         break 
   # novo mes
   data[1] = k 
   # novo dia
   data[2] = dias 
   return data

def avancaDiasJuliano(dias):
   data = [0, 0, 0, 0]
   # nova semana
   data[3] = (DATA_JULIANO[3] + dias)%7
   # terminar o ano de inicio
   dias -= sum(DIAS_MESES[DATA_JULIANO[1]+1:])
   dias -= (DIAS_MESES[DATA_JULIANO[1]]-DATA_JULIANO[2])
   # calcula novo ano
   data[0] = DATA_JULIANO[0]
   while (dias >= 365):
       if ((dias - 365) == 0 or 
          ((dias - 366) == 0 and anoComDiaExtraJuliano(data[0]+1))):
          break
       dias -= 365
       data[0] += 1
       if anoComDiaExtraJuliano(data[0]):
          dias -= 1
   data[0] += 1
   # consome meses
   k = 0
   for i in  range(12):
      if anoComDiaExtraJuliano(data[0]) and (i == 1):
         if dias <= 29:
            k = i
            break 
         else:
            dias -= (DIAS_MESES[i]+1)
      elif (dias-DIAS_MESES[i] > 0):
         dias -= DIAS_MESES[i]
      else:
         k = i
         break 
   # novo mes
   data[1] = k
   # novo dia
   data[2] = dias
   return data

def classificaData(data):
   global DIAS_G, DIAS_J
   DIAS_J = contaDiasJuliano(data)
   DIAS_G = contaDiasGregoriano(data)
   semanaJ = (DATA_JULIANO[3] + DIAS_J)%7
   semanaG = (DATA_GREGORIANO[3] + DIAS_G)%7
   if (semanaJ == data[3]):
      return 0
   if (semanaG == data[3]):
      return 1
   # erro
   return -1

linhas = sys.stdin.readlines()
for linha in linhas:
   linha = linha.replace("\n","")
   if linha[0] == "#":
       break

   data_txt = linha.split();
   data = extraiData(data_txt)
   tipo = classificaData(data)
   tipo = (tipo + 1) % 2
   #print (data)
   #print(avancaDiasJuliano(DIAS_J), avancaDiasGregoriano(DIAS_G))
   if (not tipo):
      nova_data = avancaDiasJuliano(DIAS_G)
   else:
      nova_data = avancaDiasGregoriano(DIAS_J)
   imprimeData(nova_data, tipo) 
   # if tipo == 0:
   #   print("ERRO")   

exit(0)
