import sys,math

global fatoriais
fatoriais = {0:1, 1:1}

def fat(n):
    if n in fatoriais.keys():
        return fatoriais[n]
    valor = n*fat(n-1)
    fatoriais.update({n: valor})
    return valor

def comb(n, k):
    return fat(n)//(fat(n-k)*fat(k))

def binomio_newton(a,b,n):
    soma = 0
    # nao usa primeiro termo n`a aproximacao de raiz (a+b)^n - a^n
    for i in range(n):
        soma += comb(n,i) * (a**(i) * b**(n-i))    
    return soma

def raiz_enesima(n, radicando):
    linha = str(radicando)
    pos_inicial = (len(linha) % n)
    primeiro = True 
    idx = 0
    e = 0
    if (pos_inicial == 0):
        pos_inicial = n
    parte = int(linha[:pos_inicial])
    for i in range(1,11):
       valor = i**n
       if valor > parte:
           idx = i-1
           break
       e = valor
    resto = parte - e
    if resto == 0:
        return idx
    pos = pos_inicial
    while (resto > 0):
        parte = int(str(resto) + linha[pos:pos+n])
        a = int(str(idx) + '0')
        e = 0
        for i in range(10):
            valor = binomio_newton(a,i,n)
            if valor > parte:
                idx = i-1
                break
            e = valor
        resto = parte - e
        idx = a + idx 
        pos += n
    return idx

lista = []
for linha in sys.stdin:
    vars = linha.split()
    lista += [int(vars[0])]
    if (len(lista) % 2 == 0):
        sys.stdout.write(str(int(round(math.e**(math.log(float(lista[-1]))/float(lista[-2]))))) + '\n')
        #sys.stdout.write(str(raiz_enesima(lista[-2], lista[-1]))+'\n')

exit(0)
