#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <float.h>
#include <limits.h>
#include <math.h>

/* solucao imaginando os poligonos convexos como aproximacoes de circulos */
/* elipses tem 0,1 ou 2 pontos de intersecao                              */
/* removendo os pontos tangentes podemos definir quem esta dentro e fora  */
/* e assim calcular regiao de uniao e interseccao quando existe           */
/* NOTA: todo poligono convexo eh circunscrito por um circulo             */

#define mod(a,b) (b + (a % b)) % b
typedef int bool;
#define false 0
#define true 1

double POLIGONOS[2][200][2];
int TAM[2] = {0, 0};
double CENTRO[2][2];

/* armazena pontos de intersecao */
double INTERSECOES[200][4];
int ITAM;

/* uma operacao armazena uniao a outra interseccao */
double OPERACOES[2][200][2];
int DTAM[2];

void imprimePoligonos()
{
    printf("\n");
    int i,j;
    for (i=0;i<2;i++)
    {
        printf("POLIGONO %d: (%.2lf %.2lf)", i+1, POLIGONOS[i][0][0], POLIGONOS[i][0][1]);
        for (j=1;j<TAM[i];j++)
            printf(" (%.2lf %.2lf)", POLIGONOS[i][j][0], POLIGONOS[i][j][1]);
        printf("\n");
        printf("CENTRO: %.2lf %.2lf\n", CENTRO[i][0], CENTRO[i][1]);
    }
}

void imprimeOperacoes()
{
    printf("\n");
    int i,j;
    for (i=0;i<2;i++)
    {
        printf("OPERACAO %d: (%.2lf %.2lf)", i+1, OPERACOES[i][0][0], OPERACOES[i][0][1]);
        for (j=1;j<DTAM[i];j++)
            printf(" (%.2lf %.2lf)", OPERACOES[i][j][0], OPERACOES[i][j][1]);
        printf("\n");
    }
}

void imprimeInterseccoes()
{
    printf("\n");
    int i;
    for (i=0;i<ITAM;i++)
        printf("INTERSECCAO %d: (P1 %.2lf %.2lf) (P2 %.2lf %.2lf)\n", i, INTERSECOES[i][0], INTERSECOES[i][1], INTERSECOES[i][2], INTERSECOES[i][3]);
}

/* calcula distancia entre vertices 2D */
double distancia(double A[2], double B[2])
{
    double x = A[0]-B[0];
    double y = A[1]-B[1];
    return sqrt(x*x + y*y);
}

/* calcula centro de circulo circunscrito */
void calculaCentro(int idx)
{
    int i;
    double x = 0, y = 0;    
    double fator = 1.0/TAM[idx];
    for (i=0;i<TAM[idx];i++)
    {
        x += POLIGONOS[idx][i][0] * fator;
        y += POLIGONOS[idx][i][1] * fator;
    }
    CENTRO[idx][0] = x;
    CENTRO[idx][1] = y;
}

/* verifica se existe interseccao entre circulos */
bool circulosInterceptam()
{
    double r1 = distancia(CENTRO[0], POLIGONOS[0][0]);
    double r2 = distancia(CENTRO[1], POLIGONOS[1][0]);
    double d = distancia(CENTRO[0], CENTRO[1]);
    if (d < (r1+r2))
        return true;
    return false; 
}

/* realiza interseccao de segmentos */
double* intersecaoSegmentos(double P[2], double Q[2], double A[2], double B[2])
{
    /* c00 = (Q[0]-P[0]) c01 = (B[0]-A[0]) b0 = (A[0]-P[0]) */
    /* c10 = (Q[1]-P[1]) c11 = (B[1]-A[1]) b1 = (A[1]-P[1]) */
    /* u*c00 - v*c01 = b0                       */
    /* u*c10 - v*c11 = b1                       */
    double c00 = Q[0]-P[0];
    double c01 = B[0]-A[0];
    double c10 = Q[1]-P[1];
    double c11 = B[1]-A[1];
    double  b0 = A[0]-P[0];
    double  b1 = A[1]-P[1];
    double  ta, tb, t1, t2;
    double fator, u_numerador, u_denominador, v_numerador, v_denominador;
    
    /*printf("u*%lf - v*%lf = %lf\n",c00,c01,b0);
    printf("u*%lf - v*%lf = %lf\n",c10,c11,b1);*/
    if (((fabs(Q[0]-B[0]) <= 0.001) && (fabs(Q[1]-B[1]) <= 0.001)) &&
        ((fabs(P[0]-A[0]) <= 0.001) && (fabs(P[1]-A[1]) <= 0.001)))
    {
        /*printf("CASO 0\n");*/
        u_numerador = 0.0;
        u_denominador = 1.0;
        v_numerador = 0.0;
        v_denominador = 1.0;
    }
    else if ((fabs(c01) > 0.001) && (fabs(c11) > 0.001))
    {
        /*printf("CASO 1\n");*/
        fator = -c01/c11;
        u_numerador = b1*fator + b0;
        u_denominador = c10*fator + c00;
        v_numerador = (u_numerador/u_denominador)*c00 - b0;
        v_denominador = c01;
    }
    else if ((fabs(c00) > 0.001) && (fabs(c10) >  0.001))
    {
        /*printf("CASO 2\n");*/
        fator = -c00/c10;
        v_numerador = b1*fator + b0;
        v_denominador = -c11*fator - c01;
        u_numerador = b0 + (v_numerador/v_denominador)*c01;
        u_denominador = c00;
    }
    else if (((fabs(c00) >  0.001) && (fabs(c01) <= 0.001)) &&
             ((fabs(c10) <= 0.001) && (fabs(c11) >  0.001)))
    {
        /*printf("CASO 3\n");*/
        u_numerador = b0;
        u_denominador = c00;
        v_numerador = -b1;
        v_denominador = c11;
    }
    else if (((fabs(c00) <= 0.001) && (fabs(c01) >   0.001)) &&
             ((fabs(c10) >  0.001) && (fabs(c11) <=  0.001)))
    {
        /*printf("CASO 4\n");*/
        u_numerador = b1;
        u_denominador = c10;
        v_numerador = -b0;
        v_denominador = c01;
    }
    else if (((fabs(c00) <= 0.001) && (fabs(c01) <= 0.001)) &&
             ((fabs(c10) >  0.001) && (fabs(c11) >  0.001)))
    {
        /*printf("CASO 5\n");*/
        u_denominador = 0.0;
        if (fabs(Q[0] - B[0]) < 0.001)
        {
            t1 = fabs(Q[1] - A[1]) + fabs(P[1] - A[1]);
            t2 = fabs(A[1] - P[1]) + fabs(B[1] - P[1]);
            ta = fabs(Q[1] - P[1]);
            tb = fabs(A[1] - B[1]);
            if (fabs(t1 - ta) < 0.001)
            {
                u_numerador = fabs(P[1] - A[1]);
                u_denominador = ta;
                v_numerador = 0.0;
                v_denominador = 1.0;
            }
            if (fabs(t2 - tb) < 0.001)
            {
                u_numerador = 0.0;
                u_denominador = 1.0;
                v_numerador = fabs(A[1] - P[1]);
                v_denominador = tb;
            }
        }
    }
    else if (((fabs(c00) >  0.001) && (fabs(c01) >  0.001)) &&
             ((fabs(c10) <= 0.001) && (fabs(c11) <= 0.001)))
    {
        /*printf("CASO 6\n");*/
        u_denominador = 0.0;
        if (fabs(Q[1] - B[1]) < 0.001)
        {
            t1 = fabs(Q[0] - A[0]) + fabs(P[0] - A[0]);
            t2 = fabs(A[0] - P[0]) + fabs(B[0] - P[0]);
            ta = fabs(Q[0] - P[0]);
            tb = fabs(A[0] - B[0]);
            if (fabs(t1 - ta) < 0.001)
            {
                u_numerador = fabs(P[0] - A[0]);
                u_denominador = ta;
                v_numerador = 0.0;
                v_denominador = 1.0;
            }
            if (fabs(t2 - tb) < 0.001)
            {
                u_numerador = 0.0;
                u_denominador = 1.0;
                v_numerador = fabs(A[0] - P[0]);
                v_denominador = tb;
            }
        }
    }
    else
    {
        /*printf("CASO NULO\n");*/
        /* por completude */
        u_denominador = 0.0;
    }
    if ((fabs(u_denominador) < 0.001) || (fabs(v_denominador) < 0.001))
        return NULL;
    double u = u_numerador/u_denominador, v = v_numerador/v_denominador;
     
    /*printf("INTERCEPTEI u=%lf v=%lf?\n",u,v);*/
    /* soh se considera um ponto, quando u=0 e v=0, para evitar repeticoes */
    if ((u >= 0.999) || (u < -0.001) || (v >= 0.999) || (v < -0.001))
        return NULL;
    /*printf("SIM u=%lf v=%lf\n",u,v);*/
    double *pi = malloc(sizeof(double)*2);
    pi[0] = u;
    pi[1] = v;
    return pi;
}

void copiaPoligono(int destino, int origem, int id_i, int id_f, double ant, double prox)
{
    int i, ini, fim;
    ini = id_i;
    fim = id_f;
    if ((ini != fim) || ((ini == fim) && (ant > prox)))
    {
        ini++; 
        fim = id_f;
        if (ini > fim)
            fim = TAM[origem] + id_f;
        /*printf("PRoX %lf %d\n",prox, prox>0.0);*/
        /* caso o ultimo vertice seja um vertice do poligono */
        if (fabs(prox) <= 0.0)
            fim--;
        for (i=ini; (i<=fim); i++)
        {
            OPERACOES[destino][DTAM[destino]][0] = POLIGONOS[origem][i%TAM[origem]][0];
            OPERACOES[destino][DTAM[destino]][1] = POLIGONOS[origem][i%TAM[origem]][1];
            DTAM[destino]++;
        }
    }
}

/* setor circular em cruz */
bool segmentoTangente(double FIGURA_1[200][2], double FIGURA_2[200][2], int M, int N, int i, int j, double *pi)
{
    double MEIO[2], R[2], S[2], X[2], Y[2], mr, ms, mx, my;
    double ini_1, ini_2, fim_1, fim_2;
    if ((fabs(pi[0]) < 0.001) && (fabs(pi[1]) < 0.001))
    { 
        /*printf("CASO 1\n");*/
        mr = distancia(FIGURA_2[(j+1)%N], FIGURA_2[j]);
        ms = distancia(FIGURA_2[mod(j-1,N)], FIGURA_2[j]);
        mx = distancia(FIGURA_1[(i+1)%M], FIGURA_1[i]);
        my = distancia(FIGURA_1[mod(i-1,M)], FIGURA_1[i]);
                       
        R[0] = (FIGURA_2[(j+1)%N][0] - FIGURA_2[j][0])/mr;
        R[1] = (FIGURA_2[(j+1)%N][1] - FIGURA_2[j][1])/mr;
        S[0] = (FIGURA_2[mod(j-1,N)][0] - FIGURA_2[j][0])/ms;
        S[1] = (FIGURA_2[mod(j-1,N)][1] - FIGURA_2[j][1])/ms;
        X[0] = (FIGURA_1[(i+1)%M][0] - FIGURA_1[i][0])/mx;
        X[1] = (FIGURA_1[(i+1)%M][1] - FIGURA_1[i][1])/mx;
        Y[0] = (FIGURA_1[mod(i-1,M)][0] - FIGURA_1[i][0])/my;
        Y[1] = (FIGURA_1[mod(i-1,M)][1] - FIGURA_1[i][1])/my;
    }
    else if ((fabs(pi[0]) < 0.001) && (fabs(pi[1]) >= 0.001))
    {
        /*printf("CASO 2\n");*/
        mr = distancia(FIGURA_2[(j+1)%N], FIGURA_1[i]);
        ms = distancia(FIGURA_2[j], FIGURA_1[i]);
        mx = distancia(FIGURA_1[(i+1)%M], FIGURA_1[i]);
        my = distancia(FIGURA_1[mod(i-1,M)], FIGURA_1[i]);
                       
        R[0] = (FIGURA_2[(j+1)%N][0] - FIGURA_1[i][0])/mr;
        R[1] = (FIGURA_2[(j+1)%N][1] - FIGURA_1[i][1])/mr;
        S[0] = (FIGURA_2[j][0] - FIGURA_1[i][0])/ms;
        S[1] = (FIGURA_2[j][1] - FIGURA_1[i][1])/ms;
        X[0] = (FIGURA_1[(i+1)%M][0] - FIGURA_1[i][0])/mx;
        X[1] = (FIGURA_1[(i+1)%M][1] - FIGURA_1[i][1])/mx;
        Y[0] = (FIGURA_1[mod(i-1,M)][0] - FIGURA_1[i][0])/my;
        Y[1] = (FIGURA_1[mod(i-1,M)][1] - FIGURA_1[i][1])/my;
    }
    else if ((fabs(pi[0]) >= 0.001) && (fabs(pi[1]) < 0.001))
    {
        /*printf("CASO 3\n");*/
        mr = distancia(FIGURA_2[(j+1)%N], FIGURA_2[j]);
        ms = distancia(FIGURA_2[mod(j-1,N)], FIGURA_2[j]);
        mx = distancia(FIGURA_1[(i+1)%M], FIGURA_2[j]);
        my = distancia(FIGURA_1[i], FIGURA_2[j]);
                       
        R[0] = (FIGURA_2[(j+1)%N][0] - FIGURA_2[j][0])/mr;
        R[1] = (FIGURA_2[(j+1)%N][1] - FIGURA_2[j][1])/mr;
        S[0] = (FIGURA_2[mod(j-1,N)][0] - FIGURA_2[j][0])/ms;
        S[1] = (FIGURA_2[mod(j-1,N)][1] - FIGURA_2[j][1])/ms;
        X[0] = (FIGURA_1[(i+1)%M][0] - FIGURA_2[j][0])/mx;
        X[1] = (FIGURA_1[(i+1)%M][1] - FIGURA_2[j][1])/mx;
        Y[0] = (FIGURA_1[i][0] - FIGURA_2[j][0])/my;
        Y[1] = (FIGURA_1[i][1] - FIGURA_2[j][1])/my;
    }
    else
    {
        /*printf("CASO 4\n");*/
        return false;
    }
    ini_1 = Y[0];
    fim_1 = X[0];
    if (X[0] < Y[0])
    {
        ini_1 = X[0];
        fim_1 = Y[0];
    }
    ini_2 = S[0];
    fim_2 = R[0];
    if (R[0] < S[0])
    {
        ini_2 = R[0];
        fim_2 = S[0];
    }
    /*printf ("ini_1 %lf fim_1 %lf ini_2 %lf fim_2 %lf\n",ini_1,fim_1,ini_2,fim_2);*/
    if (((ini_1 < fim_2) && (fim_1  > ini_2)) || ((ini_2 < fim_1) && (ini_1 < fim_2)))
        return false;
    return true;
}

void insereOrdenadoPorUVs(double u, double i, double v, double j)
{
    int k;
    if ((ITAM > 0) && ((INTERSECOES[ITAM-1][1] == i) || (INTERSECOES[ITAM-1][3] == j)))
    {
        for (k=ITAM-1;k>=0;k--)
        {
            if (INTERSECOES[k][1] == i)
            {
                if (INTERSECOES[k][0] > u)
                {
                    INTERSECOES[k+1][0] = INTERSECOES[k][0];
                    INTERSECOES[k+1][1] = INTERSECOES[k][1];
                    INTERSECOES[k+1][2] = INTERSECOES[k][2];
                    INTERSECOES[k+1][3] = INTERSECOES[k][3];
                    if (k == 0)
                    {
                        INTERSECOES[k][0] = u; 
                        INTERSECOES[k][1] = i; 
                        INTERSECOES[k][2] = v; 
                        INTERSECOES[k][3] = j;
                    }
                }
                else
                {
                    INTERSECOES[k+1][0] = u; 
                    INTERSECOES[k+1][1] = i; 
                    INTERSECOES[k+1][2] = v; 
                    INTERSECOES[k+1][3] = j;
                    break;
                } 
            } 
            else if (INTERSECOES[k][3] == j)
            {
                if (INTERSECOES[k][2] > v)
                {
                    INTERSECOES[k+1][0] = INTERSECOES[k][0];
                    INTERSECOES[k+1][1] = INTERSECOES[k][1];
                    INTERSECOES[k+1][2] = INTERSECOES[k][2];
                    INTERSECOES[k+1][3] = INTERSECOES[k][3];
                    if (k == 0)
                    {
                        INTERSECOES[k][0] = u; 
                        INTERSECOES[k][1] = i; 
                        INTERSECOES[k][2] = v; 
                        INTERSECOES[k][3] = j;
                    }
                }
                else
                {
                    INTERSECOES[k+1][0] = u; 
                    INTERSECOES[k+1][1] = i; 
                    INTERSECOES[k+1][2] = v; 
                    INTERSECOES[k+1][3] = j;
                    break;
                } 
            } 
            else
            {
                INTERSECOES[k+1][0] = u; 
                INTERSECOES[k+1][1] = i; 
                INTERSECOES[k+1][2] = v; 
                INTERSECOES[k+1][3] = j;
                break;
            }
        }
    }
    else
    {
        INTERSECOES[ITAM][0] = u; 
        INTERSECOES[ITAM][1] = i; 
        INTERSECOES[ITAM][2] = v; 
        INTERSECOES[ITAM][3] = j; 
    }
    ITAM++;
}

/* realiza teste de interseccao entre poligonos */
void intersecaoPoligonos()
{
    int i,j;
    double *pi = NULL;
    DTAM[0] = 0;
    DTAM[1] = 0;
    ITAM = 0;
    for (i=0;i<TAM[0];i++)
    {
        for (j=0; j<TAM[1]; j++)
        {
            pi = intersecaoSegmentos(POLIGONOS[0][i], POLIGONOS[0][(i+1)%TAM[0]], POLIGONOS[1][j], POLIGONOS[1][(j+1)%TAM[1]]);
            if ((pi != NULL) && (segmentoTangente(POLIGONOS[0], POLIGONOS[1], TAM[0], TAM[1], i, j, pi)))
            {
                /*printf("(U: %lf V: %lf) (%d, %d) com  (%d,%d) SEGMENTO TANGENTE!\n",pi[0],pi[1],i,(i+1)%TAM[0], j,(j+1)%TAM[1]);*/
                free(pi);
                pi = NULL;
            }
            if (pi != NULL)
            {
                insereOrdenadoPorUVs(pi[0], i, pi[1], j);
                free(pi);
                pi = NULL;
            }
        }
    }
}

double* calculaPonto(double u, double P[2], double Q[2])
{
    double *saida = malloc(sizeof(double)*2);
    saida[0] = P[0] + u*(Q[0]-P[0]);
    saida[1] = P[1] + u*(Q[1]-P[1]);
    return saida;
}

void constroiOperacoes()
{
    int k, ultimo, i[2],j[2];
    double *pi, u[2], v[2];
    DTAM[0] = 0;
    DTAM[1] = 0;
    ultimo = -1;
    for (k=0;k<ITAM;k++)
    {
        u[0] = INTERSECOES[k][0];
        i[0] = INTERSECOES[k][1];
        v[0] = INTERSECOES[k][2];
        j[0] = INTERSECOES[k][3];
        u[1] = INTERSECOES[(k+1)%ITAM][0];
        i[1] = INTERSECOES[(k+1)%ITAM][1];
        v[1] = INTERSECOES[(k+1)%ITAM][2];
        j[1] = INTERSECOES[(k+1)%ITAM][3];
        if ((u[0] > 0) || (u[1] > 0) || (v[0] > 0) || (v[1] > 0) ||
            (abs(j[1]-j[0]) > 1.0) || (abs(i[1]-i[0]) > 1.0))
            ultimo++;
        /* primeiro ciclo */
        pi = calculaPonto(u[0], POLIGONOS[0][i[0]], POLIGONOS[0][(i[0]+1)%TAM[0]]);
        OPERACOES[0][DTAM[0]][0] = pi[0];
        OPERACOES[0][DTAM[0]][1] = pi[1];
        DTAM[0]++;
        if ((ultimo % 2) == 0)
            copiaPoligono(0, 0, i[0], i[1], u[0], u[1]);
        else
            copiaPoligono(0, 1, j[0], j[1], v[0], v[1]);
        /* segundo ciclo */
        OPERACOES[1][DTAM[1]][0] = pi[0];
        OPERACOES[1][DTAM[1]][1] = pi[1];
        DTAM[1]++;
        if ((ultimo % 2) == 0)
            copiaPoligono(1, 1, j[0], j[1], v[0], v[1]);
        else
            copiaPoligono(1, 0, i[0], i[1], u[0], u[1]);
        free(pi);
    }
}

/* calcula area de triangulo */
double area(double triangulo[3][2])
{
    double l0 = distancia(triangulo[0], triangulo[1]);
    double l1 = distancia(triangulo[0], triangulo[2]);
    double l2 = distancia(triangulo[1], triangulo[2]);
    double sp = (l0 + l1 + l2) * 0.5;
    /* triangulo degenerado (fabs) */
    return sqrt(fabs(sp*(sp-l0)*(sp-l1)*(sp-l2)));
}

/* calcula area do quadrangulo circusncrito na interseccao */
double areaEntreCirculos()
{
    double r1 = distancia(CENTRO[0], POLIGONOS[0][0]);
    double r2 = distancia(CENTRO[1], POLIGONOS[1][0]);
    double d = distancia(CENTRO[0], CENTRO[1]);
    double sp = (d + r1 + r2) * 0.5;
    return sqrt(fabs(sp*(sp-d)*(sp-r1)*(sp-r2))) * 2.0;
}

bool verticeDentroTriangulo(char MARCA[200], double FIGURA[200][2], int M, double O[2], double P[2], double Q[2], int id_i, int id_f)
{
    int i, ini, fim;
    double A[2], B[2], C[2], T[2];
    ini = id_i;
    fim = id_f;
    if (ini != fim)
    {
        ini++;
        if (ini > fim)
            fim = M + id_f;
        A[0] = Q[0] - O[0];
        A[1] = Q[1] - O[1];
        B[0] = O[0] - P[0];
        B[1] = O[1] - P[1];
        C[0] = P[0] - Q[0];
        C[1] = P[1] - Q[1];
        /*printf("Ax %lf Ay %lf\n",A[0],A[1]);
        printf("Bx %lf By %lf\n",B[0],B[1]);
        printf("Cx %lf Cy %lf\n",C[0],C[1]);*/
        for (i=ini;i<fim;i++)
        {
            if (MARCA[i%M])
                continue;
            T[0] = FIGURA[i%M][0] - O[0];
            T[1] = FIGURA[i%M][1] - O[1];
            if ((((A[0]*T[1]) - (A[1]*T[0])) < -0.001) &&
                (((B[0]*T[1]) - (B[1]*T[0])) < -0.001) &&
                (((C[0]*T[1]) - (C[1]*T[0])) < -0.001))
                return true;
        }
    }
    return false;
}

/* calcula area de poligono convexo */
double areaPoligono(double FIGURA[2][200][2], int M[2], int k, bool convexo)
{
    int i;
    double soma = 0.0;
    double triangulo[3][2];
    /* tessela poligono em (n-2) triangulos */
    if (convexo)
    {
        triangulo[0][0] = FIGURA[k][0][0];
        triangulo[0][1] = FIGURA[k][0][1];
        for (i=1;i<M[k]-1;i++)
        { 
            /*printf("TRIANGULO %d\n",i);*/
            triangulo[1][0] = FIGURA[k][i][0];
            triangulo[1][1] = FIGURA[k][i][1];
            triangulo[2][0] = FIGURA[k][i+1][0];
            triangulo[2][1] = FIGURA[k][i+1][1];
            soma += area(triangulo);
        }
    }
    else
    {
        char MARCA[200];
        /* melhor seria remover verticesi O(n) < O(n^2) */
        double A[2], B[2], C[2];
        int u, v, w, conta = 0;
        u = 0;
        memset(MARCA, 0, M[k]+1);
        while (conta < M[k]-2)
        {
            /* encontra vertices sem marca */
            while (MARCA[u])
                u = (u+1) % M[k];
            v = (u+1) % M[k];
            while (MARCA[v])
                v = (v+1) % M[k];
            w = (v+1) % M[k];
            while (MARCA[w])
                w = (w+1) % M[k];
            /* verifica se angulo convexo */
            A[0] = FIGURA[k][u][0] - FIGURA[k][v][0];  
            A[1] = FIGURA[k][u][1] - FIGURA[k][v][1];  
            B[0] = FIGURA[k][w][0] - FIGURA[k][v][0];  
            B[1] = FIGURA[k][w][1] - FIGURA[k][v][1];  
            if (((A[0]*B[1]) - (A[1]*B[0])) > 0.001)
            {
                if (!verticeDentroTriangulo(MARCA, FIGURA[k], M[k], FIGURA[k][w], FIGURA[k][v], FIGURA[k][u], w, u))
                {
                    /* calcula area e remove orelha */
                    triangulo[0][0] = FIGURA[k][u][0];
                    triangulo[0][1] = FIGURA[k][u][1];
                    triangulo[1][0] = FIGURA[k][v][0];
                    triangulo[1][1] = FIGURA[k][v][1];
                    triangulo[2][0] = FIGURA[k][w][0];
                    triangulo[2][1] = FIGURA[k][w][1];
                    soma += area(triangulo);
                    MARCA[v] = '1';
                    u = (u+2) % M[k];
                    conta++;
                }
                else
                    u = (u+1) % M[k];
            }
            else
            {
                /* triangulo degenerado  (remover vertices nao-corners) */
                if (fabs((A[0]*B[1]) - (A[1]*B[0])) <= 0.001)
                {
                    MARCA[v] = '1';
                    conta++;
                } 
                else
                    u = (u+1) % M[k];
            }
        }
    }
    return soma;
}
bool poligonoConvexo(double FIGURA[2][200][2], int M[2], int k)
{
    int i, u, v, w;
    double A[2], B[2];
    for (i=0;i<M[k];i++)
    {
        u = i;
        v = (i+1)%M[k];
        w = (i+2)%M[k];
        /* verifica se angulo convexo */
        A[0] =  FIGURA[k][u][0] - FIGURA[k][v][0];  
        A[1] =  FIGURA[k][u][1] - FIGURA[k][v][1];  
        B[0] =  FIGURA[k][w][0] - FIGURA[k][v][0];  
        B[1] =  FIGURA[k][w][1] - FIGURA[k][v][1];  
        if (((A[0]*B[1]) - (A[1]*B[0])) < 0.0)
            return false;
    }
    return true;
}

bool poligonosTangentes()
{
    /* Separating Axis Theorem */
    int i,j;
    double cf, A[2], B[2];
    double ini_1 = DBL_MAX, fim_1 = -DBL_MAX;
    double ini_2 = DBL_MAX, fim_2 = -DBL_MAX;
    /* primeiro poligono */
    for (i=0;i<TAM[0];i++)
    {
        /* captura normal */
        A[0] = POLIGONOS[0][(i+1)%TAM[0]][1] - POLIGONOS[0][i][1];
        A[1] = POLIGONOS[0][i][0] - POLIGONOS[0][(i+1)%TAM[0]][0];
        for (j=0; j<TAM[0]; j++)
        {
            B[0] = POLIGONOS[0][j][0] - POLIGONOS[0][(i+1)%TAM[0]][0];
            B[1] = POLIGONOS[0][j][1] - POLIGONOS[0][(i+1)%TAM[0]][1];
            cf = (B[0]*A[0] + B[1]*A[1])/(A[0]*A[0] + A[1]*A[1]);
            if (cf > fim_1)
                fim_1 = cf;
            if (cf < ini_1)
                ini_1 = cf;
        }
        for (j=0; j<TAM[1]; j++)
        {
            B[0] = POLIGONOS[1][j][0] - POLIGONOS[0][(i+1)%TAM[0]][0];
            B[1] = POLIGONOS[1][j][1] - POLIGONOS[0][(i+1)%TAM[0]][1];
            cf = (B[0]*A[0] + B[1]*A[1])/(A[0]*A[0] + A[1]*A[1]);
            if (cf > fim_2)
                fim_2 = cf;
            if (cf < ini_2)
                ini_2 = cf;
        }
        if ((ini_1 >= fim_2) || (ini_2 >= fim_1))
            return true;
        ini_1 = DBL_MAX; fim_1 = -DBL_MAX;
        ini_2 = DBL_MAX; fim_2 = -DBL_MAX;
    }
    /* segundo poligono */
    for (i=0;i<TAM[1];i++)
    {
        /* captura normal */
        A[0] = POLIGONOS[1][(i+1)%TAM[1]][1] - POLIGONOS[1][i][1];
        A[1] = POLIGONOS[1][i][0] - POLIGONOS[1][(i+1)%TAM[1]][0];
        for (j=0; j<TAM[0]; j++)
        {
            B[0] = POLIGONOS[0][j][0] - POLIGONOS[1][(i+1)%TAM[1]][0];
            B[1] = POLIGONOS[0][j][1] - POLIGONOS[1][(i+1)%TAM[1]][1];
            cf = (B[0]*A[0] + B[1]*A[1])/(A[0]*A[0] + A[1]*A[1]);
            if (cf > fim_1)
                fim_1 = cf;
            if (cf < ini_1)
                ini_1 = cf;
        }
        for (j=0; j<TAM[1]; j++)
        {
            B[0] = POLIGONOS[1][j][0] - POLIGONOS[1][(i+1)%TAM[1]][0];
            B[1] = POLIGONOS[1][j][1] - POLIGONOS[1][(i+1)%TAM[1]][1];
            cf = (B[0]*A[0] + B[1]*A[1])/(A[0]*A[0] + A[1]*A[1]);
            if (cf > fim_2)
                fim_2 = cf;
            if (cf < ini_2)
                ini_2 = cf;
        }
        if ((ini_1 >= fim_2) || (ini_2 >= fim_1))
            return true;
        ini_1 = DBL_MAX; fim_1 = -DBL_MAX;
        ini_2 = DBL_MAX; fim_2 = -DBL_MAX;
    }
    return false;
}

/* calcula area da diferenca */
double areaDiferenca(bool disjuntos)
{
    double AP, O1, O2;
    bool c1, c2;
    if (poligonosTangentes() || disjuntos)
    {
        c1 = poligonoConvexo(POLIGONOS, TAM, 0);
        c2 = poligonoConvexo(POLIGONOS, TAM, 1);
        O1 = areaPoligono(POLIGONOS, TAM, 0, c1);
        O2 = areaPoligono(POLIGONOS, TAM, 1, c2);
        AP = O2 + O1;
    }
    else if (DTAM[0] <= 1)
    {
        /* poligonos um dentro do outro */
        c1 = poligonoConvexo(POLIGONOS, TAM, 0);
        c2 = poligonoConvexo(POLIGONOS, TAM, 1);
        O1 = areaPoligono(POLIGONOS, TAM, 0, c1);
        O2 = areaPoligono(POLIGONOS, TAM, 1, c2);
        AP = fabs(O2 - O1);
    }
    else
    {
        /* uniao - interseccao */
        c1 = poligonoConvexo(OPERACOES, DTAM, 0);
        c2 = poligonoConvexo(OPERACOES, DTAM, 1);
        O1 = areaPoligono(OPERACOES, DTAM, 0, c1);
        O2 = areaPoligono(OPERACOES, DTAM, 1, c2);
        AP = fabs(O2 - O1);
    }
    return AP;
}

int main()
{
    int n, pos = 0;
    char txt_saida[9];
    bool disjuntos;
    double x, y, saida;
    while (fscanf(stdin, "%d", &n) != EOF)
    {
        /* fim da entrada */
        if (n == 0)
            break;
        while (TAM[pos] < n)
        {
             fscanf(stdin, "%lf %lf", &x, &y);
             POLIGONOS[pos][TAM[pos]][0]=x;
             POLIGONOS[pos][TAM[pos]][1]=y;
             TAM[pos]++;
        }
        pos++;
        if (pos % 2 == 0)
        {
           calculaCentro(0);
           calculaCentro(1);
           /*imprimePoligonos();*/
           if (circulosInterceptam())
           {
               /* realiza algoritmo */
               if (!poligonosTangentes())
               {
                   intersecaoPoligonos();
                   constroiOperacoes();
               }
               disjuntos = false;
               /*imprimeInterseccoes();
               imprimeOperacoes();*/
           }
           else
           {
               /* poligonos disjuntos */
               disjuntos = true;
           }
           saida = areaDiferenca(disjuntos);
           sprintf(txt_saida, "%8.2lf", saida);
           printf("%s", txt_saida);
           /* reseta variaveis */
           pos = 0;
           TAM[0] = 0;
           TAM[1] = 0;
        }
    }
    printf("\n");
    return 0;
}
