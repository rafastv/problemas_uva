import sys,math

global fatoriais, arvores
fatoriais = {0:1, 1:1}
arvores = {}

def fat(n):
    if n in fatoriais.keys():
        return fatoriais[n]
    valor = n*fat(n-1)
    fatoriais.update({n: valor})
    return valor

def comb(n, k):
    return fat(n)//(fat(n-k)*fat(k))

def conta_arvores(n, k=1):
    if (n,k) in arvores.keys():
        return arvores[(n,k)]
    if (n == 0):
        return 1
    soma = 0
    for i in range(1, (2*k)+1):
        if (n >= i):
            soma += comb(2*k, i) * conta_arvores(n-i, i)
    arvores.update({(n,k): soma})
    return soma

for linha in sys.stdin:
    N = int(linha[0:-1])
    if N == 0:
        break
    ca = conta_arvores(N-1)
    arvores_total = int(fat(N) * ca)
    sys.stdout.write(str(arvores_total) + "\n")

exit(0)
