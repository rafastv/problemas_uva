import sys

lista_raizes = []
extras = []
busca = False

def criarArvore(pai, filho):
    return [pai, [[filho, []]]]

def buscarRefPessoa(arvore, nome):
    if (arvore[0] == nome):
        return arvore
    bps = []
    for e in arvore[1]:
         bps = buscarRefPessoa(e, nome)
         if nome in bps:
             break
         else:
             # esvazia o cache
             bps = []
    return bps

def unirArvores(A, B):
    #print("UNIR", A, B)
    ponto_uniao = buscarRefPessoa(A, B[0])
    #print("TESTE1", ponto_uniao, B)
    if ponto_uniao:
        ponto_uniao[1] += B[1]
        return [True, A]
    ponto_uniao = buscarRefPessoa(B, A[0]) 
    #print("TESTE2", ponto_uniao, A)
    if ponto_uniao:
        ponto_uniao[1] += A[1]
        return [True, B]
    return [False, []]

def inserirNo(arvore, pai, filho):
    if not arvore:
        return False
    if pai == arvore[0]:
        encontrei = False
        for f in arvore[1]:
           if f[0] == filho:
               encontrei = True
               break
        if not encontrei:
            arvore[1].append([filho, []])
        return True
    else: 
        saida = False
        for i in range(len(arvore[1])):
            saida = inserirNo(arvore[1][i], pai, filho)
            if saida:
                break
    return saida

def imprimirArvore(arvore, conta=0):
    print(conta,arvore[0])
    for no in arvore[1]:
        #print (no)
        imprimirArvore(no, conta+1)

def buscarPessoa(arvore, nome):
    if (arvore[0] == nome):
        return [nome]
    bps = []
    for e in arvore[1]:
         bps = [arvore[0]] + buscarPessoa(e, nome)
         if nome in bps:
             break
         else:
             # esvazia o cache
             bps = []
    return bps

def imprimirGreat(grau, palavra):
    if grau == 1:
        return palavra
    elif grau == 2:
        return "grand " + palavra
    else:
        saida = ""
        # -2 por grand e somente palavra
        for i in range(grau-2):
            saida += "great "
    return saida + "grand " + palavra

def contarPrimo(lista1, lista2):
    A = lista2
    B = lista1
    #print (A, B)
    if len(lista1) < len(lista2):
        A = lista1
        B = lista2
    a = -1
    for i in range(len(A)-2, -1, -1):
        if A[i] in B:
            # se o ancestral comum eh direto conta 0
            a = len(A) - (i + 2)
            b = len(B) - (B.index(A[i]) + 2)
            break
    #print A, B, a, b
    if a >= 0:
        n = b
        if a < b:
            n = a
        m = abs(a-b)
        if n == m == 0:
            return "sibling"
        if m == 0:
            return str(n) + " cousin"
        else:
            return str(n) + " cousin removed " + str(m)
    return "no relation"

def imprimirParentesco(arvore, p1, p2):
    #print l1,p1,id1,p2,id2,"dif",gru
    l1 = buscarPessoa(arvore, p1)
    if p2 in l1:
        id1 = len(l1)-1 
        id2 = l1.index(p2)
        gru = id1 - id2 
        return imprimirGreat(gru, "child")
    else:
        l2 = buscarPessoa(arvore, p2)
        if p1 in l2:
            id1 = l2.index(p1)
            id2 = len(l2)-1
            gru = id2 - id1
            return imprimirGreat(gru, "parent")
    return contarPrimo(l1, l2)

lista_relacoes = []
tmp = []
for linha in sys.stdin:

    vars = linha.split()
    if len(vars) == 0:
        continue
    if (len(vars) % 2)  == 1:
        tmp+=vars

    if (len(tmp) % 2) == 1:
        continue
    if not busca:
        if tmp:
            vars = tmp
        tmp = []
        for i in range(0,len(vars),2):
            pai = vars[i+1]
            filho = vars[i]
            lista_relacoes += [[pai, filho]]
    else:
        p1 = vars[0]
        p2 = vars[1]
        if p1 == "no.child":
            sys.stdout.write("no relation\n")
        elif p1 == p2:
            sys.stdout.write("sibling\n")
        else:
            for arvore in lista_raizes:
                saida = imprimirParentesco(arvore, p1, p2)
                if saida != "no relation":
                    break
            sys.stdout.write(saida)
            sys.stdout.write("\n")

    if (vars[0] == "no.child") and (vars[1] == "no.parent"):
        busca = True
        # descobre todos os filhos de um pai
        lista_relacoes.sort()
        arvore = []
        for valor in lista_relacoes:
            pai = valor[0]
            filho = valor[1]
            if not inserirNo(arvore, pai, filho):
                arvore = criarArvore(pai, filho)
                lista_raizes += [arvore]
        # une pais com filhos
        conta = 0
        #print(len(lista_raizes), lista_raizes)
        while conta < (len(lista_raizes)-1):
            remove = -1
            for j in range(conta+1,len(lista_raizes)):
                saida = unirArvores(lista_raizes[conta], lista_raizes[j])
                if saida[0]:
                    remove = j
                    nova_arvore = saida[1]
                    break
            #print("NOVA ARVORE", nova_arvore)
            if remove >= 0:
                if nova_arvore == lista_raizes[conta]:
                    lista_raizes.pop(remove)
                else:
                    lista_raizes.pop(conta)
            else:
                conta+=1
            #print(len(lista_raizes), lista_raizes)
        continue
exit(0)
